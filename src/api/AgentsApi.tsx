import axios from "axios";
type searchAgentType = {
    page?:number,
    pageSize?: number,
    keyWord?: string
}
type createAgents = {
    name: string;
    address: string;
    amount: number;
    creditAmount: number;
    contactName: string;
    taxNo: string;
    agentCode: string;
    contactPhone: string;
    isActive: boolean,
    status: string;
}
const apiApp = 'https://web-manage-api-4jzvw.ondigitalocean.app';
const apiDev = 'http://localhost:8080'

export const searchAgentAPI = async ({body}:{ body: searchAgentType}) => {
        try {
            const result = await axios.post(`${apiApp}/agents/search`, body);

            return result?.data;

        } catch (e:any) {
            throw e
        }
}
export const createAgentAPI = async ({body}:{ body: createAgents}) => {
    try {
        const result = await axios.post(`${apiApp}/agents/create`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getAgentByIdAgentAPI = async ({id}:{ id: string}) => {
    try {
        const result = await axios.get(`${apiApp}/agents/getById/${id}`);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getOrderAgentsByIdAgentAPI = async (body: any) => {
    try {
        const result = await axios.post(`${apiApp}/agents/getOrderAgent`, body);

        return result?.data;
    } catch (e:any) {
        throw e
    }
}