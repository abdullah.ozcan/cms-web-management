import axios from "axios";

type searchPayReserveType = {
    page?:number,
    pageSize?: number,
    keyword?: string
}

type searchPayReserve = {
    page?:number,
    pageSize?: number;
    keyword?: string;
    id?: number;
}

type addPayReserve = {
    title?:number;
    description?: number;
    status?: boolean;
}
const apiApp = 'https://web-manage-api-4jzvw.ondigitalocean.app';
const apiDev = 'http://localhost:8080'

export const searchPayReserveAPI = async ({body}:{ body: searchPayReserveType}) => {
        try {
            const result = await axios.post(`${apiApp}/payreserve/search`, body);

            return result?.data;

        } catch (e:any) {
            throw e
        }
}

export const createPayReserveAPI = async ({body}:{ body: any}) => {
    try {
        const result = await axios.post(`${apiApp}/payreserve/create`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getPayReserveIdAPI = async ({id}:{ id: string}) => {
    try {
        const result = await axios.get(`${apiApp}/payreserve/getById/${id}`);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

