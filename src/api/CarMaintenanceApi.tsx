import axios from "axios";

type searchType = {
    page?:number,
    pageSize?: number,
    keyword?: string
}

type searchCarType = {
    page?:number,
    pageSize?: number,
    keyword?: string
    id?: string;
}

type searchPayReserve = {
    page?:number,
    pageSize?: number;
    keyword?: string;
    id?: number;
}

type addPayReserve = {
    title?:number;
    description?: number;
    status?: boolean;
}
const apiApp = 'https://web-manage-api-4jzvw.ondigitalocean.app';
const apiDev = 'http://localhost:8080'

export const searchCarMaintenaceAPI = async ({body}:{ body: searchType}) => {
        try {
            const result = await axios.post(`${apiApp}/carMaintenance/search`, body);

            return result?.data;

        } catch (e:any) {
            throw e
        }
}

export const searchCarMaintenaceAnnounceAPI = async ({body}:{ body: searchType}) => {
    try {
        const result = await axios.post(`${apiApp}/carMaintenance/announce`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const searchCarInsuranceAPI = async ({body}:{ body: searchCarType}) => {
    try {
        const result = await axios.post(`${apiApp}/cars/list/insurance`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const searchCarACTAPI = async ({body}:{ body: searchCarType}) => {
    try {
        const result = await axios.post(`${apiApp}/cars/list/act`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const searchCarRepairAPI = async ({body}:{ body: searchCarType}) => {
    try {
        const result = await axios.post(`${apiApp}/cars/list/repair`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const searchCarRequestPI = async ({body}:{ body: searchCarType}) => {
    try {
        const result = await axios.post(`${apiApp}/cars/list/request`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}
export const createCarInsuranceAPI = async ({body}:{ body: any}) => {
    try {
        const result = await axios.post(`${apiApp}/cars/create/insurance`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const createCarACTAPI = async ({body}:{ body: any}) => {
    try {
        const result = await axios.post(`${apiApp}/cars/create/act`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const createCarRepairAPI = async ({body}:{ body: any}) => {
    try {
        const result = await axios.post(`${apiApp}/cars/create/repair`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const updateCarRepairAPI = async ({body}:{ body: any}) => {
    try {
        const result = await axios.post(`${apiApp}/cars/update/repair`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const createCarRequestAPI = async ({body}:{ body: any}) => {
    try {
        const result = await axios.post(`${apiApp}/cars/create/request`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getListCarMaintenanceAPI = async ({body}:{ body: any}) => {
    try {
        const result = await axios.post(`${apiApp}/carMaintenance/getListCarMaintenanceByType`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

