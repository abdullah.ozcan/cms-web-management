import axios from "axios";
type searchRole = {
    page?:number,
    pageSize?: number,
    keyword?: string
}
const apiApp = 'https://web-manage-api-4jzvw.ondigitalocean.app';
const apiDev = 'http://localhost:8080'

export const searchOrderAPI = async ({body}:{body: {
        "page": number
        "pageSize": number
        "phone": string;
        "name": string;
        "start": string;
        "end": string;
        "type":string;
    }}) => {
    try {
        const result = await axios.post(`${apiApp}/orders/search`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const createOrderApi = async ({body}:{body:any}) => {
    try {
        const result = await axios.post(`${apiApp}/orders/create`, body, {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        });

        return result?.data;

    } catch (e:any) {
        throw e
    }
}
export const updateOrderApi = async ({body}:{body:any}) => {
    try {
        const result = await axios.post(`${apiApp}/orders/update`, body, {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        });
        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const updatePaymentOrderApi = async ({body}:{body:any}) => {
    try {
        const result = await axios.post(`${apiApp}/orders/updatePayment`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const sumOrderApi = async () => {
    try {
        const result = await axios.get(`${apiApp}/orders/summary`);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getOrderByIdApi = async (id:any) => {
    try {
        const result = await axios.get(`${apiApp}/orders/getById/${id}`);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getOrderCustomersApi = async (body:any) => {
    try {
        const result = await axios.post(`${apiApp}/customers/orderCustomers`,body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getOrderCarsApi = async (body:any) => {
    try {
        const result = await axios.post(`${apiApp}/cars/order/CarOrder`,body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getOrderEmployeesApi = async (body:any) => {
    try {
        const result = await axios.post(`${apiApp}/employee/OrderEmployee`,body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}
