import axios from "axios";
type searchAgentType = {
    page?:number,
    pageSize?: number,
    keyword?: string
}

type searchEventType = {
    page?:number;
    pageSize?: number;
    keyword?: string;
    type: string;
    employeeId: number;

}
type createAgents = {
    name: string;
    address: string;
    amount: number;
    creditAmount: number;
    contactName: string;
    taxNo: string;
    agentCode: string;
    contactPhone: string;
    isActive: boolean,
    status: string;
}
type createEvent = {
    "type":  string;
    "employeeId": number;
    "title":  string;
    "amount": number;
    "description":  string;
    "isActive": boolean
}
const apiApp = 'https://web-manage-api-4jzvw.ondigitalocean.app';
const apiDev = 'http://localhost:8080'

export const searchEmployeeAPI = async ({body}:{ body: searchAgentType}) => {
        try {
            const result = await axios.post(`${apiApp}/employee/search`, body);

            return result?.data;

        } catch (e:any) {
            throw e
        }
}
export const searchEmployeeEventAPI = async ({body}:{ body: searchEventType}) => {
    try {
        const result = await axios.post(`${apiApp}/employee/events/type`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}
// export const createEmployeeAPI = async ({body}:{ body: createAgents}) => {
//     try {
//         const result = await axios.post(`${apiApp}/agents/create`, body);

//         return result?.data;

//     } catch (e:any) {
//         throw e
//     }
// }
export const createEmployeeEventAPI = async ({body}:{ body: createEvent}) => {
    try {
        const result = await axios.post(`${apiApp}/employee/create/events`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const createEmployeeAPI = async ({body}:{ body: FormData}) => {
    try {
        const result = await axios.post(`${apiApp}/employee/create`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getByIdEmployeeAPI = async ({id}:{ id: string}) => {
    try {
        const result = await axios.get(`${apiApp}/employee/ById/${id}`);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}
