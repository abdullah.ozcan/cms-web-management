import axios from "axios";

type searchCategoryType = {
    page?:number,
    pageSize?: number,
    keyword?: string
}

type searchCarype = {
    page?:number,
    pageSize?: number;
    keyword?: string;
    id?: number;
}

type addCategoryType = {
    title?:number;
    description?: number;
    status?: boolean;
}
const apiApp = 'https://web-manage-api-4jzvw.ondigitalocean.app';
const apiDev = 'http://localhost:8080'

export const searchCategoryAPI = async ({body}:{ body: searchCategoryType}) => {
        try {
            const result = await axios.post(`${apiApp}/categories/search`, body);

            return result?.data;

        } catch (e:any) {
            throw e
        }
}

//

export const createCategoryAPI = async ({body}:{ body: FormData}) => {
    try {
        const result = await axios.post(`${apiApp}/categories/create`, body,{
            headers: {
                "Content-Type": "multipart/form-data",
            }
        });

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const createCaryAPI = async ({body}:{ body: FormData}) => {
    try {
        const result = await axios.post(`${apiApp}/cars/create`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const updateCarAPI = async ({body}:{ body: FormData}) => {
    try {
        const result = await axios.post(`${apiDev}/cars/update`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const createCategoryByIdAPI = async ({id}:{ id: string}) => {
    try {
        const result = await axios.get(`${apiApp}/categories/getById/${id}`);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getCarByIdAPI = async ({id}:{ id: string}) => {
    try {
        const result = await axios.get(`${apiApp}/cars/getById/${id}`);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const searchCarAPI = async ({body}:{ body: searchCarype}) => {
    try {
        const result = await axios.post(`${apiApp}/cars/search`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getAllCarAPI = async () => {
    try {
        const result = await axios.get(`${apiApp}/cars/all`);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const getCarViewAPI = async ({id}:{ id: string}) => {
    try {
        const result = await axios.get(`${apiApp}/cars/getById/sum/${id}`);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}
