import axios from "axios";

type searchCustomerType = {
    page?: number,
    pageSize?: number,
    keyWord?: string
}

type customerIdType = {
    id?: string
}

const apiApp = 'https://web-manage-api-4jzvw.ondigitalocean.app';
const apiDev = 'http://localhost:8080'

export const searchCustomerAPI = async ({body}:{ body: searchCustomerType }) => {
    try {
        const result = await axios.post(`${apiApp}/customers/search`, body);

        return result?.data;
    } catch (e:any) {
        throw e
    }
}

export const createCustomerAPI = async ({body}:{ body: FormData }) => {
    try {
        const result = await axios.post(`${apiApp}/customers/create`, body, {
            headers: {
                "Content-Type": "multipart/form-data",
            }
        });

        return result?.data;
    } catch (e:any) {
        throw e
    }
}
export const getCustomerByIdCustomerAPI = async ({id}:{ id: customerIdType }) => {
    try {
        const result = await axios.post(`${apiApp}/customers/getById`, id);

        return result?.data;
    } catch (e:any) {
        throw e
    }
}

export const getAllCustomers = async () => {
    try {
        const result = await axios.get(`${apiApp}/customers/all`);

        return result?.data;
    } catch (e:any) {
        throw e
    }
}


