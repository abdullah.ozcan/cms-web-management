import axios from "axios";
type searchRole = {
    page?:number,
    pageSize?: number,
    keyword?: string
}
const apiApp = 'https://web-manage-api-4jzvw.ondigitalocean.app';
const apiDev = 'http://localhost:8080'

export const searchUserAPI = async ({body}:{body:searchRole}) => {
    try {
        const result = await axios.post(`${apiApp}/users/search`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const createUserApi = async ({body}:{body: {
    roleId: string,
    username: string,
    email: string
    }}) => {
    try {
        const result = await axios.post(`${apiApp}/users/create`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}
