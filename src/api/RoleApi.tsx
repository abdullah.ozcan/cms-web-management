import axios from "axios";
type searchRole = {
    page?:number,
    pageSize?: number,
    keyWord?: string
}
const apiApp = 'https://web-manage-api-4jzvw.ondigitalocean.app';
const apiDev = 'http://localhost:8080'

export const searchRoleAPI = async ({body}:{ body: searchRole}) => {
        try {
            const result = await axios.post(`${apiApp}/roles/search`, {
                page: body?.page,
                pageSize: body?.pageSize,
                keyword: body?.keyWord
            });

            return result?.data;

        } catch (e:any) {
            throw e
        }
}

export const searchMenuPI = async () => {
    try {
        const result = await axios.post(`${apiApp}/roles/roleMenuList`);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const updateRoleApi = async (body:any) => {
    try {
        const result = await axios.post(`${apiApp}/roles/update`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const listUserRoleApi = async (body:any) => {
    try {
        const result = await axios.post(`${apiApp}/roles/searchRole`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const listMasterRoleApi = async () => {
    try {
        const result = await axios.post(`${apiApp}/roles/roleMasterDataList`);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const createRoleApi = async (body:any) => {
    try {
        const result = await axios.post(`${apiApp}/roles/create`, body);

        return result?.data;

    } catch (e:any) {
        throw e
    }
}

export const mapMenuRoleApi = async () => {
    try {
        const result = await axios.post(`${apiApp}/roles/roleMenuItemList`, {});

        return result?.data;

    } catch (e:any) {
        throw e
    }
}
