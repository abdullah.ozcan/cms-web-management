import { FC } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../_metronic/helpers';

const ModalAddUser: FC = () => {
    // const { register, handleSubmit, watch, formState: { errors } } = useForm();

    return (
        <div className=''>
            <div className='row mb-4'>
                <label className='col-lg-4 col-form-label fw-bold fs-4 mb-2'>
                    <span>Avatar</span>
                </label>
                <div className='col-lg-12 fv-row mb-2'>
                    <img src={toAbsoluteUrl('/media/car/car-1.png')} alt=''/>
                </div>
                <label className='col-lg-4 col-form-label text-muted fs-7'>
                    <span>Allowed file types:png,jpg,jpeg.</span>
                </label>
            </div>
            <div className='row'>
                <label className='col-lg-12 col-form-label fw-bold fs-4'>
                    <span className='required'>Full Name</span>
                </label>
                <div className='col-lg-12 fv-row mb-2'>
                    <input
                        type='text'
                        className='form-control form-control-lg form-control-solid'
                    />
                </div>
            </div>
            <div className='row mb-10'>
                <label className='col-lg-12 col-form-label fw-bold fs-4'>
                        <span className='required'>Email</span>
                </label>
                <div className='col-lg-12 fv-row mb-2'>
                    <input
                        type='text'
                        className='form-control form-control-lg form-control-solid'
                    />
                </div>
            </div>
            <div className='row'>
                <label className='col-lg-12 col-form-label fw-bold fs-4'>
                    <span>Role</span>
                </label>
            </div>
            <div className='row mb-4'>
                <div className='d-flex align-items-center'>
                    <div className='fv-row mb-2 form-check form-check-custom form-check-solid me-4'>
                        <input
                            type='radio'
                            className='form-check-input'
                        />
                    </div>
                    <div className='d-flex justify-content-start flex-column'>
                        <label className='fs-6 fw-bold'>
                            <span>Administrator</span>
                        </label>
                        <label className='text-muted fs-7'>
                            <span>Best for business owners and company administrators</span>
                        </label>
                    </div>
                </div>
            </div>
            <div className='row mb-4'>
                <div className='d-flex align-items-center'>
                    <div className='fv-row mb-2 form-check form-check-custom form-check-solid me-4'>
                        <input
                            type='radio'
                            className='form-check-input'
                        />
                    </div>
                    <div className='d-flex justify-content-start flex-column'>
                        <label className='fs-6 fw-bold'>
                            <span>Devoloper</span>
                        </label>
                        <label className='text-muted fs-7'>
                            <span>Best for developers or people primarily using API</span>
                        </label>
                    </div>
                </div>
            </div>
            <div className='row mb-4'>
                <div className='d-flex align-items-center'>
                    <div className='fv-row mb-2 form-check form-check-custom form-check-solid me-4'>
                        <input
                            type='radio'
                            className='form-check-input'
                        />
                    </div>
                    <div className='d-flex justify-content-start flex-column'>
                        <label className='fs-6 fw-bold'>
                            <span>Analyst</span>
                        </label>
                        <label className='text-muted fs-7'>
                            <span>Best for people who need full access to analytics data, but don't need to update business settings</span>
                        </label>
                    </div>
                </div>
            </div>
            <div className='row mb-4'>
                <div className='d-flex align-items-center'>
                    <div className='fv-row mb-2 form-check form-check-custom form-check-solid me-4'>
                        <input
                            type='radio'
                            className='form-check-input'
                        />
                    </div>
                    <div className='d-flex justify-content-start flex-column'>
                        <label className='fs-6 fw-bold'>
                            <span>Support</span>
                        </label>
                        <label className='text-muted fs-7'>
                            <span>Best for employees who regularly refund payments and respond to disputes</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ModalAddUser;