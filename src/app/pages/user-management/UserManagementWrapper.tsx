import { FC } from 'react'
import { KTCard } from '../../../_metronic/helpers'
import { PageTitle } from '../../../_metronic/layout/core'
// import UserManageHeader from './UserManagementHeader'
import UserManageList from './UserManagementList'

const UserManagementWrapper: FC = () => {
    return (
        <>
            <PageTitle breadcrumbs={[]}>User Management</PageTitle>
            <KTCard>
                {/*<UserManageHeader/>*/}
                <UserManageList className=''/>
            </KTCard>
        </>
    )
}

export default UserManagementWrapper;
