import { FC, useState } from 'react'
import { KTSVG } from '../../../_metronic/helpers';
import ModalUpdateUserRole from '../../../_metronic/layout/components/ModalUpdateUserRole';
import ModalAddUser from './ModalAddUser';

const UserManageHeader: FC = () => {
    const [show, setShow] = useState(false);
    const [searchTerm, setSearchTerm] = useState('');

    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    return (
        <div className='card-header border-0 pt-6'>
            <div className='card-title'>
                {/* begin::Search */}
                <div className='d-flex align-items-center position-relative my-1'>
                    <KTSVG
                        path='/media/icons/duotune/general/gen021.svg'
                        className='svg-icon-1 position-absolute ms-6'
                    />
                    <input
                        type='text'
                        data-kt-user-table-filter='search'
                        className='form-control form-control-solid w-250px ps-14'
                        placeholder='Search'
                        value={searchTerm}
                        onChange={(e) => setSearchTerm(e.target.value)}
                    />
                </div>
                {/* end::Search */}
            </div>
            {/* begin::Card toolbar */}
            <div className='card-toolbar'>
                {/* begin::Group actions */}
                {/* {selected.length > 0 ? <UsersListGrouping /> : <UsersListToolbar />} */}
                <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
                    <button
                        type='button'
                        className='btn btn-secondary me-3'
                    >
                        <KTSVG path='/media/icons/duotune/general/gen031.svg' className='svg-icon-2' />
                        Filter
                    </button>
                    <button
                        type='button'
                        className='btn btn-secondary me-3'
                    >
                        <KTSVG path='/media/icons/duotune/files/fil009.svg' className='svg-icon-2' />
                        Export
                    </button>
                    <button
                        type='button'
                        className='btn btn-primary'
                        onClick={handleShow}
                    >
                        <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
                        Add User
                    </button>
                    <ModalUpdateUserRole
                        show={show}
                        handleClose={handleClose}
                        title={'Add a User'}
                        chil={<ModalAddUser/>}
                    />
                    {/* end::Add user */}
                </div>
                {/* end::Group actions */}
            </div>
            {/* end::Card toolbar */}
        </div>
    );
}

export default UserManageHeader;