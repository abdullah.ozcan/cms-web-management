import {FC, useEffect, useState} from 'react'
import { KTSVG } from '../../../_metronic/helpers';
import { toAbsoluteUrl } from "../../../_metronic/helpers";
import {createUserApi, searchUserAPI} from "../../../api/UserApi";
import ModalUpdateUserRole from "../../../_metronic/layout/components/ModalUpdateUserRole";
import ModalAddUser from "./ModalAddUser";
import {listMasterRoleApi} from "../../../api/RoleApi";
import {Modal} from "react-bootstrap";
import {Controller, useForm} from "react-hook-form";
import {clsx} from "clsx";

type Props = {
    className: string
}

type UserInfoType = {
    id?: number;
    username: string;
    email: string;
    roleId: number;
    roleName?: string;
    image?: string;
    createdDate?: string;
    updatedDate?: string;
    isActive: boolean;

}

const UserManageList: FC<Props> = ({className}) => {

    const {
        handleSubmit,
        setValue,
        watch,
        formState: { errors },
        control,
    } = useForm({
        defaultValues: {
            roleId: 1,
            username: '',
            email: '',
        },
    });

    const role = [
        {id: 1, name: 'Admin', isActive: true, createdDate: '2022-08-28', updatedDate: '2022-08-28'},
        {id: 2, name: 'Super Admin', isActive: true, createdDate: '2022-08-28', updatedDate: '2022-08-28'},
        {id: 3, name: 'User', isActive: true, createdDate: '2022-08-28', updatedDate: '2022-08-28'},
        {id: 4, name: 'Account', isActive: true, createdDate: '2022-08-28', updatedDate: '2022-08-28'},
    ]
    const [show, setShow] = useState(false);
    const [searchTerm, setSearchTerm] = useState('');

    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    const [userList, setUserList ] = useState<UserInfoType[]>([])
    const [page, setPage ] = useState<number>(0)
    const [id, setId ] = useState<number>(0)
    const [pageSize, setPageSize ] = useState<number>(20)
    const [search, setSearch ] = useState<string>('')
    const [roleList, setRoleList] = useState<any>(null);


    const getUserList =async (page:number, pageSize:number, searh:string) => {
        try {
            const resultRole = await listMasterRoleApi();
            const result = await searchUserAPI({body: {
                page,
                    pageSize,
                    keyword:searh
                }})
            if(result?.data) {
                const { data } = result?.data;

                if(data && data.length >0 ) {
                    data.map((d:any) => {
                        const find = role.find((v:any) => v.id === d.roleId)

                        console.log('find', find)
                        if(find) {
                            d.roleName = find.name
                        }
                    })
                }
                console.log('data:', data)

                setUserList(data)
            }
        } catch (e) {
            console.log(e)
        }
    }

    const handleSearch = (e:any) => {
        const value = e
        setSearch(value)
        if(value !== null || true || value !== '') {
            getUserList(page, pageSize, value);
        } else {
            getUserList(page, pageSize, '');
        }
    }

    const handleGetRole = async () => {
        try {
            const result = await listMasterRoleApi();

            if (result?.data) {
                const { data } = result;
                console.log("data", data);
                setRoleList(data);
            }
        } catch (e: any) {
            console.log("error", e);
        }
    };

    const onCreate = async (value:any) => {
        try {
            console.log('value:', value)

            const result = await createUserApi({body: {
                username: value?.username,
                    email:value?.email,
                    roleId:value?.roleId
                }})

            console.log('result', result)

            if(result?.data) {
                getUserList(page, pageSize, '')
                handleClose()

            } else {
                getUserList(page, pageSize, '')
                handleClose()
            }

        } catch (e) {
            console.log('e')
            handleClose()
        }
    }

    useEffect(() => {
        getUserList(page, pageSize, '')
        handleGetRole();
    }, [])

    return (
        <div className={`card ${className}`}>
            <Modal show={show} onHide={handleClose} size={'lg'}>
                <Modal.Header closeButton>
                    <Modal.Title>{'Add User'}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form onSubmit={handleSubmit(onCreate)}>
                        <div className='row'>
                            <label className='col-lg-12 col-form-label fw-bold fs-4'>
                                <span className='required'>User Name</span>
                            </label>
                            <Controller
                                name="username"
                                control={control}
                                rules={{
                                    required: "username is require",
                                }}
                                render={({ field: { value, onChange } }) => (
                                    <input
                                        type="text"
                                        value={value}
                                        onChange={onChange}
                                        className={clsx(
                                            "form-control form-control-lg form-control-solid",
                                            {
                                                "is-invalid":
                                                    errors?.username?.message && errors?.username?.message,
                                            },
                                            {
                                                "is-valid":
                                                    errors?.username?.message && !errors?.username?.message,
                                            }
                                        )}
                                        placeholder="username"
                                    />
                                )}
                            />
                            {errors?.username?.message && (
                                <div className="fv-plugins-message-container text-danger">
                                    <span role="alert">{errors?.username?.message}</span>
                                </div>
                            )}
                        </div>
                        <div className='row mb-10'>
                            <label className='col-lg-12 col-form-label fw-bold fs-4'>
                                <span className='required'>Email</span>
                            </label>
                            <Controller
                                name="email"
                                control={control}
                                rules={{
                                    required: "email is require",
                                }}
                                render={({ field: { value, onChange } }) => (
                                    <input
                                        type="text"
                                        value={value}
                                        onChange={onChange}
                                        className={clsx(
                                            "form-control form-control-lg form-control-solid",
                                            {
                                                "is-invalid":
                                                    errors?.email?.message && errors?.email?.message,
                                            },
                                            {
                                                "is-valid":
                                                    errors?.email?.message && !errors?.email?.message,
                                            }
                                        )}
                                        placeholder="email"
                                    />
                                )}
                            />
                            {errors?.email?.message && (
                                <div className="fv-plugins-message-container text-danger">
                                    <span role="alert">{errors?.email?.message}</span>
                                </div>
                            )}
                        </div>
                        <div className='row'>
                            <label className='col-lg-12 col-form-label fw-bold fs-4'>
                                <span>Role</span>
                            </label>
                        </div>
                        {
                            roleList && roleList.map((data:any,index: number) => (
                                <div className='row mb-4' key={`index_of_role_list_create_${index}`}>

                                    <Controller
                                        name="roleId"
                                        control={control}
                                        rules={{
                                            required: "roleId is require",
                                        }}
                                        render={({ field: { value, onChange } }) => (
                                            <div className='d-flex align-items-center'>
                                                <div className='fv-row mb-2 form-check form-check-custom form-check-solid me-4'>
                                                    <input
                                                        onChange={(e)=> {
                                                            onChange(e)
                                                            setId(+e.target.value)
                                                        }}
                                                        type='radio'
                                                        value={data?.id}
                                                        checked={data?.id === +id}
                                                        className='form-check-input'
                                                    />
                                                </div>
                                                <div className='d-flex justify-content-start flex-column'>
                                                    <label className='fs-6 fw-bold'>
                                                        <span>{data?.name}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        )}
                                    />
                                    {errors?.roleId?.message && (
                                        <div className="fv-plugins-message-container text-danger">
                                            <span role="alert">{errors?.roleId?.message}</span>
                                        </div>
                                    )}
                                </div>

                            ))
                        }
                        <div className="d-flex flex-row justify-content-end">
                            <button className="btn btn-secondary "  onClick={handleClose}>
                                Close
                            </button>
                            <button className="btn btn-primary mx-2" type='submit'>
                                Save Changes
                            </button>
                        </div>
                    </form>
                </Modal.Body>
                {/* <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Discord
                </Button>
                <Button variant="primary" onClick={handleClose}>
                    Submit
                </Button>
            </Modal.Footer> */}
            </Modal>
            <div className='card-header border-0 pt-6'>
                <div className='card-title'>
                    {/* begin::Search */}
                    <div className='d-flex align-items-center position-relative my-1'>
                        <KTSVG
                            path='/media/icons/duotune/general/gen021.svg'
                            className='svg-icon-1 position-absolute ms-6'
                        />
                        <input
                            type='text'
                            data-kt-user-table-filter='search'
                            className='form-control form-control-solid w-250px ps-14'
                            placeholder='Search'
                            value={search}
                            onChange={(e) => handleSearch(e.target.value)}
                        />
                    </div>
                    {/* end::Search */}
                </div>
                {/* begin::Card toolbar */}
                <div className='card-toolbar'>
                    {/* begin::Group actions */}
                    {/* {selected.length > 0 ? <UsersListGrouping /> : <UsersListToolbar />} */}
                    <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
                        {/*<button*/}
                        {/*    type='button'*/}
                        {/*    className='btn btn-secondary me-3'*/}
                        {/*>*/}
                        {/*    <KTSVG path='/media/icons/duotune/general/gen031.svg' className='svg-icon-2' />*/}
                        {/*    Filter*/}
                        {/*</button>*/}
                        {/*<button*/}
                        {/*    type='button'*/}
                        {/*    className='btn btn-secondary me-3'*/}
                        {/*>*/}
                        {/*    <KTSVG path='/media/icons/duotune/files/fil009.svg' className='svg-icon-2' />*/}
                        {/*    Export*/}
                        {/*</button>*/}
                        <button
                            type='button'
                            className='btn btn-primary'
                            onClick={handleShow}
                        >
                            <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
                            Add User
                        </button>
                        {/* end::Add user */}
                    </div>
                    {/* end::Group actions */}
                </div>
                {/* end::Card toolbar */}
            </div>
            {/* begin::Body */}
            <div className='card-body py-3'>
                {/* begin::Table container */}
                <div className='table-responsive'>
                    {/* begin::Table */}
                    <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
                        {/* begin::Table head */}
                        <thead>
                            <tr className='fw-bolder text-muted'>
                                <th className='min-w-150px'>USER</th>
                                <th className='min-w-140px'>ROLE</th>
                                <th className='min-w-120px'>LAST LOGIN</th>
                                <th className='min-w-80px'>JOINED DATE</th>
                                <th className='min-w-60px text-end'>ACTIONS</th>
                            </tr>
                        </thead>
                        {/* end::Table head */}
                        {/* begin::Table body */}
                        <tbody>
                            {
                                userList && userList.map((value, index) => (
                                    <tr key={`index_of_users${index}`}>
                                        <td>
                                            <div className='d-flex align-items-center'>
                                                <div className='symbol symbol-45px me-5'>
                                                    <img src={toAbsoluteUrl('/media/car/car-1.png')} alt='' />
                                                </div>
                                                <div className='d-flex justify-content-start flex-column'>
                                                    <a href='#' className='text-dard fw-bolder text-hover-primary fs-6'>
                                                        { value.username }
                                                    </a>
                                                    <span className='text-muted fw-bold d-block fs-7'>
                                                        { value.email }
                                                    </span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold'> { value.roleName || '-'  } </span>
                                        </td>
                                        <td>
                                            <button className='btn btn-light' disabled> { value.createdDate } </button>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold'> { value.createdDate } </span>
                                        </td>
                                        <td className='text-end'>
                                            <select className="form-select form-select-solid form-sm w-100px float-end" aria-label="Select example">
                                                <option>Actions</option>
                                                <option value="1">Delete</option>
                                            </select>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                        {/* end::Table body */}
                    </table>
                    {/* end::Table */}
                </div>
                {/* end::Table container */}
            </div>
            {/* begin::Body */}
        </div>
    );
}

export default UserManageList;
