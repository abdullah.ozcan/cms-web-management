import { FC } from 'react'
import { PageTitle } from '../../../_metronic/layout/core'
import { KTCard } from '../../../_metronic/helpers'
import CategoryList from './CategoryList'


const CategoryWrapper: FC = () => {
    return (
        <>
            <PageTitle breadcrumbs={[]}>Car Category</PageTitle>
            <KTCard>
              <CategoryList/>
            </KTCard>
        </>
    )
}

export default CategoryWrapper;
