import { useState } from "react";
import { KTSVG, toAbsoluteUrl } from "../../../../_metronic/helpers";
import ModalApp from "../../../../_metronic/layout/components/ModalApp";
import { useForm, Controller } from "react-hook-form";
import { clsx } from "clsx";
import { getCarByIdAPI } from "../../../../api/CategoryApi";
import { createCarRequestAPI } from "../../../../api/CarMaintenanceApi";
type Props = {
  className: string;
  value?: CarRequest[];
  handleClose?: () => void;
  handleShow?: () => void;
  show?: boolean;
  id?: string;
};

type CarRequest = {
  id: number;
  title: string;
  image: string;
  createBy: string;
  startDate: string;
  description: string;
  status: string;
  isActive: boolean;
  createdDate: string;
  updatedDate: string;
};
const DriverAnnoun: React.FC<Props> = ({
  className,
  value,
  handleClose,
  handleShow,
  show,
  id,
}) => {
  const {
    handleSubmit,
    setValue,
    getValues,
    watch,
    formState: { errors },
    control,
  } = useForm({
    defaultValues: {
      title: "",
      startDate: "",
      image: "",
      createBy: "",
      description: "",
      car: null,
    },
  });
  const onCreate = async (value: any) => {
    try {
      const result = await getCarByIdAPI({ id: id || "" });

      const body:any = {
        title: value?.title,
        startDate: value?.startDate,
        image: "",
        createBy: "",
        description: value?.description,
        car: result?.data,
      }
      await createCarRequestAPI({body:body});

      window.location.href = '/car-maintenance'
      
    } catch (e: any) {
      console.log("error", e);
    }
  };
  const ModalCarCheck = () => {
    return (
      <form className="" onSubmit={handleSubmit(onCreate)}>
        <div className="row mb-4">
          <div className="col-lg-6 fv-row">
            <label className="col-form-label fw-bold fs-6">
              <span className="required">วันที่เช็ค</span>
            </label>
            <Controller
              name="startDate"
              control={control}
              rules={{
                required: "field is require",
              }}
              render={({ field: { value, onChange } }) => (
                <input
                  type="date"
                  value={value}
                  onChange={onChange}
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                    {
                      "is-invalid":
                        errors.startDate?.message && errors.startDate?.message,
                    },
                    {
                      "is-valid":
                        errors.startDate?.message && !errors.startDate?.message,
                    }
                  )}
                  placeholder=""
                />
              )}
            />
            {errors.startDate?.message && (
              <div className="fv-plugins-message-container text-danger">
                <span role="alert">{errors.startDate?.message}</span>
              </div>
            )}
          </div>
        </div>
        <div className="row mb-12">
          <div className="col-lg-6 fv-row">
            <label className="col-lg-4 col-form-label fw-bold fs-6">
              <span className="required">หัวข้อ</span>
            </label>
            <Controller
              name="title"
              control={control}
              rules={{
                required: "title is require",
              }}
              render={({ field: { value, onChange } }) => (
                <input
                  type="text"
                  value={value}
                  min={0}
                  onChange={onChange}
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                    {
                      "is-invalid":
                        errors.title?.message && errors.title?.message,
                    },
                    {
                      "is-valid":
                        errors.title?.message && !errors.title?.message,
                    }
                  )}
                  placeholder=""
                />
              )}
            />
            {errors.title?.message && (
              <div className="fv-plugins-message-container text-danger">
                <span role="alert">{errors.title?.message}</span>
              </div>
            )}
          </div>
        </div>
        <div className="row mb-12">
          <div className="col-lg-12 fv-row">
            <label className="col-lg-4 col-form-label fw-bold fs-6">
              <span className="">หมายเหตุ</span>
            </label>
            <Controller
              name="description"
              control={control}
              rules={{
                required: false,
              }}
              render={({ field: { value, onChange } }) => (
                <textarea
                  rows={4}
                  value={value}
                  onChange={onChange}
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                  )}
                  placeholder=""
                />
              )}
            />
          </div>
        </div>
        <div className="d-flex flex-row justify-content-end">
            <button className="btn btn-secondary "  onClick={handleClose}>
              Close
            </button>
            <button className="btn btn-primary mx-2" type='submit'>
              Save Changes
            </button>
          </div>
      </form>
    );
  };
  return (
    <div className={`card ${className}`}>
      {/* begin::Header */}
      <div className="card-header border-0 pt-5">
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bolder fs-3 mb-1">
            คนขับแจ้งอาการรถ
          </span>
        </h3>
        <div
          className="card-toolbar"
          data-bs-toggle="tooltip"
          data-bs-placement="top"
          data-bs-trigger="hover"
          title="Click to add a user"
        >
          <button
            type="button"
            className="btn btn-primary mx-1"
            onClick={handleShow}
          >
            Add
          </button>
          <ModalApp
            title="คนขับแจ้งอาการ"
            show={show}
            type='submit'
            handleClose={handleClose}
            handleOpen={handleShow}
            chil={<ModalCarCheck />}
          />
        </div>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body py-3">
        {/* begin::Table container */}
        <div className="table-responsive">
          {/* begin::Table */}
          <table className="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
            {/* begin::Table head */}
            <thead>
              <tr className="fw-bolder text-muted">
                <th className="min-w-25px text-capitalize">No</th>
                <th className="min-w-120px text-capitalize">Title</th>
                <th className="min-w-120px text-capitalize">Date</th>
                <th className="min-w-120px text-capitalize">อาการรถ</th>
              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
              {value &&
                value.map((value, index) => (
                  <tr key={`payment_detail_${index}`}>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {++index}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.title}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.createdDate}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.description}
                      </span>
                    </td>
                  </tr>
                ))}
            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>
        {/* end::Table container */}
      </div>
      {/* begin::Body */}
    </div>
  );
};

export default DriverAnnoun;
