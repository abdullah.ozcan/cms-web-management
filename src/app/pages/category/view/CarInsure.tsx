import { FC, useState } from "react";
import { Link } from "react-router-dom";
import ModalApp from "../../../../_metronic/layout/components/ModalApp";
import { useForm, Controller } from "react-hook-form";
import { Button } from "react-bootstrap";
import { getCarByIdAPI } from "../../../../api/CategoryApi";
import { clsx } from "clsx";
import { createCarACTAPI, createCarInsuranceAPI } from "../../../../api/CarMaintenanceApi";

type InsuranceType = {
  amount: number;
  broker: string;
  createdDate: string;
  description: string;
  expireDate: string;
  id: number;
  isActive: boolean;
  startDate: string;
  status: string;
  title: string;
  type: string;
  updatedDate: string;
};
type Props = {
  className: string;
  value?: InsuranceType[];
  type?: string;
  handleClose?: () => void;
  handleShow?: () => void;
  show?: boolean;
  id?: string;
};

const CarInsure: FC<Props> = ({
  className,
  value,
  type,
  handleClose,
  handleShow,
  show,
  id,
}) => {
  const {
    handleSubmit,
    setValue,
    formState: { errors },
    control,
  } = useForm({
    defaultValues: {
      title: "",
      startDate: "",
      expireDate: "",
      broker: "",
      amount: 0,
      description: "",
      car: null,
    },
  });

  const onCreate = async (value: any) => {
    try {
      const result = await getCarByIdAPI({ id: id || "" });
      const body:any  = {
        title: value?.title,
        startDate: value?.startDate,
        expireDate:  value?.expireDate,
        broker: value?.broker,
        amount: value?.amount,
        description: value?.description,
        car: result?.data
      }
      
      if(type === 'insurance') {
        await createCarInsuranceAPI({body:body});
      } else {
        await createCarACTAPI({body:body})
      }
      await createCarInsuranceAPI({body:body});

      window.location.href = `/category/car/view/${id}`
      
    } catch (e: any) {
      console.log("error", e);
    }
  };

  const ModalCarCheck = () => {
    return (
      <div className="">
        <form onSubmit={handleSubmit(onCreate)}>
          <div className="row mb-4">
            <div className="col-lg-6 fv-row">
              <label className="col-form-label fw-bold fs-6">
                <span className="required">วันที่เริ่ม</span>
              </label>
              {/* <input
                type="date"
                className="form-control form-control-lg form-control-solid"
                placeholder=""
              /> */}
              <Controller
                name="startDate"
                control={control}
                rules={{
                  required: "startDate is require",
                }}
                render={({ field: { value, onChange } }) => (
                  <input
                    type="date"
                    value={value}
                    onChange={onChange}
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "is-invalid":
                          errors.startDate?.message && errors.startDate?.message,
                      },
                      {
                        "is-valid":
                          errors.startDate?.message && !errors.startDate?.message,
                      }
                    )}
                    placeholder="type"
                  />
                )}
              />
              {errors.startDate?.message && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{errors.startDate?.message}</span>
                </div>
              )}
            </div>
          </div>
          <div className="row mb-12">
            <div className="col-lg-6 fv-row">
              <label className="col-lg-4 col-form-label fw-bold fs-6">
                <span className="required">Broker</span>
              </label>
              <Controller
                name="broker"
                control={control}
                rules={{
                  required: "broker is require",
                }}
                render={({ field: { value, onChange } }) => (
                  <input
                    type="text"
                    value={value}
                    onChange={onChange}
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "is-invalid":
                          errors.broker?.message && errors.broker?.message,
                      },
                      {
                        "is-valid":
                          errors.broker?.message && !errors.broker?.message,
                      }
                    )}
                    placeholder=""
                  />
                )}
              />
              {errors.broker?.message && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{errors.broker?.message}</span>
                </div>
              )}
            </div>
            <div className="col-lg-6 fv-row">
              <label className="col-lg-4 col-form-label fw-bold fs-6">
                <span className="required">Amount</span>
              </label>
              <Controller
                name="amount"
                control={control}
                rules={{
                  required: "amount is require",
                }}
                render={({ field: { value, onChange } }) => (
                  <input
                    type="number"
                    min={0}
                    value={value}
                    onChange={onChange}
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "is-invalid":
                          errors.amount?.message && errors.amount?.message,
                      },
                      {
                        "is-valid":
                          errors.amount?.message && !errors.amount?.message,
                      }
                    )}
                    placeholder=""
                  />
                )}
              />
              {errors.amount?.message && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{errors.amount?.message}</span>
                </div>
              )}
            </div>
          </div>
          <div className="row mb-12">
            <div className="col-lg-6 fv-row">
              <label className="col-lg-4 col-form-label fw-bold fs-6">
                <span className="required">วันที่หมดอายุ</span>
              </label>
              <Controller
                name="expireDate"
                control={control}
                rules={{
                  required: "startDate is require",
                }}
                render={({ field: { value, onChange } }) => (
                  <input
                    type="date"
                    value={value}
                    onChange={onChange}
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                      {
                        "is-invalid":
                          errors.expireDate?.message && errors.expireDate?.message,
                      },
                      {
                        "is-valid":
                          errors.expireDate?.message && !errors.expireDate?.message,
                      }
                    )}
                    placeholder="type"
                  />
                )}
              />
              {errors.expireDate?.message && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{errors.expireDate?.message}</span>
                </div>
              )}
            </div>
          </div>
          <div className="row mb-12">
            <div className="col-lg-12 fv-row">
              <label className="col-lg-4 col-form-label fw-bold fs-6">
                <span className="">หมายเหตุ</span>
              </label>
               <Controller
                name="description"
                control={control}
                rules={{
                  required: false
                }}
                render={({ field: { value, onChange } }) => (
                    <textarea
                    rows={4}
                    value={value}
                    onChange={onChange}
                    className={clsx(
                      "form-control form-control-lg form-control-solid",
                    )}
                    placeholder=""
                  />
                )}
              />
              {errors.description?.message && (
                <div className="fv-plugins-message-container text-danger">
                  <span role="alert">{errors.description?.message}</span>
                </div>
              )}
            </div>
          </div>
          <div className="d-flex flex-row justify-content-end">
            <button className="btn btn-secondary "  onClick={handleClose}>
              Close
            </button>
            <button className="btn btn-primary mx-2" type='submit'>
              Save Changes
            </button>
          </div>
        </form>
      </div>
    );
  };
  return (
    <div className={`card ${className}`}>
      {/* begin::Header */}
      <div className="card-header border-0 pt-5">
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bolder fs-3 mb-1">
            {type === "insurance" ? "ประกันภัย" : "พรบ."}
          </span>
        </h3>
        <div
          className="card-toolbar"
          data-bs-toggle="tooltip"
          data-bs-placement="top"
          data-bs-trigger="hover"
          title="Click to add a user"
        >
          <button
            type="button"
            className="btn btn-primary mx-1"
            onClick={handleShow}
          >
            Add
          </button>
          <ModalApp
            title={type === "insurance" ? "ประกันภัย" : "พรบ."}
            show={show}
            type="submit"
            handleClose={handleClose}
            handleOpen={handleShow}
            chil={<ModalCarCheck />}
          />
        </div>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body py-3">
        {/* begin::Table container */}
        <div className="table-responsive">
          {/* begin::Table */}
          <table className="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
            {/* begin::Table head */}
            <thead>
              <tr className="fw-bolder text-muted">
                <th className="min-w-25px text-capitalize">No</th>
                <th className="min-w-140px text-capitalize">DATE REGISTER</th>
                <th className="min-w-120px text-capitalize">INSURANCE</th>
                <th className="min-w-120px text-capitalize">TYPE</th>
                <th className="min-w-120px text-capitalize">AMOUNT</th>
                <th className="min-w-120px text-capitalize">EXPIRE</th>
                <th className="min-w-120px text-capitalize">BROKER</th>
                <th className="min-w-100px text-end text-capitalize">NOTE</th>
              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
              {value &&
                value.map((value, index) => (
                  <tr key={`payment_detail_${index}`}>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {++index}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.startDate}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {"INSURANCE"}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7"></span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.amount}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.expireDate}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.broker}
                      </span>
                    </td>
                    <td>
                      <div className="d-flex justify-content-end flex-shrink-0">
                        <Link
                          to={"/category/cardetail-page"}
                          className="menu-link px-5 text-primary"
                        >
                          ...
                        </Link>
                      </div>
                    </td>
                  </tr>
                ))}
            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>
        {/* end::Table container */}
      </div>
      {/* begin::Body */}
    </div>
  );
};
export default CarInsure;
