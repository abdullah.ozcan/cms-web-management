import { useState } from "react";
import { Link } from "react-router-dom";
import { KTSVG } from "../../../../_metronic/helpers";
import ModalApp from "../../../../_metronic/layout/components/ModalApp";
import { useForm, Controller } from "react-hook-form";
import { clsx } from "clsx";
import { getCarByIdAPI } from "../../../../api/CategoryApi";
import { createCarRepairAPI } from "../../../../api/CarMaintenanceApi";
import {Button, Modal} from "react-bootstrap";
import CarRepairUpdate from "./CarRepairUpdate";

type Props = {
  className: string;
  value?: CarRepair[];
  handleClose?: () => void;
  handleShow?: () => void;
  show?: boolean;
  id?: string;
};

type CarRepair = {
  id: number;
  currentMileCheck: number;
  nextMileCheck: number;
  checkDate: string;
  currentMile: number;
  description: string;
  status: string;
  isActive: boolean;
  createdDate: string;
  updatedDate: string;
};

const CarFix: React.FC<Props> = ({
  className,
  value,
  handleClose,
  handleShow,
  show,
  id,
}) => {
  const {
    handleSubmit,
    formState: { errors },
    control,
  } = useForm({
    defaultValues: {
        currentMileCheck: 0,
        nextMileCheck: 0,
        checkDate: '',
        currentMile: 0,
        description: '',
        car: null,
    },
  });




  const [mile, setMile] = useState<number>(0);
  const [showUpdate, setShowUpdate] = useState<boolean>(false);
  const [informationCar, setInformationCar] = useState<CarRepair| null>(null);

  const onCreate = async (value: any) => {
    try {
      const result = await getCarByIdAPI({ id: id || "" });

      const body:any = {
        currentMileCheck: value?.currentMileCheck,
        nextMileCheck: value?.nextMileCheck,
        checkDate: value?.checkDate,
        currentMile: value?.currentMile,
        description: value?.description,
        car: result?.data,
      }
      await createCarRepairAPI({body:body});

      window.location.href = '/car-maintenance'

    } catch (e: any) {
      console.log("error", e);
    }
  };

    const onUpdate = async (value: any) => {
        try {

            const result = await getCarByIdAPI({ id: id || "" });

            const body:any = {
                currentMileCheck: value?.currentMileCheck,
                nextMileCheck: value?.nextMileCheck,
                checkDate: value?.checkDate,
                currentMile: value?.currentMile,
                description: value?.description,
                car: result?.data,
            }
            await createCarRepairAPI({body:body});

            window.location.href = '/car-maintenance'

        } catch (e: any) {
            console.log("error", e);
        }
    };
  const ModalCarCheck = () => {
    return (
      <form className="" onSubmit={handleSubmit(onCreate)}>
        <div className="row mb-4">
          <div className="col-lg-6 fv-row">
            <label className="col-form-label fw-bold fs-6">
              <span className="required">วันที่เช็ค</span>
            </label>
            <Controller
              name="checkDate"
              control={control}
              rules={{
                required: "checkDate is require",
              }}
              render={({ field: { value, onChange } }) => (
                <input
                  type="date"
                  value={value}
                  onChange={onChange}
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                    {
                      "is-invalid":
                        errors.checkDate?.message && errors.checkDate?.message,
                    },
                    {
                      "is-valid":
                        errors.checkDate?.message && !errors.checkDate?.message,
                    }
                  )}
                  placeholder="type"
                />
              )}
            />
            {errors.checkDate?.message && (
              <div className="fv-plugins-message-container text-danger">
                <span role="alert">{errors.checkDate?.message}</span>
              </div>
            )}
          </div>
        </div>
        <div className="row mb-12">
          <div className="col-lg-6 fv-row">
            <label className="col-lg-4 col-form-label fw-bold fs-6">
              <span className="required">ไมค์เช็คระยะล่าสุด</span>
            </label>
            <Controller
              name="currentMileCheck"
              control={control}
              rules={{
                required: "field is require",
              }}
              render={({ field: { value, onChange } }) => (
                <input
                  type="number"
                  value={value}
                  min={0}
                  onChange={onChange}
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                    {
                      "is-invalid":
                        errors.currentMileCheck?.message && errors.currentMileCheck?.message,
                    },
                    {
                      "is-valid":
                        errors.currentMileCheck?.message && !errors.currentMileCheck?.message,
                    }
                  )}
                  placeholder="type"
                />
              )}
            />
            {errors.currentMileCheck?.message && (
              <div className="fv-plugins-message-container text-danger">
                <span role="alert">{errors.currentMileCheck?.message}</span>
              </div>
            )}
          </div>
          <div className="col-lg-6 fv-row">
            <label className="col-lg-4 col-form-label fw-bold fs-6">
              <span className="required">กำหนดครั้งต่อไป</span>
            </label>
            <Controller
              name="nextMileCheck"
              control={control}
              rules={{
                required: "field is require",
              }}
              render={({ field: { value, onChange } }) => (
                <input
                  type="text"
                  value={value}
                  onChange={onChange}
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                    {
                      "is-invalid":
                        errors.nextMileCheck?.message && errors.nextMileCheck?.message,
                    },
                    {
                      "is-valid":
                        errors.nextMileCheck?.message && !errors.nextMileCheck?.message,
                    }
                  )}
                  placeholder="type"
                />
              )}
            />
            {errors.nextMileCheck?.message && (
              <div className="fv-plugins-message-container text-danger">
                <span role="alert">{errors.nextMileCheck?.message}</span>
              </div>
            )}
          </div>
        </div>
        <div className="row mb-12">
          <div className="col-lg-6 fv-row">
            <label className="col-lg-4 col-form-label fw-bold fs-6">
              <span className="required">เลขปัจจุบัน</span>
            </label>
            <Controller
              name="currentMile"
              control={control}
              rules={{
                required: "field is require",
              }}
              render={({ field: { value, onChange } }) => (
                <input
                  type="number"
                  value={value}
                  min={0}
                  onChange={onChange}
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                    {
                      "is-invalid":
                        errors.currentMile?.message && errors.currentMile?.message,
                    },
                    {
                      "is-valid":
                        errors.currentMile?.message && !errors.currentMile?.message,
                    }
                  )}
                  placeholder=""
                />
              )}
            />
            {errors.currentMile?.message && (
              <div className="fv-plugins-message-container text-danger">
                <span role="alert">{errors.currentMile?.message}</span>
              </div>
            )}
          </div>
          <div className="col-lg-6 fv-row">
            <label className="col-lg-4 col-form-label fw-bold fs-6">
              <span className="">คงเหลือ</span>
            </label>
            <input
              type="text"
              disabled
              value={mile}
            //   value={watch('nextMileCheck') - watch('currentMile')}
              className="form-control form-control-lg form-control-solid"
              placeholder=""
            />
          </div>
        </div>
        <div className="row mb-12">
          <div className="col-lg-12 fv-row">
            <label className="col-lg-4 col-form-label fw-bold fs-6">
              <span className="">หมายเหตุ</span>
            </label>
             <Controller
              name="description"
              control={control}
              rules={{
                required: false,
              }}
              render={({ field: { value, onChange } }) => (
                <textarea
                  rows={4}
                  value={value}
                  onChange={onChange}
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                  )}
                  placeholder=""
                />
              )}
            />
          </div>
        </div>
        <div className="d-flex flex-row justify-content-end">
            <button className="btn btn-secondary "  onClick={handleClose}>
              Close
            </button>
            <button className="btn btn-primary mx-2" type='submit'>
              Save Changes
            </button>
          </div>
      </form>
    );
  };
  return (
    <div className={`card ${className}`}>
      <CarRepairUpdate
          value={informationCar || null}
          id={id}
          className={''}
          show={showUpdate}
          handleClose={()=>{setShowUpdate(false)}}
          handleShow={()=>{setShowUpdate(true)}}
      />
      {/* begin::Header */}
      <div className="card-header border-0 pt-5">
        <h3 className="card-title align-items-start flex-column">
          <span className="card-label fw-bolder fs-3 mb-1">ซ่อมบำรุง.</span>
        </h3>
        <div
          className="card-toolbar"
          data-bs-toggle="tooltip"
          data-bs-placement="top"
          data-bs-trigger="hover"
          title="Click to add a user"
        >
          <button
            type="button"
            className="btn btn-primary mx-1"
            onClick={handleShow}
          >
            Add
          </button>
          <ModalApp
            title="Car Check"
            show={show}
            type='submit'
            handleClose={handleClose}
            handleOpen={handleShow}
            chil={<ModalCarCheck />}
          />
        </div>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body py-3">
        {/* begin::Table container */}
        <div className="table-responsive">
          {/* begin::Table */}
          <table className="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
            {/* begin::Table head */}
            <thead>
              <tr className="fw-bolder text-muted">
                <th className="min-w-25px text-capitalize">No</th>
                <th className="min-w-140px text-capitalize">DATE</th>
                <th className="min-w-120px text-capitalize">
                  ไมล์เช็คระยะล่าสุด
                </th>
                <th className="min-w-120px text-capitalize">ครั้งต่อไป</th>
                <th className="min-w-120px text-capitalize">วันที่เช็ค</th>
                <th className="min-w-120px text-capitalize">เลขปัจจุบัน</th>
                <th className="min-w-120px text-capitalize">คงเหลือ</th>
                <th className="min-w-100px text-end text-capitalize">NOTE</th>
              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
              {value &&
                value.map((value, index) => (
                  <tr key={`payment_detail_${index}`}>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {++index}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.createdDate}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.currentMileCheck}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.nextMileCheck}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {"-"}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.currentMile}
                      </span>
                    </td>
                    <td>
                      <span className="text-muted fw-bold text-muted d-block fs-7">
                        {value?.nextMileCheck - value?.currentMile}
                      </span>
                    </td>
                    <td>
                      <div className="d-flex justify-content-end flex-shrink-0">
                          <button
                              type="button"
                              className="btn btn-primary mx-1"
                              onClick={()=>{
                                  console.log('value==>', value)
                                  setInformationCar(value)
                                  setShowUpdate(true)}}
                          >
                              Edit
                          </button>
                      </div>
                    </td>
                  </tr>
                ))}
            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>
        {/* end::Table container */}
      </div>
      {/* begin::Body */}
    </div>
  );
};

export default CarFix;
