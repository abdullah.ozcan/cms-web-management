import {FC, useEffect} from "react";
import {Modal} from "react-bootstrap";
import {Controller, useForm} from "react-hook-form";
import {clsx} from "clsx";
import {getCarByIdAPI} from "../../../../api/CategoryApi";
import {updateCarRepairAPI} from "../../../../api/CarMaintenanceApi";

type CarRepair = {
    id: number | null;
    currentMileCheck: number;
    nextMileCheck: number;
    checkDate: string;
    currentMile: number;
    description: string;
    status: string;
    isActive: boolean;
    createdDate: string;
    updatedDate: string;
};
type Props = {
    className: string;
    value?: CarRepair | null;
    handleClose?: () => void;
    handleShow?: () => void;
    show?: boolean;
    id?: string;
};

const CarRepairUpdate:FC<Props> = ({className,value,handleClose,handleShow,show,id }) => {
    const {
        handleSubmit,
        setValue,
        formState: { errors },
        control,
    } = useForm({
        defaultValues: {
            id: 0,
            currentMileCheck: 0,
            nextMileCheck: 0,
            checkDate: '',
            currentMile: 0,
            description: '',
            car: null,
        },
    });

    const initValue = () => {
        setValue('currentMileCheck', value?.currentMileCheck || 0);
        setValue('nextMileCheck', value?.nextMileCheck || 0);
        setValue('checkDate', value?.checkDate || '');
        setValue('currentMile', value?.currentMile ||  0);
        setValue('description', value?.description || '');
        setValue('id', value?.id || 0);
    }

    useEffect(()=> {
        initValue()
    }, [value])
    const onUpdate = async (value: any) => {
        try {

            console.log('value', value)
            const result = await getCarByIdAPI({ id: id || "" });

            const body:any = {
                currentMileCheck: value?.currentMileCheck,
                nextMileCheck: value?.nextMileCheck,
                checkDate: value?.checkDate,
                currentMile: value?.currentMile,
                description: value?.description,
                id:value?.id,
                car: result?.data,
            }
            console.log('body:', body)
            await updateCarRepairAPI({body:body});
            //
            window.location.href = '/car-maintenance'

        } catch (e: any) {
            console.log("error", e);
        }
    };
    return (
        <Modal show={show} onHide={handleClose} size={'lg'}>
            <Modal.Header closeButton>
                <Modal.Title>แก้ไข</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form className="" onSubmit={handleSubmit(onUpdate)}>
                    <div className="row mb-4">
                        <div className="col-lg-6 fv-row">
                            <label className="col-form-label fw-bold fs-6">
                                <span className="required">วันที่เช็ค</span>
                            </label>
                            <Controller
                                name="checkDate"
                                control={control}
                                rules={{
                                    required: "checkDate is require",
                                }}
                                render={({ field: { value, onChange } }) => (
                                    <input
                                        type="date"
                                        value={value}
                                        onChange={onChange}
                                        className={clsx(
                                            "form-control form-control-lg form-control-solid",
                                            {
                                                "is-invalid":
                                                    errors?.checkDate?.message && errors?.checkDate?.message,
                                            },
                                            {
                                                "is-valid":
                                                    errors?.checkDate?.message && !errors?.checkDate?.message,
                                            }
                                        )}
                                        placeholder="type"
                                    />
                                )}
                            />
                            {errors?.checkDate?.message && (
                                <div className="fv-plugins-message-container text-danger">
                                    <span role="alert">{errors?.checkDate?.message}</span>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="row mb-12">
                        <div className="col-lg-6 fv-row">
                            <label className="col-lg-4 col-form-label fw-bold fs-6">
                                <span className="required">ไมค์เช็คระยะล่าสุด</span>
                            </label>
                            <Controller
                                name="currentMileCheck"
                                control={control}
                                rules={{
                                    required: "field is require",
                                }}
                                render={({ field: { value, onChange } }) => (
                                    <input
                                        type="number"
                                        value={value}
                                        min={0}
                                        onChange={onChange}
                                        className={clsx(
                                            "form-control form-control-lg form-control-solid",
                                            {
                                                "is-invalid":
                                                    errors?.currentMileCheck?.message && errors?.currentMileCheck?.message,
                                            },
                                            {
                                                "is-valid":
                                                    errors?.currentMileCheck?.message && !errors?.currentMileCheck?.message,
                                            }
                                        )}
                                        placeholder="type"
                                    />
                                )}
                            />
                            {errors?.currentMileCheck?.message && (
                                <div className="fv-plugins-message-container text-danger">
                                    <span role="alert">{errors?.currentMileCheck?.message}</span>
                                </div>
                            )}
                        </div>
                        <div className="col-lg-6 fv-row">
                            <label className="col-lg-4 col-form-label fw-bold fs-6">
                                <span className="required">กำหนดครั้งต่อไป</span>
                            </label>
                            <Controller
                                name="nextMileCheck"
                                control={control}
                                rules={{
                                    required: "field is require",
                                }}
                                render={({ field: { value, onChange } }) => (
                                    <input
                                        type="text"
                                        value={value}
                                        onChange={onChange}
                                        className={clsx(
                                            "form-control form-control-lg form-control-solid",
                                            {
                                                "is-invalid":
                                                    errors?.nextMileCheck?.message && errors?.nextMileCheck?.message,
                                            },
                                            {
                                                "is-valid":
                                                    errors?.nextMileCheck?.message && !errors?.nextMileCheck?.message,
                                            }
                                        )}
                                        placeholder="type"
                                    />
                                )}
                            />
                            {errors?.nextMileCheck?.message && (
                                <div className="fv-plugins-message-container text-danger">
                                    <span role="alert">{errors?.nextMileCheck?.message}</span>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="row mb-12">
                        <div className="col-lg-6 fv-row">
                            <label className="col-lg-4 col-form-label fw-bold fs-6">
                                <span className="required">เลขปัจจุบัน</span>
                            </label>
                            <Controller
                                name="currentMile"
                                control={control}
                                rules={{
                                    required: "field is require",
                                }}
                                render={({ field: { value, onChange } }) => (
                                    <input
                                        type="number"
                                        value={value}
                                        min={0}
                                        onChange={onChange}
                                        className={clsx(
                                            "form-control form-control-lg form-control-solid",
                                            {
                                                "is-invalid":
                                                    errors?.currentMile?.message && errors?.currentMile?.message,
                                            },
                                            {
                                                "is-valid":
                                                    errors?.currentMile?.message && !errors?.currentMile?.message,
                                            }
                                        )}
                                        placeholder=""
                                    />
                                )}
                            />
                            {errors?.currentMile?.message && (
                                <div className="fv-plugins-message-container text-danger">
                                    <span role="alert">{errors?.currentMile?.message}</span>
                                </div>
                            )}
                        </div>
                        <div className="col-lg-6 fv-row">
                            <label className="col-lg-4 col-form-label fw-bold fs-6">
                                <span className="">คงเหลือ</span>
                            </label>
                            <input
                                type="text"
                                disabled
                                value={0}
                                //   value={watch('nextMileCheck') - watch('currentMile')}
                                className="form-control form-control-lg form-control-solid"
                                placeholder=""
                            />
                        </div>
                    </div>
                    <div className="row mb-12">
                        <div className="col-lg-12 fv-row">
                            <label className="col-lg-4 col-form-label fw-bold fs-6">
                                <span className="">หมายเหตุ</span>
                            </label>
                            <Controller
                                name="description"
                                control={control}
                                rules={{
                                    required: false,
                                }}
                                render={({ field: { value, onChange } }) => (
                                    <textarea
                                        rows={4}
                                        value={value}
                                        onChange={onChange}
                                        className={clsx(
                                            "form-control form-control-lg form-control-solid",
                                        )}
                                        placeholder=""
                                    />
                                )}
                            />
                        </div>
                    </div>
                    <div className="d-flex flex-row justify-content-end">
                        <button className="btn btn-secondary "  onClick={handleClose}>
                            Close
                        </button>
                        <button className="btn btn-primary mx-2" type='submit'>
                            Save Changes
                        </button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    )
}

export default CarRepairUpdate;
