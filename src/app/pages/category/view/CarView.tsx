import { FC, useEffect, useState } from "react";
import { Tabs, Tab } from "react-bootstrap";
import {
  searchCarACTAPI,
  searchCarInsuranceAPI,
  searchCarRepairAPI,
  searchCarRequestPI,
} from "../../../../api/CarMaintenanceApi";
import { PageTitle } from "../../../../_metronic/layout/core";
import CarFix from "./CarFix";
import CarInsure from "./CarInsure";
import DriverAnnoun from "./DriverAnnoun";
import {Link, useParams} from "react-router-dom";
import { getCarViewAPI } from "../../../../api/CategoryApi";
import {getOrderCarsApi} from "../../../../api/OrderApi";
import Pagination from "@vlsergey/react-bootstrap-pagination";
import moment from 'moment';

type CarType = {
  brand: string;
  id: number;
  model: string;
  status: string;
  title: string;
  vinNo: string;
  year: string;
}
type CustomerType = {
  "id": number;
  "name": string;
  "email": string;
}

type EmployeeType = {
  "id": number;
  "firstName": string;
  "lastName": string;
}

type AgentType = {
  "id": number;
  "name": string;
  "contactName": string;
}
type OrderType = {
  address1: string;
  address2: string;
  agentId: number;
  agentName: string;
  carId: number;
  city: string;
  createdDate: string;
  currentMile: number;
  customerId: number;
  customerName: string;
  description: string;
  distantTotalMile: number;
  dropPlace: string;
  dropTime: string;
  employeeId: number;
  endMile: number;
  id: number;
  isPayment: boolean;
  name: string;
  oilAVG: number;
  oilPercent: number;
  paymentAmount: number;
  paymentDate: string;
  paymentDescription: string;
  paymentSlip: string;
  paymentType: string;
  phone: string;
  pickUpPlace: string;
  pickUpTime: string;
  postCode: string;
  pricePerDay: number;
  province: string;
  rentType: string;
  startMile: number;
  status: string;
  totalDate: number;
  updatedDate: string;
  agents: AgentType;
  employee: EmployeeType;
  customers: CustomerType;
  cars: CarType;
}
type InsuranceType = {
  amount: number;
  broker: string;
  createdDate: string;
  description: string;
  expireDate: string;
  id: number;
  isActive: boolean;
  startDate: string;
  status: string;
  title: string;
  type: string;
  updatedDate: string;
};

type CarRepair = {
  id: number;
  currentMileCheck: number;
  nextMileCheck: number;
  checkDate: string;
  currentMile: number;
  description: string;
  status: string;
  isActive: boolean;
  createdDate: string;
  updatedDate: string;
};

type CarRequest = {
  id: number;
  title: string;
  image: string;
  createBy: string;
  startDate: string;
  description: string;
  status: "ACTIVE";
  isActive: boolean;
  createdDate: string;
  updatedDate: string;
};

type CarViewType = {
  "insuranceAmount":number;
  "actAmount": number;
  "total": number;
  "data": {
      "id": number;
      "title": string;
      "currentVin":  string;
      "beforeVin":  string;
      "brand":  string;
      "model":  string;
      "year":  string;
      "regisDate": string;
      "regisVinDate": string;
      "age": number;
      "vinNo":  string;
      "color":  string;
      "mile": number;
      "fixTimes":number;
      "description":  string;
      "status":  string;
      "isActive": boolean;
      "createdDate":  string;
      "updatedDate":  string;
      "imgUrl": string;
  }
}
const CarView: FC = () => {
  let { id } = useParams();

  const [key, setKey] = useState("order");

  // INSURANCE
  const [pageInsurance, setPageInsurance] = useState<number>(0);
  const [pageSizeInsurance, setPageSizeInsurance] = useState<number>(0);
  const [dataInsurance, setDataInsurance] = useState<InsuranceType[]>([]);

  const [carInformation, setCarInformation] = useState<CarViewType | null>(null);


  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [showACT, setShowACT] = useState(false);
  const handleCloseACT = () => setShowACT(false);
  const handleShowACT = () => setShowACT(true);

  const [showRepair, setShowRepair] = useState(false);
  const handleCloseRepair = () => setShowRepair(false);
  const handleShowRepair = () => setShowRepair(true);

  const [showRequest, setShowRequest] = useState(false);
  const handleCloseRequest = () => setShowRequest(false);
  const handleShowRequest = () => setShowRequest(true);

  // ACT
  const [pageACT, setPageACT] = useState<number>(0);
  const [pageSizeACT, setPageSizeACT] = useState<number>(0);
  const [dataACT, setDataACT] = useState<InsuranceType[]>([]);

  // REPAIR
  const [pageRepair, setPageRepair] = useState<number>(0);
  const [pageSizeRepair, setPageSizeRepair] = useState<number>(0);
  const [dataRepair, setDataRepair] = useState<CarRepair[]>([]);

  // REQUEST
  const [pageRequest, setPageRequest] = useState<number>(0);
  const [pageSizeRequest, setPageSizeRequest] = useState<number>(0);
  const [dataRequest, setDataRequest] = useState<CarRequest[]>([]);


  // CARS TAB
  const [totalPageCar, setTotalPageCar] = useState<number>(1);
  const [pageCar, setPageCar] = useState<number>(1);
  const [pageSizeCar, setPageSizeCar] = useState<number>(10);
  const [dataCars, setDataCars] = useState<OrderType[]>([]);

  const handleChangePageCar =async (num:number) => {
    setPageCar(num)
    const result = await getOrderCarsApi({
      page: num || 1,
      pageSize: pageSizeCar || 10,
      carId: id || "",
    });

    if (result?.data) {
      const { data, totalPages } = result?.data;
      setTotalPageCar(totalPages)
      setDataCars(data)
    }
  }

  const handleSelectTab = async (value: any) => {
    try {
      console.log("value:", value);
      setKey(value);
      if (value === "insurance") {
        const result = await searchCarInsuranceAPI({
          body: {
            page: 0,
            pageSize: 15,
            id: id || "",
          },
        });

        if (result?.data) {
          const { data } = result?.data;
          setDataInsurance(data);
        }
      } else if (value === "act") {
        const result = await searchCarACTAPI({
          body: {
            page: 0,
            pageSize: 15,
            id: id || "",
          },
        });

        if (result?.data) {
          const { data } = result?.data;
          setDataACT(data);
        }
      } else if (value === "repair") {
        const result = await searchCarRepairAPI({
          body: {
            page: 0,
            pageSize: 15,
            id: id || "",
          },
        });

        if (result?.data) {
          const { data } = result?.data;
          setDataRepair(data);
        }
      } else if (value === "request") {
        const result = await searchCarRequestPI({
          body: {
            page: 0,
            pageSize: 15,
            id: id || "",
          },
        });

        if (result?.data) {
          const { data } = result?.data;
          setDataRequest(data);
        }
      } else if(value ==='order') {
        const result = await getOrderCarsApi({
          page: 0,
          pageSize: 15,
          carId: id || "",
        });

        if (result?.data) {
          const { data, totalPages } = result?.data;
          setTotalPageCar(totalPages)
          setDataCars(data)
        }
      }  else {
        const result = await searchCarInsuranceAPI({
          body: {
            page: 0,
            pageSize: 15,
            id: id || "",
          },
        });

        if (result?.data) {
          const { data } = result?.data;
          setDataInsurance(data);
        }
      }
    } catch (e: any) {
      console.log("error", e);
    }
  };

  const inintCar = async (p:number, size:number) => {
    const result = await getOrderCarsApi({
      page: p || 1,
      pageSize: size || 10,
      carId: id || "",
    });

    if (result?.data) {
      const { data, totalPages } = result?.data;
      setTotalPageCar(totalPages)
      setDataCars(data)
    }
  };

  const getCarInformation = async () => {
    try {
      const result = await getCarViewAPI({id: id || ''});
      if(result?.data) {
        setCarInformation(result?.data)
      }

    } catch(e:any) {
      console.log('e', e);

    }
  }

  useEffect(() => {
    inintCar(pageCar, pageSizeCar);
    getCarInformation();
  }, []);

  return (
    <>
      <PageTitle breadcrumbs={[]}>Category Detail Page</PageTitle>
      <div className="row g-5 g-xxl-8">
        <div className="col-xl-4">
          <div className="mb-5 mb-xxl-8 bg-white">
            {/* begin::Body */}
            <div className="flex-grow-1">
              <div className="d-flex justify-content-center align-items-center flex-wrap mb-2">
                <div className="d-flex flex-column mb-5">
                  <div className="me-7 mb-4 mt-5">
                    <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
                      <img
                        src={carInformation?.data?.imgUrl}
                        alt="carInformation"
                      />
                    </div>
                    <h3 className="stepper-title mx-auto text-center mt-2">
                      {carInformation?.data?.title}
                    </h3>
                  </div>
                </div>
              </div>
              <div className="px-6 px-lg-10 px-xxl-15 py-5">
                <div className="stepper-nav">
                  <h3 className="stepper-title border-bottom-dotted border-secondary py-5">
                    ข้อมูลรถ
                  </h3>
                  <div className="d-flex flex-row justify-content-between mt-5">
                    <span className="fs-6 fw-bolder mb-1">ทะเบียนรถ</span>
                    <span className="fw-bold text-gray-600"> {carInformation?.data?.vinNo}</span>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">
                      ทะเบียนรถก่อนหน้า
                    </span>
                    <span className="fw-bold text-gray-600"> {carInformation?.data?.beforeVin}</span>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">ยี่ห้อ</span>
                    <span className="fw-bold text-gray-600"> {carInformation?.data?.brand}</span>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">สี</span>
                    <span className="fw-bold text-gray-600"> {carInformation?.data?.color || '-'}</span>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">รุ่น</span>
                    <span className="fw-bold text-gray-600"> {carInformation?.data?.model}</span>
                  </div>
                  {/* <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">สี</span>
                    <span className="fw-bold text-gray-600">ID-45453423</span>
                  </div> */}
                  <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">โฉมปี</span>
                    <span className="fw-bold text-gray-600"> {carInformation?.data?.year}</span>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">วันที่ออกรถ</span>
                    <span className="fw-bold text-gray-600"> {carInformation?.data?.regisDate}</span>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">วันที่จดทะเบียน</span>
                    <span className="fw-bold text-gray-600"> {carInformation?.data?.regisVinDate}</span>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">อายุรถ</span>
                    <span className="fw-bold text-gray-600">
                    {moment().diff(carInformation?.data?.regisDate, 'year')} ปี
                    </span>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">หมายเลขตัวถัง</span>
                    <span className="fw-bold text-gray-600">
                    {carInformation?.data?.currentVin}
                    </span>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">
                      เลขไมล์รถปัจจุบัน
                    </span>
                    <span className="fw-bold text-gray-600"> {new Intl.NumberFormat('en-EN').format(carInformation?.data?.mile|| 0)} Km.</span>
                  </div>
                  {/* <div className="d-flex flex-row justify-content-between">
                    <span className="fs-6 fw-bolder mb-1">ประวัติเครม</span>
                    <span className="fw-bold text-gray-600">8 ครั้ง</span>
                  </div> */}
                </div>
              </div>
            </div>
          </div>
          <div className="px-6 px-lg-10 px-xxl-15 py-5 bg-white">
            <div className="stepper-nav">
              <h3 className="stepper-title border-bottom-dotted border-secondary py-5">
                ค่าใช้จ่ายโดยรวม
              </h3>
              <div className="d-flex flex-row justify-content-between mt-5">
                <span className="fs-6 fw-bolder mb-1">ประกัน</span>
                <span className="fw-bold text-gray-600"> {new Intl.NumberFormat('en-EN').format(carInformation?.insuranceAmount  || 0)}</span>
              </div>
              <div className="d-flex flex-row justify-content-between">
                <span className="fs-6 fw-bolder mb-1">พรบ.</span>
                <span className="fw-bold text-gray-600"> {new Intl.NumberFormat('en-EN').format(carInformation?.actAmount || 0)}</span>
              </div>
              <div className="border-bottom-dotted border-secondary mt-4 mb-4" />
              <div className="d-flex flex-row justify-content-between">
                <span className="fs-6 fw-bolder mb-1">รวมทั้งหมด</span>
                <span className="text-danger fw-bold"> { new Intl.NumberFormat('en-EN').format(carInformation?.total || 0)} บาท</span>
              </div>
              {/* <div className="d-flex flex-row justify-content-between">
                <span className="fs-6 fw-bolder mb-1">เฉลี่ยต่อวัน</span>
                <span className="fw-bold text-gray-600">1020 บาท</span>
              </div> */}
            </div>
          </div>
        </div>
        <div className="col-xl-8">
          <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k: any) => handleSelectTab(k)}
            className="mb-3 nav nav-tabs nav-line-tabs mb-5 fs-5"
          >
            <Tab eventKey="order" title="รายการใช้รถยนต์">
              <div className={'card'}>
                <div className="card-body py-3">
                  {/* begin::Table container */}
                  <div className="table-responsive">
                    {/* begin::Table */}
                    <table className="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                      {/* begin::Table head */}
                      <thead>
                      <tr className="fw-bolder text-muted">
                        <th className="min-w-25px text-capitalize">No</th>
                        <th className="min-w-100px text-capitalize">CAR</th>
                        <th className="min-w-100px text-capitalize">DATE</th>
                        <th className="min-w-100px text-capitalize">AMOUNT</th>
                        <th className="min-w-100px text-capitalize">STATUS</th>
                      </tr>
                      </thead>
                      {/* end::Table head */}
                      {/* begin::Table body */}
                      <tbody>
                      {dataCars &&
                          dataCars.map((value, index) => (
                              <tr key={`order_car_detail_${index}`}>
                                <td>
                                  <span className="text-muted fw-bold text-muted d-block fs-7">
                                    #{new Date().getTime()}{value?.id}
                                  </span>
                                </td>
                                <td>
                                    <span className="text-muted fw-bold text-muted d-block fs-7">
                                       {value?.cars?.title}
                                </span>
                                </td>
                                <td>
                                    <span className="text-muted fw-bold text-muted d-block fs-7">
                                      {value?.createdDate}

                                      </span>
                                </td>
                                <td>
                                    <span className="text-muted fw-bold text-muted d-block fs-7">
                                        {value?.paymentAmount}
                                      </span>
                                </td>
                                <td>
                                     <span className="text-muted fw-bold text-muted d-block fs-7">
                                      {value?.status}
                                 </span>
                                </td>
                                {/*<td>*/}
                                {/*  <div className="d-flex justify-content-end flex-shrink-0">*/}
                                {/*    <Link*/}
                                {/*        to={"/category/cardetail-page"}*/}
                                {/*        className="menu-link px-5 text-primary"*/}
                                {/*    >*/}
                                {/*      ...*/}
                                {/*    </Link>*/}
                                {/*  </div>*/}
                                {/*</td>*/}
                              </tr>
                          ))}
                      </tbody>
                      {/* end::Table body */}
                    </table>
                    {/* end::Table */}
                    <div className='d-flex justify-content-end mt-2'>
                      <Pagination
                          value={pageCar}
                          firstPageValue={1}
                          onChange={(e)=>{handleChangePageCar(Number(e.target?.value))}}
                          totalPages={totalPageCar}/>
                    </div>
                  </div>
                  {/* end::Table container */}
                </div>
              </div>
            </Tab>
            <Tab eventKey="insurance" title="ประกันรถ">
              <CarInsure
                handleClose={handleClose}
                handleShow={handleShow}
                className={""}
                value={dataInsurance}
                type={"insurance"}
                show={show}
                id={id}
              />
            </Tab>
            <Tab eventKey="act" title="พรบ">
              <CarInsure
                className={""}
                show={showACT}
                handleClose={handleCloseACT}
                handleShow={handleShowACT}
                value={dataACT}
                type={"act"}
                id={id}
              />
            </Tab>
            <Tab eventKey="repair" title="ซ่อมบำรุง">
              <CarFix
                className={""}
                value={dataRepair}
                handleClose={handleCloseRepair}
                handleShow={handleShowRepair}
                show={showRepair}
                id={id}
              />
            </Tab>
            <Tab eventKey="request" title="แจ้งอาการกับคนขับ">
              <DriverAnnoun
                className={""}
                value={dataRequest}
                handleClose={handleCloseRequest}
                handleShow={handleShowRequest}
                show={showRequest}
                id={id}
              />
            </Tab>
          </Tabs>
        </div>
      </div>
    </>
  );
};

export default CarView;
