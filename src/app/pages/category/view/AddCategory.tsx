import { FC } from "react";
import { toAbsoluteUrl } from "../../../../_metronic/helpers";
import { PageTitle } from "../../../../_metronic/layout/core";
import { useForm, Controller } from "react-hook-form";
import { useParams } from "react-router-dom";
import { clsx } from "clsx";
import { createCategoryAPI } from "../../../../api/CategoryApi";
import ImageUploader from "react-images-upload";

const AddCategory: FC = () => {
  let { id } = useParams();
  const { handleSubmit, reset, formState: { errors }, control } = useForm({
    defaultValues: {
      title: '',
      files: null,
      description: '',
      status: '0',
    }
  });

  const onCreate = async(value:any) => {
    try {
      const req:FormData = new FormData();
      req.append('title', value?.title);
      req.append('description', value?.description);
      req.append('status',value?.status);
      req.append('files', value?.files ? value?.files[0] : null)
      const result = await createCategoryAPI({ body: req});

      console.log('result:', result);
      window.location.href = '/category'
    } catch (e:any) {
      console.log('e', e);

    }
  }
  return (
    <>
      <PageTitle breadcrumbs={[]}>Add Category</PageTitle>
      <form onSubmit={handleSubmit(onCreate)}>
      <div className='row g-5 g-xxl-8'>
        <div className='col-xl-4'>
          <div className='mb-5 mb-xxl-8 bg-white'>
            {/* begin::Body */}
            <div className='flex-grow-1'>
              <div className='d-flex justify-content-center align-items-center flex-wrap mb-2'>
                <div className='d-flex flex-column mb-5'>
                  <div className='me-7 mb-4 mt-5'>
                    <Controller
                        name="files"
                        control={control}
                        rules={{
                          required: false,
                        }}
                        render={({field: {value, onChange}}) =>
                            <ImageUploader
                                singleImage={true}
                                withIcon={false}
                                withPreview={true}
                                label=""
                                // buttonText={image? '':'Upload image'}
                                onChange={(e)=>{
                                  onChange(e)
                                }}
                                imgExtension={[".jpg",]}
                                maxFileSize={1048576}
                                fileSizeError=" file size is too large"
                            />}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='d-flex justify-content-center bg-white rounded justify-content-xl-start flex-row-auto'>
            <div className='px-6 px-lg-10 px-xxl-15 py-20'>
              <div className='stepper-nav'>
                <div className='stepper-item current' data-kt-stepper-element='nav'>
                  <div className='stepper-line w-40px'></div>

                  <div className='stepper-label'>
                    <h3 className='stepper-title'>Status</h3>
                    <Controller
                      name="status"
                      control={control}
                      rules={{
                        required: 'title is require',
                      }}
                      render={({ field: { value, onChange } }) =>
                        <select onChange={onChange} value={value}  className="form-select" aria-label="Select example">
                        <option disabled>Open this select menu</option>
                        <option value={'1'}>ACTIVE</option>
                        <option value={'0'}>IN ACTIVE</option>
                      </select>
                      }
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='card col-xl-8'>
          <div className={`bg-white`}>
            {/* begin::Header */}
            <div className='card-header align-items-center border-0 mt-4'>
              <h3 className='card-title align-items-start flex-column'>
                <span className='card-label fw-bolder fs-3 mb-1'>General</span>
              </h3>
              <div
                className='card-toolbar'
                data-bs-toggle='tooltip'
                data-bs-placement='top'
                data-bs-trigger='hover'
                title='Click to add a user'
              >
              </div>
            </div>
            {/* end::Header */}
            {/* begin::Body */}
            <div className='card-body pt-5'>
              <div className='d-flex justify-content-start flex-column'>
                <div className='row mb-6'>
                  <div className='col-lg-12'>
                    <label className='col-form-label required fw-bold fs-6'>Category Name</label>
                    <Controller
                      name="title"
                      control={control}
                      rules={{
                        required: 'title is require',
                      }}
                      render={({ field: { value, onChange } }) =>
                        <input
                          type='text'
                          value={value}
                          onChange={onChange}
                          className={clsx(
                            'form-control form-control-lg form-control-solid',
                            {
                              'is-invalid': errors.title?.message && errors.title?.message,
                            },
                            {
                              'is-valid': errors.title?.message && !errors.title?.message,
                            }
                          )}
                          placeholder='Category Name'
                        />
                      }
                    />
                    {errors.title?.message && (
                      <div className='fv-plugins-message-container text-danger'>
                        <span role='alert'>{errors.title?.message}</span>
                      </div>
                    )}
                  </div>
                </div>
                <div className='row mb-6'>
                  <div className='col-lg-12'>
                    <label className='col-form-label required fw-bold fs-6'>Description</label>
                    <Controller
                      name="description"
                      control={control}
                      rules={{
                        required: false,
                      }}
                      render={({ field: { value, onChange } }) =>
                        <textarea
                          rows={10}
                          value={value}
                          onChange={onChange}
                          className={clsx(
                            'form-control form-control-lg form-control-solid',
                            {
                              'is-invalid': errors.description?.message && errors.description?.message,
                            },
                            {
                              'is-valid': errors.description?.message && !errors.description?.message,
                            }
                          )}
                          placeholder='Category Name'
                        />
                      }
                    />
                    {errors.description?.message && (
                      <div className='fv-plugins-message-container text-danger'>
                        <span role='alert'>{errors.description?.message}</span>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
            {/* end: Card Body */}
          </div>
        </div>
        <div className='d-flex flex-row justify-content-end'>
          <button type='button' className='btn btn-secondary me-3' onClick={() => {reset()}}>
            Cancel
          </button>
          <button type='submit' className='btn btn-primary me-3'>
            Save Change
          </button>
        </div>
      </div>
      </form>
    </>
  )
}

export default AddCategory;
