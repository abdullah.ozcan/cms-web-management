import { FC } from "react";
import { toAbsoluteUrl } from "../../../../_metronic/helpers";
import { PageTitle } from "../../../../_metronic/layout/core";

const CategoryView: FC = () => {
    return (
        <>
            <PageTitle breadcrumbs={[]}>Category Detail</PageTitle>
            <div className='row g-5 g-xxl-8'>
                <div className='col-xl-4'>
                    <div className='mb-5 mb-xxl-8 bg-white'>
                        {/* begin::Body */}
                        <div className='flex-grow-1'>
                            <div className='d-flex justify-content-center align-items-center flex-wrap mb-2'>
                                <div className='d-flex flex-column mb-5'>
                                    <div className='me-7 mb-4 mt-5'>
                                        <div className='symbol symbol-100px symbol-lg-160px symbol-fixed position-relative'>
                                            <img src={toAbsoluteUrl('/media/car/car-5.png')} alt='Metornic' />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='d-flex justify-content-center bg-white rounded justify-content-xl-start flex-row-auto w-100 w-xl-300px w-xxl-400px me-9'>
                        <div className='px-6 px-lg-10 px-xxl-15 py-20'>
                            <div className='stepper-nav'>
                                <div className='stepper-item current' data-kt-stepper-element='nav'>
                                    <div className='stepper-line w-40px'></div>

                                    <div className='stepper-label'>
                                        <h3 className='stepper-title'>Status</h3>
                                        <select className="form-select" aria-label="Select example">
                                            <option>Open this select menu</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='card col-xl-8'>
                    <div className={`bg-white`}>
                        {/* begin::Header */}
                        <div className='card-header align-items-center border-0 mt-4'>
                            <h3 className='card-title align-items-start flex-column'>
                                <span className='card-label fw-bolder fs-3 mb-1'>General</span>
                            </h3>
                            <div
                                className='card-toolbar'
                                data-bs-toggle='tooltip'
                                data-bs-placement='top'
                                data-bs-trigger='hover'
                                title='Click to add a user'
                            >
                            </div>
                        </div>
                        {/* end::Header */}
                        {/* begin::Body */}
                        <div className='card-body pt-5'>
                            <div className='d-flex justify-content-start flex-column'>
                                <div className='row mb-6'>
                                    <div className='col-lg-12'>
                                        <label className='col-form-label required fw-bold fs-6'>Category Name</label>
                                        <div className='row'>
                                            <div className='col-lg-12 fv-row'>
                                                <input
                                                    type='text'
                                                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                                    placeholder='Category Name'
                                                />
                                            </div>
                                            <label className='col-form-label text-muted fw-bold fs-6'>A category name is require</label>
                                        </div>
                                    </div>
                                </div>
                                <div className='row mb-6'>
                                    <div className='col-lg-12'>
                                        <label className='col-form-label required fw-bold fs-6'>Description</label>
                                        <div className='row'>
                                            <div className='col-lg-12 fv-row'>
                                                <textarea
                                                    rows={10}
                                                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                                    placeholder='Description'
                                                />
                                            </div>
                                            <label className='col-form-label text-muted fw-bold fs-6'>A category name is require</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* end: Card Body */}
                    </div>
                </div>
                <div className='d-flex flex-row justify-content-end'>
                    <button type='button' className='btn btn-secondary me-3'>
                        Cancel
                    </button>
                    <button type='button' className='btn btn-primary me-3'>
                        Save Change
                    </button>
                </div>
            </div>
        </>
    )
}

export default CategoryView;