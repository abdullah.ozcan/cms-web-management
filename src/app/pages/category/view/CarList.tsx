import { FC, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { KTSVG } from "../../../../_metronic/helpers";
import { PageTitle } from "../../../../_metronic/layout/core";
import { useParams } from "react-router-dom";
import { searchCarAPI } from "../../../../api/CategoryApi";
import Pagination from "@vlsergey/react-bootstrap-pagination";
import Dropdown from 'react-bootstrap/Dropdown';

type CategoryType = {
    "id": number;
    "title": string;
    "description": string;
    "status": string;
    "isActive": boolean;
    "createdDate": string;
    "updatedDate": string;
    "cars": CarType[]
}

type CarType = {
    "id":number
    "title": string;
    "currentVin": string;
    "beforeVin": string;
    "brand": string;
    "model": string;
    "year": string;
    "regisDate":string;
    "regisVinDate":string;
    "age": number
    "vinNo": string;
    "mile": number
    "fixTimes":number
    "description":string;
    "status": string;
    "isActive": boolean;
    "createdDate": string;
    "updatedDate": string;
    "imageUrl": string;
}

const CarList: FC = () => {

    let { id } = useParams();

    const [value, setValue] = useState<CarType[]>([]);
    const [page, setPage] = useState<number>(1);
    const [pagesize, setPageSize] = useState<number>(10);
    const [totalPage, setTotalPage] = useState<number>(1);
    const [search, setSearch] = useState<string>('');

    const getCarByCategoryId = async (page: number, pageSize: number, key: string) => {
        try {
            const result = await searchCarAPI({
                body: {
                    page,
                    pageSize,
                    keyword: key,
                    id: +Number(id)
                }
            });
            if(result && result?.data) {
                const { content, totalPages } = result?.data;
                console.log('content, totalPages ', content, totalPages )
                setTotalPage(totalPages)
                setValue(content)
            }

        } catch (e: any) {
            console.log('error', e);

        }
    }

    useEffect(() => {
        getCarByCategoryId(page, pagesize, search)
    }, [])

    const handleSearch = async (value:any) => {
        try {
            console.log('value', value);

            setSearch(value)
            if(value !== null  || value !== '' || value !== undefined) {
                await getCarByCategoryId(page, pagesize, value)
            } else {
                await getCarByCategoryId(page, pagesize, '')
            }

        } catch (e: any) {
            console.log('error', e);

        }
    }
    // console.log('result', value);

    const handleChangePage = async (value:number) => {
        setPage(value);
        await getCarByCategoryId(value, pagesize, search);
    }
    return (
        <>
            <PageTitle breadcrumbs={[]}>Home - Car Management - Product list</PageTitle>
            <div className={`card mb-5 mb-xxl-8`}>
                <div className='card-header border-0 pt-6'>
                    <div className='card-title'>
                        {/* begin::Search */}
                        <div className='d-flex align-items-center position-relative my-1'>
                            <KTSVG
                                path='/media/icons/duotune/general/gen021.svg'
                                className='svg-icon-1 position-absolute ms-6'
                            />
                            <input
                                type='text'
                                data-kt-user-table-filter='search'
                                className='form-control form-control-solid w-250px ps-14'
                                placeholder='Search Product'
                                value={search}
                                onChange={(e) => {handleSearch(e.target.value)}}
                            />
                        </div>
                        {/* end::Search */}
                    </div>
                    {/* begin::Card toolbar */}
                    <div className='card-toolbar'>
                        <div className='d-flex justify-content-end w-100' data-kt-user-table-toolbar='base'>
                            <select className="form-select h-25 w-50" aria-label="Select example">
                                <option>Select Status</option>
                                <option value="1">Active</option>
                                <option value="2">InActive</option>
                            </select>
                            <button type="button"
                                className="btn btn-primary mx-1"
                            // data-bs-toggle="modal"
                            // data-bs-target="#kt_modal_1"
                            >
                                <Link to={`/category/car/detail/${id}`} className='text-white fw-bolder text-hover-white fs-6'>
                                    ADD Product
                                </Link>
                            </button>
                        </div>
                        <div className="modal fade" tabIndex={-1} id="kt_modal_1">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title">Add Agent</h5>
                                        <div
                                            className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                            data-bs-dismiss="modal"
                                            aria-label="Close"
                                        >
                                            <KTSVG
                                                path="/media/icons/duotune/arrows/arr061.svg"
                                                className="svg-icon svg-icon-2x"
                                            />
                                        </div>
                                    </div>
                                    <div className="modal-body">
                                        <div className='row mb-4'>
                                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                                <span className='required'>Name</span>
                                            </label>
                                            <div className='col-lg-12 fv-row'>
                                                <input
                                                    type='text'
                                                    className='form-control form-control-lg form-control-solid'
                                                    placeholder='Name'
                                                />
                                            </div>
                                        </div>
                                        <div className='row mb-12'>
                                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                                <span className='required'>ID Type</span>
                                            </label>
                                            <div className='col-lg-12 fv-row'>
                                                <input
                                                    type='tel'
                                                    className='form-control form-control-lg form-control-solid'
                                                    placeholder='ID Type'
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button
                                            type="button"
                                            className="btn btn-light"
                                            data-bs-dismiss="modal"
                                        >
                                            Discard
                                        </button>
                                        <button type="button" className="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* end::Card toolbar */}
                </div>
                {/* begin::Body */}
                <div className='card-body py-3'>
                    {/* begin::Table container */}
                    <div className='table-responsive'>
                        {/* begin::Table */}
                        <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
                            {/* begin::Table head */}
                            <thead>
                                <tr className='fw-bolder text-muted'>
                                    <th className='w-25px'>Pic</th>
                                    <th className='min-w-250px'>Plate</th>
                                    <th className='min-w-25px'>Calendar</th>
                                    <th className='min-w-50px'>Status</th>
                                    <th className='min-w-50px'>Actions</th>
                                </tr>
                            </thead>
                            {/* end::Table head */}
                            {/* begin::Table body */}
                            <tbody>
                                {
                                    value && value.map((value, index) => (
                                        <tr key={`user_role_detail_list${index}`}>
                                            <td>
                                                <div className='d-flex align-items-center'>
                                                    <div className='symbol symbol-45px me-5'>
                                                        <img src={value?.imageUrl} alt='' />
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                                <Link to='/category/car/view/' className='text-dark fw-bolder text-hover-primary fs-6'>
                                                    {value?.vinNo || '-'}
                                                </Link>
                                            </td>
                                            <td>
                                                <span className='text-dark fw-bolder text-hover-primary fs-6'>
                                                    {'-'}
                                                </span>
                                            </td>
                                            <td>
                                                <span className='text-primary fw-bolder text-hover-primary fs-6'>
                                                   {value?.isActive ? 'Published' : 'Un Published' }
                                                </span>
                                            </td>
                                            <td>
                                                <div className='d-flex justify-content-start flex-shrink-0'>
                                                    <Dropdown>
                                                        <Dropdown.Toggle variant="secondary" id="dropdown-basic" size={'sm'}>
                                                            Action
                                                        </Dropdown.Toggle>
                                                        <Dropdown.Menu>
                                                            <Dropdown.Item href={`/category/car/view/${value?.id}`}>View</Dropdown.Item>
                                                            <Dropdown.Item href={`/category/car/update/${value?.id}`}>Edit</Dropdown.Item>
                                                        </Dropdown.Menu>
                                                    </Dropdown>
                                                </div>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                            {/* end::Table body */}
                        </table>
                        {/* end::Table */}
                    </div>
                    {/* end::Table container */}
                    <div className='d-flex justify-content-end mt-2 mb-5 me-7'>
                        <Pagination
                            value={page}
                            firstPageValue={1}
                            onChange={(e) => {handleChangePage(Number(e.target?.value))}}
                            totalPages={totalPage}/>
                    </div>
                </div>
                {/* begin::Body */}
            </div>
        </>
    )
}

export default CarList;
