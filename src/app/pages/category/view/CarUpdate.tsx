import { clsx } from "clsx";
import {FC, useEffect, useState} from "react";
import { useForm, Controller } from "react-hook-form";
import { useParams } from "react-router-dom";
import {createCaryAPI, getCarViewAPI, updateCarAPI} from "../../../../api/CategoryApi";
import { PageTitle } from "../../../../_metronic/layout/core";
import ImageUploader from "react-images-upload";


type CarViewType = {
    "insuranceAmount":number;
    "actAmount": number;
    "total": number;
    "data": {
        "id": number;
        "title": string;
        "currentVin":  string;
        "beforeVin":  string;
        "brand":  string;
        "model":  string;
        "year":  string;
        "regisDate": string;
        "regisVinDate": string;
        "age": number;
        "vinNo":  string;
        "mile": number;
        "fixTimes":number;
        "description":  string;
        "status":  string;
        "isActive": boolean;
        "createdDate":  string;
        "updatedDate":  string;
        "imgUrl": string;
    }
}
const CarUpdate: FC = () => {
    let { id } = useParams();
    const { handleSubmit,setValue, getValues,  reset, formState: { errors }, control } = useForm({
        defaultValues: {
            id:'',
            currentVin: '',
            beforeVin: '',
            description: '',
            status: '1',
            brand: '',
            model: '',
            year: '',
            regisDate: '',
            regisVinDate: '',
            categoryId: 0,
            mile: 0,
            fixTimes: 0,
            vinNo: '',
            title: '',
            files: '',
            color: '',
            imgUrl: '',
            category: {
                "id": '',
                "title": '',
                "description": '',
                "status": '',
                "isActive": '',
                "createdDate": '',
                "updatedDate": '',
            }
        }
    });
    const [carInformation, setCarInformation] = useState<CarViewType | null>(null);

    const onUpdate = async(value:any) => {
        try {
            console.log('value:', value)
            const req: FormData = new FormData();
            req.append('id', value?.id)
            req.append('beforeVin', value?.beforeVin)
            req.append('brand', value?.brand)
            req.append('category', value?.category)
            req.append('currentVin', value?.currentVin)
            req.append('files',  value?.files ? value?.files[0]: '')
            req.append('description', value?.beforeVin)
            req.append('fixTimes', value?.fixTimes)
            req.append('mile', value?.mile)
            req.append('model', value?.model)
            req.append('regisDate', value?.regisDate)
            req.append('regisVinDate', value?.regisVinDate)
            req.append('status', value?.status)
            req.append('title', value?.title)
            req.append('vinNo', value?.vinNo)
            req.append('year', value?.year)
            req.append('categoryId', value?.categoryId)
            req.append('color', value?.color)

            await updateCarAPI({body: req})
                .then(() => {
                    window.location.href = `/category/car/${value?.categoryId}`
                })
                .catch(() => {
                    window.location.href = `/category/car/${value?.categoryId}`
                })
        } catch (e:any) {
            console.log('e', e);

        }
    }

    const getCarInformation = async () => {
        try {
            const result = await getCarViewAPI({id: id || ''});
            if(result?.data) {
                setCarInformation(result?.data)

                const { data } = result?.data;

                console.log('data', data)
                setValue('id', data?.id)
                setValue('title', data?.title)
                setValue('currentVin', data?.currentVin)
                setValue('categoryId', data?.categoryId)
                setValue('beforeVin', data?.beforeVin)

                setValue('brand', data?.brand)
                setValue('model', data?.model)
                setValue('year', data?.year)

                setValue('regisDate', data?.regisDate)
                setValue('regisVinDate', data?.regisVinDate)
                setValue('vinNo', data?.vinNo)

                setValue('mile', data?.mile)
                setValue('fixTimes', data?.fixTimes)
                setValue('description', data?.description)

                setValue('status', data?.status ? '1' : '0')
                // setValue('isActive', data?.isActive)
                setValue('imgUrl', data?.imgUrl)
            }
        } catch(e:any) {
            console.log('e', e);

        }
    }

    useEffect(()=> {
        getCarInformation()
    }, [])

    return (
        <>
            <PageTitle breadcrumbs={[]}>Category Detail</PageTitle>
            <form onSubmit={handleSubmit(onUpdate)}>
                <div className='row g-5 g-xxl-8'>
                    <div className='col-xl-4'>
                        <div className='mb-5 mb-xxl-8 bg-white'>
                            {/* begin::Body */}
                            <div className='flex-grow-1'>
                                <div className='d-flex justify-content-center align-items-center flex-wrap mb-2'>
                                    <div className='d-flex flex-column mb-5'>
                                        <div className='me-7 mb-4 mt-5'>
                                            <Controller
                                                name="files"
                                                control={control}
                                                rules={{
                                                    required: false,
                                                }}
                                                render={({field: {value, onChange}}) =>
                                                    <ImageUploader
                                                        singleImage={true}
                                                        withIcon={false}
                                                        withPreview={true}
                                                        label=""
                                                        defaultImage={getValues('imgUrl')}
                                                        onChange={onChange}
                                                        imgExtension={[".jpg",]}
                                                        maxFileSize={1048576}
                                                        fileSizeError=" file size is too big"
                                                    />}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='d-flex justify-content-center bg-white rounded justify-content-xl-start flex-row-auto w-100 me-9'>
                            <div className='px-6 px-lg-10 px-xxl-15 py-20'>
                                <div className='stepper-nav'>
                                    <div className='stepper-item current' data-kt-stepper-element='nav'>
                                        <div className='stepper-line w-40px'></div>

                                        <div className='stepper-label'>
                                            <h3 className='stepper-title'>Status</h3>
                                            <Controller
                                                name="status"
                                                control={control}
                                                rules={{
                                                    required: false,
                                                }}
                                                render={({ field: { value, onChange } }) =>
                                                    <select onChange={onChange} value={value} className="form-select" aria-label="Select example">
                                                        <option disabled>Open this select menu</option>
                                                        <option value={'1'}>ACTIVE</option>
                                                        <option value={'0'}>IN ACTIVE</option>
                                                    </select>
                                                }
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-xl-8'>
                        <div className='row'>
                            <div className={`col-lg-12 bg-white`}>
                                {/* begin::Header */}
                                <div className='card-header align-items-center border-0 mt-4'>
                                    <h3 className='card-title align-items-start flex-column'>
                                        <span className='card-label fw-bolder fs-3 mb-1'>General</span>
                                    </h3>
                                    <div
                                        className='card-toolbar'
                                        data-bs-toggle='tooltip'
                                        data-bs-placement='top'
                                        data-bs-trigger='hover'
                                        title='Click to add a user'
                                    >
                                    </div>
                                </div>
                                {/* end::Header */}
                                {/* begin::Body */}
                                <div className='card-body pt-5'>
                                    <div className='d-flex justify-content-start flex-column'>
                                        <div className='row mb-6'>
                                            <div className='col-lg-12'>
                                                <label className='col-form-label required fw-bold fs-6'>Car name</label>
                                                <Controller
                                                    name="title"
                                                    control={control}
                                                    rules={{
                                                        required: 'Car name is require',
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='text'
                                                            maxLength={50}
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                                {
                                                                    'is-invalid': errors.title?.message && errors.title?.message,
                                                                },
                                                                {
                                                                    'is-valid': errors.title?.message && !errors.title?.message,
                                                                }
                                                            )}
                                                            placeholder='ชื่อ'
                                                        />
                                                    }
                                                />
                                                {errors.title?.message && (
                                                    <div className='fv-plugins-message-container text-danger'>
                                                        <span role='alert'>{errors.title?.message}</span>
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                        <div className='row mb-6'>
                                            <div className='col-lg-6'>
                                                <label className='col-form-label required fw-bold fs-6'>วันที่ออกรถ</label>
                                                <Controller
                                                    name="regisDate"
                                                    control={control}
                                                    rules={{
                                                        required: 'field is require',
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='date'
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                                {
                                                                    'is-invalid': errors.regisDate?.message && errors.regisDate?.message,
                                                                },
                                                                {
                                                                    'is-valid': errors.regisDate?.message && !errors.regisDate?.message,
                                                                }
                                                            )}
                                                            placeholder='วันที่ออกรถ'
                                                        />
                                                    }
                                                />
                                                {errors.regisDate?.message && (
                                                    <div className='fv-plugins-message-container text-danger'>
                                                        <span role='alert'>{errors.regisDate?.message}</span>
                                                    </div>
                                                )}
                                            </div>
                                            <div className='col-lg-6'>
                                                <label className='col-form-label required fw-bold fs-6'>วันที่จดทะเบียน</label>
                                                <Controller
                                                    name="regisVinDate"
                                                    control={control}
                                                    rules={{
                                                        required: 'field is require',
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='date'
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                                {
                                                                    'is-invalid': errors.regisVinDate?.message && errors.regisVinDate?.message,
                                                                },
                                                                {
                                                                    'is-valid': errors.regisVinDate?.message && !errors.regisVinDate?.message,
                                                                }
                                                            )}
                                                            placeholder='วันที่จดทะเบียน'
                                                        />
                                                    }
                                                />
                                                {errors.regisVinDate?.message && (
                                                    <div className='fv-plugins-message-container text-danger'>
                                                        <span role='alert'>{errors.regisVinDate?.message}</span>
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                        <div className='row mb-6'>
                                            <div className='col-lg-6'>
                                                <label className='col-form-label required fw-bold fs-6'>ทะเบียนรถ</label>
                                                <Controller
                                                    name="vinNo"
                                                    control={control}
                                                    rules={{
                                                        required: 'field is require',
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='text'
                                                            maxLength={50}
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                                {
                                                                    'is-invalid': errors.vinNo?.message && errors.vinNo?.message,
                                                                },
                                                                {
                                                                    'is-valid': errors.vinNo?.message && !errors.vinNo?.message,
                                                                }
                                                            )}
                                                            placeholder='ทะเบียนรถ'
                                                        />
                                                    }
                                                />
                                                {errors.vinNo?.message && (
                                                    <div className='fv-plugins-message-container text-danger'>
                                                        <span role='alert'>{errors.vinNo?.message}</span>
                                                    </div>
                                                )}
                                            </div>
                                            <div className='col-lg-6'>
                                                <label className='col-form-label required fw-bold fs-6'>ทะเบียนรถก่อนหน้า</label>
                                                <Controller
                                                    name="beforeVin"
                                                    control={control}
                                                    rules={{
                                                        required: 'field is require',
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='text'
                                                            value={value}
                                                            maxLength={50}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                                {
                                                                    'is-invalid': errors.beforeVin?.message && errors.beforeVin?.message,
                                                                },
                                                                {
                                                                    'is-valid': errors.beforeVin?.message && !errors.beforeVin?.message,
                                                                }
                                                            )}
                                                            placeholder='ทะเบียนรถก่อนหน้า'
                                                        />
                                                    }
                                                />
                                                {errors.beforeVin?.message && (
                                                    <div className='fv-plugins-message-container text-danger'>
                                                        <span role='alert'>{errors.beforeVin?.message}</span>
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                        <div className='row mb-6'>
                                            <div className='col-lg-6'>
                                                <label className='col-form-label required fw-bold fs-6'>ยี่ห้อ</label>
                                                <Controller
                                                    name="brand"
                                                    control={control}
                                                    rules={{
                                                        required: 'field is require',
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='text'
                                                            maxLength={50}
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                                {
                                                                    'is-invalid': errors.brand?.message && errors.brand?.message,
                                                                },
                                                                {
                                                                    'is-valid': errors.brand?.message && !errors.brand?.message,
                                                                }
                                                            )}
                                                            placeholder='brand'
                                                        />
                                                    }
                                                />
                                                {errors.brand?.message && (
                                                    <div className='fv-plugins-message-container text-danger'>
                                                        <span role='alert'>{errors.brand?.message}</span>
                                                    </div>
                                                )}
                                            </div>
                                            <div className='col-lg-6'>
                                                <label className='col-form-label required fw-bold fs-6'>รุ่น</label>
                                                <Controller
                                                    name="model"
                                                    control={control}
                                                    rules={{
                                                        required: 'model is require',
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='text'
                                                            maxLength={50}
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                                {
                                                                    'is-invalid': errors.model?.message && errors.model?.message,
                                                                },
                                                                {
                                                                    'is-valid': errors.model?.message && !errors.model?.message,
                                                                }
                                                            )}
                                                            placeholder='model'
                                                        />
                                                    }
                                                />
                                                {errors.model?.message && (
                                                    <div className='fv-plugins-message-container text-danger'>
                                                        <span role='alert'>{errors.model?.message}</span>
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                        <div className='row mb-6'>
                                            <div className='col-lg-6'>
                                                <label className='col-form-label required fw-bold fs-6'>สี</label>
                                                <Controller
                                                    name="color"
                                                    control={control}
                                                    rules={{
                                                        required: 'field is require',
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='text'
                                                            maxLength={50}
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                                {
                                                                    'is-invalid': errors.color?.message && errors.color?.message,
                                                                },
                                                                {
                                                                    'is-valid': errors.color?.message && !errors.color?.message,
                                                                }
                                                            )}
                                                            placeholder='color'
                                                        />
                                                    }
                                                />
                                                {errors.color?.message && (
                                                    <div className='fv-plugins-message-container text-danger'>
                                                        <span role='alert'>{errors.color?.message}</span>
                                                    </div>
                                                )}
                                            </div>
                                            <div className='col-lg-6'>
                                                <label className='col-form-label required fw-bold fs-6'>โฉมปี</label>
                                                <Controller
                                                    name="year"
                                                    control={control}
                                                    rules={{
                                                        required: 'field is require',
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='text'
                                                            maxLength={50}
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                                {
                                                                    'is-invalid': errors.year?.message && errors.year?.message,
                                                                },
                                                                {
                                                                    'is-valid': errors.year?.message && !errors.year?.message,
                                                                }
                                                            )}
                                                            placeholder='year'
                                                        />
                                                    }
                                                />
                                                {errors.year?.message && (
                                                    <div className='fv-plugins-message-container text-danger'>
                                                        <span role='alert'>{errors.year?.message}</span>
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                        <div className='row mb-6'>
                                            <div className='col-lg-6'>
                                                <label className='col-form-label required fw-bold fs-6'>หมายเลขตัวถัง</label>
                                                <Controller
                                                    name="currentVin"
                                                    control={control}
                                                    rules={{
                                                        required: 'field is require',
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='text'
                                                            maxLength={50}
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                                {
                                                                    'is-invalid': errors.currentVin?.message && errors.currentVin?.message,
                                                                },
                                                                {
                                                                    'is-valid': errors.currentVin?.message && !errors.currentVin?.message,
                                                                }
                                                            )}
                                                            placeholder='หมายเลขตัวถัง'
                                                        />
                                                    }
                                                />
                                                {errors.currentVin?.message && (
                                                    <div className='fv-plugins-message-container text-danger'>
                                                        <span role='alert'>{errors.currentVin?.message}</span>
                                                    </div>
                                                )}
                                            </div>
                                            <div className='col-lg-6'>
                                                <label className='col-form-label fw-bold fs-6'>ระยะไมค์</label>
                                                <Controller
                                                    name="mile"
                                                    control={control}
                                                    rules={{
                                                        required: false,
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='number'
                                                            defaultValue={0}
                                                            disabled={true}
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                            )}
                                                            placeholder='ระยะไมค์'
                                                        />
                                                    }
                                                />
                                            </div>
                                        </div>
                                        <div className='row mb-6'>
                                            <div className='col-lg-6'>
                                                <label className='col-form-label fw-bold fs-6'>จำนวนครั้งที่ซ่อม</label>
                                                <Controller
                                                    name="fixTimes"
                                                    control={control}
                                                    rules={{
                                                        required: false,
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <input
                                                            type='number'
                                                            defaultValue={0}
                                                            maxLength={50}
                                                            disabled={true}
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                            )}
                                                            placeholder='จำนวนครั้งที่ซ่อม'
                                                        />
                                                    }
                                                />
                                            </div>
                                        </div>
                                        <div className='row mb-6'>
                                            <div className='col-lg-12'>
                                                <label className='col-form-label required fw-bold fs-6'>Description</label>
                                                <Controller
                                                    name="description"
                                                    control={control}
                                                    rules={{
                                                        required: false,
                                                    }}
                                                    render={({ field: { value, onChange } }) =>
                                                        <textarea
                                                            rows={4}
                                                            maxLength={200}
                                                            value={value}
                                                            onChange={onChange}
                                                            className={clsx(
                                                                'form-control form-control-lg form-control-solid',
                                                            )}
                                                            placeholder='Description'
                                                        />
                                                    }
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* end: Card Body */}
                            </div>
                        </div>
                    </div>
                    <div className='d-flex flex-row justify-content-end'>
                        <button type='button' className='btn btn-secondary me-3'>
                            Cancel
                        </button>
                        <button type='submit' className='btn btn-primary me-3'>
                            Save Change
                        </button>
                    </div>
                </div>
            </form>
        </>
    )
}

export default CarUpdate;
