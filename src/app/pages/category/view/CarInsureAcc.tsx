import { FC, useState } from "react";
import { Link } from "react-router-dom";
import { KTSVG } from "../../../../_metronic/helpers";
import ModalApp from "../../../../_metronic/layout/components/ModalApp";

type Props = {
    className: string
}
type PaymentType = {
    id: string;
    invoice: string;
    amount: string;
    status?: string;
    date: string;
    action: string;
}
const CarInsureAcc: FC<Props> = ({ className }) => {
    const paymentList: PaymentType[] = [
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
    ]
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const ModalCarCheck = () => {
        return (
            <div className=''>
                <div className='row mb-4'>
                    <div className='col-lg-6 fv-row'>
                        <label className='col-form-label fw-bold fs-6'>
                            <span className='required'>วันที่เช็ค</span>
                        </label>
                        <input
                            type='text'
                            className='form-control form-control-lg form-control-solid'
                            placeholder=''
                        />
                    </div>
                </div>
                <div className='row mb-12'>
                    <div className='col-lg-6 fv-row'>
                        <label className='col-lg-4 col-form-label fw-bold fs-6'>
                            <span className='required'>ไมค์เช็คระยะล่าสุด</span>
                        </label>
                        <input
                            type='text'
                            className='form-control form-control-lg form-control-solid'
                            placeholder=''
                        />
                    </div>
                    <div className='col-lg-6 fv-row'>
                        <label className='col-lg-4 col-form-label fw-bold fs-6'>
                            <span className='required'>กำหนดครั้งต่อไป</span>
                        </label>
                        <input
                            type='text'
                            className='form-control form-control-lg form-control-solid'
                            placeholder=''
                        />
                    </div>
                </div>
                <div className='row mb-12'>
                    <div className='col-lg-6 fv-row'>
                        <label className='col-lg-4 col-form-label fw-bold fs-6'>
                            <span className='required'>เลขปัจจุบัน</span>
                        </label>
                        <input
                            type='text'
                            className='form-control form-control-lg form-control-solid'
                            placeholder=''
                        />
                    </div>
                    <div className='col-lg-6 fv-row'>
                        <label className='col-lg-4 col-form-label fw-bold fs-6'>
                            <span className='required'>คงเหลือ</span>
                        </label>
                        <input
                            type='text'
                            className='form-control form-control-lg form-control-solid'
                            placeholder=''
                        />
                    </div>
                </div>
                <div className='row mb-12'>
                    <div className='col-lg-12 fv-row'>
                        <label className='col-lg-4 col-form-label fw-bold fs-6'>
                            <span className=''>หมายเหตุ</span>
                        </label>
                        <textarea
                            rows={4}
                            className='form-control form-control-lg form-control-solid'
                            placeholder=''
                        />
                    </div>
                </div>
            </div>
        )
    }
    return (
        <div className={`card ${className}`}>
            {/* begin::Header */}
            <div className='card-header border-0 pt-5'>
                <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder fs-3 mb-1'>พรบ.</span>
                </h3>
                <div
                    className='card-toolbar'
                    data-bs-toggle='tooltip'
                    data-bs-placement='top'
                    data-bs-trigger='hover'
                    title='Click to add a user'
                >
                   <button type='button'
                        className='btn btn-primary mx-1'
                        onClick={handleShow}
                    >
                        Add
                    </button>
                    <ModalApp
                        title="Car Check"
                        show={show}
                        handleClose={handleClose}
                        handleOpen={handleShow}
                        chil={<ModalCarCheck/>}
                    />
                </div>
            </div>
            {/* end::Header */}
            {/* begin::Body */}
            <div className='card-body py-3'>
                {/* begin::Table container */}
                <div className='table-responsive'>
                    {/* begin::Table */}
                    <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
                        {/* begin::Table head */}
                        <thead>
                            <tr className='fw-bolder text-muted'>
                                <th className='min-w-25px text-capitalize'>No</th>
                                <th className='min-w-140px text-capitalize'>DATE REGISTER</th>
                                <th className='min-w-120px text-capitalize'>INSURANCE</th>
                                <th className='min-w-120px text-capitalize'>AMOUNT</th>
                                <th className='min-w-120px text-capitalize'>EXPIRE</th>
                                <th className='min-w-120px text-capitalize'>BROKER</th>
                                <th className='min-w-100px text-end text-capitalize'>NOTE</th>
                            </tr>
                        </thead>
                        {/* end::Table head */}
                        {/* begin::Table body */}
                        <tbody>
                            {
                                paymentList && paymentList.map(((value, index) =>
                                    <tr key={`payment_detail_${index}`}>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                {++index}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                03/08/20
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                กรุงเทพประกันภัย
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                50,000 บาท
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                03/08/20
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                บริษัท บานาน่า อินชัวร์รัน
                                            </span>
                                        </td>
                                        <td>
                                            <div className='d-flex justify-content-end flex-shrink-0'>
                                                <Link to={'/category/cardetail-page'} className='menu-link px-5 text-primary'>
                                                    ...
                                                </Link>
                                            </div>
                                        </td>
                                    </tr>))
                            }
                        </tbody>
                        {/* end::Table body */}
                    </table>
                    {/* end::Table */}
                </div>
                {/* end::Table container */}
            </div>
            {/* begin::Body */}
        </div>
    )
}

export default CarInsureAcc;