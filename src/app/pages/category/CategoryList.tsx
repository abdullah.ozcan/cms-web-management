import { FC, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { searchCategoryAPI } from "../../../api/CategoryApi";
import { KTSVG, toAbsoluteUrl } from "../../../_metronic/helpers";

type UserRoleType = {
    id: string;
    user: string;
    image?: string;
    phone?: string;
    age?: string;
    bookbank?: string;
    birthdate?: string;
    dateAmount?: string;
    joinDate: string;
    des?: string;
    action: string;
}
type CategoryType = {
    "id": number;
    "title":string;
    "description":string;
    "status": string;
    "isActive": boolean;
    "createdDate": string;
    "updatedDate": string;
    "imgUrl": string;
}
const CategoryList: FC = () => {
    const [page, setPage] = useState<number>(0)
    const [pageSize, setPageSize] = useState<number>(10)
    const [search, setSearch] = useState<string>('')

    const [list, setList ] = useState<CategoryType[] | null>([]);


    const handleGetList = async(page: number, pageSize: number, keyword:string) => {
        try {
            const result = await searchCategoryAPI({
                body: {
                    page,
                    pageSize,
                    keyword,
                }
            });

            if(result && result?.data) {
                const { data } = result;
                setList(data)
            }

        } catch (e: any) {
            console.log('e', e);

        }
    }

    const numberWithCommas = (x : any) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    const handleSearch = async(value:any) => {
        try {
            setSearch(value)
            await handleGetList(page, pageSize, value);
        } catch (e: any) {
            console.log('e', e);
        }
    }

    useEffect(() => {
        handleGetList(page, pageSize, search);
    }, [])
    return (
        <>
          <div className='card-header border-0 pt-6'>
            <div className='card-title'>
                {/* begin::Search */}
                <div className='d-flex align-items-center position-relative my-1'>
                    <KTSVG
                        path='/media/icons/duotune/general/gen021.svg'
                        className='svg-icon-1 position-absolute ms-6'
                    />
                    <input
                        type='text'
                        data-kt-user-table-filter='search'
                        className='form-control form-control-solid w-250px ps-14'
                        placeholder='Search'
                        value={search}
                        onChange={(e) => handleSearch(e.target.value)}
                    />
                </div>
                {/* end::Search */}
            </div>
            {/* begin::Card toolbar */}
            <div className='card-toolbar'>
                {/* begin::Group actions */}
                {/* {selected.length > 0 ? <UsersListGrouping /> : <UsersListToolbar />} */}
                <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
                    {/* begin::Add user */}
                    <button className='btn'>
                        <Link to='/category/add/' className='text-white btn btn-primary'>
                            <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
                            Add Category
                        </Link>
                    </button>
                    {/* end::Add user */}
                </div>
                {/* end::Group actions */}
            </div>
            {/* end::Card toolbar */}
        </div>
        < div className='card-body py-3' >
            {/* begin::Table container */}
            < div className='table-responsive' >
                {/* begin::Table */}
                < table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4' >
                    {/* begin::Table head */}
                    < thead >
                        <tr className='text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0'>
                            {/* <th className='w-25px'>
                                <div className='form-check form-check-sm form-check-custom form-check-solid'>
                                    <input
                                        className='form-check-input'
                                        type='checkbox'
                                        value='1'
                                        data-kt-check='true'
                                        data-kt-check-target='.widget-13-check'
                                    />
                                </div>
                            </th> */}
                            <th className='min-w-120px'>Category</th>
                            <th className='min-w-50px'>Quality</th>
                            <th className='min-w-50px'>Actions</th>
                        </tr>
                    </thead >
                    {/* end::Table head */}
                    {/* begin::Table body */}
                    <tbody className="fw-bold text-gray-600">
                        {
                            list && list.map((value, index) => (
                                <tr key={`user_role_detail_list${index}`}>
                                    {/* <td>
                                        <div className='form-check form-check-sm form-check-custom form-check-solid'>
                                            <input className='form-check-input widget-13-check' type='checkbox' value='1' />
                                        </div>
                                    </td> */}
                                    <td>
                                        <div className='d-flex align-items-center'>
                                            <div className='symbol symbol-45px me-5'>
                                                <span className="symbol-label bg-light overflow-hidden">
                                                    {
                                                        value?.imgUrl ?
                                                            <img
                                                                src={value?.imgUrl}
                                                                className='h-100 w-100 align-self-end'
                                                                alt=''
                                                            />
                                                            :
                                                            <img
                                                                src={toAbsoluteUrl('/media/car/car-1.png')}
                                                                className='h-100 w-100 align-self-end'
                                                                alt=''
                                                            />
                                                    }
                                                </span>
                                            </div>
                                            <div className='d-flex justify-content-start flex-column'>

                                                <Link to={`/category/car/${value?.id}`} className='text-primary fs-4 me-1'>
                                                    {value.title}
                                                </Link>
                                                <span className='text-muted fw-bold text-muted d-block fs-6'>
                                                    {value.description}
                                                </span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <span className='   text-dark fw-bolder text-hover-primary d-block mb-1 fs-6'>
                                            {numberWithCommas(0)}
                                        </span>
                                    </td>
                                    <td>
                                        <Link to={`/category/car/${value?.id}`} className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2'>
                                            VIEW
                                        </Link>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                    {/* end::Table body */}
                </table >
                {/* end::Table */}
            </div >
            {/* end::Table container */}
        </div >
        </>

    );
}

export default CategoryList;
