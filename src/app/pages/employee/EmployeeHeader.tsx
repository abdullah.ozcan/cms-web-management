import { FC, useState } from "react";
import { Link } from "react-router-dom";
import { KTSVG } from "../../../_metronic/helpers";

const EmployeeHeader: FC = () => {
    const [searchTerm, setSearchTerm] = useState('');

    const [role, setRole] = useState<string | undefined>()
    const [lastLogin, setLastLogin] = useState<string | undefined>()


    const resetData = () => {
        //   updateState({filter: undefined, ...initialQueryState})
    }

    const filterData = () => {
        //   updateState({
        //     filter: {role, last_login: lastLogin},
        //     ...initialQueryState,
        //   })
    }
    
    return (
        <div className='card-header border-0 pt-6'>
            <div className='card-title'>
                {/* begin::Search */}
                <div className='d-flex align-items-center position-relative my-1'>
                    <KTSVG
                        path='/media/icons/duotune/general/gen021.svg'
                        className='svg-icon-1 position-absolute ms-6'
                    />
                    <input
                        type='text'
                        data-kt-user-table-filter='search'
                        className='form-control form-control-solid w-250px ps-14'
                        placeholder='Search user'
                        value={searchTerm}
                        onChange={(e) => setSearchTerm(e.target.value)}
                    />
                </div>
                {/* end::Search */}
            </div>
            {/* begin::Card toolbar */}
            <div className='card-toolbar'>
                {/* begin::Group actions */}
                {/* {selected.length > 0 ? <UsersListGrouping /> : <UsersListToolbar />} */}
                <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
                    <>
                        {/* begin::Filter Button */}
                        <button
                            disabled={false}
                            type='button'
                            className='btn btn-light-primary me-3'
                            data-kt-menu-trigger='click'
                            data-kt-menu-placement='bottom-end'
                        >
                            <KTSVG path='/media/icons/duotune/general/gen031.svg' className='svg-icon-2' />
                            Filter
                        </button>
                        {/* end::Filter Button */}
                        {/* begin::SubMenu */}
                        <div className='menu menu-sub menu-sub-dropdown w-300px w-md-325px' data-kt-menu='true'>
                            {/* begin::Header */}
                            <div className='px-7 py-5'>
                                <div className='fs-5 text-dark fw-bolder'>Filter Options</div>
                            </div>
                            {/* end::Header */}

                            {/* begin::Separator */}
                            <div className='separator border-gray-200'></div>
                            {/* end::Separator */}

                            {/* begin::Content */}
                            <div className='px-7 py-5' data-kt-user-table-filter='form'>
                                {/* begin::Input group */}
                                <div className='mb-10'>
                                    <label className='form-label fs-6 fw-bold'>Role:</label>
                                    <select
                                        className='form-select form-select-solid fw-bolder'
                                        data-kt-select2='true'
                                        data-placeholder='Select option'
                                        data-allow-clear='true'
                                        data-kt-user-table-filter='role'
                                        data-hide-search='true'
                                        onChange={(e) => setRole(e.target.value)}
                                        value={role}
                                    >
                                        <option value=''></option>
                                        <option value='Administrator'>Administrator</option>
                                        <option value='Analyst'>Analyst</option>
                                        <option value='Developer'>Developer</option>
                                        <option value='Support'>Support</option>
                                        <option value='Trial'>Trial</option>
                                    </select>
                                </div>
                                {/* end::Input group */}

                                {/* begin::Input group */}
                                <div className='mb-10'>
                                    <label className='form-label fs-6 fw-bold'>Last login:</label>
                                    <select
                                        className='form-select form-select-solid fw-bolder'
                                        data-kt-select2='true'
                                        data-placeholder='Select option'
                                        data-allow-clear='true'
                                        data-kt-user-table-filter='two-step'
                                        data-hide-search='true'
                                        onChange={(e) => setLastLogin(e.target.value)}
                                        value={lastLogin}
                                    >
                                        <option value=''></option>
                                        <option value='Yesterday'>Yesterday</option>
                                        <option value='20 mins ago'>20 mins ago</option>
                                        <option value='5 hours ago'>5 hours ago</option>
                                        <option value='2 days ago'>2 days ago</option>
                                    </select>
                                </div>
                                {/* end::Input group */}

                                {/* begin::Actions */}
                                <div className='d-flex justify-content-end'>
                                    <button
                                        type='button'
                                        disabled={false}
                                        onClick={filterData}
                                        className='btn btn-light btn-active-light-primary fw-bold me-2 px-6'
                                        data-kt-menu-dismiss='true'
                                        data-kt-user-table-filter='reset'
                                    >
                                        Reset
                                    </button>
                                    <button
                                        disabled={false}
                                        type='button'
                                        onClick={resetData}
                                        className='btn btn-primary fw-bold px-6'
                                        data-kt-menu-dismiss='true'
                                        data-kt-user-table-filter='filter'
                                    >
                                        Apply
                                    </button>
                                </div>
                                {/* end::Actions */}
                            </div>
                            {/* end::Content */}
                        </div>
                        {/* end::SubMenu */}
                    </>
                    {/* begin::Export */}
                    <button type='button' className='btn btn-light-primary me-3'>
                        <KTSVG path='/media/icons/duotune/arrows/arr078.svg' className='svg-icon-2' />
                        Export
                    </button>
                    {/* end::Export */}

                    {/* begin::Add user */}
                    <button
                        type='button'
                        className='btn btn-primary'
                    >
                        <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />

                        <Link to={'/employee/add/'} className='text-white'>
                            Add
                        </Link>
                    </button>
                    {/* end::Add user */}
                </div>
                {/* end::Group actions */}
            </div>
            {/* end::Card toolbar */}
        </div>
    );
}
export default EmployeeHeader;
