import { FC, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { searchEmployeeAPI } from "../../../api/EmployeeApi";
import { KTSVG, toAbsoluteUrl } from "../../../_metronic/helpers";
import Pagination from "@vlsergey/react-bootstrap-pagination";

type Props = {
    className: string
}
type CustomerType = {
    id: string;
    name: string;
    phone: string;
    birthday: string;
    age: string;
    address?: string;
    action: string;
}

type EmployeeType = {
    "id": number;
    "firstName": string;
    "lastName": string;
    "image": string;
    "type": string;
    "address":string;
    "mobileNumber1": string;
    "mobileNumber2":string;
    "province": string;
    "district":string;
    "country": string;
    "postCode":string;
    "gender": string;
    "language": string;
    "bankName":string;
    "bankNo": string;
    "driveLicence":string;
    "driveType": string;
    "birthDate":string;
    "isActive": boolean
    "startDateOfWork": string;
    "createdDate": string;
    "updatedDate": string;
    "imageUrl": string;
}
const EmployeeList: FC<Props> = ({ className }) => {
    const [page, setPage ] = useState<number>(1);
    const [list, setList ] = useState<EmployeeType[]>([]);
    const [total, setTotal ] = useState<number>(0);
    const [pageSize, setPageSize ] = useState<number>(10);
    const [totalPages, setTotalPages ] = useState<number>(1);
    const [search, setSearch ] = useState<string>('');
    const [searchTerm, setSearchTerm] = useState('');

    const [role, setRole] = useState<string | undefined>()
    const [lastLogin, setLastLogin] = useState<string | undefined>()


    const resetData = () => {

    }

    const filterData = () => {

    }
    const numberWithCommas = (x : any) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
    const formatPhone = (phone : any) => {
        phone = phone + ('x'.repeat(10 - phone.length));
        let cleaned = ('' + phone).replace(/\D/g, '');
        var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
        if(match){
            return match[1] + '-' + match[2] + '-' + match[3]
        }
        return '-'
    }
    const getEmployeeList = async(page: number, pageSize: number, search:string) => {
        try {
            const result = await searchEmployeeAPI({body: {
                page:page,
                pageSize: pageSize,
                keyword: search
            }});

            if(result?.data) {
                const { content, total, totalPage } = result?.data;
                setList(content)
                setPageSize(pageSize)
                setTotal(total)
                setTotalPages(totalPage)
            }

        } catch (e: any) {
            console.log('error', e);

        }
    }

    const handleChange = (num:number) => {
        setPage(num)
        getEmployeeList(num, pageSize, search)
    }

    const handleSearch = async(e:any) => {
        setSearch(e.target.value);
        await getEmployeeList(page, pageSize, e.target.value);
    }

    useEffect(() => {
        getEmployeeList(page, pageSize, search);
    }, [])


    return (
        <>
           <div className='card-header border-0 pt-6'>
            <div className='card-title'>
                {/* begin::Search */}
                <div className='d-flex align-items-center position-relative my-1'>
                    <KTSVG
                        path='/media/icons/duotune/general/gen021.svg'
                        className='svg-icon-1 position-absolute ms-6'
                    />
                    <input
                        type='text'
                        data-kt-user-table-filter='search'
                        className='form-control form-control-solid w-250px ps-14'
                        placeholder='Search user'
                        value={search}
                        onChange={handleSearch}
                    />
                </div>
                {/* end::Search */}
            </div>
            {/* begin::Card toolbar */}
            <div className='card-toolbar'>
                {/* begin::Group actions */}
                {/* {selected.length > 0 ? <UsersListGrouping /> : <UsersListToolbar />} */}
                <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
                    <>
                        <div className='menu menu-sub menu-sub-dropdown w-300px w-md-325px' data-kt-menu='true'>
                            <div className='separator border-gray-200'></div>
                            {/* end::Content */}
                        </div>
                        {/* end::SubMenu */}
                    </>
                    <Link to={'/employee/add/'} className='text-white btn btn-primary'>
                        <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
                        Add
                    </Link>
                    {/* end::Add user */}
                </div>
                {/* end::Group actions */}
            </div>
            {/* end::Card toolbar */}
        </div>
        <div className={`card ${className}`}>
            {/* begin::Body */}
            <div className='card-body py-3'>
                {/* begin::Table container */}
                <div className='table-responsive'>
                    {/* begin::Table */}
                    <table className='table align-middle gs-0 gy-4'>
                        {/* begin::Table head */}
                        <thead>
                            <tr className='fw-bolder text-muted fs-7'>
                                <th className='w-25px'>
                                    <div className='form-check form-check-sm form-check-custom form-check-solid'>
                                        {/* <input
                                            className='form-check-input'
                                            type='checkbox'
                                            value='1'
                                            data-kt-check='true'
                                            data-kt-check-target='.widget-13-check'
                                        /> */}
                                    </div>
                                </th>
                                <th className='ps-4 min-w-200px rounded-start'>NAME</th>
                                <th className='min-w-70px'>ชื่อเล่น</th>
                                <th className='min-w-125px'>แผนก</th>
                                <th className='min-w-70px'>เพศ</th>
                                <th className='min-w-150px'>ธนาคาร</th>
                                <th className='min-w-150px'>เงินเดือน</th>
                                <th className='min-w-150px'>ยอดเงินเบิกล่วงหน้า</th>
                                <th className='min-w-125px'>Phone</th>
                                <th className='min-w-100px'>Birthday</th>
                                <th className='min-w-100px'>Age</th>
                                <th className='min-w-150px'>Address</th>
                                <th className='min-w-100px rounded-end'>Action</th>
                            </tr>
                        </thead>
                        {/* end::Table head */}
                        {/* begin::Table body */}
                        <tbody>
                            {
                                list && list.map((value, index) => (
                                    <tr>
                                        <td>
                                            <div className='form-check form-check-sm form-check-custom form-check-solid'>
                                                {/* <input className='form-check-input widget-13-check' type='checkbox' value='1' /> */}
                                            </div>
                                        </td>
                                        <td>
                                            <div className='d-flex align-items-center'>
                                                <div className='symbol symbol-50px me-5'>
                                                    <span className='symbol-label bg-light'>
                                                        <img
                                                            src={toAbsoluteUrl(value.imageUrl)}
                                                            className='h-75 align-self-end'
                                                            alt=''
                                                        />
                                                    </span>
                                                </div>
                                                <div className='d-flex justify-content-start flex-column'>

                                                    <span className='text-muted fw-bold text-muted d-block fs-6 mt-1'>
                                                        {value.firstName || '-'}
                                                    </span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-6 mt-1'>
                                            {value.language || '-'}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-6 mt-1'>
                                            {'-'}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-6 mt-1'>
                                            {value.gender === 'F' ? 'หญิง' :'ชาย'}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-6 mt-1'>
                                                {value?.bankName} {value?.bankNo}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-6 mt-1'>
                                                {numberWithCommas(0)}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-6 mt-1'>
                                                {numberWithCommas(0)}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-6 mt-1'>
                                                {formatPhone(value?.mobileNumber1)}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-6 mt-1'>
                                                {value.birthDate || '-'}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-6 mt-1'>
                                                {'-'}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-6 mt-1'>
                                                {value.address}
                                            </span>
                                        </td>
                                        <td className=''>
                                            <Link to={`/employee/detail/${value.id}`} className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2'>
                                                VIEW
                                            </Link>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                        {/* end::Table body */}
                    </table>
                    {/* end::Table */}
                </div>
                {/* end::Table container */}
                <div className='d-flex justify-content-end mt-2'>
                    <Pagination
                        value={page}
                        firstPageValue={1}
                        onChange={(e) => {handleChange(Number(e?.target?.value))}}
                        totalPages={totalPages}/>
                </div>
            </div>
            {/* begin::Body */}
            <div className='mt-6 d-flex flex-row justify-content-start'>
                <div className='mt-4 border-end border-1 border-gray-400 px-4'>
                    <span className={'text-dark'}>รายการเงินเดือนทั้งหมด: <span className='text-primary'>{'-'}</span> รายการ</span>
                </div>
                <div className='mt-4 px-4'>
                    <span className={'text-dark'}>จำนวนเงินทั้งหมด: <span className='text-primary'>{0}</span> บาท</span>
                </div>
            </div>
            <div className='mb-6 d-flex flex-row justify-content-start'>
                <div className='mt-4 px-4'>
                    <span className={'text-dark'}>แผนก :: <span className='text-primary'>{total}</span> คน</span>
                </div>
                <div className='mt-4 px-4'>
                    <span className={'text-dark'}>{'-'}</span>
                </div>
            </div>
        </div>
        </>

    )

}
export default EmployeeList;
