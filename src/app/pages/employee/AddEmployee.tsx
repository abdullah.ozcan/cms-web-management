import { FC, useEffect, useState } from "react";
import { PageTitle } from "../../../_metronic/layout/core";
import { useForm, Controller } from "react-hook-form";
import { DISTRICT, PROVICE } from "../../../api/Address";
import { clsx } from "clsx";
import { createEmployeeAPI } from "../../../api/EmployeeApi";
import ImageUploader from "react-images-upload";

const typeEmployee = [
  {
    label:'คนขับรถ',
    value:'DRIVER'
  },
  {
    label:'พนักงานทั่ว ',
    value:'NORMAL'
  }
]
const AddEmployee: FC = () => {
  const { handleSubmit, setValue, reset, getValues, watch, formState: { errors }, control } = useForm({
    defaultValues: {
      firstName: '',
      lastName: '',
      language: '',
      type: typeEmployee[0].value,
      email: '',
      address: '',
      mobileNumber1: '',
      mobileNumber2: '',
      province: 0,
      district: 0,
      country: '',
      postCode: '',
      gender: '',
      bankName: '',
      bankNo: '',
      driveType: '',
      driveLicence: '',
      isActive: '',
      startDateOfWork: '',
      birthDate: '',
      files: null
    }
  });
  const [districtList, setDistrictList] = useState<any[]>([])
  const handleChangeDistrict = (proviceId: number) => {
    setDistrictList(DISTRICT.filter((d) => d.PROVINCE_ID === proviceId))
  }


  const inintValue = () => {
    setValue('province', PROVICE[0].PROVINCE_ID)

    setDistrictList(DISTRICT.filter((d) => d.PROVINCE_ID === PROVICE[0].PROVINCE_ID))
  }

  const onCreate = async (value: any) => {
    try {

      console.log('value', value)
      let req:FormData = new FormData()

      req.append('files', value?.files ? value?.files[0] : null)
      req.append('firstName', value?.firstName)
      req.append('lastName', value?.lastName)
      req.append('language', 'TH')
      req.append('type', value?.type)
      req.append('email', value?.email)
      req.append('address', value?.address)
      req.append('mobileNumber1', value?.mobileNumber1)
      req.append('mobileNumber2', value?.mobileNumber2)
      req.append('country', value?.country)
      req.append('district', DISTRICT.find((d)=> d.DISTRICT_ID === value?.district)?.DISTRICT_NAME || '' )
      req.append('province', PROVICE.find((d)=> d.PROVINCE_ID === value?.province)?.PROVINCE_NAME || '')
      req.append('gender', value?.gender)
      req.append('bankName', value?.bankName)
      req.append('bankNo', value?.bankNo)
      req.append('driveType', value?.driveType)
      req.append('driveLicence', value?.driveLicence)
      req.append('isActive', value?.isActive)
      req.append('startDateOfWork', value?.startDateOfWork)
      req.append('birthDate', value?.birthDate)


      await createEmployeeAPI({body: req})

      window.location.href = `/employee`
    } catch (e: any) {
      console.log('e', e);

    }
  }
  useEffect(() => {
    inintValue()
  }, [])


  return (
    <>
      <PageTitle breadcrumbs={[]}>Add Employee</PageTitle>
      <form onSubmit={handleSubmit(onCreate)}>
        <div className='row g-5 g-xxl-8'>
          <div className='col-xl-4'>
            <div className='mb-5 mb-xxl-8 bg-white'>
              {/* begin::Body */}
              <div className='flex-grow-1'>
                <div className='d-flex justify-content-center align-items-center flex-wrap mb-2'>
                  <div className='d-flex flex-column mb-5'>
                    <div className='me-7 mb-4 mt-5'>
                      <div className='symbol symbol-100px symbol-lg-160px symbol-fixed position-relative'>
                        {/* <img src={toAbsoluteUrl('/media/avatars/300-1.jpg')} alt='Metornic' /> */}
                      </div>
                    </div>
                    <div className='d-flex justify-content-start flex-column'>
                      <Controller
                          name="files"
                          control={control}
                          rules={{
                            required: false,
                          }}
                          render={({field: {value, onChange}}) =>
                              <ImageUploader
                                  singleImage={true}
                                  withIcon={false}
                                  withPreview={true}
                                  label=""
                                  // buttonText={image? '':'Upload image'}
                                  onChange={(e)=>{
                                    onChange(e)
                                  }}
                                  imgExtension={[".jpg",]}
                                  maxFileSize={1048576}
                                  fileSizeError=" file size is too large"
                              />}
                      />
                      <span className='text-muted fw-bold text-muted d-block fs-7'>
                        Set the category thumnail image.
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='mb-5 mb-xxl-8 bg-white'>
              {/* begin::Body */}
              <div className='flex-grow-1'>
                <div className='d-flex justify-content-center align-items-center flex-wrap mb-2'>
                  <div className='d-flex flex-column mb-5'>
                    <div className='d-flex justify-content-between'>
                      <label className='col-form-label fw-bold fs-6'>Status</label>
                      <label className='col-form-label fw-bold fs-6'>.</label>
                    </div>
                    <div className='row'>
                      <div className='col-lg-12 fv-row w-200px'>
                        <Controller
                          name="isActive"
                          control={control}
                          rules={{
                            required: false,
                          }}
                          render={({ field: { value, onChange } }) =>
                            <select onChange={onChange} value={value} className="form-select" aria-label="Select example">
                              <option disabled>Open this select menu</option>
                              <option value={'1'}>ACTIVE</option>
                              <option value={'0'}>IN ACTIVE</option>
                            </select>
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-xl-8'>
            <div className='mb-5 mb-xxl-8 bg-white'>
              {/* begin::Body */}
              <form onSubmit={undefined} noValidate className='form'>
                <div className='card-body border-top p-9'>
                  <div className='row mb-6'>
                    <div className='col-lg-12'>
                      <label className='col-form-label required fw-bold fs-6'>Type Employee</label>
                      <div className='row'>
                        <div className='col-lg-6 fv-row'>
                          <Controller
                              name="type"
                              control={control}
                              rules={{
                                required: 'type is require',
                              }}
                              render={({ field: { value, onChange } }) =>
                                  <select onChange={onChange} value={value} className="form-select" aria-label="Select example">
                                    {
                                        typeEmployee && typeEmployee.map((d, index) => {
                                          return <option value={d.value} key={`index_of_emp_${index}`}>{d.label}</option>
                                        })
                                    }
                                  </select>
                              }
                            />
                        </div>
                      </div>
                    </div>
                    <div className='row mb-6'>
                      <div className='col-lg-12'>
                        <label className='col-form-label required fw-bold fs-6'>Full Name</label>
                        <div className='row'>
                          <div className='col-lg-6 fv-row'>
                            <Controller
                              name="firstName"
                              control={control}
                              rules={{
                                required: 'firstName is require',
                              }}
                              render={({ field: { value, onChange } }) =>
                                <input
                                  type='text'
                                  value={value}
                                  onChange={onChange}
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    {
                                      'is-invalid': errors.firstName?.message && errors.firstName?.message,
                                    },
                                    {
                                      'is-valid': errors.firstName?.message && !errors.firstName?.message,
                                    }
                                  )}
                                  placeholder='ชื่อ'
                                />
                              }
                            />
                            {errors.firstName?.message && (
                              <div className='fv-plugins-message-container text-danger'>
                                <span role='alert'>{errors.firstName?.message}</span>
                              </div>
                            )}
                          </div>

                          <div className='col-lg-6 fv-row'>
                            <Controller
                              name="lastName"
                              control={control}
                              rules={{
                                required: 'lastName is require',
                              }}
                              render={({ field: { value, onChange } }) =>
                                <input
                                  type='text'
                                  value={value}
                                  onChange={onChange}
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    {
                                      'is-invalid': errors.lastName?.message && errors.lastName?.message,
                                    },
                                    {
                                      'is-valid': errors.lastName?.message && !errors.lastName?.message,
                                    }
                                  )}
                                  placeholder='นามสกุล'
                                />
                              }
                            />
                            {errors.lastName?.message && (
                              <div className='fv-plugins-message-container text-danger'>
                                <span role='alert'>{errors.lastName?.message}</span>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className='row mb-6'>
                      <div className='col-lg-12'>
                        <label className='col-form-label required fw-bold fs-6'>Address</label>
                        <div className='row'>
                          <div className='col-lg-12 fv-row'>
                            <Controller
                              name="address"
                              control={control}
                              rules={{
                                required: 'address is require',
                              }}
                              render={({ field: { value, onChange } }) =>
                                <input
                                  type='text'
                                  value={value}
                                  onChange={onChange}
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    {
                                      'is-invalid': errors.address?.message && errors.address?.message,
                                    },
                                    {
                                      'is-valid': errors.address?.message && !errors.address?.message,
                                    }
                                  )}
                                  placeholder='address'
                                />
                              }
                            />
                            {errors.address?.message && (
                              <div className='fv-plugins-message-container text-danger'>
                                <span role='alert'>{errors.address?.message}</span>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className='row mb-6'>
                      <div className='col-lg-12'>
                        <label className='col-form-label required fw-bold fs-6'>Email</label>
                        <div className='row'>
                          <div className='col-lg-6 fv-row'>
                            <Controller
                              name="email"
                              control={control}
                              rules={{
                                required: 'address is require',
                              }}
                              render={({ field: { value, onChange } }) =>
                                <input
                                  type='text'
                                  value={value}
                                  onChange={onChange}
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    {
                                      'is-invalid': errors.email?.message && errors.email?.message,
                                    },
                                    {
                                      'is-valid': errors.email?.message && !errors.email?.message,
                                    }
                                  )}
                                  placeholder='email'
                                />
                              }
                            />
                            {errors.email?.message && (
                              <div className='fv-plugins-message-container text-danger'>
                                <span role='alert'>{errors.email?.message}</span>
                              </div>
                            )}
                          </div>
                          <div className='col-lg-6 fv-row'>
                            <Controller
                              name="mobileNumber1"
                              control={control}
                              rules={{
                                required: 'mobile is require',
                              }}
                              render={({ field: { value, onChange } }) =>
                                <input
                                  type='text'
                                  value={value}
                                  maxLength={10}
                                  onChange={onChange}
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    {
                                      'is-invalid': errors.mobileNumber1?.message && errors.mobileNumber1?.message,
                                    },
                                    {
                                      'is-valid': errors.mobileNumber1?.message && !errors.mobileNumber1?.message,
                                    }
                                  )}
                                  placeholder='เบอร์โทรศัพท์'
                                />
                              }
                            />
                            {errors.mobileNumber1?.message && (
                              <div className='fv-plugins-message-container text-danger'>
                                <span role='alert'>{errors.mobileNumber1?.message}</span>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className='row mb-6'>
                      <div className='col-lg-12'>
                        <label className='col-form-label required fw-bold fs-6'>ชื่อบัญชีธนาคาร</label>
                        <div className='row'>
                          <div className='col-lg-6 fv-row'>
                            <Controller
                              name="bankName"
                              control={control}
                              rules={{
                                required: 'bank is require',
                              }}
                              render={({ field: { value, onChange } }) =>
                                <input
                                  type='text'
                                  value={value}
                                  maxLength={30}
                                  onChange={onChange}
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    {
                                      'is-invalid': errors.bankName?.message && errors.bankName?.message,
                                    },
                                    {
                                      'is-valid': errors.bankName?.message && !errors.bankName?.message,
                                    }
                                  )}
                                  placeholder='ชื่อบัญชีธนาคาร'
                                />
                              }
                            />
                            {errors.bankName?.message && (
                              <div className='fv-plugins-message-container text-danger'>
                                <span role='alert'>{errors.bankName?.message}</span>
                              </div>
                            )}
                          </div>
                          <div className='col-lg-6 fv-row'>
                            <Controller
                              name="bankNo"
                              control={control}
                              rules={{
                                required: 'bankNo is require',
                              }}
                              render={({ field: { value, onChange } }) =>
                                <input
                                  type='text'
                                  value={value}
                                  maxLength={10}
                                  onChange={onChange}
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    {
                                      'is-invalid': errors.bankNo?.message && errors.bankNo?.message,
                                    },
                                    {
                                      'is-valid': errors.bankNo?.message && !errors.bankNo?.message,
                                    }
                                  )}
                                  placeholder='เลขที่บัญชีธนาคาร'
                                />
                              }
                            />
                            {errors.bankNo?.message && (
                              <div className='fv-plugins-message-container text-danger'>
                                <span role='alert'>{errors.bankNo?.message}</span>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className='row mb-6'>
                      <div className='col-lg-12'>
                        <label className='col-form-label required fw-bold fs-6'>State / Province</label>
                        <div className='row'>
                          <div className="col-lg-6 fv-row'">
                            <Controller
                              name="province"
                              control={control}
                              rules={{
                                required: 'กรุณาระบุข้อมูล',
                              }}
                              render={({ field: { value, onChange } }) =>
                                <select onChange={(e) => {
                                  handleChangeDistrict(+e.target.value)
                                  onChange(e)
                                }} value={value} className="form-select" aria-label="Select example">
                                  {
                                    PROVICE && PROVICE.map((d, index) => {
                                      return <option value={d.PROVINCE_ID} key={`index_of_province_${index}`}>{d.PROVINCE_NAME}</option>
                                    })
                                  }
                                </select>
                              }
                            />
                          </div>
                          <div className="col-lg-6 fv-row'">
                            <Controller
                              name="district"
                              control={control}
                              rules={{
                                required: 'กรุณาระบุข้อมูล',
                              }}
                              render={({ field: { value, onChange } }) =>
                                <select onChange={onChange} value={value} className="form-select" aria-label="Select example">
                                  {
                                    districtList && districtList.map((d, index) => {
                                      return <option value={d.DISTRICT_ID} key={`index_of_province_${index}`}>{d.DISTRICT_NAME}</option>
                                    })
                                  }
                                </select>
                              }
                            />
                          </div>

                        </div>
                      </div>
                    </div>
                    <div className='row mb-6'>
                      <div className='col-lg-12'>
                        <label className='col-form-label required fw-bold fs-6'>Country</label>
                        <div className='row'>
                          <div className='col-lg-6 fv-row'>
                            <Controller
                              name="country"
                              control={control}
                              rules={{
                                required: '',
                              }}
                              render={({ field: { value, onChange } }) =>
                                <input
                                  type='text'
                                  value={value}
                                  onChange={onChange}
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid'
                                  )}
                                  placeholder='country'
                                />
                              }
                            />
                          </div>
                          <div className='col-lg-6 fv-row'>
                            <Controller
                              name="postCode"
                              control={control}
                              rules={{
                                required: '',
                              }}
                              render={({ field: { value, onChange } }) =>
                                <input
                                  type='text'
                                  value={value}
                                  onChange={onChange}
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid'
                                  )}
                                  placeholder='postCode'
                                />
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className='row mb-6'>
                      <div className='col-lg-12'>
                        <label className='col-form-label required fw-bold fs-6'>Gender</label>
                        <Controller
                          name="gender"
                          control={control}
                          rules={{
                            required: 'กรุณาระบุข้อมูล',
                          }}
                          render={({ field: { value, onChange } }) => {
                            return (
                              <div>
                                <div className='row'>
                                  <div className='col-lg-6 fv-row'>
                                    <div className="form-check form-check-custom form-check-solid">
                                      <input className="form-check-input" type="radio" onChange={(e) => {
                                        console.log(e?.target?.value);
                                        onChange(e)
                                      }} value={'M'} id="flexRadioChecked"
                                        checked={watch('gender') === 'M'} />
                                      <label className="form-check-label" htmlFor="flexRadioChecked">
                                        Male
                                      </label>
                                    </div>
                                  </div>
                                  <div className='col-lg-6 fv-row'>
                                    <div className="form-check form-check-custom form-check-solid">
                                      <input className="form-check-input" type="radio" value={'F'} onChange={(e) => {
                                        console.log(e?.target?.value);
                                        onChange(e)
                                      }} id="flexRadioChecked"
                                        checked={watch('gender') === 'F'} />
                                      <label className="form-check-label" htmlFor="flexRadioChecked">
                                        FeMale
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            )
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              {/* end::Body */}
            </div>
            <button type='button' className='btn btn-secondary me-3' onClick={(e)=> {
              e.preventDefault()
              window.location.href = `/employee`
            }}>
              Cancel
            </button>
            <button type='submit' className='btn btn-primary me-3'>
              Add Employee
            </button>
          </div>
        </div>
      </form>
    </>
  )
}

export default AddEmployee;
