import { FC } from "react";
import { KTCard } from "../../../_metronic/helpers";
import { PageTitle } from "../../../_metronic/layout/core";
import EmployeeHeader from "./EmployeeHeader";
import EmployeeList from "./EmployeeList";

const EmployeeWrapper:FC = () => {
    return (
        <>
            <PageTitle breadcrumbs={[]}>Employee</PageTitle>
            <KTCard>
              <EmployeeList className={""}/>
            </KTCard>
        </>
    )
}
export default EmployeeWrapper;