import React, { FC, useEffect, useState } from "react";
import { Tabs, Tab, Modal } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import { createEmployeeEventAPI, getByIdEmployeeAPI, searchEmployeeEventAPI } from "../../../api/EmployeeApi";
import { KTSVG } from "../../../_metronic/helpers";
import { PageTitle } from "../../../_metronic/layout/core";
import { useForm, Controller } from "react-hook-form";
import clsx from "clsx";
import {getOrderEmployeesApi} from "../../../api/OrderApi";
import Pagination from "@vlsergey/react-bootstrap-pagination";

type CarType = {
    brand: string;
    id: number;
    model: string;
    status: string;
    title: string;
    vinNo: string;
    year: string;
}
type CustomerType = {
    "id": number;
    "name": string;
    "email": string;
}


type AgentType = {
    "id": number;
    "name": string;
    "contactName": string;
}
type OrderType = {
    address1: string;
    address2: string;
    agentId: number;
    agentName: string;
    carId: number;
    city: string;
    createdDate: string;
    currentMile: number;
    customerId: number;
    customerName: string;
    description: string;
    distantTotalMile: number;
    dropPlace: string;
    dropTime: string;
    employeeId: number;
    endMile: number;
    id: number;
    isPayment: boolean;
    name: string;
    oilAVG: number;
    oilPercent: number;
    paymentAmount: number;
    paymentDate: string;
    paymentDescription: string;
    paymentSlip: string;
    paymentType: string;
    phone: string;
    pickUpPlace: string;
    pickUpTime: string;
    postCode: string;
    pricePerDay: number;
    province: string;
    rentType: string;
    startMile: number;
    status: string;
    totalDate: number;
    updatedDate: string;
    agents: AgentType;
    employee: EmployeeType;
    customers: CustomerType;
    cars: CarType;
}
type EventsProps = {
    className: string;
    id: string | undefined;
    data?: EventsType[];
}
type EmployeeType = {
    "id": number;
    "firstName": string;
    "lastName": string;
    "image": string;
    "type": string;
    "address": string;
    "mobileNumber1": string;
    "mobileNumber2": string;
    "province": string;
    "district": string;
    "country": string;
    "postCode": string;
    "gender": string;
    "language": string;
    "bankName": string;
    "bankNo": string;
    "driveLicence": string;
    "driveType": string;
    "birthDate": string;
    "isActive": boolean
    "startDateOfWork": string;
    "createdDate": string;
    "updatedDate": string;
    "imgUrl": string;
}

type EventsType = {
    amount: number;
    createdDate: string;
    description: string;
    employeeId: number;
    id: number;
    isActive: boolean;
    startDate: string;
    title: string;
    type: string;
    updatedDate: string;
}

const EmployeeDetail: FC = () => {
    let { id } = useParams();
    const [key, setKey] = useState("home");
    const [value, setValue] = useState<EmployeeType | null>(null);
    const [total, setTotal] = useState<number>(0);
    const [pageSize, setPageSize] = useState<number>(10);
    const [page, setPage] = useState<number>(1);
    const [search, setSearch] = useState<string>('');



    const [eventList, setEventList] = useState<EventsType[] | null>([]);
    const [pageSizeEvent, setPageSizeEvent] = useState<number>(0);
    const [pageEvent, setPageEvent] = useState<number>(0);
    const [type, setType] = useState<string>('');


    // ORDER
    const [orderList, setOrderList] = useState<OrderType[] | null>([]);
    const [pageOrder, setPageOrder] = useState<number>(1);
    const [totalPageOrder, setTotalPageOrder] = useState<number>(0);


    const getEventByType = async (page: number, pageSize: number, keyword: string, type: string) => {
        try {
            setEventList([])
            const result = await searchEmployeeEventAPI({
                body: {
                    page: page,
                    pageSize: pageSize,
                    keyword: keyword,
                    type: type,
                    employeeId: Number(id) || 0
                }
            });

            if (result?.data) {
                const { content, total } = result?.data;
                setEventList(content)
            }

        } catch (e: any) {
            console.log('e', e);

        }
    }
    useEffect(() => {
        getEventByType(pageEvent, pageSizeEvent, '', type);
    }, [])

    const getEmployeeDetail = async () => {
        try {
            const result = await getByIdEmployeeAPI({ id: id || '' })

            if (result?.data) {
                const { data } = result;
                setValue(data);
            }

        } catch (e: any) {
            console.log('e', e);

        }
    }

    const getOrderEmployee = async (page:number) => {
        try {
            const result = await getOrderEmployeesApi({
                page: page,
                pageSize: 10,
                empId: id
            })

            if(result?.data) {
                const { data, totalPages } = result?.data;
                setTotalPageOrder(totalPages)
                setOrderList(data)
            }
        } catch (e) {
            console.log('e', e)
        }
    }

    const handleSelectTab = async (value: any) => {
        setKey(value);
        if(value !== 'home') {
            await getEventByType(page, pageSize, search, value.toString().toLowerCase())
        } else {
            getOrderEmployee(page)
        }
    }

    const handleChange =async (num:number) => {
        setPageOrder(num);
        await getOrderEmployee(num)
    }



    useEffect(() => {
        getEmployeeDetail();
        getOrderEmployee(page)
    }, [])
    return (
        <>
            <PageTitle breadcrumbs={[]}>Employee Detail</PageTitle>
            <div className='row g-5 g-xxl-8'>
                <div className='col-xl-4'>
                    <div className='mb-5 mb-xxl-8 bg-white'>
                        {/* begin::Body */}
                        <div className='flex-grow-1'>
                            <div className='d-flex justify-content-center align-items-center flex-wrap mb-2'>
                                <div className='d-flex flex-column mb-5'>
                                    <div className='me-7 mb-4 mt-5'>
                                        <div className='symbol symbol-100px symbol-lg-160px symbol-fixed position-relative'>
                                            <img src={value?.imgUrl} alt='Metornic' className="rounded" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='m-8'>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>ชื่อ</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{`${value?.firstName} ${value?.lastName}`}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>วัน/เดือน/ปีเกิด</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{value?.birthDate}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>ธนาคาร</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{value?.bankName}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>เลขบัญชี</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{value?.bankNo}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>บัตรประชาชน</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{'-'}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>ใบขับขี่</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{value?.driveLicence}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>ประเภทใบขับขี่</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{value?.driveType}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>เบอร์โทร</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{value?.mobileNumber1}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>เบอร์โทรฉุกเฉิน</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{value?.mobileNumber2}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>ระดับภาษา</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{value?.language}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>เพศ</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{value?.language === 'TH' ? 'ภาษาไทย' : value?.language}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>สถานะ</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{'-'}</div>
                            </div>
                            <div className='mt-4 row mx-auto' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>วันที่เริ่มงาน</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{value?.startDateOfWork}</div>
                            </div>
                            <div className='mt-4 row mx-auto mb-5' id='kt_signin_email'>
                                <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>ระยะเวลาที่ทำงาน</div>
                                <div className='col-6 col-lg-6 fw-bold text-gray-600'>{'-'}</div>
                            </div>
                            <div className='mt-4 row mx-auto mb-5' id='kt_signin_email' />
                            <div className='mt-4 row mx-auto mb-5' id='kt_signin_email' />
                        </div>
                    </div>
                </div>
                <div className='col-xl-8'>
                    <div className='mb-5 mb-xxl-8'>
                        <Tabs
                            id="controlled-tab-example"
                            activeKey={key}
                            onSelect={(k: any) => handleSelectTab(k)}
                            className="mb-3 nav nav-tabs nav-line-tabs mb-5 fs-5"
                        >
                            <Tab eventKey="home" title="รายการใช้รถยนต์" className="tab-pane fade h-100 border-0">
                                <div className='card'>
                                    <div className='card-header border-0 pt-6'>
                                        <div className='card-title'>
                                            <span className='card-label fw-bolder fs-3 mb-1'>History</span>
                                        </div>
                                    </div>
                                    <div className='card-body py-3'>
                                        {/* begin::Table container */}
                                        <div className='table-responsive'>
                                            {/* begin::Table */}
                                            <table className='table align-middle gs-0 gy-4'>
                                                {/* begin::Table head */}
                                                <thead>
                                                <tr className='fw-bolder text-muted'>
                                                    <th className='text-start ps-4 min-w-50px'>Id</th>
                                                    <th className='min-w-125px'>Status</th>
                                                    <th className='min-w-125px'>Amount</th>
                                                    <th className='min-w-125px'>Date</th>
                                                    <th className='min-w-125px rounded-end'>Action</th>
                                                </tr>
                                                </thead>
                                                {/* end::Table head */}
                                                {/* begin::Table body */}
                                                <tbody>
                                                {
                                                    orderList && orderList.map((value, index) => (
                                                        <tr>
                                                            <td>
                                                                <div className='d-flex align-items-center'>
                                                                    <div className='d-flex justify-content-start flex-column'>
                                                                        <Link className='' to={'/customer/detail'}>
                                                                            {value.id}
                                                                        </Link>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7 mt-1'>
                                                {value.status  ===  'SUCCESS'? 'SUCCESS': 'PENDDING'}
                                            </span>
                                                            </td>
                                                            <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7 mt-1'>
                                                {value?.paymentAmount}
                                            </span>
                                                            </td>
                                                            <td>

                                            <span className='text-muted fw-bold text-muted d-block fs-7 mt-1'>
                                                {value?.createdDate}
                                            </span>
                                                            </td>
                                                            <td className=''>
                                                                {/* <Link to={'/customer/detail/invoice'} className='text-primary fs-6 btn btn-bg-light btn-active-color-primary btn-sm me-1'>
                                                VIEW
                                            </Link> */}
                                                                {/*<div className='text-primary fs-6 btn btn-bg-light btn-active-color-primary btn-sm me-1'>*/}
                                                                {/*    VIEW*/}
                                                                {/*</div>*/}
                                                            </td>
                                                        </tr>
                                                    ))
                                                }
                                                </tbody>
                                                {/* end::Table body */}
                                            </table>
                                            {/* end::Table */}
                                        </div>
                                        {/* end::Table container */}
                                    </div>
                                    <div className='d-flex justify-content-end m-4'>
                                        <Pagination
                                            value={pageOrder}
                                            firstPageValue={1}
                                            onChange={(e) => {handleChange(Number(e.target.value))}}
                                            totalPages={totalPageOrder}/>
                                    </div>
                                </div>
                            </Tab>
                            <Tab eventKey="salary" title="เงินเดือน" className="tab-pane fade h-100">
                                <EmployeeHistory className="" data={eventList || []} id={id} />
                            </Tab>
                            <Tab eventKey="request" title="เงินเบิกล่วงหน้า" className="tab-pane fade h-100">
                                <EmployeeHistory className="" data={eventList || []} id={id} />
                            </Tab>
                            <Tab eventKey="reserve" title="เงินสำรอง" className="tab-pane fade h-100">
                                <EmployeeHistory className="" data={eventList || []} id={id} />
                            </Tab>
                            <Tab eventKey="order" title="ใบสั่ง" className="tab-pane fade h-100">
                                <EmployeeHistory className="" data={eventList || []} id={id} />
                            </Tab>
                            <Tab eventKey="accident" title="เชี่ยวชน" className="tab-pane fade h-100">
                                <EmployeeHistory className="" data={eventList || []} id={id} />
                            </Tab>
                            <Tab eventKey="jobs" title="ออกงาน" className="tab-pane fade h-100">
                                <EmployeeHistory className="" data={eventList || []} id={id} />
                            </Tab>
                        </Tabs>
                    </div>
                </div>
            </div>
        </>
    )
}

export default EmployeeDetail;
const EmployeeHistory: React.FC<EventsProps> = ({ className, data = [], id }) => {
    const { handleSubmit, formState: { errors }, control } = useForm({
        defaultValues: {
            type: '',
            employeeId: id,
            title: '',
            amount: 0,
            description: '',
            isActive: true,
            startDate: ''
        }
    });
    const [show, setShow] = useState<boolean>(false);
    const handleClose = () => {
        setShow(false)
    }

    const selectTypeList = [
        {
            'label': 'ทั่วไป',
            'value': ''
        },
        {
            'label': 'เงินเดือน',
            'value': 'SALARY'
        },
        {
            'label': 'เงินเบิกล่วงหน้า',
            'value': 'request'.toUpperCase()
        },
        {
            'label': 'เงินสำรอง',
            'value': 'reserve'.toUpperCase()
        },
        {
            'label': 'ใบสั่ง',
            'value': 'order'.toUpperCase()
        },
        {
            'label': 'เชี่ยวชน',
            'value': 'accident'.toUpperCase()
        },
        {
            'label': 'ออกงาน',
            'value': 'jobs'.toUpperCase()
        }
    ]
    const onCreate = async (value: any) => {
        // const { id } = useParams()
        try {
            console.log('value', value);
            await createEmployeeEventAPI({body:value})

            window.location.href = `/employee/detail/${id}`

        } catch (e: any) {
            console.log('e', e);
        }
    }
    return (
        <div className={`card ${className}`}>
            <div className='card-header border-0 pt-6'>
                <div className='card-title'>
                    <span className='card-label fw-bolder fs-3 mb-1'>History</span>
                </div>
                {/* begin::Card toolbar */}
                <div className='card-toolbar'>
                    <div className='d-flex justify-content-end w-100' data-kt-user-table-toolbar='base'>
                        <button type='button'
                            className='btn btn-primary mx-1'
                            data-bs-toggle='modal'
                            data-bs-dismiss='modal'
                            onClick={() => { setShow(true) }}
                        >
                            Add Case
                        </button>
                    </div>
                    <Modal show={show} onHide={handleClose} size={'lg'}>
                        <Modal.Header closeButton>
                            <Modal.Title>
                                <h5 className='modal-title'>Add Case</h5>
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <form onSubmit={handleSubmit(onCreate)}>
                                <div className='row mb-4'>
                                    <div className='col-lg-6 fv-row'>
                                        <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                            <span className=''>หัวข้อ</span>
                                        </label>
                                        <Controller
                                            name="title"
                                            control={control}
                                            rules={{
                                                required: 'title is require',
                                            }}
                                            render={({ field: { value, onChange } }) =>
                                                <input
                                                    type='text'
                                                    value={value}
                                                    onChange={onChange}
                                                    className={clsx(
                                                        'form-control form-control-lg form-control-solid',
                                                        {
                                                            'is-invalid': errors.title?.message && errors.title?.message,
                                                        },
                                                        {
                                                            'is-valid': errors.title?.message && !errors.title?.message,
                                                        }
                                                    )}
                                                    placeholder=''
                                                />
                                            }
                                        />
                                        {errors.title?.message && (
                                            <div className='fv-plugins-message-container text-danger'>
                                                <span role='alert'>{errors.title?.message}</span>
                                            </div>
                                        )}
                                    </div>
                                    <div className='col-lg-6 fv-row'>
                                        <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                            <span className=''>เลือกประเภท</span>
                                        </label>
                                        <Controller
                                            name="type"
                                            control={control}
                                            rules={{
                                                required: 'title is require',
                                            }}
                                            render={({ field: { value, onChange } }) =>
                                                <select
                                                    className='form-select form-select-solid fw-bolder'
                                                    data-kt-select2='true'
                                                    data-placeholder='Select option'
                                                    data-allow-clear='true'
                                                    data-kt-user-table-filter='two-step'
                                                    data-hide-search='true'
                                                    onChange={onChange}
                                                    value={value}
                                                >
                                                    {
                                                        selectTypeList && selectTypeList.map((d, index) =>
                                                            <option key={`index_of_select_type_${index}`} value={d.value}>{d.label}</option>)
                                                    }
                                                </select>
                                            }
                                        />
                                    </div>
                                </div>
                                <div className='row mb-4'>
                                    <div className='col-lg-6 fv-row'>
                                        <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                            <span className=''>จำนวนเงิน</span>
                                        </label>
                                        <Controller
                                            name="amount"
                                            control={control}
                                            rules={{
                                                required: 'amount is require',
                                            }}
                                            render={({ field: { value, onChange } }) =>
                                                <input
                                                    type='number'
                                                    min={0}
                                                    value={value}
                                                    onChange={onChange}
                                                    className={clsx(
                                                        'form-control form-control-lg form-control-solid',
                                                        {
                                                            'is-invalid': errors.amount?.message && errors.amount?.message,
                                                        },
                                                        {
                                                            'is-valid': errors.amount?.message && !errors.amount?.message,
                                                        }
                                                    )}
                                                    placeholder=''
                                                />
                                            }
                                        />
                                        {errors.amount?.message && (
                                            <div className='fv-plugins-message-container text-danger'>
                                                <span role='alert'>{errors.amount?.message}</span>
                                            </div>
                                        )}
                                    </div>
                                    <div className='col-lg-6 fv-row'>
                                        <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                            <span className=''>วันที่ทำรายการ</span>
                                        </label>
                                        <Controller
                                            name="startDate"
                                            control={control}
                                            rules={{
                                                required: 'startDate is require',
                                            }}
                                            render={({ field: { value, onChange } }) =>
                                                <input
                                                    type='date'
                                                    value={value}
                                                    onChange={onChange}
                                                    className={clsx(
                                                        'form-control form-control-lg form-control-solid',
                                                        {
                                                            'is-invalid': errors.startDate?.message && errors.startDate?.message,
                                                        },
                                                        {
                                                            'is-valid': errors.startDate?.message && !errors.startDate?.message,
                                                        }
                                                    )}
                                                    placeholder=''
                                                />
                                            }
                                        />
                                        {errors.startDate?.message && (
                                            <div className='fv-plugins-message-container text-danger'>
                                                <span role='alert'>{errors.startDate?.message}</span>
                                            </div>
                                        )}
                                    </div>
                                </div>
                                <div className='row mb-4'>
                                    <div className='col-lg-12 fv-row'>
                                        <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                            <span className=''>หมายเหตุ</span>
                                        </label>
                                        <Controller
                                            name="description"
                                            control={control}
                                            render={({ field: { value, onChange } }) =>
                                                <textarea
                                                    value={value}
                                                    rows={4}
                                                    onChange={onChange}
                                                    className='form-control form-control-lg form-control-solid'
                                                    placeholder=''
                                                />
                                            }
                                        />
                                    </div>
                                </div>
                                <div>
                                    <div className=''>
                                        <button
                                            type='button'
                                            className='btn btn-light'
                                            onClick={() => { setShow(false) }}
                                        >
                                            Discard
                                        </button>
                                        <button type='submit' className='btn btn-primary'>
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </Modal.Body>
                    </Modal>
                </div>
                {/* end::Card toolbar */}
                <div className='modal fade' tabIndex={-1} id='kt_modal_1_add'>
                    <div className='modal-dialog'>
                        <div className='modal-content'>
                            <div className='modal-header'>
                                <h5 className='modal-title'>Add Agent</h5>
                                <div
                                    className='btn btn-icon btn-sm btn-active-light-primary ms-2'
                                    data-bs-dismiss='modal'
                                    aria-label='Close'
                                >
                                    <KTSVG
                                        path='/media/icons/duotune/arrows/arr061.svg'
                                        className='svg-icon svg-icon-2x'
                                    />
                                </div>
                            </div>
                            <div className='modal-body'>
                                <div className='row mb-4'>
                                    <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                        <span className='required'>Name</span>
                                    </label>
                                    <div className='col-lg-12 fv-row'>
                                        <input
                                            type='text'
                                            className='form-control form-control-lg form-control-solid'
                                            placeholder='Name'
                                        />
                                    </div>
                                </div>
                                <div className='row mb-12'>
                                    <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                        <span className='required'>ID Type</span>
                                    </label>
                                    <div className='col-lg-12 fv-row'>
                                        <input
                                            type='tel'
                                            className='form-control form-control-lg form-control-solid'
                                            placeholder='ID Type'
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className='modal-footer'>
                                <button
                                    type='button'
                                    className='btn btn-light'
                                    data-bs-dismiss='modal'
                                >
                                    Discard
                                </button>
                                <button type='button' className='btn btn-primary'>
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* begin::Body */}
            <div className='card-body py-3'>
                {/* begin::Table container */}
                <div className='table-responsive'>
                    {/* begin::Table */}
                    <table className='table align-middle gs-0 gy-4'>
                        {/* begin::Table head */}
                        <thead>
                            <tr className='fw-bolder text-muted'>
                                <th className='ps-4 min-w-125px'>No</th>
                                <th className='min-w-125px'>Title</th>
                                <th className='min-w-125px'>Status</th>
                                <th className='min-w-125px'>Amount</th>
                                <th className='min-w-125px'>Date</th>
                                <th className='min-w-125px rounded-end'>Action</th>
                            </tr>
                        </thead>
                        {/* end::Table head */}
                        {/* begin::Table body */}
                        <tbody>
                            {
                                data && data.map((value, index) => (
                                    <tr>
                                        <td>
                                            <div className='d-flex align-items-center'>
                                                <div className='d-flex justify-content-start flex-column'>
                                                    <Link className='' to={'/customer/detail'}>
                                                        {++index}
                                                    </Link>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7 mt-1'>
                                                {value.title}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7 mt-1'>
                                                {value.isActive ? 'PENDDING' : 'CANCEL'}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7 mt-1'>
                                                {value?.amount}
                                            </span>
                                        </td>
                                        <td>

                                            <span className='text-muted fw-bold text-muted d-block fs-7 mt-1'>
                                                {value?.startDate}
                                            </span>
                                        </td>
                                        <td className=''>
                                            {/* <Link to={'/customer/detail/invoice'} className='text-primary fs-6 btn btn-bg-light btn-active-color-primary btn-sm me-1'>
                                                VIEW
                                            </Link> */}
                                            {/*<div className='text-primary fs-6 btn btn-bg-light btn-active-color-primary btn-sm me-1'>*/}
                                            {/*    VIEW*/}
                                            {/*</div>*/}
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                        {/* end::Table body */}
                    </table>
                    {/* end::Table */}
                </div>
                {/* end::Table container */}
            </div>
            {/* begin::Body */}
        </div>
    )
}
