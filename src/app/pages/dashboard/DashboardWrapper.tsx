/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {FC} from 'react'
import {useIntl} from 'react-intl'
import {PageTitle} from '../../../_metronic/layout/core'
import InCome from './Income'
import OutCome from './OutCome'
import OutCome2 from './OutCome2'
import OutCome3 from './OutCome3'
import Work1 from './Work1'
import Work2 from './Work2'

const DashboardPage: FC = () => (
  <>
    {/* begin::Row */}
    <div className='row gy-5 g-xl-8'>
      <div className='col-xxl-12'>
        <div className='row gy-5'>
          <div className='col-xxl-3'>
            <InCome
              className='card-xl-stretch mb-xl-8'
              chartColor='primary'
              chartHeight='150px'
            />
          </div>
          <div className='col-xxl-3'>
            <OutCome
              className='card-xl-stretch mb-xl-8'
              chartColor='warning'
              chartHeight='150px'
            />
          </div>
          <div className='col-xxl-3'>
            <OutCome2
              className='card-xl-stretch mb-xl-8'
              chartColor='primary'
              chartHeight='150px'
            />
          </div>
          <div className='col-xxl-3'>
            <OutCome3
              className='card-xl-stretch mb-xl-8'
              chartColor='warning'
              chartHeight='150px'
            />
          </div>
        </div>
      </div>
      {/*<div className='col-xxl-4'>*/}
      {/*  <ListsWidget5 className='card-xxl-stretch' />*/}
      {/*</div>*/}
    </div>
    {/* end::Row */}
    <div className='row g-5 gx-xxl-8'>
      <div className='col-xxl-6'>
        <Work1
          className='card-xxl-stretch mb-xl-3'
          chartColor='success'
          chartHeight='150px'
        />
      </div>
      <div className='col-xxl-6'>
        <Work2
          className='card-xxl-stretch mb-xl-3'
          chartColor='primary'
          chartHeight='150px'
        />
      </div>
    </div>
  </>
)

const DashboardWrapper: FC = () => {
  const intl = useIntl()
  return (
    <>
      <PageTitle breadcrumbs={[]}>{intl.formatMessage({id: 'MENU.DASHBOARD'})}</PageTitle>
      <DashboardPage />
    </>
  )
}

export default DashboardWrapper
