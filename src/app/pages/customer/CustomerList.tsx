import {FC, useEffect, useState} from "react"
import {Link} from "react-router-dom"
import {KTSVG, toAbsoluteUrl} from "../../../_metronic/helpers"
import {createCustomerAPI, searchCustomerAPI} from "../../../api/CustomerApi"
import {Button, Modal} from "react-bootstrap"
import {useForm, Controller} from "react-hook-form";
import ImageUploader from "react-images-upload";
import clsx from "clsx";
import {PROVICE} from "../../../api/Address";
import Pagination from "@vlsergey/react-bootstrap-pagination";

type CustomerType = {
    id: number,
    name: string,
    profile: string,
    email: string,
    description: string,
    address1: string,
    address2: string,
    country: string,
    province: string,
    postCode: string,
    language: string,
    job: string,
    taxNo: string,
    contactPhone: string,
    credieBalanceAmount: number,
    status: string,
    isActive: boolean,
    createdDate: string,
    updatedDate: string
    imageUrl: string;
}

const CustomerList: FC = () => {
    const {handleSubmit, reset, formState: {errors}, control} = useForm({
        defaultValues: {
            name: '',
            age: '',
            profile: '',
            email: '',
            description: '',
            address1: '',
            address2: '',
            province: PROVICE[0].PROVINCE_NAME,
            country: '',
            postCode: '',
            language: 'TH',
            taxNo: '',
            job: '',
            contactPhone: '',
            creditBalanceAmount: 0,
        }
    });

    const langList = [
        {
            label:'ภาษาไทย',
            value:'TH'
        },
        {
            label:'ภาษาอังกฤษ',
            value:'EN'
        },
        {
            label:'ภาษาจีน',
            value:'CN'
        }
    ]
    const [searchTerm, setSearchTerm] = useState('');

    const [role, setRole] = useState<string | undefined>()
    const [lastLogin, setLastLogin] = useState<string | undefined>()
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [image, setImage] = useState<any>(null)


    const onDrop = (value:any) => {
        console.log('value:', value)
        // setImage(value[0])
    }

    const createCustomer = async (value:any) => {
        try {

            console.log('value:', value)
            const req:FormData = new FormData();

            req.append('name', value?.name)
            req.append('age', value?.age)
            req.append('files', value?.profile[0]? value?.profile[0]: '')
            req.append('email', value?.email)
            req.append('description', value?.description)
            req.append('address1', value?.address1)
            req.append('address2', value?.address2)
            req.append('province', value?.province)
            req.append('country', value?.country)
            req.append('postCode', value?.postCode)
            req.append('language', value?.language)
            req.append('job', value?.job)
            req.append('contactPhone', value?.contactPhone)
            req.append('creditBalanceAmount', value?.creditBalanceAmount)

            await createCustomerAPI({body: req})
                .then((r)=> {
                    handleClose()
                    searchCustomers(page, pageSize, '')
                })
                .catch((e)=> {
                    handleClose()
                    searchCustomers(page, pageSize, '')
                })
            //
            // window.location.href = '/customer'

        } catch (e) {
            console.log('e', e)
        }
    }
    const ModalAddCustomer = () => {
        return (
            <Modal show={show} onHide={handleClose} size='lg'>
                <Modal.Header closeButton>
                    <Modal.Title>Add a Customer</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form onSubmit={handleSubmit(createCustomer)}>
                        <div className='row mb-4'>
                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                <span className=''>Profile</span>
                            </label>
                            <div className='col-lg-12 fv-row'>
                                <Controller
                                    name="profile"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) =>
                                        <ImageUploader
                                            singleImage={true}
                                            withIcon={false}
                                            withPreview={true}
                                            label=""
                                            // buttonText={image? '':'Upload image'}
                                            onChange={(e)=>{
                                                onChange(e)
                                                onDrop(e)
                                            }}
                                            imgExtension={[".jpg",]}
                                            maxFileSize={1048576}
                                            fileSizeError=" file size is too big"
                                        />}
                                />
                            </div>
                        </div>
                        <div className='row mb-4'>
                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                <span className='required'>Name</span>
                            </label>
                            <div className='col-lg-12 fv-row'>
                                <Controller
                                    name="name"
                                    control={control}
                                    rules={{
                                        required: 'name is require',
                                    }}
                                    render={({field: {value, onChange}}) =>
                                        <input
                                            placeholder=''
                                            id="name"
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                'form-control form-control-lg form-control-solid',
                                                {
                                                    'is-invalid': errors.name?.message && errors.name?.message,
                                                },
                                                {
                                                    'is-valid': errors.name?.message && !errors.name?.message,
                                                }
                                            )}
                                            type='text'
                                            name='name'
                                            autoComplete='off'
                                        />}
                                />
                                {errors.name?.message && (
                                    <div className='fv-plugins-message-container text-danger'>
                                        <span role='alert'>{errors.name?.message}</span>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className='row mb-4'>
                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                <span className='required'>Email</span>
                            </label>
                            <div className='col-lg-12 fv-row'>
                                <Controller
                                    name="email"
                                    control={control}
                                    rules={{
                                        required: 'email is require',
                                    }}
                                    render={({field: {value, onChange}}) =>
                                        <input
                                            placeholder=''
                                            type='email'
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                'form-control form-control-lg form-control-solid',
                                                {
                                                    'is-invalid': errors.email?.message && errors.email?.message,
                                                },
                                                {
                                                    'is-valid': errors.email?.message && !errors.email?.message,
                                                }
                                            )}
                                            name='name'
                                            autoComplete='off'
                                        />}
                                />
                                {errors.email?.message && (
                                    <div className='fv-plugins-message-container text-danger'>
                                        <span role='alert'>{errors.email?.message}</span>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className='row mb-6'>
                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                <span className=''>Description</span>
                            </label>
                            <div className='col-lg-12 fv-row'>
                                <Controller
                                    name="description"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) =>
                                        <input
                                            placeholder=''
                                            type='text'
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                'form-control form-control-lg form-control-solid',
                                            )}
                                            name='name'
                                            autoComplete='off'
                                        />}
                                />
                            </div>
                        </div>
                        <div className='row mb-4'>
                            <div className='col-lg-12 fv-row'>
                                <label className='col-12 col-form-label fw-bold fs-6'>
                                    <h5 className="modal-title">Information</h5>
                                </label>
                            </div>
                        </div>
                        <div className='row mb-4'>
                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                <span className='required'>Address Line 1</span>
                            </label>
                            <div className='col-lg-12 fv-row'>
                                <Controller
                                    name="address1"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) =>
                                        <input
                                            placeholder=''
                                            type='text'
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                'form-control form-control-lg form-control-solid',
                                            )}
                                            name='name'
                                            autoComplete='off'
                                        />}
                                />
                            </div>
                        </div>
                        <div className='row mb-4'>
                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                <span className='required'>Address Line</span>
                            </label>
                            <div className='col-lg-12 fv-row'>
                                <Controller
                                    name="address2"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) =>
                                        <input
                                            placeholder=''
                                            type='text'
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                'form-control form-control-lg form-control-solid',
                                            )}
                                            name='name'
                                            autoComplete='off'
                                        />}
                                />
                            </div>
                        </div>
                        <div className='row mb-4'>
                            <div className='col-lg-6 fv-row'>
                                <label className='col-form-label fw-bold fs-6'>
                                    <span className=''>State / Province</span>
                                </label>
                                <Controller
                                    name="province"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) => (
                                        <select onChange={onChange} value={value} className="form-select"
                                                aria-label="Select City">
                                            {
                                                PROVICE && PROVICE.map((value, index) => (
                                                    <option key={`index_order_province_${index}`}
                                                            value={value?.PROVINCE_NAME}>{value?.PROVINCE_NAME}</option>
                                                ))
                                            }
                                        </select>
                                    )}
                                />
                            </div>
                            <div className='col-lg-6 fv-row'>
                                <label className='col-form-label fw-bold fs-6'>
                                    <span className=''>Post Code</span>
                                </label>
                                <Controller
                                    name="postCode"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) =>
                                        <input
                                            placeholder=''
                                            type='text'
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                'form-control form-control-lg form-control-solid',
                                            )}
                                            name='name'
                                            autoComplete='off'
                                        />}
                                />
                            </div>
                        </div>
                        <div className='row mb-4'>
                            <div className='col-lg-6 fv-row'>
                                <label className='col-form-label fw-bold fs-6'>
                                    <span className=''>ภาษาที่ใช้</span>
                                </label>
                                <Controller
                                    name="language"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) => (
                                        <select onChange={onChange} className="form-select"
                                                aria-label="Select City">
                                            {
                                                langList && langList.map((value, index) => (
                                                    <option key={`index_order_province_${index}`}
                                                            value={value?.value}>{value?.label}</option>
                                                ))
                                            }
                                        </select>
                                    )}
                                />
                            </div>
                            <div className='col-lg-6 fv-row'>
                                <label className='col-form-label fw-bold fs-6'>
                                    <span className=''>อาชีพ</span>
                                </label>
                                <Controller
                                    name="job"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) =>
                                        <input
                                            placeholder=''
                                            type='text'
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                'form-control form-control-lg form-control-solid',
                                            )}
                                            name='name'
                                            autoComplete='off'
                                        />}
                                />
                            </div>
                        </div>
                        <div className='row mb-4'>
                            <div className='col-lg-6 fv-row'>
                                <label className='col-form-label fw-bold fs-6'>
                                    <span className=''>เบอร์โทรศัพท์</span>
                                </label>
                                <Controller
                                    name="contactPhone"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) =>
                                        <input
                                            placeholder=''
                                            type='text'
                                            maxLength={10}
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                'form-control form-control-lg form-control-solid',
                                            )}
                                            name='name'
                                            autoComplete='off'
                                        />}
                                />
                            </div>
                            <div className='col-lg-6 fv-row'>
                                <label className='col-form-label fw-bold fs-6'>
                                    <span className=''>อายุ</span>
                                </label>
                                <Controller
                                    name="age"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) =>
                                        <input
                                            placeholder=''
                                            type='text'
                                            maxLength={10}
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                'form-control form-control-lg form-control-solid',
                                            )}
                                            name='name'
                                            autoComplete='off'
                                        />}
                                />
                            </div>
                        </div>
                        <div className='d-flex flex-row justify-content-end'>
                            <button className='mx-2 btn btn-secondary'  type={'button'} onClick={(e) => {
                                e.preventDefault()
                                reset()
                                handleClose()
                            }}>
                                Close
                            </button>
                            <button  className='mx-2 btn btn-primary' type={'submit'}>
                                Save Changes
                            </button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        );
    }

    const resetData = () => {
        //   updateState({filter: undefined, ...initialQueryState})
    }

    const filterData = () => {
        //   updateState({
        //     filter: {role, last_login: lastLogin},
        //     ...initialQueryState,
        //   })
    }

    const [customerList, setCustomerList] = useState<CustomerType[] | null>(null)
    const [page, setPage] = useState<number>(1)
    const [pageSize, setPageSize] = useState<number>(10)
    const [total, setTotal] = useState<number>(0)
    const [search, setSearch] = useState<string>('')
    const [totalPage, setTotalPage] = useState<number>(1)



    const searchCustomers = async (page: number, pageSize: number, search: string) => {
        try {
            const result = await searchCustomerAPI({
                body: {
                    page: page,
                    pageSize: pageSize,
                    keyWord: search
                }
            })
            console.log('result:', result)
            if (result) {
                const {content, pageSize, total, totalPages} = result?.data;
                setTotal(total);
                setPageSize(pageSize);
                setCustomerList(content);
                setTotalPage(totalPages)
            }
        } catch (e: any) {
            console.log('error', e)
        }
    }

    const handleChangePage = async (value:number) => {
        console.log('value', value)
        setPage(value);
        await searchCustomers(value, pageSize, searchTerm);
    }

    const handleSearch = async (value: any) => {
        setSearchTerm(value);
        await searchCustomers(page, pageSize, value);
    }

    const formatPhone = (phone: any) => {
        phone = phone + ('x'.repeat(10 - phone.length));
        let cleaned = ('' + phone).replace(/\D/g, '');
        var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
        if (match) {
            return match[1] + '-' + match[2] + '-' + match[3]
        }
        return '-'
    }

    useEffect(() => {
        searchCustomers(page, pageSize, '')
    }, [])

    return (
        <>
            <div className='card-header border-0 pt-6'>
                <div className='card-title'>
                    {/* begin::Search */}
                    <div className='d-flex align-items-center position-relative my-1'>
                        <KTSVG
                            path='/media/icons/duotune/general/gen021.svg'
                            className='svg-icon-1 position-absolute ms-6'
                        />
                        <input
                            type='text'
                            data-kt-user-table-filter='search'
                            className='form-control form-control-solid w-250px ps-14'
                            placeholder='Search user'
                            value={searchTerm}
                            onChange={(e) => handleSearch(e.target.value)}
                        />
                    </div>
                    {/* end::Search */}
                </div>
                {/* begin::Card toolbar */}
                <div className='card-toolbar'>
                    {/* begin::Group actions */}
                    {/* {selected.length > 0 ? <UsersListGrouping /> : <UsersListToolbar />} */}
                    <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
                        <>
                            {/* begin::Filter Button */}
                            {/*<button*/}
                            {/*    disabled={false}*/}
                            {/*    type='button'*/}
                            {/*    className='btn btn-light-primary me-3'*/}
                            {/*    data-kt-menu-trigger='click'*/}
                            {/*    data-kt-menu-placement='bottom-end'*/}
                            {/*>*/}
                            {/*    <KTSVG path='/media/icons/duotune/general/gen031.svg' className='svg-icon-2'/>*/}
                            {/*    Filter*/}
                            {/*</button>*/}
                            {/* end::Filter Button */}
                            {/* begin::SubMenu */}
                            <div className='menu menu-sub menu-sub-dropdown w-300px w-md-325px' data-kt-menu='true'>
                                {/* begin::Header */}
                                <div className='px-7 py-5'>
                                    <div className='fs-5 text-dark fw-bolder'>Filter Options</div>
                                </div>
                                {/* end::Header */}

                                {/* begin::Separator */}
                                <div className='separator border-gray-200'></div>
                                {/* end::Separator */}

                                {/* begin::Content */}
                                <div className='px-7 py-5' data-kt-user-table-filter='form'>
                                    {/* begin::Input group */}
                                    <div className='mb-10'>
                                        <label className='form-label fs-6 fw-bold'>Role:</label>
                                        <select
                                            className='form-select form-select-solid fw-bolder'
                                            data-kt-select2='true'
                                            data-placeholder='Select option'
                                            data-allow-clear='true'
                                            data-kt-user-table-filter='role'
                                            data-hide-search='true'
                                            onChange={(e) => setRole(e.target.value)}
                                            value={role}
                                        >
                                            <option value=''></option>
                                            <option value='Administrator'>Administrator</option>
                                            <option value='Analyst'>Analyst</option>
                                            <option value='Developer'>Developer</option>
                                            <option value='Support'>Support</option>
                                            <option value='Trial'>Trial</option>
                                        </select>
                                    </div>
                                    {/* end::Input group */}

                                    {/* begin::Input group */}
                                    <div className='mb-10'>
                                        <label className='form-label fs-6 fw-bold'>Last login:</label>
                                        <select
                                            className='form-select form-select-solid fw-bolder'
                                            data-kt-select2='true'
                                            data-placeholder='Select option'
                                            data-allow-clear='true'
                                            data-kt-user-table-filter='two-step'
                                            data-hide-search='true'
                                            onChange={(e) => setLastLogin(e.target.value)}
                                            value={lastLogin}
                                        >
                                            <option value=''></option>
                                            <option value='Yesterday'>Yesterday</option>
                                            <option value='20 mins ago'>20 mins ago</option>
                                            <option value='5 hours ago'>5 hours ago</option>
                                            <option value='2 days ago'>2 days ago</option>
                                        </select>
                                    </div>
                                    {/* end::Input group */}

                                    {/* begin::Actions */}
                                    <div className='d-flex justify-content-end'>
                                        <button
                                            type='button'
                                            disabled={false}
                                            onClick={filterData}
                                            className='btn btn-light btn-active-light-primary fw-bold me-2 px-6'
                                            data-kt-menu-dismiss='true'
                                            data-kt-user-table-filter='reset'
                                        >
                                            Reset
                                        </button>
                                        <button
                                            disabled={false}
                                            type='button'
                                            onClick={resetData}
                                            className='btn btn-primary fw-bold px-6'
                                            data-kt-menu-dismiss='true'
                                            data-kt-user-table-filter='filter'
                                        >
                                            Apply
                                        </button>
                                    </div>
                                    {/* end::Actions */}
                                </div>
                                {/* end::Content */}
                            </div>
                            {/* end::SubMenu */}
                        </>
                        {/* begin::Export */}
                        {/*<button type='button' className='btn btn-light-primary me-3'>*/}
                        {/*    <KTSVG path='/media/icons/duotune/arrows/arr078.svg' className='svg-icon-2'/>*/}
                        {/*    Export*/}
                        {/*</button>*/}
                        {/* end::Export */}

                        {/* begin::Add user */}
                        <button
                            type='button'
                            className='btn btn-primary'
                            onClick={handleShow}
                        >
                            <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2'/>
                            Add
                        </button>
                        <ModalAddCustomer/>
                        {/* end::Add user */}
                    </div>
                    {/* end::Group actions */}
                </div>
                {/* end::Card toolbar */}
            </div>
            <div className='card-body py-3'>
                <div className='table-responsive'>
                    <table className='table align-middle gs-0 gy-4 fs-6'>
                        <thead>
                        <tr className='text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0'>
                            <th className='w-25px'>
                                <div className='form-check form-check-sm form-check-custom form-check-solid'>
                                    <input
                                        className='form-check-input'
                                        type='checkbox'
                                        value='1'
                                        data-kt-check='true'
                                        data-kt-check-target='.widget-13-check'
                                    />
                                </div>
                            </th>
                            <th className="min-w-20px">ID</th>
                            <th className="min-w-120px">Profile</th>
                            <th className='min-w-125px'>Name</th>
                            <th className='min-w-125px'>Phone</th>
                            <th className='min-w-125px'>Birthday</th>
                            <th className='min-w-125px'>Age</th>
                            <th className='min-w-150px'>Order Amount</th>
                            <th className='min-w-150px'>Payment method</th>
                            <th className='min-w-150px'>Address</th>
                            <th className='min-w-125px rounded-end'>Action</th>
                        </tr>
                        </thead>
                        <tbody className="fw-bold text-gray-600">
                        {
                            customerList && customerList.map((value, index) => (
                                <tr key={`index_of_customer${index}`}>
                                    <td>
                                        <div className='form-check form-check-sm form-check-custom form-check-solid'>
                                            <input className='form-check-input widget-13-check' type='checkbox' value='1'/>
                                        </div>
                                    </td>
                                    <td>
                                        <div className='d-flex justify-content-start flex-column'>
                                            <span className='text-dark text-hover-primary d-block mb-1 fs-6'>
                                                {value.id}
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div className='d-flex align-items-center'>
                                            <div className='symbol symbol-45px me-5'>
                                                <span className='symbol-label bg-light overflow-hidden'>
                                                    <img
                                                        src={toAbsoluteUrl(value.imageUrl)}
                                                        className='h-100 align-self-end'
                                                        alt=''
                                                    />
                                                </span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div className='d-flex justify-content-start flex-column'>
                                            <span className='text-dark text-hover-primary d-block mb-1 fs-6'>
                                                {value.name}
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <span className='text-dark text-hover-primary d-block mb-1 fs-6'>
                                            {formatPhone(value?.contactPhone)}
                                        </span>
                                    </td>
                                    <td>
                                        <span className='text-dark text-hover-primary d-block mb-1 fs-6'>
                                            {"-"}
                                        </span>
                                    </td>
                                    <td>
                                        <span className='text-dark text-hover-primary d-block mb-1 fs-6'>
                                            {"-"}
                                        </span>
                                    </td>
                                    <td>
                                        <span className='text-dark text-hover-primary d-block mb-1 fs-6'>
                                            {"-"}
                                        </span>
                                    </td>
                                    <td>
                                        <span className='text-dark text-hover-primary d-block mb-1 fs-6'>
                                            {"-"}
                                        </span>
                                    </td>
                                    <td>
                                        <span className='text-dark text-hover-primary d-block mb-1 fs-6'>
                                            {value?.address1}
                                        </span>
                                    </td>
                                    <td>
                                        <Link
                                            className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2'
                                            to={`/customer/detail/${value.id}`}>
                                            VIEW
                                        </Link>
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </table>
                    <div className='d-flex justify-content-end mt-2 mb-5 me-7'>
                        <Pagination
                            value={page}
                            firstPageValue={1}
                            onChange={(e) => {handleChangePage(Number(e.target?.value))}}
                            totalPages={totalPage}/>
                    </div>
                </div>
            </div>
        </>
    );
}

export default CustomerList;
