import { FC } from "react";
import { toAbsoluteUrl } from "../../../../_metronic/helpers";

type Props = {
    className: string
  }
const ProfileInformationComponent:FC<Props> = ({className}) => {
    return (
<div className={`card ${className} p-2`}>
      {/* begin::Body */}
      <div className='flex-grow-1'>
        <div className='d-flex justify-content-center align-items-center flex-wrap mb-2'>
          <div className='d-flex flex-column'>
            <div className='me-7 mb-4 mt-5'>
              <div className='symbol symbol-100px symbol-lg-160px symbol-fixed position-relative px-2'>
                <img src={toAbsoluteUrl('/media/avatars/300-1.jpg')} className='rounded-circle' alt='Metornic' />
              </div>
            </div>
            <div className='d-flex justify-content-start flex-column'>
              <a href='#' className='text-gray-800 text-hover-primary fs-2 fw-bolder me-1 text-center'>
                สมหมาย
              </a>
            </div>
          </div>
        </div>
        <div className='mt-4 mx-auto'>
          <div className='accordion-body'>
            <span className='badge badge-light-secondary p-5'>Premium user</span>
            <div className='d-flex flex-column flex-wrap align-items-start p-5'>
              <div className='mt-4' id='kt_signin_email'>
                <div className='fs-6 fw-bolder mb-1'>Account ID</div>
                <div className='fw-bold text-gray-600'>ID-AA34543</div>
              </div>
              <div className='mt-4' id='kt_signin_email'>
                <div className='fs-6 fw-bolder mb-1'>Billing Email</div>
                <div className='fw-bold text-gray-600'>support@keenthemes.com</div>
              </div>
              <div className='mt-4' id='kt_signin_email'>
                <div className='fs-6 fw-bolder mb-1'>Email Address</div>
                <div className='fw-bold text-gray-600'>support@keenthemes.com</div>
              </div>
              <div className='mt-4' id='kt_signin_email'>
                <div className='fs-6 fw-bolder mb-1'>Language</div>
                <div className='fw-bold text-gray-600'>Thai</div>
              </div>
              <div className='mt-4'>
                <div className='fs-6 fw-bolder mb-1'>Upcoming Invoice</div>
                <div className='fw-bold text-gray-600'>543234-8693</div>
              </div>
              <div className='mt-4' id='kt_signin_email'>
                <div className='fs-6 fw-bolder mb-1'>Tax ID</div>
                <div className='fw-bold text-gray-600'>Tax-12345678</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* end::Body */}
    </div>
    );
}

export default ProfileInformationComponent;