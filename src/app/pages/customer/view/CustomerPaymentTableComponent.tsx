import { FC } from "react";
import { Link } from "react-router-dom";

type Props = {
    className: string
}
type PaymentType = {
    id: string;
    invoice: string;
    amount: string;
    status?: string;
    date: string;
    action: string;
}
const CustomerPaymentTableComponent: FC<Props> = ({ className }) => {
    const paymentList: PaymentType[] = [
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        },
        {
            id: '114-356',
            invoice: '114-356',
            amount: '1,240',
            status: 'success',
            date: '15 JAN 2022 16:30',
            action: '',
        }
    ]
    return (
        <div className={`card ${className}`}>
            {/* begin::Header */}
            <div className='card-header border-0 pt-5'>
                <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder fs-3 mb-1'>Payment records</span>
                </h3>
                <div
                    className='card-toolbar'
                    data-bs-toggle='tooltip'
                    data-bs-placement='top'
                    data-bs-trigger='hover'
                    title='Click to add a user'
                >
                    {/*<a*/}
                    {/*  href='#'*/}
                    {/*  className='btn btn-sm btn-light-primary'*/}
                    {/*  data-bs-toggle='modal'*/}
                    {/*  data-bs-target='#kt_modal_invite_friends'*/}
                    {/*>*/}
                    {/*  <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-3' />*/}
                    {/*  Add payment*/}
                    {/*</a>*/}
                </div>
            </div>
            {/* end::Header */}
            {/* begin::Body */}
            <div className='card-body py-3'>
                {/* begin::Table container */}
                <div className='table-responsive'>
                    {/* begin::Table */}
                    <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
                        {/* begin::Table head */}
                        <thead>
                            <tr className='fw-bolder text-muted'>
                                <th className='min-w-150px text-capitalize'>Invoice No</th>
                                <th className='min-w-140px text-capitalize'>Status</th>
                                <th className='min-w-120px text-capitalize'>Amount</th>
                                <th className='min-w-120px text-capitalize'>Date</th>
                                <th className='min-w-100px text-end text-capitalize'>Actions</th>
                            </tr>
                        </thead>
                        {/* end::Table head */}
                        {/* begin::Table body */}
                        <tbody>
                            {
                                paymentList && paymentList.map(((value, index) =>
                                    <tr key={`payment_detail_${index}`}>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                {value.invoice}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                {
                                                    value.status === 'success' ?
                                                        <span className="badge badge-success">{value.status}</span>
                                                        :
                                                        <span className="badge badge-light">{value.status}</span>
                                                }
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                {value.amount}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                {value.date}
                                            </span>
                                        </td>
                                        <td>
                                            <Link className='text-uppercase text-primary btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2' to={'/customer/detail/invoice'}>
                                                VIEW
                                            </Link>
                                        </td>
                                    </tr>))
                            }
                        </tbody>
                        {/* end::Table body */}
                    </table>
                    {/* end::Table */}
                </div>
                {/* end::Table container */}
            </div>
            {/* begin::Body */}
        </div>
    );
}
export default CustomerPaymentTableComponent;