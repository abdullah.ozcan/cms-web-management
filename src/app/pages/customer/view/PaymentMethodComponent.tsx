import { FC, useState } from 'react'
import { KTSVG } from '../../../../_metronic/helpers';
import AccordionPaymentMethod from '../customer-accordion/AccordionPaymentMethod';

type Prop = {
    className : string
}

type PaymentAbout = {
    title: string
    name: string
    number: string
    expires: string
    type: string
    issuer: string
    id: string
    biladd: string
    phone?: string
    email: string
    origin: string
    cvcheck?: any
}

const PaymentMethodComponent: FC<Prop> = ({ className }) => {
    const PaymentList: PaymentAbout[] = [
        {
            title: 'Mastercard',
            name: 'Emma Smith',
            number: '**** 6974',
            expires: '12/2024',
            type: 'Mastercard credit card',
            issuer: 'VICBANK',
            id: 'id_4301f3234we1',
            biladd: 'AU',
            email: 'smith@kpmg.com',
            origin: 'Australia',
            cvcheck: 'Passed'
        },
        {
            title: 'Visa',
            name: 'Emma Smith',
            number: '**** 6974',
            expires: '12/2024',
            type: 'Mastercard credit card',
            issuer: 'VICBANK',
            id: 'id_4301f3234we1',
            biladd: 'AU',
            email: 'smith@kpmg.com',
            origin: 'Australia',
            cvcheck: 'Passed'
        },
        {
            title: 'Amarican Express',
            name: 'Emma Smith',
            number: '**** 6974',
            expires: '12/2024',
            type: 'Mastercard credit card',
            issuer: 'VICBANK',
            id: 'id_4301f3234we1',
            biladd: 'AU',
            email: 'smith@kpmg.com',
            origin: 'Australia',
            cvcheck: 'Passed'
        },
    ]
    return (
        <div className={`card ${className}`}>
            {/* begin::Header */}
            <div className='card-header border-0 pt-5'>
                <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder fs-3 mb-1'>Payment Methods</span>
                </h3>
                <div 
                    className='card-toolbar'
                    data-bs-toggle='tooltip'
                    data-bs-placement='top'
                    data-bs-trigger='hover'
                >
                <a
                    href='#'
                    className='btn btn-sm btn-light-primary'
                    >
                        <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-3' />
                    Add new method
                </a>
                </div>
            </div>
            {/* end::Header */}
            {/* begin::Body */}
            <div className='card-body py-3'>
                {
                    PaymentList && PaymentList.map((( value, index) =>
                        <div key={`payment_list_${index}`}>
                            <AccordionPaymentMethod
                                title={value.title}
                                name={value.name}
                                number={value.number}
                                expires={value.expires}
                                type={value.type}
                                issuer={value.issuer}
                                id={value.id}
                                biladd={value.biladd}
                                email={value.email}
                                origin={value.origin}
                                cvcheck={value.cvcheck}
                            />
                        </div>
                    ))
                }
            </div>
            {/* end:Body */}
        </div>
    )
}

export default PaymentMethodComponent;