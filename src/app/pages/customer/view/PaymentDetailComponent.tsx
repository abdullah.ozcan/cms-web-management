import { FC } from "react";

type Props = {
    className: string
}

const PaymentDetailComponent:FC<Props> = ({className}) => {
    return (
        <div className={`card ${className}`}>
        {/* begin::Header */}
        <div className='card-header align-items-center border-0 mt-4'>
            <h3 className='card-title align-items-start flex-column'>
                <span className='card-label fw-bolder fs-3 mb-1'>Credit Balance</span>
            </h3>
        </div>
        {/* end::Header */}
        {/* begin::Body */}
        <div className='card-body pt-5'>
            <div className='d-flex justify-content-start flex-column'>
                <a href='#' className='text-dark fw-bolder text-hover-primary fs-6'>
                    $7234.64 USD
                </a>
                <span className='text-muted fw-bold text-muted d-block fs-7'>
                    Balance will increase due on the customer
                </span>
            </div>
        </div>
        {/* end: Card Body */}
    </div>
    );
}

export default PaymentDetailComponent;