import { FC } from "react";
import { Link } from "react-router-dom";
import { KTSVG } from "../../../../_metronic/helpers";
import { PageTitle } from "../../../../_metronic/layout/core";

const CustomerInVoice:FC = () => {
    const valueList = [
        {
          desription: 'Car 1',
          hours: '8',
          rate: '400',
          amount: '3200'
        },
        {
          desription: 'Car 1',
          hours: '8',
          rate: '400',
          amount: '3200'
        },
        {
          desription: 'Car 1',
          hours: '8',
          rate: '400',
          amount: '3200'
        }
      ]
    return (
        <>
          <PageTitle breadcrumbs={[]}>Home - Customer - Invoice Detail</PageTitle>
          <div className='card mb-5 mb-xl-10'>
            <div className='card mb-5 mb-xl-10' id='kt_profile_details_view'>
              <div className='card-body p-9'>
                <div className='row'>
                  <div className='col-lg-8'>
                    <div className='row mb-7'>
                      <div className='col-lg-8'>
                        <h2 className='fw-bold text-dark'>WEB MANAGEMENT</h2>
                        <div
                          className='d-none d-md-flex flex-row-fluid bgi-no-repeat bgi-position-y-center bgi-position-x-right bgi-size-cover'
                          style={{
                            transform: 'translateX(10%) rotate(-26deg)',
                            backgroundImage: `url('/media/icons/duotune/finance/fin002.svg')`,
                          }}
                        ></div>
                        <span className='fw-bolder fs-6 text-muted'>Max Smith</span>
                      </div>
                      <div className='col-lg-4'>
                        <Link to='/customer/detail/invoice'  className='btn btn-secondary align-self-center'>
                          Pay Now
                        </Link>
                      </div>
                    </div>
                    <div className='row mb-7'>
                      <div className='col-lg-4'>
                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Issue Date:</div>
                        <div className='fw-bold'>Tax-12345678</div>
                      </div>
                      <div className='col-lg-8 fv-row'>
                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Due Date:</div>
                        <span className='fw-bolder fs-6 me-2'>02 May 2022</span>
                        <span className='fw-bolder fs-6 me-2 text-gray-600'>Due in 7 days</span>
                      </div>
                    </div>
    
                    <div className='row mb-7'>
                      <div className='col-lg-4'>
                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Issue for:</div>
                        <div className='fw-bold'>Somsak manop</div>
                      </div>
                      <div className='col-lg-8 fv-row'>
                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Issue By:</div>
                        <span className='fw-bolder fs-6 me-2'>Narong Kulthasi</span>
                      </div>
                    </div>
                    <div className='row mb-7'>
                      <div className='col-lg-12'>
                        <div className='table-responsive'>
                          {/* begin::Table */}
                          <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
                            {/* begin::Table head */}
                            <thead>
                              <tr className='fw-bolder text-muted'>
                                <th className='min-w-150px'>Description</th>
                                <th className='min-w-140px'>Hours</th>
                                <th className='min-w-120px'>Rate</th>
                                <th className='min-w-120px'>Amount</th>
                              </tr>
                            </thead>
                            {/* end::Table head */}
                            {/* begin::Table body */}
                            <tbody>
                              {
                                valueList && valueList.map((d, index) => (
                                  <tr key={`invoice_${index}`}>
                                    <td>
                                      <a href='#' className='text-dark fw-bolder text-hover-primary fs-6'>
                                        {d.desription}
                                      </a>
                                    </td>
                                    <td>
                                      <span className='text-dark fw-bolder text-hover-primary d-block mb-1 fs-6'>{d.hours}</span>
                                    </td>
                                    <td>
                                      <span className='text-dark fw-bolder text-hover-primary d-block mb-1 fs-6'>{d.rate}</span>
                                    </td>
                                    <td>
                                      <span className='text-dark fw-bolder text-hover-primary d-block mb-1 fs-6'>{d.amount}</span>
                                    </td>
                                  </tr>
                                ))
                              }
                            </tbody>
                            {/* end::Table body */}
                          </table>
                          {/* end::Table */}
                        </div>
                        <div className='row mb-7 mt-4'>
                          <div className='col-7'></div>
                          <div className='col-5'>
                            <div className='row mb-7'>
                              <label className='col-lg-8 fw-bold text-muted'>SubTotal</label>
                              <div className='col-lg-4'>
                                <span className='fw-bolder fs-6 text-dark'>{3200 * 4}</span>
                              </div>
                            </div>
                            <div className='row mb-7'>
                              <label className='col-lg-8 fw-bold text-muted'>VAT 0%</label>
    
                              <div className='col-lg-4 fv-row'>
                                <span className='fw-bold fs-6'>{0}</span>
                              </div>
                            </div>
    
                            <div className='row mb-7'>
                              <label className='col-lg-8 fw-bold text-muted'>SubTotal + VAT</label>
                              <div className='col-lg-4 d-flex align-items-center'>
                                <span className='fw-bolder fs-6 me-2'>{3200 * 4}</span>
                              </div>
                            </div>
    
                            <div className='row mb-7'>
                              <label className='col-lg-8 fw-bold text-muted'>Total</label>
                              <div className='col-lg-4'>
                                <span className='fw-bolder fs-6 me-2'>{3200 * 4}</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='col-lg-4'>
                    <div className='notice d-flex bg-secondary rounded border-secondary border border-dashed p-6'>
                      <KTSVG
                        path='icons/duotune/general/gen044.svg'
                        className='svg-icon-2tx svg-icon-warning me-4'
                      />
                      <div className='d-flex flex-stack flex-grow-1 h-100'>
                        <div className='fw-bold'>
                          <div className='card-body p-9'>
                            <div className='fs-6 fw-bolder text-gray-600 mb-1'>Payment Detail:</div>
                            <div className='d-flex flex-column flex-wrap align-items-start'>
                              <div className='mt-4' id='kt_signin_email'>
                                <div className='fs-6 fw-bolder mb-1'>Paypal</div>
                                <div className='fw-bold text-gray-600'>ID-AA34543</div>
                              </div>
                              <div className='mt-4' id='kt_signin_email'>
                                <div className='fs-6 fw-bolder mb-1'>Account</div>
                                <div className='fw-bold text-gray-600'>support@keenthemes.com</div>
                              </div>
                              <div className='mt-4' id='kt_signin_email'>
                                <div className='fs-6 fw-bolder mb-1'>Payment term</div>
                                <div className='fw-bold text-gray-600'>14 Day Due in 7</div>
                              </div>
                            </div>
                            <div className='fs-6 fw-bolder text-gray-600 mb-1 mt-5'>Project Overview:</div>
                            <div className='d-flex flex-column flex-wrap align-items-start'>
                              <div className='mt-4' id='kt_signin_email'>
                                <div className='fs-6 fw-bolder mb-1'>Project Name</div>
                                <div className='fw-bold text-gray-600'>Computer</div>
                              </div>
                              <div className='mt-4' id='kt_signin_email'>
                                <div className='fs-6 fw-bolder mb-1'>Completed by</div>
                                <div className='fw-bold text-gray-600'>keenthemes</div>
                              </div>
                              <div className='mt-4' id='kt_signin_email'>
                                <div className='fs-6 fw-bolder mb-1'>Time spent</div>
                                <div className='fw-bold text-gray-600'>224 hr.</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )
}

export default CustomerInVoice;