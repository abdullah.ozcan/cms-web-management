import { FC } from "react";
import { Link } from "react-router-dom";

type Props = {
    className: string
}
type Invoice = {
    orderId: string;
    amount: string;
    status: string;
    date: string;
    action: string;
}
const CustomerInvoiceComponent: FC<Props> = ({ className }) => {
    const invoiceList: Invoice[] = [
        {
            orderId: '201719940378138',
            amount: '$1.71',
            status: 'Demangeon',
            date: '9/17/2021',
            action: ''
        },
        {
            orderId: '201719940378138',
            amount: '$8.04',
            status: 'Demangeon',
            date: '9/17/2021',
            action: ''
        },
        {
            orderId: '201719940378138',
            amount: '$1.71',
            status: 'Demangeon',
            date: '9/17/2021',
            action: ''
        },
        {
            orderId: '201719940378138',
            amount: '$1.71',
            status: 'Demangeon',
            date: '9/17/2021',
            action: ''
        },
        {
            orderId: '201719940378138',
            amount: '$1.71',
            status: 'Demangeon',
            date: '9/17/2021',
            action: ''
        },
    ];
    return (
        <div className={`card ${className}`}>
            {/* begin::Header */}
            <div className='card-header border-0 pt-5'>
                <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder fs-3 mb-1'>Invoice</span>
                </h3>
                <div className='card-toolbar'>
                    <ul className='nav'>
                        <li className='nav-item'>
                            <a
                                className='nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary active fw-bolder px-4 me-1'
                                data-bs-toggle='tab'
                                href='#kt_table_widget_5_tab_1'
                            >
                                Month
                            </a>
                        </li>
                        <li className='nav-item'>
                            <a
                                className='nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bolder px-4 me-1'
                                data-bs-toggle='tab'
                                href='#kt_table_widget_5_tab_2'
                            >
                                Week
                            </a>
                        </li>
                        <li className='nav-item'>
                            <a
                                className='nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bolder px-4'
                                data-bs-toggle='tab'
                                href='#kt_table_widget_5_tab_3'
                            >
                                Day
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            {/* end::Header */}
            {/* begin::Body */}
            <div className='card-body py-3'>
                <div className='tab-content'>
                    {/* begin::Tap pane */}
                    <div className='tab-pane fade show active' id='kt_table_widget_5_tab_1'>
                        {/* begin::Table container */}
                        <div className='table-responsive'>
                            {/* begin::Table */}
                            <table className='table table-row-dashed table-row-gray-200 align-middle gs-0 gy-4'>
                                {/* begin::Table head */}
                                <thead>
                                    <tr className='border-0'>
                                        <th className='p-0 min-w-110px'></th>
                                        <th className='p-0 min-w-150px'></th>
                                        <th className='p-0 min-w-140px'></th>
                                        <th className='p-0 min-w-110px'></th>
                                        <th className='p-0 min-w-50px'></th>
                                    </tr>
                                </thead>
                                {/* end::Table head */}
                                {/* begin::Table body */}
                                <tbody>
                                    {
                                        invoiceList && invoiceList.map((value, index) => (
                                            <tr key={`invoce_index${index}`}>
                                                <td>
                                                    <span className='text-dark fw-bold d-block'>{value.orderId}</span>
                                                </td>
                                                <td>
                                                    <span className='text-dark fw-bold d-block'>{value.amount}</span>
                                                </td>
                                                <td className='text-end text-muted fw-bold'>{value.date}</td>
                                                <td className='text-end'>
                                                    <span className='badge badge-light-success'>Approved</span>
                                                </td>
                                                <td className='text-end'>
                                                    <a
                                                        href='/web/management/customer/detail/invoice'
                                                        className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2'
                                                    >
                                                        <Link className='text-uppercase' to={'/customer/detail/invoice'}>
                                                            download
                                                        </Link>
                                                    </a>
                                                </td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                                {/* end::Table body */}
                            </table>
                        </div>
                        {/* end::Table */}
                    </div>
                    {/* end::Tap pane */}
                    {/* begin::Tap pane */}
                    <div className='tab-pane fade' id='kt_table_widget_5_tab_2'>
                        {/* begin::Table container */}
                        <div className='table-responsive'>
                            {/* begin::Table */}
                            <table className='table table-row-dashed table-row-gray-200 align-middle gs-0 gy-4'>
                                {/* begin::Table head */}
                                <thead>
                                    <tr className='border-0'>
                                        <th className='p-0 w-50px'></th>
                                        <th className='p-0 min-w-150px'></th>
                                        <th className='p-0 min-w-140px'></th>
                                        <th className='p-0 min-w-110px'></th>
                                        <th className='p-0 min-w-50px'></th>
                                    </tr>
                                </thead>
                                {/* end::Table head */}
                                {/* begin::Table body */}
                                <tbody>
                                    {
                                        invoiceList && invoiceList.map((value, index) => (
                                            <tr key={`invice_index_week${index}`}>
                                                <td>
                                                    <span className='text-dark fw-bold d-block'>{value.orderId}</span>
                                                </td>
                                                <td>
                                                    <span className='text-dark fw-bold d-block'>{value.amount}</span>
                                                </td>
                                                <td className='text-end text-muted fw-bold'>
                                                    <span className='text-dark fw-bold d-block'>{value.date}</span>
                                                </td>
                                                <td className='text-end'>
                                                    <span className={index % 2 === 0 ? 'badge badge-light-success' : 'badge badge-warning'}>
                                                        {index % 2 === 0 ? 'Approved' : 'Inprogress'}
                                                    </span>
                                                </td>
                                                <td className='text-end'>
                                                    <a
                                                        href='/web/management/customer/detail/invoice'
                                                        className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2'
                                                    >
                                                        <Link className='text-uppercase' to={'/customer/detail/invoice'}>
                                                            download
                                                        </Link>
                                                    </a>
                                                </td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                                {/* end::Table body */}
                            </table>
                        </div>
                        {/* end::Table */}
                    </div>
                    {/* end::Tap pane */}
                    {/* begin::Tap pane */}
                    <div className='tab-pane fade' id='kt_table_widget_5_tab_3'>
                        {/* begin::Table container */}
                        <div className='table-responsive'>
                            {/* begin::Table */}
                            <table className='table table-row-dashed table-row-gray-200 align-middle gs-0 gy-4'>
                                {/* begin::Table head */}
                                <thead>
                                    <tr className='border-0'>
                                        <th className='p-0 min-w-110px'></th>
                                        <th className='p-0 min-w-150px'></th>
                                        <th className='p-0 min-w-140px'></th>
                                        <th className='p-0 min-w-110px'></th>
                                        <th className='p-0 min-w-50px'></th>
                                    </tr>
                                </thead>
                                {/* end::Table head */}
                                {/* begin::Table body */}
                                <tbody>
                                    {
                                        invoiceList && invoiceList.map((value, index) => (
                                            <tr key={`invice_index_week${index}`}>
                                                <td>
                                                    <span className='text-dark fw-bold d-block'>{value.orderId}</span>
                                                </td>
                                                <td>
                                                    <span className='text-dark fw-bold d-block'>{value.amount}</span>
                                                </td>
                                                <td className='text-end text-muted fw-bold'>
                                                    <span className='text-dark fw-bold d-block'>{value.date}</span>
                                                </td>
                                                <td className='text-end'>
                                                    <span className={index % 2 === 0 ? 'badge badge-light-success' : 'badge badge-warning'}>
                                                        {index % 2 === 0 ? 'Approved' : 'Inprogress'}
                                                    </span>
                                                </td>
                                                <td className='text-end'>
                                                    <Link className='text-uppercase btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2' to={'/customer/detail/invoice'}>
                                                        download
                                                    </Link>
                                                </td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                                {/* end::Table body */}
                            </table>
                        </div>
                        {/* end::Table */}
                    </div>
                    {/* end::Tap pane */}
                </div>
            </div>
            {/* end::Body */}
        </div>
    );
}

export default CustomerInvoiceComponent;