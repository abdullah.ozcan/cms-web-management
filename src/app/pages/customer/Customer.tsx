import { FC } from 'react'
import { PageTitle } from '../../../_metronic/layout/core'
import { KTCard } from '../../../_metronic/helpers'
import CustomerHeader from './customer-header/CustomerHeader';
import CustomerList from './CustomerList';


const CustomerWrapper: FC = () => {
    return (
        <>
            <PageTitle breadcrumbs={[]}>Customer</PageTitle>
            <KTCard>
              <CustomerList/>
            </KTCard>
        </>
    )
}

export default CustomerWrapper;
