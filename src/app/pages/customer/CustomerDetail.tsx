import {FC, useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import {getCustomerByIdCustomerAPI} from "../../../api/CustomerApi";
import {toAbsoluteUrl} from "../../../_metronic/helpers";
import {PageTitle} from "../../../_metronic/layout/core";
import {getOrderCustomersApi} from "../../../api/OrderApi";
import Pagination from "@vlsergey/react-bootstrap-pagination";

type CustomerType = {
    id: number,
    name: string,
    profile: string,
    email: string,
    description: string,
    address1: string,
    address2: string,
    country: string,
    province: string,
    postCode: string,
    language: string,
    job: string,
    taxNo: string,
    contactPhone: string,
    credieBalanceAmount: number,
    status: string,
    isActive: boolean,
    createdDate: string,
    updatedDate: string
    imageUrl: string;
}
type OrderType = {
    address1: string;
    address2: string;
    agentId: number;
    agentName: string;
    carId: number;
    city: string;
    createdDate: string;
    currentMile: number;
    customerId: number;
    customerName: string;
    description: string;
    distantTotalMile: number;
    dropPlace: string;
    dropTime: string;
    employeeId: number;
    endMile: number;
    id: number;
    isPayment: boolean;
    name: string;
    oilAVG: number;
    oilPercent: number;
    paymentAmount: number;
    paymentDate: string;
    paymentDescription: string;
    paymentSlip: string;
    paymentType: string;
    phone: string;
    pickUpPlace: string;
    pickUpTime: string;
    postCode: string;
    pricePerDay: number;
    province: string;
    rentType: string;
    startMile: number;
    status: string;
    totalDate: number;
    updatedDate: string;
    customers: CustomersType;
}
type CustomersType = {
    "id": number;
    "name": string;
    "email": string;
}

const CustomerDetail: FC = () => {

    let {id} = useParams();

    const [customer, setCustomer] = useState<CustomerType | null>(null);
    const [orderList, setOrderList] = useState<OrderType[] | null>([]);
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [total, setTotal] = useState<number>(0);
    const [totalPage, setTotalPage] = useState<number>(0);

    const [pageInvoice, setPageInvoice] = useState<number>(1);
    const [pageSizeInvoice, setPageSizeInvoice] = useState<number>(10);
    const [totalInvoice, setTotalInvoice] = useState<number>(0);
    const [totalPageInvoice, setTotalPageInvoice] = useState<number>(0);
    const [orderListInvoice, setOrderListInvoice] = useState<OrderType[] | null>([]);

    //
    const getCustomerDetail = async () => {
        try {
            const result = await getCustomerByIdCustomerAPI({
                id: {
                    id: id || ''
                }
            })

            if (result) {
                const {data} = result;

                console.log('data:', data)
                setCustomer(data)
            }
        } catch (e: any) {
            console.log('error', e)
        }
    }

    const formatPhone = (phone : any) => {
        let cleaned = ('' + phone).replace(/\D/g, '');
        var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
        if(match){
            return match[1] + '-' + match[2] + '-' + match[3]
        }
        return '-'
    }

    const getOrderCustomersList = async (page: number, pageSize: number) => {
        try {
            const body: any = {
                page: page,
                pageSize: pageSize,
                customerId: id
            }
            const result = await getOrderCustomersApi(body)
            if (result?.data) {
                const {total, data, totalPages} = result?.data;
                setOrderList(data)
                setTotalPage(totalPages)
                setTotal(total)
            }
        } catch (e) {
            console.log('e')
        }
    }

    const getOrderInVoiceCustomersList = async (page: number, pageSize: number) => {
        try {
            const body: any = {
                page: page,
                pageSize: pageSize,
                customerId: id
            }
            const result = await getOrderCustomersApi(body)
            if (result?.data) {
                const {total, data, totalPages} = result?.data;
                setOrderListInvoice(data)
                setTotalPageInvoice(totalPages)
                setTotalInvoice(total)
            }
        } catch (e) {
            console.log('e')
        }
    }

    console.log(orderListInvoice)

    const handleChangePage = async (value:any) => {
        setPage(Number(value?.target?.value))
        await getOrderCustomersList(Number(value?.target?.value), pageSize)
    }

    const handleChangePageInVoice = async (value:any) => {
        setPage(Number(value?.target?.value))
        await getOrderInVoiceCustomersList(Number(value?.target?.value), pageSize)
    }

    useEffect(() => {
        getCustomerDetail()
        getOrderCustomersList(page, pageSize);
        getOrderInVoiceCustomersList(pageInvoice, pageSizeInvoice)
    }, [])
    return (
        <>
            <PageTitle breadcrumbs={[]}>Customer Detail</PageTitle>
            <div className='row g-5 g-xxl-8'>
                <div className='col-xl-3'>
                    {/* <ProfileInformationComponent className='mb-5 mb-xxl-8' /> */}
                    <div className="card p-2">
                        {/* begin::Body */}
                        <div className='flex-grow-1'>
                            <div className='d-flex justify-content-center align-items-center flex-wrap mb-2'>
                                <div className='d-flex flex-column'>
                                    <div className='me-7 mb-4 mt-5'>
                                        <div
                                            className='symbol symbol-100px symbol-lg-160px symbol-fixed position-relative px-2'>
                                            <img src={customer?.imageUrl}
                                                 className='rounded-circle' alt='customer'/>
                                        </div>
                                    </div>
                                    <div className='d-flex justify-content-start flex-column'>
                                        <a href='#'
                                           className='text-gray-800 text-hover-primary fs-2 fw-bolder me-1 text-center'>
                                            {customer?.name}
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className='mt-4 mx-auto'>
                                <div className='accordion-body'>
                                    <span className='badge badge-light-secondary p-5'>Premium user</span>
                                    <div className='d-flex flex-column flex-wrap align-items-start p-5'>
                                        <div className='mt-4' id='kt_signin_email'>
                                            <div className='fs-6 fw-bolder mb-1'>Account ID</div>
                                            <div className='fw-bold text-gray-600'>#{customer?.id}</div>
                                        </div>
                                        <div className='mt-4' id='kt_signin_email'>
                                            <div className='fs-6 fw-bolder mb-1'>Email Address</div>
                                            <div className='fw-bold text-gray-600'>{customer?.email}</div>
                                        </div>
                                        <div className='mt-4' id='kt_signin_email'>
                                            <div className='fs-6 fw-bolder mb-1'>Phone Number</div>
                                            <div className='fw-bold text-gray-600'>{formatPhone(customer?.contactPhone)}</div>
                                        </div>
                                        <div className='mt-4'>
                                            <div className='fs-6 fw-bolder mb-1'>Age</div>
                                            <div className='fw-bold text-gray-600'>{'-'}</div>
                                        </div>
                                        <div className='mt-4' id='kt_signin_email'>
                                            <div className='fs-6 fw-bolder mb-1'>Year of birth</div>
                                            <div className='fw-bold text-gray-600'>{'-'}</div>
                                        </div>
                                        <div className='mt-4' id='kt_signin_email'>
                                            <div className='fs-6 fw-bolder mb-1'>Address</div>
                                            <div className='fw-bold text-gray-600'>{customer?.address1 + " " + customer?.address2 + " " + customer?.postCode}</div>
                                        </div>
                                        <div className='mt-4' id='kt_signin_email'>
                                            <div className='fs-6 fw-bolder mb-1'>Job</div>
                                            <div className='fw-bold text-gray-600'>{customer?.job}</div>
                                        </div>
                                        <div className='mt-4' id='kt_signin_email'>
                                            <div className='fs-6 fw-bolder mb-1'>Language</div>
                                            <div className='fw-bold text-gray-600'>{customer?.language}</div>
                                        </div>
                                        <div className="mt-4" id="kt_signin_email">
                                            <div className="fs-6 fw-bolder mb-1">Description</div>
                                            <div className="fw-bold text-gray-600">{customer?.description}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* end::Body */}
                    </div>
                </div>
                <div className='col-xl-9'>
                    <div className={`card mb-5 mb-xxl-8`}>
                        {/* begin::Header */}
                        <div className='card-header border-0 pt-5'>
                            <h3 className='card-title align-items-start flex-column'>
                                <span className='card-label fw-bolder fs-3 mb-1'>Payment records</span>
                            </h3>
                            <div
                                className='card-toolbar'
                                data-bs-toggle='tooltip'
                                data-bs-placement='top'
                                data-bs-trigger='hover'
                                title='Click to add a user'
                            >
                            </div>
                        </div>
                        {/* end::Header */}
                        {/* begin::Body */}
                        <div className='card-body py-3'>
                            {/* begin::Table container */}
                            <div className='table-responsive'>
                                {/* begin::Table */}
                                <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
                                    {/* begin::Table head */}
                                    <thead>
                                    <tr className='fw-bolder text-muted'>
                                        <th className='min-w-150px text-capitalize'>Invoice No</th>
                                        <th className='min-w-140px text-capitalize'>Status</th>
                                        <th className='min-w-120px text-capitalize'>Amount</th>
                                        <th className='min-w-120px text-capitalize'>Date</th>
                                        <th className='min-w-50px text-end text-capitalize'>Action</th>
                                    </tr>
                                    </thead>
                                    {/* end::Table head */}
                                    {/* begin::Table body */}
                                    <tbody>
                                    {
                                        orderList && orderList.map(((value, index) =>
                                            <tr key={`payment_detail_${index}`}>
                                                <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                #{value?.id}
                                            </span>
                                                </td>
                                                <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                {
                                                    value.status === 'SUCCESS' ?
                                                        <span className="badge badge-success">SUCCESS</span>
                                                        :
                                                        <span className="badge badge-light">PENDING</span>
                                                }
                                            </span>
                                                </td>
                                                <td>
                                            <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                {value.paymentAmount}
                                            </span>
                                                </td>
                                                <td>
                                                    <span className='text-muted fw-bold text-muted d-block fs-7'>
                                                         {value.createdDate}
                                                    </span>
                                                </td>
                                                <td className={'text-end '}>
                                                    <Link
                                                        className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2'
                                                        to={'/customer/detail/invoice'}>
                                                        VIEW
                                                    </Link>
                                                </td>
                                            </tr>))
                                    }
                                    </tbody>
                                    {/* end::Table body */}
                                </table>
                                {/* end::Table */}
                            </div>
                            <div className='d-flex justify-content-end mt-2'>
                                <Pagination
                                    value={page}
                                    firstPageValue={1}
                                    onChange={handleChangePage}
                                    totalPages={totalPage}/>
                            </div>
                            {/* end::Table container */}
                        </div>
                        {/* begin::Body */}
                    </div>
                    <div className={`card mb-5 mb-xxl-8`}>
                        {/* begin::Header */}
                        <div className='card-header border-0 pt-5'>
                            <h3 className='card-title align-items-start flex-column'>
                                <span className='card-label fw-bolder fs-3 mb-1'>Invoice</span>
                            </h3>
                        </div>
                        {/* end::Header */}
                        {/* begin::Body */}
                        <div className='card-body py-3'>
                            <div className='tab-content'>
                                {/* begin::Tap pane */}
                                <div className='tab-pane fade show active' id='kt_table_widget_5_tab_1'>
                                    {/* begin::Table container */}
                                    <div className='table-responsive'>
                                        {/* begin::Table */}
                                        <table className='table table-row-dashed table-row-gray-200 align-middle gs-0 gy-4'>
                                            {/* begin::Table head */}
                                            <thead>
                                            <tr className='border-0'>
                                                <th className='p-0 min-w-110px'></th>
                                                <th className='p-0 min-w-150px'></th>
                                                <th className='p-0 min-w-140px'></th>
                                                <th className='p-0 min-w-110px'></th>
                                                <th className='p-0 min-w-50px'></th>
                                            </tr>
                                            </thead>
                                            {/* end::Table head */}
                                            {/* begin::Table body */}
                                            <tbody>
                                            {
                                                orderListInvoice && orderListInvoice.map((value, index) => (
                                                    <tr key={`invoce_index${index}`}>
                                                        <td>
                                                            <span className='text-dark fw-bold d-block'>#{new Date().getTime()}{value?.id}</span>
                                                        </td>
                                                        <td>
                                                            <span className='text-dark fw-bold d-block'>{value.paymentAmount}</span>
                                                        </td>
                                                        <td className='text-end text-muted fw-bold'>{value.createdDate}</td>
                                                        <td className='text-end'>
                                                            <span className='badge badge-light-success'>{value?.status === 'SUCCESS'? 'Approved': 'PENDING'}</span>
                                                        </td>
                                                        <td className='text-end'>
                                                        <Link className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2' to={'/customer/detail/invoice'}>
                                                                VIEW
                                                            </Link>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                            {/* end::Table body */}
                                        </table>
                                    </div>
                                    {/* end::Table */}
                                </div>
                            </div>
                        </div>
                        <div className='d-flex justify-content-end mt-2 mb-5 me-7'>
                            <Pagination
                                value={pageInvoice}
                                firstPageValue={1}
                                onChange={handleChangePageInVoice}
                                totalPages={totalPageInvoice}/>
                        </div>
                        {/* end::Body */}
                    </div>

                </div>
            </div>
        </>
    );
}

export default CustomerDetail;
