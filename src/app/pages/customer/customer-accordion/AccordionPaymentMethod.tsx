import { FC } from 'react';
import { Accordion } from 'react-bootstrap';
import { KTSVG } from '../../../../_metronic/helpers';

type Prop = {
    title: string
    name: string
    number: string
    expires: string
    type: string
    issuer: string
    id: string
    biladd: string
    phone?: string
    email: string
    origin: string
    cvcheck: any
}

const AccordionPaymentMethod: FC<Prop> = ({ title, name, number, expires, type, issuer, id, biladd, phone, email, origin, cvcheck }) => {
    return (
        <div className='mb-1'>
            <Accordion>
                <Accordion.Item eventKey='0'>
                    <Accordion.Header>
                        <div className='d-flex justify-content-between align-items-center w-100'>
                            <div>
                                <span className='fw-bold fs-5'>{title}</span><br />
                                <span className='text-muted'>Expires {expires}</span>
                            </div>
                            <div className='px-3'>
                                <button
                                    type='button'
                                    className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-2'
                                >
                                    <KTSVG
                                        path='/media/icons/duotune/art/art005.svg'
                                        className='svg-icon-2 m-0'
                                    />
                                </button>
                                <button
                                    type='button'
                                    className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-2'
                                >
                                    <KTSVG
                                        path='/media/icons/duotune/general/gen027.svg'
                                        className='svg-icon-2 m-0'
                                    />
                                </button>
                                <button
                                    type='button'
                                    className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-2'
                                >
                                    <KTSVG
                                        path='/media/icons/duotune/general/gen019.svg'
                                        className='svg-icon-2 m-0'
                                    />
                                </button>
                            </div>
                        </div>
                    </Accordion.Header>
                    <Accordion.Body>
                        <table className='row border-bottom'>
                            <tbody className='col-xl-6 mb-6'>
                                <tr className=''>
                                    <td className='fw-bold fs-5 w-200px'>Name</td>
                                    <td className='fw-bold fs-5'>{name}</td>
                                </tr>
                                <tr>
                                    <td className='fw-bold fs-5 w-200px'>Number</td>
                                    <td className='fw-bold fs-5'>{number}</td>
                                </tr>
                                <tr>
                                    <td className='fw-bold fs-5 w-200px'>Expires</td>
                                    <td className='fw-bold fs-5'>{expires}</td>
                                </tr>
                                <tr>
                                    <td className='fw-bold fs-5 w-200px'>Type</td>
                                    <td className='fw-bold fs-5'>{type}</td>
                                </tr>
                                <tr>
                                    <td className='fw-bold fs-5 w-200px'>Issuer</td>
                                    <td className='fw-bold fs-5'>{issuer}</td>
                                </tr>
                                <tr>
                                    <td className='fw-bold fs-5 w-200px'>ID</td>
                                    <td className='fw-bold fs-5'>{id}</td>
                                </tr>
                            </tbody>
                            <tbody className='col-xl-6'>
                                <tr>
                                    <td className='fw-bold fs-5 w-200px'>Billing address</td>
                                    <td className='fw-bold fs-5'>{biladd}</td>
                                </tr>
                                <tr>
                                    <td className='fw-bold fs-5 w-200px'>Phone</td>
                                    <td className='fw-bold fs-5'>{phone || 'No phone provided'}</td>
                                </tr>
                                <tr>
                                    <td className='fw-bold fs-5 w-200px'>Email</td>
                                    <td className='fw-bold fs-5'>{email}</td>
                                </tr>
                                <tr>
                                    <td className='fw-bold fs-5 w-200px'>Origin</td>
                                    <td className='fw-bold fs-5'>{origin}</td>
                                </tr>
                                <tr>
                                    <td className='fw-bold fs-5 w-200px'>CVC Check</td>
                                    <td className='fw-bold fs-5'>
                                        {cvcheck}
                                        <KTSVG
                                            path='/media/icons/duotune/general/gen043.svg'
                                            className='svg-icon svg-icon-2'
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
        </div>
    )
}

export default AccordionPaymentMethod;