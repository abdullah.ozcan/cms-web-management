/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { searchMenuPI, searchRoleAPI } from "../../../api/RoleApi";
import { KTSVG } from "../../../_metronic/helpers";
import ModalUpdateUserRole from "../../../_metronic/layout/components/ModalUpdateUserRole";
import ModalUpdateRole from "./ModalUpdateRole";

type Props = {
  className: string;
};

type UserRole = {
  id: number;
  name: string;
  isActive: number;
  createdDate: string;
  updatedDate: string;
  menuId: number;
};

type Menu = {
  id: number;
  title: string;
  isActive: boolean;
  createdDate: string;
  updatedDate: string;
  roles: UserRole[];
};

const UserRoleTablecomponent: React.FC<Props> = ({ className }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [id, setId] = useState<number>(0);
  const [roleInfo, setRoleInfo] = useState<UserRole[]>([]);

  const [roleList, setRoleList] = useState<Menu[] | null>(null);

  const [menuList, setMenuList] = useState<Menu[]>([]);

  const [menuItem, setMenuItem] = useState<Menu|null>(null);

  const handleGetMenu = async () => {
    try {
      const result = await searchMenuPI();

      if (result?.data) {
        const { data } = result;
        setMenuList(data);
      }
    } catch (e: any) {
      console.log("error", e);
    }
  };

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const handleGetRole = async (key: string) => {
    try {
      const result = await searchRoleAPI({ body: { keyWord: key } });

      if (result?.data) {
        const { data } = result;
        
        setRoleList(data);
      }
    } catch (e: any) {
      console.log("error", e);
    }
  };
  useEffect(() => {
    handleGetRole(searchTerm);
    handleGetMenu();
  }, []);

  const handleShowAndSetValue = (id: number, roleList: UserRole[]) => {
    setId(id);
    setRoleInfo(roleList);
    setShow(true);
  };

  const handleSearchRole = (value: string) => {
    setSearchTerm(value);
    handleGetRole(value);
  };

  return (
    <div className={`card ${className}`}>
      {/* begin::Header */}
      <div className="card-header border-0 flex-row pt-5">
        <div className="card-title">
          {/* begin::Search */}
          <div className="d-flex align-items-center position-relative my-1">
            <KTSVG
              path="/media/icons/duotune/general/gen021.svg"
              className="svg-icon-1 position-absolute ms-6"
            />
            <input
              type="text"
              data-kt-user-table-filter="search"
              className="form-control form-control-solid w-250px ps-14"
              placeholder="Search Permission"
              value={searchTerm}
              onChange={(e) => handleSearchRole(e.target.value)}
            />
          </div>
          {/* end::Search */}
        </div>
        <div className="card-toolbar">
          <div
            className="d-flex justify-content-end"
            data-kt-user-table-toolbar="base"
          >
            {/* <button type="button" className="btn btn-light-primary">
              <KTSVG
                path="/media/icons/duotune/arrows/arr075.svg"
                className="svg-icon-3"
              />
              Add Permission
            </button> */}
          </div>
        </div>
      </div>
      {/* end::Header */}
      {/* begin::Body */}
      <div className="card-body py-3">
        {/* begin::Table container */}
        <div className="table-responsive">
          {/* begin::Table */}
          <table className="table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3">
            {/* begin::Table head */}
            <thead>
              <tr className="fw-bolder text-muted fs-7">
                <th className="min-w-150px">NAME</th>
                <th className="min-w-140px">ASSIGNED TO</th>
                <th className="min-w-120px">CREATED DATE</th>
                <th className="min-w-100px text-end">ACTIONS</th>
              </tr>
            </thead>
            {/* end::Table head */}
            {/* begin::Table body */}
            <tbody>
              {roleList &&
                roleList.map((value, index) => (
                  <tr key={`index_of_user_role${index}`}>
                    <td>
                      <a
                        href="#"
                        className="text-dark fw-bolder text-muted text-hover-primary fs-6"
                      >
                        {value.title}
                      </a>
                    </td>
                    <td>
                      {value &&
                        value.roles.map((value) => (
                          <button className="btn btn-sm btn-light me-3">
                            {value.name}
                          </button>
                        ))}
                    </td>
                    <td>
                      <a
                        href="#"
                        className="text-dark fw-bolder text-muted text-hover-primary d-block mb-1 fs-6"
                      >
                        {value.createdDate}
                      </a>
                    </td>
                    <td className="text-end">
                      <button
                        type="button"
                        className="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1"
                        onClick={() => {
                          handleShowAndSetValue(value.id, value.roles);
                          setMenuItem(value)
                        }}
                      >
                        <KTSVG
                          path="/media/icons/duotune/general/gen019.svg"
                          className="svg-icon-3"
                        />
                      </button>
                      <ModalUpdateUserRole
                        show={show}
                        handleClose={handleClose}
                        title={"Update Role"}
                        chil={
                          <ModalUpdateRole
                            handleClose={handleClose}
                            id={id}
                            menu={menuList}
                            roleList={roleInfo}
                            itemMenu={menuItem}
                          />
                        }
                      />
                      {/* <a href='#' className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm'>
                        <KTSVG path='/media/icons/duotune/general/gen027.svg' className='svg-icon-3' />
                      </a> */}
                    </td>
                  </tr>
                ))}
            </tbody>
            {/* end::Table body */}
          </table>
          {/* end::Table */}
        </div>
        {/* end::Table container */}
      </div>
      {/* begin::Body */}
    </div>
  );
};

export default UserRoleTablecomponent;
