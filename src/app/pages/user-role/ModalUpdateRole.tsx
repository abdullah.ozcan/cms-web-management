import _ from 'lodash';
import { FC, useEffect, useState } from 'react'
import { Button } from 'react-bootstrap';
import { listMasterRoleApi, listUserRoleApi, searchMenuPI, updateRoleApi } from '../../../api/RoleApi';
import { KTSVG } from '../../../_metronic/helpers';
type Props = {
    id: number,
    menu?: Menu[],
    roleList?: UserRole[];
    handleClose?: () => void;
    handleConfirm?: () => void;
    itemMenu?:Menu | null

}
type Menu = {
    "id": number;
    "title": string;
    "isActive": boolean,
    "createdDate": string;
    "updatedDate": string;
}
type UserRole = {
    "id": number
    "name": string;
    "isActive": number;
    "createdDate": string;
    "updatedDate": string;
    "menuId": number;
}
const ModalUpdateRole: FC<Props> = ({ id, menu, roleList, handleClose, handleConfirm, itemMenu }) => {


    const [checkList, setCheckList] = useState<number[]>([])
    const [checkListName, setCheckListName] = useState<string[]>([])

    const [role, setRole] = useState<UserRole[] | undefined>([])

    const [title, setTitle] = useState<string[] | undefined>([])
    const [listRole, setListRole] = useState<any>(null);


    const handleGetRole = async (key: string) => {
        try {
         const result = await listMasterRoleApi();

    
          if (result?.data) {
            const { data } = result;
            setListRole(data);
          }
        } catch (e: any) {
          console.log("error", e);
        }
      };
    useEffect(() => {
        const getIdList =roleList &&  roleList?.map((d)=>d.name);
        if(getIdList) {
            setCheckList(roleList.map((d)=>d.id));
            setCheckListName(roleList.map((d)=>d.name));

        }

        setRole(roleList)
        handleAddTitle()
        handleGetRole('')
        
    }, [])    
    
    const handleAddTitle = () => {
        let list: string[] = [];
        roleList?.map((d) => {
            list.push(d.name);
        })
        setTitle(list)
    }
    const handleUpdate = async () => {

        let valueRoleInfo:any[] = []
        if(listRole && listRole.length > 0) {
            listRole.map((value:any) => {
                valueRoleInfo.push(value)
            })
        }

        let listid:number[] = []

        checkListName.map((value:string) => {
            const find = listRole.find((d:any) => d.name ===value)

            if(find) {
                listid.push(find?.id)
            }
        })
        
        const bodyReq = {
            roleListId: listid,
            menuId: itemMenu?.id,
            menu:listRole
        }

        
        await updateRoleApi(bodyReq)

        window.location.href = '/usersRole'

    }
    const handleChange = (id: number) => {

        const find = _.findIndex(checkList, function (o) { return o === id; });
        if (find === -1) {
            const newList = checkList;
            newList.push(id)

            const result = _.uniq(newList)

            setCheckList(result)
        } else {
            const evens = checkList.filter((d) => d !== id)
            setCheckList(evens)
        }

    }

    const handleChangeSelect = (name: string) => {

        const find = _.findIndex(checkListName, function (o) { return o === name; });
        if (find === -1) {
            const newList = checkListName;
            newList.push(name)

            const result = _.uniq(newList)

            setCheckListName(result)
        } else {
            const evens = checkListName.filter((d) => d !== name)
            setCheckListName(evens)
        }

    }
    console.log('checkList:', checkList);

    return (
        <div className=''>
            <div className='row mb-4'>
                <label className='col-lg-4 col-form-label fw-bold fs-4'>
                    <span className='required'>Menu</span>
                </label>
                <div className='col-lg-12 fv-row'>
                    <input
                        type='text'
                        className='form-control form-control-solid'
                        placeholder='Administrator'
                        disabled
                        value={itemMenu?.title}
                    />
                </div>
            </div>
            <div className='row'>
                <label className='col-lg-12 col-form-label fw-bold fs-4'>
                    <span>Role Permissions</span>
                </label>
            </div>
            {/* <div className='row mb-4'>
                <div className='d-flex align-items-center'>
                    <label className='col-lg-4 col-form-label fs-6'>
                        <span className='me-2'>Administrator Access</span>
                        <KTSVG
                            path="/media/icons/duotune/general/gen045.svg"
                            className='svg-icon-3'
                        />
                    </label>
                    <div className='form-check form-check-solid'>
                        <input
                            className='form-check-input'
                            type='checkbox'
                            value='1'
                        />
                        <label className='text-muted fs-6'>Select all</label>
                    </div>
                </div>
            </div> */}
            {
                listRole && listRole.map((value:any, index:number) => (
                    <div className='row mb-4' key={`index_of_menu_${index}`}>
                        <div className='d-flex align-items-center'>
                            <label className='col-lg-4 col-form-label fs-6'>
                                <span className='me-2'>{value.name}</span>
                            </label>
                            <div className='col-lg-2 form-check form-check-solid'>
                                <input
                                    className='form-check-input'
                                    type='checkbox'
                                    value={value.name}
                                    checked={checkListName.indexOf(value.name) !== -1}
                                    onChange={() => { handleChangeSelect(value.name) }}
                                />
                                <label className='text-muted fs-6'>Enable</label>
                            </div>
                            {/* <div className='col-lg-2 form-check form-check-solid'>
                                <input
                                    className='form-check-input'
                                    type='checkbox'
                                    value='1'
                                />
                                <label className='text-muted fs-6'>Write</label>
                            </div>
                            <div className='col-lg-2 form-check form-check-solid'>
                                <input
                                    className='form-check-input'
                                    type='checkbox'
                                    value='1'
                                />
                                <label className='text-muted fs-6'>Create</label>
                            </div> */}
                        </div>
                    </div>
                ))
            }
            <div className='d-flex flex-row justify-content-end'>
                <Button variant="secondary" onClick={handleClose}>
                    Discord
                </Button>
                <Button variant="primary" onClick={handleUpdate} className='mx-2'>
                    Submit
                </Button>
            </div>


        </div>
    )
}

export default ModalUpdateRole;