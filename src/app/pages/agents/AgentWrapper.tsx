import { FC } from "react";
import { KTCard } from "../../../_metronic/helpers";
import { PageTitle } from "../../../_metronic/layout/core";
import AgentHeader from "./AgentHeader";
import AgentList from "./AgentList";

const AgentWrapper:FC = () => {
    return (
        <>
            <PageTitle breadcrumbs={[]}>Agents</PageTitle>
            <KTCard>
                <AgentList className={""}/>
            </KTCard>
        </>
    )
}

export default AgentWrapper;