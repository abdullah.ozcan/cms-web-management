import { useEffect, useState } from "react"
import Pagination from "@vlsergey/react-bootstrap-pagination"
import { Link, useParams } from "react-router-dom"
import { getOrderAgentsByIdAgentAPI } from "../../../api/AgentsApi"
import { KTSVG } from "../../../_metronic/helpers"
type Props = {
    className: string
}
type CarType = {
    id: number,
    title: string,
    currentVin: string,
    beforeVin: string,
    brand: string,
    model: string,
    year: string,
    regisDate: string,
    regisVinDate: string,
    age: number,
    vinNo: string,
    mile: number,
    fixTimes: number,
    description: string,
    status: string,
    isActive: boolean,
    createdDate: string,
    updatedDate: string
}
type AgentDetailType = {
    id: number,
    name: string,
    address: string,
    amount: number,
    creditAmount: number,
    contactName: string,
    taxNo: string,
    agentCode: string,
    contactPhone: string,
    status: string,
    isActive: boolean,
    createdDate: string,
    updatedDate: string
}
type AgentType = {
    id: number,
    totalDate: number,
    rentType: string,
    phone: string,
    name: string,
    agentId: number,
    customerId: number,
    carId: number,
    pricePerDay: number,
    employeeId: number,
    agentName: string,
    pickUpPlace: string,
    pickUpTime: string,
    dropPlace: string,
    dropTime: string,
    description: string,
    paymentType: string,
    isPayment: boolean,
    paymentAmount: number,
    paymentDate: string,
    paymentSlip: string,
    paymentDescription: string,
    currentMile: number,
    distantTotalMile: number,
    startMile: number,
    endMile: number,
    oilPercent: number,
    oilAVG: number,
    customerName: string,
    address1: string,
    address2: string,
    city: string,
    province: string,
    postCode: string,
    status: string,
    createdDate: string,
    updatedDate: string,
    agents: AgentDetailType,
    cars: CarType
}
const AgentsTable: React.FC<Props> = ({ className }) => {
    let {id} = useParams();

    const [page, setPage] = useState<number>(1)
    const [pageSize, setPageSize] = useState<number>(10)
    const [total, setTotal] = useState<number>(0)
    const [totalPages, setTotalPages] = useState<number>(0)

    const [customerHistory, setCustomerHistory] = useState<AgentType[] | null>([])

    const getAgentList = async (page: number, pageSize: number) => {
        try {
            const body: any = {
                page: page,
                pageSize: pageSize,
                agentId: id
            }
            const result = await getOrderAgentsByIdAgentAPI(body)

            if(result?.data){
                const {total, totalPages, data} = result?.data;
                setTotal(total)
                setTotalPages(totalPages)
                setCustomerHistory(data)
            }
        } catch (e: any) {
            console.log('error', e)
        }
    }

    const handleChangePage = async (value:any) => {
        setPage(Number(value?.target?.value))
        await getAgentList(Number(value?.target?.value), pageSize)
    }

    const numberWithCommas = (x : any) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    useEffect(() => {
        getAgentList(page, pageSize)
    })
    return (
        <div className={`card ${className}`}>
            <div className='card-header border-0 pt-6'>
                <div className='card-title'>
                    <span className='card-label fw-bolder fs-3 mb-1'>History</span>
                </div>
                {/* begin::Card toolbar */}
                <div className='card-toolbar'>
                    <div className='d-flex justify-content-end w-100' data-kt-user-table-toolbar='base'>
                        {/*<button type='button'*/}
                        {/*        className='btn btn-primary mx-1'*/}
                        {/*        data-bs-toggle='modal'*/}
                        {/*        data-bs-dismiss='modal'*/}
                        {/*        data-bs-target='#kt_modal_1_add'*/}
                        {/*>*/}
                        {/*  Add Case*/}
                        {/*</button>*/}
                    </div>
                    <div className='modal fade' tabIndex={-1} id='kt_modal_1_add'>
                        <div className='modal-dialog modal-lg'>
                            <div className='modal-content'>
                                <div className='modal-header'>
                                    <h5 className='modal-title'>Add Case</h5>
                                    <div
                                        className='btn btn-icon btn-sm btn-active-light-primary ms-2'
                                        data-bs-dismiss='modal'
                                        aria-label='Close'
                                    >
                                        <KTSVG
                                            path='/media/icons/duotune/arrows/arr061.svg'
                                            className='svg-icon svg-icon-2x'
                                        />
                                    </div>
                                </div>
                                <div className='modal-body'>
                                    <div className='row mb-4'>
                                        <div className='col-lg-6 fv-row'>
                                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                                <span className=''>วันที่ทำรายการ</span>
                                            </label>
                                            <input
                                                type='text'
                                                className='form-control form-control-lg form-control-solid'
                                                placeholder=''
                                            />
                                        </div>
                                        <div className='col-lg-6 fv-row'>
                                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                                <span className=''>เลือกประเภท</span>
                                            </label>
                                            <input
                                                type='text'
                                                className='form-control form-control-lg form-control-solid'
                                                placeholder=''
                                            />
                                        </div>
                                    </div>
                                    <div className='row mb-4'>
                                        <div className='col-lg-6 fv-row'>
                                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                                <span className=''>ยอดเงินที่ยืมไป</span>
                                            </label>
                                            <input
                                                type='text'
                                                className='form-control form-control-lg form-control-solid'
                                                placeholder=''
                                            />
                                        </div>
                                        <div className='col-lg-6 fv-row'>
                                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                                <span className=''>วันที่ยืมไป</span>
                                            </label>
                                            <input
                                                type='text'
                                                className='form-control form-control-lg form-control-solid'
                                                placeholder=''
                                            />
                                        </div>
                                    </div>
                                    <div className='row mb-4'>
                                        <div className='col-lg-12 fv-row'>
                                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                                <span className=''>หมายเหตุ</span>
                                            </label>
                                            <textarea
                                                rows={4}
                                                className='form-control form-control-lg form-control-solid'
                                                placeholder=''
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className='modal-footer'>
                                    <button
                                        type='button'
                                        className='btn btn-light'
                                        data-bs-dismiss='modal'
                                    >
                                        Discard
                                    </button>
                                    <button type='button' className='btn btn-primary'>
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end::Card toolbar */}
                <div className='modal fade' tabIndex={-1} id='kt_modal_1_add'>
                    <div className='modal-dialog'>
                        <div className='modal-content'>
                            <div className='modal-header'>
                                <h5 className='modal-title'>Add Agent</h5>
                                <div
                                    className='btn btn-icon btn-sm btn-active-light-primary ms-2'
                                    data-bs-dismiss='modal'
                                    aria-label='Close'
                                >
                                    <KTSVG
                                        path='/media/icons/duotune/arrows/arr061.svg'
                                        className='svg-icon svg-icon-2x'
                                    />
                                </div>
                            </div>
                            <div className='modal-body'>
                                <div className='row mb-4'>
                                    <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                        <span className='required'>Name</span>
                                    </label>
                                    <div className='col-lg-12 fv-row'>
                                        <input
                                            type='text'
                                            className='form-control form-control-lg form-control-solid'
                                            placeholder='Name'
                                        />
                                    </div>
                                </div>
                                <div className='row mb-12'>
                                    <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                        <span className='required'>ID Type</span>
                                    </label>
                                    <div className='col-lg-12 fv-row'>
                                        <input
                                            type='tel'
                                            className='form-control form-control-lg form-control-solid'
                                            placeholder='ID Type'
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className='modal-footer'>
                                <button
                                    type='button'
                                    className='btn btn-light'
                                    data-bs-dismiss='modal'
                                >
                                    Discard
                                </button>
                                <button type='button' className='btn btn-primary'>
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* begin::Body */}
            <div className='card-body py-3'>
                {/* begin::Table container */}
                <div className='table-responsive'>
                    {/* begin::Table */}
                    <table className='table align-middle gs-0 gy-4'>
                        {/* begin::Table head */}
                        <thead>
                            <tr className='fw-bolder text-muted'>
                                <th className='ps-4 min-w-125px'>No</th>
                                <th className='min-w-125px'>Car</th>
                                <th className='min-w-125px'>Status</th>
                                <th className='min-w-125px'>Amount</th>
                                <th className='min-w-125px'>Date</th>
                                <th className='min-w-125px rounded-end'>Action</th>
                            </tr>
                        </thead>
                        {/* end::Table head */}
                        {/* begin::Table body */}
                        <tbody>
                            {
                                customerHistory && customerHistory.map((value, index) => (
                                    <tr key={`index_of_agents${index}`}>
                                        <td>
                                            <div className='d-flex align-items-center'>
                                                <div className='d-flex justify-content-start flex-column'>
                                                    <Link className='' to={'/customer/detail/invoice'}>
                                                        {'#'+value.id}
                                                    </Link>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <span className='text-dark fw-bold d-block fs-7 mt-1'>
                                                {value.cars?.title || '-'}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-dark fw-bold d-block fs-7 mt-1'>
                                                {
                                                    value.status === 'SUCCESS' ?
                                                    <span className="badge badge-success">SUCCESS</span>
                                                        :
                                                    <span className="badge badge-light">PENDING</span>
                                                }
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-dark fw-bold d-block fs-7 mt-1'>
                                                {numberWithCommas(value.paymentAmount)}
                                            </span>
                                        </td>
                                        <td>
                                            <span className='text-dark fw-bold d-block fs-7 mt-1'>
                                                {value.paymentDate}
                                            </span>
                                        </td>
                                        <td className=''>
                                            <Link to={'/customer/detail/invoice'} className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2'>
                                                VIEW
                                            </Link>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                        {/* end::Table body */}
                    </table>
                    {/* end::Table */}
                    <div className='d-flex justify-content-end mt-2'>
                        <Pagination
                            value={page}
                            firstPageValue={1}
                            onChange={handleChangePage}
                            totalPages={totalPages}/>
                    </div>
                </div>
                {/* end::Table container */}
            </div>
            {/* begin::Body */}
        </div>
    )
}

export default AgentsTable