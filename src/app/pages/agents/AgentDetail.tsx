import {FC, useEffect, useState} from "react"
import { toAbsoluteUrl } from "../../../_metronic/helpers"
import { PageTitle } from "../../../_metronic/layout/core"
import AgentsTable from "./AgentTable"
import {getAgentByIdAgentAPI} from "../../../api/AgentsApi";
import {useParams} from "react-router-dom";


type AgentType = {
  address: string;
  agentCode: string;
  amount: number;
  contactName: string;
  contactPhone: string;
  createdDate: string;
  creditAmount: number;
  id: number;
  isActive: number;
  name: string;
  status: string;
  taxNo: string;
  updatedDate: string;
}

const AgentDetail:FC = () => {
    let { id } = useParams();

    const [agent, setAgent ] = useState<AgentType| null>(null);

    const getAgentDetail = async () => {
      try {
        const result = await getAgentByIdAgentAPI({ id: id || '' })

        if(result) {
          const { data } = result;
          setAgent(data)

        }
      } catch (e: any) {
        console.log('error', e)
      }
    }

    const numberWithCommas = (x : any) => {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    const formatPhone = (phone : any) => {
        phone = phone + ('x'.repeat(10 - phone.length));
        let cleaned = ('' + phone).replace(/\D/g, '');
        var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
        if(match){
            return match[1] + '-' + match[2] + '-' + match[3]
        }
        return '-'
    }

    useEffect(() => {
      getAgentDetail()
    }, [])
    return (
        <>
          <PageTitle breadcrumbs={[]}>Agent Detail</PageTitle>
          <div className='row g-5 g-xxl-8'>
            <div className='col-xl-4'>
              <div className='mb-5 mb-xxl-8 bg-white'>
                {/* begin::Body */}
                <div className='flex-grow-1'>
                  <div className='d-flex justify-content-center align-items-center flex-wrap mb-2'>
                    <div className='d-flex flex-column mb-5'>
                      <div className='me-7 mb-4 mt-5'>
                        <div className='symbol symbol-100px symbol-lg-160px symbol-fixed position-relative'>
                          <img src={toAbsoluteUrl('/media/avatars/300-1.jpg')} alt='Metornic' />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='m-8'>
                  <div className='mt-4 row mx-auto' id='kt_signin_email'>
                    <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>ชื่อเอเจนต์</div>
                    <div className='col-6 col-lg-6 fw-bold text-gray-600'>{agent?.name}</div>
                  </div>
                  <div className='mt-4 row mx-auto' id='kt_signin_email'>
                    <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>จำนวนเงิน</div>
                    <div className='col-6 col-lg-6 fw-bold text-gray-600'>{numberWithCommas(agent?.amount || 0)} ฿</div>
                  </div>
                  <div className='mt-4 row mx-auto' id='kt_signin_email'>
                    <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>จำนวนครั้งที่ใช้</div>
                    <div className='col-6 col-lg-6 fw-bold text-gray-600'>{0} ครั้ง</div>
                  </div>
                  <div className='mt-4 row mx-auto' id='kt_signin_email'>
                    <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>จำนวนเครดิต</div>
                    <div className='col-6 col-lg-6 fw-bold text-gray-600'>{numberWithCommas(agent?.creditAmount || 0)} บาท</div>
                  </div>
                  <div className='mt-4 row mx-auto' id='kt_signin_email'>
                    <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>ที่อยู่</div>
                    <div className='col-6 col-lg-6 fw-bold text-gray-600'>{agent?.address}</div>
                  </div>
                  <div className='mt-4 row mx-auto' id='kt_signin_email'>
                    <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>เลขที่ผู้เสียภาษี</div>
                    <div className='col-6 col-lg-6 fw-bold text-gray-600'>{agent?.taxNo}</div>
                  </div>
                  <div className='mt-4 row mx-auto' id='kt_signin_email'>
                    <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>ชื่อบริษัท</div>
                    <div className='col-6 col-lg-6 fw-bold text-gray-600'>{agent?.contactName}</div>
                  </div>
                  <div className='mt-4 row mx-auto' id='kt_signin_email'>
                    <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>รหัส</div>
                    <div className='col-6 col-lg-6 fw-bold text-gray-600'>{agent?.agentCode || '-'}</div>
                  </div>
                  <div className='mt-4 row mx-auto' id='kt_signin_email'>
                    <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>วันที่เข้าร่วม</div>
                    <div className='col-6 col-lg-6 fw-bold text-gray-600'>{agent?.createdDate}</div>
                  </div>
                  <div className='mt-4 row mx-auto' id='kt_signin_email'>
                    <div className='col-6 col-lg-6 fs-6 fw-bolder mb-1'>เบอร์โทรติดต่อ</div>
                    <div className='col-6 col-lg-6 fw-bold text-gray-600'>{formatPhone(agent?.contactPhone || '-')}</div>
                  </div>
                  <div className='mt-4 row mx-auto mb-5' id='kt_signin_email'/>
                  <div className='mt-4 row mx-auto mb-5' id='kt_signin_email'/>
                </div>
              </div>
            </div>
            <div className='col-xl-8'>
              <div className='mb-5 mb-xxl-8'>
                <ul className="nav nav-tabs nav-line-tabs mb-5 fs-6">
                  <li className="nav-item p-2">
                    <a
                      className="nav-link active"
                      data-bs-toggle="tab"
                      href="#kt_tab_pane_1"
                    >
                      ดูภาพรวมทั้งหมด
                    </a>
                  </li>
                </ul>
                <div className="tab-content" id="myTabContent">
                  <div
                    className="tab-pane fade active show"
                    id="kt_tab_pane_1"
                    role="tabpanel"
                  >
                    <AgentsTable className={''}/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )
}

export default AgentDetail