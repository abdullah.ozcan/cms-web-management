import {FC, useEffect, useState} from "react";
import {Link} from "react-router-dom";
import {KTSVG} from "../../../_metronic/helpers";
import {createAgentAPI, searchAgentAPI} from "../../../api/AgentsApi";
import ModalApp from "../../../_metronic/layout/components/ModalApp";
import { useForm, Controller } from "react-hook-form";
import clsx from "clsx";
import {Button} from "react-bootstrap";
import { format } from "path";

type Props = {
    className: string
}
type AgentType = {
    address: string;
    agentCode: string;
    amount: number;
    contactName: string;
    contactPhone: string;
    createdDate: string;
    creditAmount: number;
    id: number;
    isActive: number;
    name: string;
    status: string;
    taxNo: string;
    updatedDate: string;
}

const AgentList: FC<Props> = ({className}) => {
    const {handleSubmit, formState: {errors}, control} = useForm({
        defaultValues: {
            name: '',
            address: '',
            amount: 0,
            creditAmount: 0,
            contactPhone: '',
            contactName: '',
            taxNo: '',
            agentCode: ''
        }
    });
    const [searchTerm, setSearchTerm] = useState('');

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleCreate = async (value: any) => {
        try {
            await createAgentAPI({
                body: {
                    name: value?.name,
                    address: value?.address,
                    amount: value?.amount,
                    creditAmount: value?.creditAmount,
                    contactName: value?.contactName,
                    taxNo: value?.taxNo,
                    agentCode: value?.agentCode,
                    contactPhone: value?.contactPhone,
                    status: '',
                    isActive: true,
                }
            });

            window.location.href = '/agents'
        } catch (e: any) {
            console.log('error', e)
        }
    }
    const ModalAddAgent = () => {
        return (
            <form onSubmit={handleSubmit(handleCreate)} noValidate id='kt_create_agent_form'>
                <div className='row mb-4'>
                    <label className='col-lg-4 col-form-label fw-bold fs-6'>
                        <span className='required'>Name</span>
                    </label>
                    <div className='col-lg-12 fv-row'>
                        <Controller
                            name="name"
                            control={control}
                            rules={{
                                required: 'name is require',
                            }}
                            render={({field: {value, onChange}}) =>
                                <input
                                    placeholder=''
                                    id="name"
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        'form-control form-control-lg form-control-solid',
                                        {
                                            'is-invalid': errors.name?.message && errors.name?.message,
                                        },
                                        {
                                            'is-valid': errors.name?.message && !errors.name?.message,
                                        }
                                    )}
                                    type='text'
                                    name='name'
                                    autoComplete='off'
                                />}
                        />
                        {errors.name?.message && (
                            <div className='fv-plugins-message-container text-danger'>
                                <span role='alert'>{errors.name?.message}</span>
                            </div>
                        )}
                    </div>
                </div>
                <div className='row mb-4'>
                    <label className='col-lg-4 col-form-label fw-bold fs-6'>
                        <span className='required'>Address</span>
                    </label>
                    <div className='col-lg-12 fv-row'>
                        <Controller
                            name="address"
                            control={control}
                            rules={{
                                required: 'address is require',
                            }}
                            render={({field: {value, onChange}}) =>
                                <input
                                    type='text'
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        'form-control form-control-lg form-control-solid',
                                        {
                                            'is-invalid': errors.address?.message && errors.address?.message,
                                        },
                                        {
                                            'is-valid': errors.address?.message && !errors.address?.message,
                                        }
                                    )}
                                    placeholder=''
                                    name='address'
                                    autoComplete='off'
                                />}
                        />
                        {errors.address?.message && errors.address?.message && (
                            <div className='fv-plugins-message-container text-danger'>
                                <span role='alert'>{errors.address?.message}</span>
                            </div>
                        )}
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-lg-6 col-form-label fw-bold fs-6'>
                        <label className='col-form-label fw-bold fs-6'>
                            <span className=''>จำนวนเงิน</span>
                        </label>
                        <div className='col-lg-12 fv-row'>
                            <Controller
                                name="amount"
                                control={control}
                                rules={{
                                    required: false,
                                }}
                                render={({field: {value, onChange}}) =>
                                    <input
                                        type='number'
                                        min={0}
                                        onChange={onChange}
                                        value={value}
                                        className={clsx(
                                            'form-control form-control-lg form-control-solid'
                                        )}
                                        placeholder=''
                                        name='amount'
                                        autoComplete='off'
                                    />}
                            />
                        </div>
                    </div>
                    <div className='col-lg-6 col-form-label fw-bold fs-6'>
                        <label className='col-form-label fw-bold fs-6'>
                            <span className=''>จำนวนเครดิต</span>
                        </label>
                        <div className='col-lg-12 fv-row'>
                            <Controller
                                name="creditAmount"
                                control={control}
                                rules={{
                                    required: false,
                                }}
                                render={({field: {value, onChange}}) =>
                                    <input
                                        type='number'
                                        min={0}
                                        onChange={onChange}
                                        value={value}
                                        className={clsx(
                                            'form-control form-control-lg form-control-solid'
                                        )}
                                        placeholder=''
                                        name='amount'
                                        autoComplete='off'
                                    />}
                            />
                        </div>
                    </div>

                </div>
                <div className='row mb-4'>
                    <div className='col-lg-12 fv-row'>
                        <label className='col-12 col-form-label fw-bold fs-6'>
                            <h5 className="modal-title">Information</h5>
                        </label>
                    </div>
                </div>
                <div className='row mb-6'>
                    <label className='col-lg-4 col-form-label fw-bold fs-6'>
                        <span className=''>ชื่อผู้ติดต่อ</span>
                    </label>
                    <div className='col-lg-12 fv-row'>
                        <Controller
                            name="contactName"
                            control={control}
                            rules={{
                                required: false,
                            }}
                            render={({field: {value, onChange}}) =>
                                <input
                                    type='text'
                                    onChange={onChange}
                                    value={value}
                                    className={clsx(
                                        'form-control form-control-lg form-control-solid'
                                    )}
                                    placeholder=''
                                    name='amount'
                                    autoComplete='off'
                                />}
                        />
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-lg-6 fv-row'>
                        <label className='col-form-label fw-bold fs-6'>
                            <span className=''>เลขผู้เสียภาษี</span>
                        </label>
                        <Controller
                            name="taxNo"
                            control={control}
                            rules={{
                                required: false,
                            }}
                            render={({field: {value, onChange}}) =>
                                <input
                                    type='text'
                                    onChange={onChange}
                                    value={value}
                                    className={clsx(
                                        'form-control form-control-lg form-control-solid'
                                    )}
                                    placeholder=''
                                    name='amount'
                                    autoComplete='off'
                                />}
                        />
                    </div>
                    <div className='col-lg-6 fv-row'>
                        <label className='col-form-label fw-bold fs-6'>
                            <span className=''>รหัส</span>
                        </label>
                        <Controller
                            name="agentCode"
                            control={control}
                            rules={{
                                required: false,
                            }}
                            render={({field: {value, onChange}}) =>
                                <input
                                    type='text'
                                    onChange={onChange}
                                    value={value}
                                    className={clsx(
                                        'form-control form-control-lg form-control-solid'
                                    )}
                                    placeholder=''
                                    name='amount'
                                    autoComplete='off'
                                />}
                        />
                    </div>
                </div>
                <div className='row mb-4'>
                    <div className='col-lg-6 fv-row'>
                        <label className='col-form-label fw-bold fs-6'>
                            <span className=''>เบอร์โทรติดต่อ</span>
                        </label>
                        <Controller
                            name="contactPhone"
                            control={control}
                            rules={{
                                required: false,
                            }}
                            render={({field: {value, onChange}}) =>
                                <input
                                    type='text'
                                    onChange={onChange}
                                    value={value}
                                    className={clsx(
                                        'form-control form-control-lg form-control-solid'
                                    )}
                                    placeholder=''
                                    name='contactPhone'
                                    autoComplete='off'
                                />}
                        />
                    </div>
                </div>
                <div className='d-flex flex-row justify-content-end'>
                    <Button variant="secondary" type={'button'} onClick={handleClose}>
                        Close
                    </Button>

                    <Button variant="primary" className={'mx-2'} type={'submit'}>
                        Save Changes
                    </Button>
                </div>
            </form>

        )
    }

    const [agentList, setAgentList] = useState<AgentType[] | null>(null)
    const [page, setPage] = useState<number>(0)
    const [pageSize, setPageSize] = useState<number>(30)
    const [total, setTotal] = useState<number>(0)
    const [search, setSearch] = useState<string>('')

    const searchAgents = async (page: number, pageSize: number, search: string) => {
        try {
            const result = await searchAgentAPI({
                body: {
                    page: page,
                    pageSize: pageSize,
                    keyWord: search
                }
            })
            if (result) {
                const {content, pageSize, total} = result?.data;
                setTotal(total);
                setPageSize(pageSize)
                setAgentList(content)
            }
        } catch (e: any) {
            console.log('error', e)
        }
    }

    const handleSearch = async (value: any) => {
        setSearchTerm(value);
        await searchAgents(page, pageSize, value);
    }

    const numberWithCommas = (x : any) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    const formatPhone = (phone : any) => {
        phone = phone + ('x'.repeat(10 - phone.length));
        let cleaned = ('' + phone).replace(/\D/g, '');
        var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
        if(match){
            return match[1] + '-' + match[2] + '-' + match[3]
        }
        return '-'
    }

    useEffect(() => {
        searchAgents(page, pageSize, search)
    }, [])
    return (
        <>
            <div className='card-header border-0 pt-6'>
                <div className='card-title'>
                    {/* begin::Search */}
                    <div className='d-flex align-items-center position-relative my-1'>
                        <KTSVG
                            path='/media/icons/duotune/general/gen021.svg'
                            className='svg-icon-1 position-absolute ms-6'
                        />
                        <input
                            type='text'
                            data-kt-user-table-filter='search'
                            className='form-control form-control-solid w-250px ps-14'
                            placeholder='Search'
                            value={searchTerm}
                            onChange={(e) => handleSearch(e.target.value)}
                        />
                    </div>
                    {/* end::Search */}
                </div>
                {/* begin::Card toolbar */}
                <div className='card-toolbar'>
                    {/* begin::Group actions */}
                    {/* {selected.length > 0 ? <UsersListGrouping /> : <UsersListToolbar />} */}
                    <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
                        <button
                            type='submit'
                            className='btn btn-primary'
                            onClick={handleShow}
                        >
                            <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2'/>
                            Add
                        </button>
                        <ModalApp
                            show={show}
                            type={'submit'}
                            handleClose={handleClose}
                            title={'Add a Agent'}
                            chil={<ModalAddAgent/>}/>
                        {/* end::Add user */}
                    </div>
                    {/* end::Group actions */}
                </div>
                {/* end::Card toolbar */}
            </div>
            <div className='card-body py-3'>
                {/* begin::Table container */}
                <div className='table-responsive'>
                    {/* begin::Table */}
                    <table className='table align-middle gs-0 gy-4'>
                        {/* begin::Table head */}
                        <thead>
                        <tr className='text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0'>
                            <th className='text-start min-w-75px'>ชื่อเอเจนท์</th>
                            <th className='text-start min-w-100px'>จำนวนครั้งที่ใช้</th>
                            <th className='text-start min-w-90px'>จำนวนเงิน</th>
                            <th className='text-start min-w-90px'>ชื่อบริษัท</th>
                            <th className='text-start min-w-125px'>ที่อยู่</th>
                            <th className='text-start min-w-125px'>เลขที่ผู้เสียภาษี</th>
                            <th className='text-start min-w-90px'>ตำนวนเครดิต</th>
                            <th className='text-start min-w-80px'>รหัส</th>
                            <th className='text-start min-w-125px'>วันที่เข้าร่วม</th>
                            <th className='text-start min-w-100px'>ชื่อผู้ติดต่อ</th>
                            <th className='text-start min-w-125px'>เบอร์โทรติดต่อ</th>
                            <th className='text-start min-w-80px'>จำนวนวัน</th>
                            <th className='text-start min-w-125px rounded-end'>Action</th>
                        </tr>
                        </thead>
                        {/* end::Table head */}
                        {/* begin::Table body */}
                        <tbody className="fw-bold text-gray-600">
                        {
                            agentList && agentList.map((value, index) => (
                                <tr key={`index_of_agents_${index}`}>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1 '>
                                                {value.name}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1'>
                                                {0}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1'>
                                                {numberWithCommas(value.amount)}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1'>
                                                {value.contactName}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1'>
                                                {value.address}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1'>
                                                {value.taxNo}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1'>
                                                {numberWithCommas(value.creditAmount)}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1'>
                                                {value.agentCode}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1'>
                                                {value.createdDate}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1'>
                                                {value.contactName}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1'>
                                                {formatPhone(value.contactPhone)}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                            <span className='text-dark fw-bold d-block fs-6 mt-1'>
                                                {0}
                                            </span>
                                    </td>
                                    <td className="text-start">
                                        <Link to={`/agents/detail/${value?.id}`}
                                              className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2'>
                                            VIEW
                                        </Link>
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>
                        {/* end::Table body */}
                    </table>
                    {/* end::Table */}
                </div>
                {/* end::Table container */}
            </div>
            {/* begin::Body */}
        </>
    )

}

export default AgentList;