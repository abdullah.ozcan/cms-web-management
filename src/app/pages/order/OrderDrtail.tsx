import {FC, useEffect, useState} from "react";
import { KTCard } from "../../../_metronic/helpers";
import { PageTitle } from "../../../_metronic/layout/core";
import {Link, useParams} from 'react-router-dom'
import {getOrderByIdApi} from "../../../api/OrderApi";


type CarType = {
    brand: string;
    id: number;
    model: string;
    status: string;
    title: string;
    vinNo: string;
    year: string;
}
type CustomerType = {
    "id": number;
    "name": string;
    "email": string;
}

type EmployeeType = {
    "id": number;
    "firstName": string;
    "lastName": string;
    "mobileNumber1": string;
}

type AgentType = {
    "id": number;
    "name": string;
    "contactName": string;
}
type OrderType = {
    address1: string;
    address2: string;
    agentId: number;
    agentName: string;
    carId: number;
    city: string;
    createdDate: string;
    currentMile: number;
    customerId: number;
    customerName: string;
    description: string;
    distantTotalMile: number;
    dropPlace: string;
    dropTime: string;
    employeeId: number;
    endMile: number;
    id: number;
    isPayment: boolean;
    name: string;
    oilAVG: number;
    oilPercent: number;
    paymentAmount: number;
    paymentDate: string;
    paymentDescription: string;
    paymentSlip: string;
    paymentType: string;
    phone: string;
    pickUpPlace: string;
    pickUpTime: string;
    postCode: string;
    pricePerDay: number;
    province: string;
    rentType: string;
    startMile: number;
    status: string;
    totalDate: number;
    updatedDate: string;
    agents: AgentType;
    employee: EmployeeType;
    customers: CustomerType;
    cars: CarType;
}
const OrderDetail:FC = () => {

    let { id } = useParams();

    const [orderDetail, setOrderDetail ] = useState<OrderType| null>(null)

    const list = [1,2]

    const numberFormat = (value:any) => {
        return  new Intl.NumberFormat('en-EN').format(value || 0)
    }

    const getOrderById = async () => {
        try {
            const result = await getOrderByIdApi(id);
            setOrderDetail(result?.data)
        } catch (e) {
            console.log('error', e)
        }
    }

    useEffect(() =>{
        getOrderById()
    }, [])

    console.log('orderDetail', orderDetail)
    const Order = () => {
        return (
            <KTCard className='h-350px'>
                <h4 className='text-lg-start text-dark m-4 mb-10 mt-6'>
                    Order Details #{orderDetail?.id}
                </h4>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300'>
                    <div>
                        <span className='text-muted'>Date Add</span>
                    </div>
                    <span className='text-dark'>{orderDetail?.createdDate}</span>
                </div>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300'>
                    <div>
                        <span className='text-muted'>Payment Method</span>
                    </div>
                    <span className='text-dark'>{orderDetail?.paymentType === 'N' ? 'เงินสด': 'โอน'}</span>
                </div>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300'>
                    <div>
                        <span className='text-muted'>Status Payment</span>
                    </div>
                    <span className='text-warning'>{orderDetail?.status}</span>
                </div>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300 mb-10'>
                    <div>
                        <span className='text-muted'>Status Order</span>
                    </div>
                    <span className='text-primary'>{orderDetail?.status}</span>
                </div>
            </KTCard>
        )
    }
    const Customer = () => {
        return (
            <KTCard className='h-350px'>
                <h4 className='text-lg-start text-dark m-4 mb-10 mt-6'>
                    Customer Details
                </h4>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300'>
                    <div>
                        <span className='text-muted'>Customer</span>
                    </div>
                    <span className='text-dark'>{orderDetail?.customerName} </span>
                </div>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300'>
                    <div>
                        <span className='text-muted'>Phone</span>
                    </div>
                    <span className='text-dark'>{ orderDetail?.phone}</span>
                </div>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300 mb-10'>
                    <div>
                        <span className='text-muted'>Email</span>
                    </div>
                    <span className='text-dark'>{orderDetail?.customers?.email || '-'}</span>
                </div>
            </KTCard>
        )
    }
    const Driver = () => {
        return (
            <KTCard className='h-350px'>
                <h4 className='text-lg-start text-dark m-4 mb-10 mt-6'>
                    Driver
                </h4>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300'>
                    <div>
                        <span className='text-muted'>Driver</span>
                    </div>
                    <span className='text-dark'>{orderDetail?.employee?.firstName}</span>
                </div>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300 mb-10'>
                    <div>
                        <span className='text-muted'>Phone</span>
                    </div>
                    <span className='text-dark'>{orderDetail?.employee?.mobileNumber1 || '-'}</span>
                </div>
            </KTCard>
        )
    }
    const PaymentAddress = () => {
        return (
            <KTCard className='h-300px'>
                <h4 className='text-lg-start text-dark m-4 mb-10 mt-6'>
                    Payment Address
                </h4>
                <div className='m-4 d-flex flex-column'>
                    <span className='text-dark'>{orderDetail?.address1} {orderDetail?.address2}</span>
                    <span className='text-dark'>{orderDetail?.province} {orderDetail?.postCode}</span>
                </div>
            </KTCard>
        )
    }
    const Detail = () => {
        return (
            <KTCard className='h-300px'>
                <h4 className='text-lg-start text-dark m-4 mb-10 mt-6'>
                    รายละเอียดใบงาน
                </h4>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300'>
                    <div>
                        <span className='text-muted'>สถานที่ไปรับ / เวลา</span>
                    </div>
                    <span className='text-dark'>{orderDetail?.pickUpPlace} ,{ orderDetail?.pickUpTime}</span>
                </div>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300 mb-10'>
                    <div>
                        <span className='text-muted'>สถานที่ไปส่ง / เวลา</span>
                    </div>
                    <span className='text-dark'>{orderDetail?.dropPlace} ,{orderDetail?.dropTime}</span>
                </div>
            </KTCard>
        )
    }
    const Summary = () => {
        return (
            <KTCard className=''>
                <h4 className='text-lg-start text-dark m-4 mb-10 mt-6'>
                    Order (#19822)
                </h4>
                <div className='table-responsive mx-4'>
                    <table className="table align-middle table-row-dashed fs-6 gy-4" id="kt_docs_datatable_subtable">
                        <thead>
                        <tr className="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                            <th className="min-w-25px">Products</th>
                            <th className="min-w-25px text-end">Order Date</th>
                            <th className="min-w-25px text-end">Day</th>
                            <th className="min-w-20px text-end">Type</th>
                            <th className="min-w-25px text-end">Plate</th>
                            <th className="min-w-25px text-end">Prices/Day</th>
                        </tr>
                        </thead>
                        <tbody className="fw-bold text-gray-600 table-row-dashed">
                        <tr key={`index_of_summary`}>
                            <td className="text-muted">
                                {orderDetail?.cars?.title || '-'}
                            </td>
                            <td className="text-muted text-end">
                                {orderDetail?.createdDate}
                            </td>
                            <td className="text-muted text-end">
                                {orderDetail?.totalDate}
                            </td>
                            <td className="text-dark text-end">
                                {orderDetail?.rentType === 'N' ? 'เช่า' : 'อื่นๆ'}
                            </td>
                            <td className="text-dark text-end">
                                {orderDetail?.cars?.vinNo || '-'}
                            </td>
                            <td className="text-dark text-end">
                                {numberFormat(orderDetail?.pricePerDay)}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div className='d-flex flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300'>
                    <div>
                        <span className='text-muted'></span>
                    </div>
                    <div className={''}>
                        <span className='text-dark mx-10'>Subtotal</span>
                        <span className='text-dark'>{numberFormat(orderDetail?.paymentAmount)}</span>
                    </div>
                </div>
                <div className='d-flex mt-8 flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300'>
                    <div>
                        <span className='text-muted'></span>
                    </div>
                    <div className={''}>
                        <span className='text-dark mx-10'>VAT (7%)</span>
                        <span className='text-dark'>{numberFormat((Number(orderDetail?.paymentAmount||0)*7)/100)}</span>
                    </div>
                </div>
                <div className='d-flex mt-8 flex-row justify-content-between m-4 border-bottom-dotted border-start-0 border-end-0 border-top-0 border-dotted border-gray-300 mb-10'>
                    <div>
                        <span className='text-muted'></span>
                    </div>
                    <div className={''}>
                        <span className='text-dark mx-10'>Total</span>
                        <span className='text-dark'>{numberFormat(orderDetail?.paymentAmount)}</span>
                    </div>
                </div>
            </KTCard>
        )
    }
    return (
    <>
    <PageTitle breadcrumbs={[]}>Order Detail</PageTitle>
        <div className="d-flex flex-row justify-content-between">
            <h3 className='border-bottom border-primary text-lg-start'>
                Order Summary
            </h3>
            <div>
                <a href="#" className="btn btn-bg-primary text-white mx-2">
                    <Link to={`/order/detail/invoice/${orderDetail?.id}`}  className='text-white'>
                        View Invoice
                    </Link>
                </a>
                {/*<a href="#" className="btn btn-bg-secondary text-white mx-2">*/}
                {/*    Edit Order*/}
                {/*</a>*/}
            </div>
        </div>
        <div className='row mx-auto mt-10'>
            <div className='col-12 col-lg-4'>
                <Order/>
            </div>
            <div className='col-12 col-lg-4'>
                <Customer/>
            </div>
            <div className='col-12 col-lg-4'>
                <Driver/>
            </div>
        </div>
        <div className='row mx-auto mt-10'>
            <div className='col-12 col-lg-6'>
                <PaymentAddress/>
            </div>
            <div className='col-12 col-lg-6'>
                <Detail/>
            </div>
        </div>
        <div className='row mx-auto mt-10'>
            <div className='col-12 col-lg-12'>
                <Summary/>
            </div>
        </div>
</>
)
}
export default OrderDetail;
