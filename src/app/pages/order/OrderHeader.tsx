import { FC } from "react";
import OrderGroup from "./Ordergroup";
import OrderSearch from "./OrderSearch";
type Props = {
    total: number
}
const OrderHeader: FC<Props> = ({total}) => {

    return (
        <div className='card-header border-0 pt-6'>
            {/* begin::Card toolbar */}
            <div className='card-toolbar'>
                {/* begin::Group actions */}
                {/* {selected.length > 0 ? <UsersListGrouping /> : <UsersListToolbar />} */}

                {/*{*/}
                {/*    total === 0 ?*/}
                {/*        <OrderSearch/>*/}
                {/*        : <OrderGroup total={total} />*/}
                {/*}*/}

                {/* end::Group actions */}
            </div>
            {/* end::Card toolbar */}
        </div>
    );
}
export default OrderHeader;
