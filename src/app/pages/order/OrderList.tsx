import {FC, useEffect, useState} from "react";
import {Link} from "react-router-dom";
import Pagination from '@vlsergey/react-bootstrap-pagination';
import OrderGroup from "./Ordergroup";
import {searchOrderAPI, sumOrderApi} from "../../../api/OrderApi";
import {Controller, useForm} from "react-hook-form";
import {clsx} from "clsx";
import OrderSearch from "./OrderSearch";
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import EditOrder from "./EditOrder";
type Props = {
    className: string
}
type CarType = {
    brand: string;
    id: number;
    model: string;
    status: string;
    title: string;
    vinNo: string;
    year: string;
}
type CustomerType = {
    "id": number;
    "name": string;
    "email": string;
}

type EmployeeType = {
    "id": number;
    "firstName": string;
    "lastName": string;
}

type AgentType = {
    "id": number;
    "name": string;
    "contactName": string;
}
export type OrderType = {
    address1: string;
    address2: string;
    agentId: number;
    agentName: string;
    carId: number;
    city: string;
    contactPhone: string;
    createDate: string;
    contactName: string;
    createdDate: string;
    currentMile: number;
    customerId: number;
    customerName: string;
    description: string;
    distantTotalMile: number;
    dropPlace: string;
    dropTime: string;
    employeeId: number;
    endMile: number;
    id: number;
    isPayment: boolean;
    name: string;
    oilAVG: number;
    oilPercent: number;
    paymentAmount: number;
    paymentDate: string;
    paymentDescription: string;
    paymentSlip: string;
    paymentType: string;
    phone: string;
    pickUpPlace: string;
    pickUpTime: string;
    postCode: string;
    pricePerDay: number;
    province: string;
    rentType: string;
    startMile: number;
    status: string;
    totalDate: number;
    updatedDate: string;
    agents: AgentType;
    employee: EmployeeType;
    customers: CustomerType;
    cars: CarType;
}

type SumaryOrder = {
    "sum": string;
    "total": number;
    "customer": number;
}
const OrderList: FC<Props> = ({className}) => {
    const [temp, setTemp] = useState<string[]>([]);
    const [showAdd, setShowAdd] = useState<boolean>(false);
    const [showEdit, setShowEdit] = useState<boolean>(false);

    const [orderInfo, setOrderInfo] = useState<OrderType |null>(null);

    const [orderList, setOderList] = useState<OrderType[]>([]);
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [start, setStart] = useState<string>('');
    const [end, setEnd] = useState<string>('');
    const [name, setName] = useState<string>('');
    const [phone, setPhone] = useState<string>('');
    const [type, setType] = useState<string>('');


    const handleClose = () => {
        setShowAdd(false)
    }
    const handleOpen = () => {
        setShowAdd(true);
    }

    const handleCloseEdit = () => {
        setShowEdit(false)
    }
    const handleOpenEdit = () => {
        setShowEdit(true);
    }

    const [orderSelecteList, setOrderSelecteList] = useState<OrderType[]>([]);
    const [sumary, setSumary] = useState<SumaryOrder| null>(null);


    const [totalPages, setTotalPages] = useState<number>(20);
    const [total, setTotal] = useState<number>(0);

    const getOrderList = async (phone: string, pageSize: number, page: number, type: string, start: string, end: string, name: string) => {
        try {
            const result = await searchOrderAPI({
                body: {
                    phone,
                    pageSize,
                    page,
                    type,
                    start,
                    end,
                    name,
                }
            })

            if (result?.data) {
                const value = result?.data;
                console.log('value?.data', value?.data)

                setOderList(value?.data)
                setTotalPages(value?.totalPages)
                setTotal(value?.total)
            }

        } catch (e: any) {
            console.log('error', e)
        }
    }

    const handleChange = (value: any) => {
        setPage(value?.target?.value)
        getOrderList(phone, pageSize, value?.target?.value, type, start, end, name);
    }

    const handleSelected = () => {

    }
    useEffect(() => {
        getOrderList(phone, pageSize, page, type, start, end, name);
        sumaryOrder();
    }, [])

    const typeStatus = [
        {
            'value': '',
            'label': 'All'
        },
        {
            'value': 'P',
            'label': 'Pending'
        },
        {
            'value': 'S',
            'label': 'Success'
        }
    ]

    const typePayment = [
        {
            'value': '',
            'label': 'All'
        },
        {
            'value': 'C',
            'label': 'เงินสด'
        },
        {
            'value': 'T',
            'label': 'โอน'
        }
    ]
    const {
        handleSubmit,
        formState: {errors},
        control,
        setValue,
    } = useForm({
        defaultValues: {
            name: '',
            phone: '',
            start: '',
            end: '',
            status: '',
            type: '',
        }
    });

    const onSearch = async (value: any) => {
        try {
            console.log('value', value)
            await getOrderList(value?.phone, 20, 1, value?.type, value?.start, value?.end, value?.name)
        } catch (e) {
            console.log('e', e)
        }
    }

    const sumaryOrder = async () => {
        try {
            const result = await sumOrderApi()

            if(result?.data) {
                setSumary(result?.data)
            }
        } catch (e) {
            console.log('e', e)
        }
    }
    return (
        <>
            {
                orderSelecteList.length > 0 ?
                    <div className='d-flex justify-content-start m-5'>
                        <OrderGroup total={0} value={orderSelecteList}/>
                    </div>
                    :
                    <div className='d-flex flex-row justify-content-center'>
                        <form onSubmit={handleSubmit(onSearch)} className='d-flex justify-content-start mt-5'
                              data-kt-user-table-toolbar='base'>
                            <div className='d-flex align-items-center position-relative my-1'>
                                <Controller
                                    name="name"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) => (
                                        <input
                                            type="text"
                                            maxLength={20}
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                "form-control form-control-lg form-control-solid",
                                            )}
                                            placeholder="Name"
                                        />
                                    )}
                                />
                            </div>
                            <div className='d-flex align-items-center position-relative my-1 mx-1'>
                                <Controller
                                    name="phone"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) => (
                                        <input
                                            type="text"
                                            maxLength={20}
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                "form-control form-control-lg form-control-solid",
                                            )}
                                            placeholder="Phone"
                                        />
                                    )}
                                />
                            </div>
                            <div className='d-flex align-items-center position-relative my-1 mx-1 w-150px'>
                                <Controller
                                    name="type"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) => (
                                        <select onChange={onChange} className="form-select"
                                                aria-label="Select Payment method">
                                            {
                                                typePayment && typePayment.map((value, index) => {
                                                    return (
                                                        <option key={`index_order_type_payment_search_${index}`}
                                                                value={value.value}>{value.label}</option>
                                                    )
                                                })
                                            }
                                        </select>
                                    )}
                                />
                            </div>
                            <div className='d-flex align-items-center position-relative my-1 mx-1 w-150px'>
                                <Controller
                                    name="status"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) => (
                                        <select onChange={onChange} className="form-select"
                                                aria-label="Select Payment method">
                                            {
                                                typeStatus && typeStatus.map((value, index) => {
                                                    return (
                                                        <option key={`index_order_type_payment_search_${index}`}
                                                                value={value.value}>{value.label}</option>
                                                    )
                                                })
                                            }
                                        </select>
                                    )}
                                />
                            </div>
                            <div className='d-flex align-items-center position-relative my-1 mx-2'>
                                <Controller
                                    name="start"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) => (
                                        <input
                                            type="date"
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                "form-control form-control-lg form-control-solid",
                                            )}
                                            placeholder="Start"
                                        />
                                    )}
                                />
                            </div>
                            <div className='d-flex align-items-center position-relative my-1 mx-2'>
                                <Controller
                                    name="end"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) => (
                                        <input
                                            type="date"
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                "form-control form-control-lg form-control-solid",
                                            )}
                                            placeholder="End"
                                        />
                                    )}
                                />
                            </div>

                            {/* begin::Add user */}
                            <button
                                style={{height: '45px'}}
                                type='submit'
                                className='btn btn-primary btn-sm mt-1 fs-5'
                            >
                                Search
                            </button>
                        </form>
                        <div className='d-flex align-items-center mt-5'>
                            <OrderSearch show={showAdd} handleClose={handleClose} handleOpen={handleOpen}/>
                        </div>
                    </div>
            }
            <div className={`card ${className}`}>
                {/* begin::Body */}
                <div>
                    <EditOrder orderInfo={orderInfo} show={showEdit} handleClose={handleCloseEdit} handleOpen={handleCloseEdit}/>
                </div>
                <div className='card-body py-3'>
                    <div className='table-responsive'>
                        {/* begin::Table container */}
                        <table className="table align-middle table-row-dashed fs-6 gy-4"
                               id="kt_docs_datatable_subtable">
                            <thead>
                            <tr className="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                <th className="text-center min-w-210px">วันที่ทำรายการ</th>
                                <th className="text-center min-w-50px">ประเภท</th>
                                <th className="text-center min-w-50px">วัน</th>
                                <th className="text-center min-w-70px">ชื่อเล่น</th>
                                <th className="text-center min-w-80px">เบอร์ติดต่อ</th>
                                <th className="text-center min-w-80px">รถ</th>
                                <th className="text-center min-w-70px">ทะเบียน</th>
                                <th className="text-center min-w-70px">คนขับ</th>
                                <th className="text-center min-w-80px">เอเจนต์</th>
                                <th className="text-center min-w-90px">รายรับ</th>
                                <th className="text-center min-w-100px">รับวันที่</th>
                                <th className="text-center min-w-90px">รายจ่าย</th>
                                <th className="text-center min-w-100px">จ่ายวันที่</th>
                                <th className="text-center min-w-150px">สถานที่</th>
                                <th className="text-end"/>
                            </tr>
                            </thead>
                            <tbody className="fw-bold text-gray-600">
                            {
                                orderList && orderList.map((d, index) => (
                                    <tr key={`index_of_order_${index}`}>
                                        <td className='d-flex flex-row border-bottom-0'>
                                            <div className='d-flex flex-row justify-content-evenly'>
                                                {
                                                    d.status !== 'SUCCESS' ?
                                                        <input
                                                            className=' mx-2 form-check form-check-lg form-check-custom form-check-solid'
                                                            type='checkbox'
                                                            onChange={(e) => {
                                                                const findItem = temp.find(v => v === index.toString())
                                                                if (!findItem) {
                                                                    const resultSelect = [...temp, index.toString()]
                                                                    setTemp(resultSelect)

                                                                    let tempData: OrderType[] = [];
                                                                    orderList.map((v, i) => {
                                                                        resultSelect.map((d) => {
                                                                            if (i === Number(d)) {
                                                                                tempData.push(v)
                                                                            }
                                                                        })
                                                                    })

                                                                    setOrderSelecteList(tempData)
                                                                } else {
                                                                    const remove = temp.filter(v1 => v1 !== index.toString());
                                                                    const resultSelect = [...remove]
                                                                    setTemp(resultSelect)
                                                                    let tempData: OrderType[] = [];
                                                                    orderList.map((v, i) => {
                                                                        resultSelect.map((d) => {
                                                                            if (i === Number(d)) {
                                                                                tempData.push(v)
                                                                            }
                                                                        })
                                                                    })

                                                                    setOrderSelecteList(tempData)
                                                                }
                                                            }}
                                                            value={index.toString()}
                                                            data-kt-check='true'
                                                            data-kt-check-target='.widget-13-check'
                                                        /> : <div className='d-flex flex-row justify-content-evenly'>
                                                            <div className='me-7'/>
                                                            {/*<span className="text-dark mx-2">{d.createdDate}</span>*/}
                                                        </div>
                                                }
                                                <div className='d-flex flex-row justify-content-evenly'>
                                                    <span className="text-dark">{d.createdDate}</span>
                                                </div>

                                            </div>
                                        </td>
                                        <td className="text-center text-muted">
                                            {d.rentType === 'N' ? 'รถเช่า' : 'อื่นๆ '}
                                        </td>
                                        <td className="text-center text-muted">
                                            {d.totalDate}
                                        </td>
                                        <td className="text-center text-dark">
                                            {d.name}
                                        </td>
                                        <td className="text-center text-dark">
                                            {d.phone}
                                        </td>
                                        <td className="text-center text-dark">
                                            {d?.cars?.title || '-'}
                                        </td>
                                        <td className="text-center text-dark">
                                            {d?.cars?.vinNo || '-'}
                                        </td>
                                        <td className="text-center text-muted">
                                            {d?.employee?.firstName || '-'}
                                        </td>
                                        <td className="text-center text-muted">
                                            {d?.agents?.name || '-'}
                                        </td>
                                        <td className="text-center text-primary">
                                            {d.paymentAmount}
                                        </td>
                                        <td className="text-center text-dark">
                                            {d.paymentDate}
                                        </td>
                                        <td className="text-center text-danger">
                                            {0}
                                        </td>
                                        <td className="text-center text-dark">
                                            -
                                        </td>
                                        <td className="text-center text-dark">
                                            {d.pickUpPlace}
                                        </td>
                                        <td className="text-center">
                                            {/*<select className="form-select form-select-solid form-sm w-100px float-end">*/}
                                            {/*    <option>Actions</option>*/}
                                            {/*    <option value={d?.id}>View</option>*/}
                                            {/*    <option value={d?.id}>Edit</option>*/}
                                            {/*</select>*/}
                                            {/*<Link className="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2" to={`/order/detail/${d?.id}`}>*/}
                                            {/*    VIEW*/}
                                            {/*</Link>*/}
                                            <ButtonGroup vertical>
                                                <DropdownButton

                                                    className='btn-secondary'
                                                    as={ButtonGroup}
                                                    title="Action"
                                                    size={'sm'}
                                                    id="bg-vertical-dropdown-3"
                                                >
                                                    <Dropdown.Item eventKey="1" onClick={(e) => {
                                                        e.preventDefault();
                                                        window.location.href = `/order/detail/${d?.id}`
                                                    }}>View</Dropdown.Item>
                                                    <Dropdown.Item eventKey="2" onClick={(e)=> {
                                                        e.preventDefault()
                                                        setOrderInfo(d)
                                                        handleOpenEdit()
                                                    }}>Edit</Dropdown.Item>
                                                </DropdownButton>
                                            </ButtonGroup>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                        {/* end::Table container */}
                    </div>
                    <div className='d-flex justify-content-end mt-2'>
                        <Pagination
                            value={page}
                            firstPageValue={1}
                            onChange={handleChange}
                            totalPages={totalPages}/>
                    </div>
                    <div
                        className='mt-6 mb-6 border-top-dotted border-gray-300 d-flex flex-row justify-content-start'>
                        <div className='mt-4 border-end border-1 border-gray-400 px-4'>
                            <span className={'text-dark'}>รายกายทั้งหมด: <span className='text-primary'>{sumary?.total || 0}</span> รายการ</span>
                        </div>
                        <div className='mt-4 border-end border-1 border-gray-400 px-4'>
                            <span className={'text-dark'}>ลูกค้าใช้ทั้งหมด: <span className='text-primary'>{sumary?.customer || 0}</span> คน</span>
                        </div>
                        <div className='mt-4 border-end border-1 border-gray-400 px-4'>
                            <span className={'text-dark'}>ยอดทั้งหมด: <span
                                className='text-primary'>{new Intl.NumberFormat('en-EN').format(Number(sumary?.sum) || 0)} บาท</span></span>
                        </div>
                        <div className='mt-4 border-end border-1 border-gray-400 px-4'>
                           <span className={'text-dark'}>รายจ่ายทั้งหมด: <span className='text-primary'>1,341,445</span> บาท</span>
                        </div>
                        <div className='mt-4 border-end border-1 border-gray-400 px-4'>
                           <span className={'text-dark'}>ยอดคงเหลือ: <span className='text-primary'>341,445</span> บาท</span>
                        </div>
                    </div>
                </div>
                {/* begin::Body */}
            </div>
        </>
    )
}

export default OrderList;
