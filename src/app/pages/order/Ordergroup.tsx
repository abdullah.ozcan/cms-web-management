import {FC, useEffect, useState} from "react";
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { KTSVG } from "../../../_metronic/helpers";
import {Controller, useForm} from "react-hook-form";
import {clsx} from "clsx";
import {updatePaymentOrderApi} from "../../../api/OrderApi";
type Props = {
    total: number;
    value?: OrderType[]
}
type OrderType = {
    address1: string;
    address2: string;
    agentId: number;
    agentName: string;
    carId: number;
    city: string;
    createdDate: string;
    currentMile: number;
    customerId:number;
    customerName: string;
    description:string;
    distantTotalMile: number;
    dropPlace: string;
    dropTime: string;
    employeeId: number;
    endMile: number;
    id: number;
    isPayment: boolean;
    name:string;
    oilAVG: number;
    oilPercent: number;
    paymentAmount: number;
    paymentDate: string;
    paymentDescription: string;
    paymentSlip: string;
    paymentType: string;
    phone: string;
    pickUpPlace: string;
    pickUpTime: string;
    postCode: string;
    pricePerDay: number;
    province: string;
    rentType: string;
    startMile: number;
    status: string;
    totalDate: number;
    updatedDate: string;
}
const OrderGroup: FC<Props> = ({ total, value }) => {

    const {
        handleSubmit,
        formState: {errors},
        control,
        setValue,
    } = useForm({
        defaultValues: {
            paymentOption: '',
            totalAmount: 0,
            paymentDate: '',
            note: '',
            listId: []
        }
    });
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    const [listId, setListId ] = useState<number[]>([])


    const [totalAmount, setTotalAmount ] = useState<number>(0)


    const initValue = () => {
        const mapId:number[] = value?.map((value) => value?.id) || []
        const mapAmount:number[] = value?.map((value) => value?.paymentAmount) || []

        const sumAmount: number = mapAmount.reduce((a,b) => a+b,0)
        setListId(mapId)
        setTotalAmount(sumAmount)
        // @ts-ignore
        setValue('listId', mapId);
        setValue('totalAmount', sumAmount);
    }
    const typePayment = [
        {
            'value': 'C',
            'label': 'เงินสด'
        },
        {
            'value': 'T',
            'label': 'โอน'
        }
    ]

    const updatePayment = async (value:any) => {
        try {
            await updatePaymentOrderApi({body: {
                    listId: value?.listId,
                    note: '',
                    paymentDate: value?.paymentDate,
                    paymentOption:  value?.paymentOption,
                    amount: value?.totalAmount
                }})
            handleClose();
            window.location.href = '/order'
        } catch (e) {
            console.log('e', e)
        }
    }

    useEffect(() => {
        initValue()
    }, [value])

    const ModalPayment = () => {
        return (
            <Modal show={show} onHide={handleClose} size='lg'>
                <Modal.Header closeButton>
                    <Modal.Title>Payment Approve </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form onSubmit={handleSubmit(updatePayment)}>
                        <div className='row mb-4'>
                            <div className='col-lg-6 fv-row'>
                                <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                    <span className=''>Payment Option</span>
                                </label>
                                <Controller
                                    name="paymentOption"
                                    control={control}
                                    rules={{
                                        required: false,
                                    }}
                                    render={({field: {value, onChange}}) => (
                                        <select onChange={onChange} className="form-select"
                                                aria-label="Select Payment method">
                                            {
                                                typePayment && typePayment.map((value, index) => {
                                                    return (
                                                        <option key={`index_order_type_payment_sum_${index}`}
                                                                value={value.value}>{value.label}</option>
                                                    )
                                                })
                                            }
                                        </select>
                                    )}
                                />
                            </div>
                            <div className='col-lg-6 fv-row'>
                                {/*<input*/}
                                {/*  className='mx-2 mt-5 form-check-input'*/}
                                {/*  type='checkbox'*/}
                                {/*  value={1}*/}
                                {/*  data-kt-check='true'*/}
                                {/*  data-kt-check-target='.widget-13-check'*/}
                                {/*/>*/}
                            </div>
                        </div>
                        <div className='row mb-4'>
                            <div className='col-lg-4 fv-row'>
                                <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                    <span className=''>ยอดเงิน</span>
                                </label>
                                <Controller
                                    name="totalAmount"
                                    control={control}
                                    rules={{
                                        required: "totalAmount is require",
                                    }}
                                    render={({field: {value, onChange}}) => (
                                        <input
                                            type="number"
                                            min={0}
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                "form-control form-control-lg form-control-solid",
                                                {
                                                    "is-invalid":
                                                        errors?.totalAmount?.message && errors?.totalAmount?.message,
                                                },
                                                {
                                                    "is-valid":
                                                        errors?.totalAmount?.message && !errors?.totalAmount?.message,
                                                }
                                            )}
                                            placeholder=""
                                        />
                                    )}
                                />
                                {errors?.totalAmount?.message && (
                                    <div className="fv-plugins-message-container text-danger">
                                        <span role="alert">{errors?.totalAmount?.message}</span>
                                    </div>
                                )}
                            </div>
                            <div className='col-lg-4 fv-row'>
                                <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                    <span className=''>วันที่ชำระ</span>
                                </label>
                                <Controller
                                    name="paymentDate"
                                    control={control}
                                    rules={{
                                        required: "paymentDate is require",
                                    }}
                                    render={({field: {value, onChange}}) => (
                                        <input
                                            type="date"
                                            value={value}
                                            onChange={onChange}
                                            className={clsx(
                                                "form-control form-control-lg form-control-solid",
                                                {
                                                    "is-invalid":
                                                        errors?.paymentDate?.message && errors?.paymentDate?.message,
                                                },
                                                {
                                                    "is-valid":
                                                        errors?.paymentDate?.message && !errors?.paymentDate?.message,
                                                }
                                            )}
                                            placeholder=""
                                        />
                                    )}
                                />
                                {errors?.paymentDate?.message && (
                                    <div className="fv-plugins-message-container text-danger">
                                        <span role="alert">{errors?.paymentDate?.message}</span>
                                    </div>
                                )}
                            </div>
                            {/*<div className='col-lg-4 fv-row'>*/}
                            {/*    <button type='button'*/}
                            {/*        className='btn btn-primary mx-1 mt-lg-14'*/}
                            {/*    >*/}
                            {/*        Upload Slip*/}
                            {/*    </button>*/}
                            {/*</div>*/}
                        </div>
                        <div className='row mb-4'>
                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                <span className=''>หมายเหตุ</span>
                            </label>
                            <Controller
                                name="note"
                                control={control}
                                rules={{
                                    required: "paymentDate is require",
                                }}
                                render={({field: {value, onChange}}) => (
                                    <textarea
                                        rows={4}
                                        className='form-control form-control-lg form-control-solid'
                                        value={value}
                                        onChange={onChange}
                                        placeholder=""
                                    />
                                )}
                            />
                        </div>
                        <div className='mt-6 mb-6 d-flex flex-row justify-content-between'>
                            <div className='mt-4 border-end border-3 border-gray-300 px-4'>
                                <span className={'text-dark'}>เลือกรายการทั้งหมด: <span className='text-primary'>{listId.length}</span> รายการ</span>
                            </div>
                            <div className='mt-4 border-end border-3 border-gray-300 px-4'>
                                <span className={'text-dark'}>ยอดทั้งหมด: <span className='text-primary'>{new Intl.NumberFormat('en-EN').format(totalAmount || 0)}</span> บาท</span>
                            </div>
                            <div className='mt-4 px-4'>
                                <button
                                    type='button'
                                    className='btn btn-light'
                                    onClick={handleClose}
                                >
                                    Discard
                                </button>
                                <button type='submit' className='btn btn-primary mx-2'>
                                    Update Payment
                                </button>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        )
    }
    return (
        <div className='d-flex justify-content-end align-items-center'>
            <div className='fw-bolder me-5'>
                <span className='me-2'>{value?.length}</span> Selected
            </div>

            <Button
                type='button'
                className='btn btn-danger'
                onClick={handleShow}
            >
                Payment Detail
            </Button>
            <ModalPayment />
            <div className="modal fade" tabIndex={-1} id="kt_modal_add_payment_order">
                <div className='modal-dialog modal-lg'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Payment Approve <span className='text-muted'>{total}</span></h5>
                            <div
                                className='btn btn-icon btn-sm btn-active-light-primary ms-2'
                                data-bs-dismiss='modal'
                                aria-label='Close'
                            >
                                <KTSVG
                                    path='/media/icons/duotune/arrows/arr061.svg'
                                    className='svg-icon svg-icon-2x'
                                />
                            </div>
                        </div>
                        <div className='modal-body'>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default OrderGroup;
