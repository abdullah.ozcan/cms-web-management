import {FC} from 'react'
import { KTCard } from '../../../_metronic/helpers';
import { PageTitle } from '../../../_metronic/layout/core';
import OrderHeader from './OrderHeader';
import OrderList from './OrderList';
// import DataTable, { ExpanderComponentProps, TableColumn }  from 'react-data-table-component';
// import {PageTitle} from '../../../_metronic/layout/core'
const OrderWrapper: FC = () => {
    
//     type DataRow = {
//         title: string;
//         director: string;
//         year: string;
//     };
    
//     const columns: TableColumn<DataRow>[] = [
//         {
//             name: 'Title',
//             selector: (row: { title: any; }) => row.title,
//         },
//         {
//             name: 'Director',
//             selector: (row: { director: any; }) => row.director,
//         },
//         {
//             name: 'Year',
//             selector: (row: { year: any; }) => row.year,
//         },
//     ];

//     const data:DataRow[] = [
//         {
//         title: 'AAAa',
//         year: 'BBBB',
//         director: 'ddddd'
//         },
//         {
//         title: 'AAAa',
//         year: 'BBBB',
//         director: 'ddddd'
//         },
//         {
//             title: 'AAAa',
//             year: 'BBBB',
//             director: 'ddddd'
//             }   
//     ]
    
//     // data provides access to your row data
//     const ExpandedComponent: React.FC<ExpanderComponentProps<DataRow>> = ({ data }) => {
//         return <pre>{JSON.stringify(data, null, 2)}</pre>;
//     };
//     const handleChange = ({ selectedRows }: { selectedRows: any} ) => {
//         // You can set state or dispatch with something like Redux so we can use the retrieved data
//         console.log('Selected Rows: ', selectedRows);
//       };
//   return (
//     <>
//       <PageTitle breadcrumbs={[]}>Order</PageTitle>
//       <DataTable
//             columns={columns}
//             data={data}
//             selectableRows
//             onSelectedRowsChange={handleChange}
//             expandableRows expandableRowsComponent={ExpandedComponent}
//         />
//     </>
//   )
return (
    <>
        <PageTitle breadcrumbs={[]}>Order/Booking</PageTitle>
        <KTCard>
        <OrderList className={''}/>
        </KTCard>
    </>
)
}

export default OrderWrapper;
