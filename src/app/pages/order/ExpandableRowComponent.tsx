import * as React from 'react';
import DataTable, { ExpanderComponentProps } from 'react-data-table-component';

interface Row {
    title: string;
    id: string;
    year: string;
}

interface Props extends ExpanderComponentProps<Row> {
    // currently, props that extend ExpanderComponentProps must be set to optional.
    someTitleProp?: string;
}

const ExpandableRowComponent: React.FC<Props> = ({ data, someTitleProp }) => {
    return (
        <>
            <p>{someTitleProp}</p>
            <p>{data.title}</p>
            <p>{data.id}</p>
            <p>{data.year}</p>
        </>
    );
};

export default ExpandableRowComponent;