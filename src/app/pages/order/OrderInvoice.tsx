import {FC, useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import { KTSVG } from "../../../_metronic/helpers";
import { PageTitle } from "../../../_metronic/layout/core";
import {getOrderByIdApi} from "../../../api/OrderApi";




type CarType = {
    brand: string;
    id: number;
    model: string;
    status: string;
    title: string;
    vinNo: string;
    year: string;
}
type CustomerType = {
    "id": number;
    "name": string;
    "email": string;
}

type EmployeeType = {
    "id": number;
    "firstName": string;
    "lastName": string;
    "mobileNumber1": string;
}

type AgentType = {
    "id": number;
    "name": string;
    "contactName": string;
}
type OrderType = {
    address1: string;
    address2: string;
    agentId: number;
    agentName: string;
    carId: number;
    city: string;
    createdDate: string;
    currentMile: number;
    customerId: number;
    customerName: string;
    description: string;
    distantTotalMile: number;
    dropPlace: string;
    dropTime: string;
    employeeId: number;
    endMile: number;
    id: number;
    isPayment: boolean;
    name: string;
    oilAVG: number;
    oilPercent: number;
    paymentAmount: number;
    paymentDate: string;
    paymentDescription: string;
    paymentSlip: string;
    paymentType: string;
    phone: string;
    pickUpPlace: string;
    pickUpTime: string;
    postCode: string;
    pricePerDay: number;
    province: string;
    rentType: string;
    startMile: number;
    status: string;
    totalDate: number;
    updatedDate: string;
    createdBy: string;
    agents: AgentType;
    employee: EmployeeType;
    customers: CustomerType;
    cars: CarType;
}
const OrderInvoice: FC = () => {

    let { id } = useParams();

    const [orderDetail, setOrderDetail ] = useState<OrderType| null>(null)


    const numberFormat = (value:any) => {
        return  new Intl.NumberFormat('en-EN').format(value || 0)
    }

    const getOrderById = async () => {
        try {
            const result = await getOrderByIdApi(id);
            setOrderDetail(result?.data)
        } catch (e) {
            console.log('error', e)
        }
    }

    useEffect(() =>{
        getOrderById()
    }, [])
    return (
        <>
            <PageTitle breadcrumbs={[]}>Rent Dashboaed | Home - Car Management - Invoice Detail</PageTitle>
            <div className='card mb-5 mb-xl-10'>
                <div className='card mb-5 mb-xl-10' id='kt_profile_details_view'>
                    <div className='card-body p-9'>
                        <div className='row'>
                            <div className='col-lg-8'>
                                <div className='row mb-7'>
                                    <div className='col-lg-8'>
                                        <h2 className='fw-bolder text-dark'>WEB MANAGEMENT</h2>
                                        <div
                                            className='d-none d-md-flex flex-row-fluid bgi-no-repeat bgi-position-y-center bgi-position-x-right bgi-size-cover'
                                            style={{
                                                transform: 'translateX(10%) rotate(-26deg)',
                                                backgroundImage: `url('/media/icons/duotune/finance/fin002.svg')`,
                                            }}
                                        >
                                        </div>
                                        <span className="fw-bold fs-4 text-dark">Invoice #{orderDetail?.id}</span>
                                    </div>
                                </div>
                                <div className='row mb-7'>
                                    <div className='col-lg-4'>
                                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Issue Date:</div>
                                        <div className='fw-bolder fs-6 me-2'>{orderDetail?.createdDate}</div>
                                    </div>
                                    <div className='col-lg-8 fv-row'>
                                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Due Date:</div>
                                        <span className='fw-bolder fs-6 me-2'>{orderDetail?.createdDate || ''}</span>
                                        {/*<span className='fw-bolder fs-6 me-2 text-gray-600'>Due in 7 days</span>*/}
                                    </div>
                                </div>
                                <div className='row mb-7'>
                                    <div className='col-lg-4'>
                                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Issue for:</div>
                                        <div className='fw-bolder fs-6 me-2'>{orderDetail?.customerName}</div>
                                    </div>
                                    <div className='col-lg-8 fv-row'>
                                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Issue By:</div>
                                        <span className='fw-bolder fs-6 me-2'>{orderDetail?.createdBy || '-'}</span>
                                    </div>
                                </div>
                                <div className='row mb-7'>
                                    <div className='col-lg-12'>
                                        <div className='table-responsive'>
                                            {/* begin::Table */}
                                            <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
                                                {/* begin::Table head */}
                                                <thead>
                                                    <tr className='fw-bolder text-muted'>
                                                        <th className='min-w-150px'>Description</th>
                                                        <th className='min-w-140px'>Days</th>
                                                        <th className='min-w-120px'>Price/Day</th>
                                                        <th className='min-w-120px'>Amount</th>
                                                    </tr>
                                                </thead>
                                                {/* end::Table head */}
                                                {/* begin::Table body */}
                                                <tbody>
                                                <tr key={`invoice`}>
                                                    <td>
                                                        <a href='#' className='text-dark fw-bolder text-hover-primary fs-6'>
                                                            {orderDetail?.cars?.title}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <span className='text-dark fw-bolder text-hover-primary d-block mb-1 fs-6'>{orderDetail?.totalDate}</span>
                                                    </td>
                                                    <td>
                                                        <span className='text-dark fw-bolder text-hover-primary d-block mb-1 fs-6'>{numberFormat(orderDetail?.pricePerDay || 0)}</span>
                                                    </td>
                                                    <td>
                                                        <span className='text-dark fw-bolder text-hover-primary d-block mb-1 fs-6'>{numberFormat(orderDetail?.paymentAmount|| 0)}</span>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                {/* end::Table body */}
                                            </table>
                                            {/* end::Table */}
                                        </div>
                                        <div className='row mb-7 mt-4'>
                                            <div className='col-8'></div>
                                            <div className='col-4'>
                                                <div className='row mb-7'>
                                                    <label className='col-lg-8 fw-bold text-muted'>SubTotal</label>
                                                    <div className='col-lg-4'>
                                                        <span className='fw-bolder fs-6 text-dark'>{numberFormat(orderDetail?.paymentAmount)}</span>
                                                    </div>
                                                </div>
                                                <div className='row mb-7'>
                                                    <label className='col-lg-8 fw-bold text-muted'>VAT 7%</label>

                                                    <div className='col-lg-4 fv-row'>
                                                        <span className='fw-bolder fs-6'>{numberFormat((Number(orderDetail?.paymentAmount)*7)/100)}</span>
                                                    </div>
                                                </div>
                                                <div className='row mb-7'>
                                                    <label className='col-lg-8 fw-bold text-muted'>Total</label>
                                                    <div className='col-lg-4'>
                                                        <span className='fw-bolder fs-6 me-2'>{numberFormat(orderDetail?.paymentAmount)}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-4'>
                                <div className='notice d-flex bg-light rounded border-secondary border border-dashed p-6'>
                                    <KTSVG
                                        path='icons/duotune/general/gen044.svg'
                                        className='svg-icon-2tx svg-icon-warning me-4'
                                    />
                                    <div className='d-flex flex-stack flex-grow-1 h-100'>
                                        <div className='fw-bold'>
                                            <div className='card-body p-5'>
                                                <button
                                                    type="button"
                                                    className={orderDetail?.status === 'SUCCESS' ? 'btn btn-light btn-bg-secondary btn-sm mb-5': 'btn btn-light btn-sm mb-5 text-muted'}
                                                    disabled
                                                >
                                                    Approved
                                                </button>
                                                <button
                                                    type="button"
                                                    className={orderDetail?.status !== 'SUCCESS' ? 'btn btn-light btn-bg-secondary btn-sm mb-5': 'btn btn-light btn-sm mb-5 text-muted'}
                                                    disabled
                                                >
                                                    Pending Payment
                                                </button>
                                                <div className='fs-4 fw-bolder text-gray-600 mb-5'>PAYMENT DETAILS</div>
                                                <div className='d-flex flex-column flex-wrap align-items-start mb-13'>
                                                    <div className='mt-4' id='kt_signin_email'>
                                                        <div className='fs-6 fw-bolder mb-1'>Account:</div>
                                                        <div className='fw-bold text-gray-600'>{orderDetail?.createdBy || '-'}</div>
                                                    </div>
                                                    <div className='mt-4' id='kt_signin_email'>
                                                        <div className='fs-6 fw-bolder mb-1'>Payment term:</div>
                                                        <div className='fw-bold text-gray-600'>14 Day Due in 7</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default OrderInvoice;
