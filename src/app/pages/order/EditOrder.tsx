import {FC, useEffect, useState} from "react";
import Button from 'react-bootstrap/Button';
import {Accordion} from "react-bootstrap";
import {Controller, useForm} from "react-hook-form";
import {getAllCarAPI} from "../../../api/CategoryApi";
import {searchAgentAPI} from "../../../api/AgentsApi";
import {clsx} from "clsx";
import {searchEmployeeAPI} from "../../../api/EmployeeApi";
import Form from 'react-bootstrap/Form';
import {PROVICE} from "../../../api/Address";
import {getAllCustomers} from "../../../api/CustomerApi";
import {createOrderApi, updateOrderApi} from "../../../api/OrderApi";
import ModalApp from "../../../_metronic/layout/components/ModalApp";
import {OrderType} from "./OrderList";

type Props = {
    show: boolean;
    handleOpen: ()=>void;
    handleClose: ()=>void;
    orderInfo?:OrderType|null
}
type AgentType = {
    address: string;
    agentCode: string;
    amount: number;
    contactName: string;
    contactPhone: string;
    createdDate: string;
    creditAmount: number;
    id: number;
    isActive: number;
    name: string;
    status: string;
    taxNo: string;
    updatedDate: string;
}

type CarType = {
    brand: string;
    id: number;
    model: string;
    status: string;
    title: string;
    vinNo: string;
    year: string;
}
type CustomerType = {
    "id": number;
    "name": string;
    "email": string;
}

type EmployeeType = {
    "id": number;
    "firstName": string;
    "lastName": string;
}


const typeCarRent = [
    {
        'value': 'L',
        'label': 'เช่ารถ'
    }
]

const typePayment = [
    {
        'value': 'C',
        'label': 'เงินสด'
    }
]

const EditOrder: FC<Props> = ({show, handleClose, handleOpen, orderInfo}) => {

    const [carName, setCarName] = useState<string>('');
    const [slip, setSlip] = useState<any>(null);

    const [agentList, setAgentList] = useState<AgentType[]>([]);
    const [carList, setCarList] = useState<CarType[]>([]);
    const [empList, setEmpList] = useState<EmployeeType[]>([]);
    const [customerList, setCustomerList] = useState<CustomerType[]>([]);
    const {
        handleSubmit,
        setValue,
        formState: {errors},
        control,
    } = useForm({
        defaultValues: {
            id:0,
            contactName:'',
            createDate: '',
            phone: '',
            name: '',
            paymentDate: '',
            totalDate: 0,
            rentType: 'N',
            agentId: 0,
            customerId: customerList ? customerList[0]?.id : 0,
            carId: carList ? carList[0]?.id: 0,
            pricePerDay: 0,
            employeeId: empList ? empList[0]?.id: 0,
            agentName: '',
            pickUpPlace: '',
            pickUpTime: '',
            dropPlace: '',
            dropTime: '',
            description: '',
            paymentType: 'N',
            isPayment: false,
            paymentAmount: 0,
            paymentSlip: '',
            paymentDescription: '',
            currentMile: 0,
            distantTotalMile: 0,
            startMile: 0,
            endMile: 0,
            oilPercent: 0,
            oilAVG: 0,
            customerName: '',
            address1: '',
            address2: '',
            contactPhone: '',
            city: '',
            province: '',
            postCode: '',
        }
    });

    const getAllCustomersList = async () => {
        try {
            const result = await getAllCustomers()
            if (result) {
                setCustomerList(result?.data)
            }
        } catch (e) {
            console.log('e')
        }
    }

    const getAgentList = async () => {
        try {
            const result = await searchAgentAPI({
                body: {
                    page: 1,
                    pageSize: 10,
                    keyWord: ''
                }
            })
            if (result) {
                const {content} = result?.data;

                console.log('content:', content)
                setAgentList(content)
            }
        } catch (e) {
            console.log('e', e)
        }
    }

    const getAllCar = async () => {
        try {
            const result = await getAllCarAPI()
            if (result?.data) {
                let data: CarType[] = [
                    {
                        id:null,
                        vinNo: '',
                        brand: '',
                        model: ''
                    },
                    ...result?.data
                ]
                setCarList(data)
            }
        } catch (e: any) {
            console.log('e', e)
        }
    }

    const getAllEmployee = async () => {
        try {
            const result = await searchEmployeeAPI({
                body: {
                    page: 1,
                    pageSize: 20,
                    keyword: ''
                }
            })

            if (result?.data) {
                const {content} = result?.data;
                let data: EmployeeType[] = [
                    {
                        value: null,
                        label: 'Please select'
                    },
                    ...content
                ]
                setEmpList(data)
            }

        } catch (e) {
            console.log('e', e)
        }
    }

    const initialOrder = () => {
        console.log("orderInfo?.createDate:", orderInfo?.createDate)
        setValue('id', orderInfo?.id || 0)
        setValue('contactName', orderInfo?.contactName || '')
        setValue('phone', orderInfo?.phone || '')
        setValue('name', orderInfo?.name || '')
        setValue('paymentDate', orderInfo?.paymentDate || '')
        setValue('totalDate', orderInfo?.totalDate || 0)
        setValue('agentId', orderInfo?.agentId || 0)
        setValue('customerId', orderInfo?.customers?.id || 0 )
        setValue('carId', orderInfo?.carId || 0 )
        setValue('pricePerDay', orderInfo?.pricePerDay || 0 )
        setValue('employeeId', orderInfo?.employeeId || 0 )
        setValue('agentName', orderInfo?.agentName || '' )
        setValue('pickUpPlace', orderInfo?.pickUpPlace || '' )
        setValue('pickUpTime', orderInfo?.pickUpTime || '' )
        setValue('dropPlace', orderInfo?.dropPlace || '' )
        setValue('dropTime', orderInfo?.dropTime || '' )
        setValue('description', orderInfo?.description || '' )
        setValue('paymentType', orderInfo?.paymentType || '' )
        setValue('isPayment', orderInfo?.isPayment || false )
        setValue('paymentAmount', orderInfo?.paymentAmount || 0 )
        setValue('paymentSlip', orderInfo?.paymentSlip || '' )
        setValue('paymentDescription', orderInfo?.paymentDescription || '' )
        setValue('currentMile', orderInfo?.currentMile || 0 )
        setValue('distantTotalMile', orderInfo?.distantTotalMile || 0 )
        setValue('startMile', orderInfo?.startMile || 0 )
        setValue('endMile', orderInfo?.endMile || 0 )
        setValue('oilPercent', orderInfo?.oilPercent || 0 )
        setValue('oilAVG', orderInfo?.oilAVG || 0 )
        setValue('customerName', orderInfo?.customerName || '' )
        setValue('address1', orderInfo?.address1 || '' )
        setValue('address2', orderInfo?.address2 || '' )
        setValue('contactPhone', orderInfo?.contactPhone || '' )
        setValue('city', orderInfo?.city || '' )
        setValue('province', orderInfo?.province || '' )
        setValue('postCode', orderInfo?.postCode || '' )
        setValue('createDate', orderInfo?.createdDate || '' )
    }

    useEffect(() => {
        getAgentList()
        getAllCar();
        getAllEmployee();
        getAllCustomersList();
        initialOrder();
    }, [orderInfo])

    const handleUpdate = async (value: any) => {
        try {

            console.log('value:', value)
            const req: FormData = new FormData();

            req.append('id', value?.id || 0)
            req.append('createDate', value?.createDate)
            req.append('phone', value?.phone)
            req.append('paymentDate', value?.paymentDate)
            req.append('name', value?.name)
            req.append('postCode', value?.postCode)
            req.append('customerName', value?.customerName)
            req.append('contactName', value?.contactName)
            // @ts-ignore
            req.append('totalDate', value?.totalDate )
            req.append('rentType', value?.rentType)
            // @ts-ignore
            req.append('agentId', value?.agentId)
            // @ts-ignore
            req.append('customerId', value?.customerId)
            // @ts-ignore
            req.append('carId', value?.carId)
            // @ts-ignore
            req.append('pricePerDay', value?.pricePerDay)
            // @ts-ignore
            req.append('employeeId', value?.employeeId)
            req.append('agentName', value?.agentName)
            req.append('pickUpPlace', value?.pickUpPlace)
            req.append('dropPlace', value?.dropPlace)
            req.append('pickUpTime', value?.pickUpTime)

            req.append('province', value?.province)
            req.append('address1', value?.address1)
            req.append('address2', value?.address2)
            req.append('dropTime', value?.dropTime)
            req.append('description', value?.description)
            req.append('paymentDate', value?.paymentDate)
            req.append('paymentType', value?.paymentType)
            // @ts-ignore
            req.append('isPayment', value?.isPayment)
            // @ts-ignore
            req.append('paymentAmount', value?.paymentAmount)
            req.append('paymentSlip', value?.paymentSlip)
            req.append('paymentDescription', value?.paymentDescription)
            // @ts-ignore
            req.append('currentMile', value?.currentMile)
            // @ts-ignore
            req.append('distantTotalMile', value?.distantTotalMile)
            // @ts-ignore
            req.append('startMile', value?.startMile)
            // @ts-ignore
            req.append('endMile', value?.endMile)
            // @ts-ignore
            req.append('oilPercent', value?.oilPercent)
            // @ts-ignore
            req.append('files', slip)

            await updateOrderApi({ body: req})
                .then((r:any) => {
                    handleClose()
                    window.location.href = '/order'
                })
                .catch((e:any) => {
                    handleClose()
                    window.location.href = '/order'
                })
        } catch (e: any) {
            console.log('e', e)
            handleClose()
        }
    }

    const PaymentModal = () => {
        return (
            <form onSubmit={handleSubmit(handleUpdate)}>
                <h4 className='modal-title'>General</h4>
                <div className='d-flex flex-rows justify-content-start'>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className='required'>Order date</span>
                        </label>
                        <Controller
                            name="createDate"
                            control={control}
                            rules={{
                                required: "date is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="date"
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                        {
                                            "is-invalid":
                                                errors?.createDate?.message && errors?.createDate?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors?.createDate?.message && !errors?.createDate?.message,
                                        }
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                        {errors?.createDate?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors?.createDate?.message}</span>
                            </div>
                        )}
                    </div>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className=''>จำนวน (วัน)</span>
                        </label>
                        <Controller
                            name="totalDate"
                            control={control}
                            rules={{
                                required: "field is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type='number'
                                    min={1}
                                    defaultValue={1}
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                        {
                                            "is-invalid":
                                                errors?.totalDate?.message && errors?.totalDate?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors?.totalDate?.message && !errors?.totalDate?.message,
                                        }
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                        {errors?.totalDate?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors?.totalDate?.message}</span>
                            </div>
                        )}
                    </div>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className=''>เลือกการเช่ารถ</span>
                        </label>
                        <select className="form-select w-150px" aria-label="Select example">
                            {
                                typeCarRent && typeCarRent.map((value, index) => {
                                    return (<option key={`index_of_order_type_car_${index}`}
                                                    value={value.value}>{value?.label}</option>
                                    )
                                })
                            }
                        </select>

                    </div>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className=''>กลุ่มลูกค้า</span>
                        </label>
                        <Controller
                            name="agentId"
                            control={control}
                            rules={{
                                required: "customerName is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <select onChange={onChange} value={value} className="form-select w-150px"
                                        aria-label="Select example">
                                    <option disabled={true}>Select</option>
                                    {
                                        agentList && agentList.map((data, index) => (
                                            <option key={`list_order_agent_${index}`}
                                                    value={data?.id}>{data.name}</option>
                                        ))
                                    }
                                </select>
                            )}
                        />
                    </div>
                </div>
                <div className='d-flex flex-rows justify-content-start mt-3'>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className='required'>Customer</span>
                        </label>
                        <Controller
                            name="customerId"
                            control={control}
                            rules={{
                                required: "customer is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <select onChange={onChange} value={value} className="form-select">
                                    {
                                        customerList && customerList.map((data, index) => (
                                            <option key={`index_order_customer_${index}`}
                                                    value={data?.id}>{data?.name || 'Please Select'}</option>
                                        ))
                                    }
                                </select>
                            )}
                        />
                    </div>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className='required'>Nickname</span>
                        </label>
                        <Controller
                            name="name"
                            control={control}
                            rules={{
                                required: "Nickname is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="text"
                                    maxLength={30}
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                        {
                                            "is-invalid":
                                                errors?.name?.message && errors?.name?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors?.name?.message && !errors?.name?.message,
                                        }
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                        {errors?.name?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors?.name?.message}</span>
                            </div>
                        )}
                    </div>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className='required'>Mobile</span>
                        </label>
                        <Controller
                            name="phone"
                            control={control}
                            rules={{
                                required: "Mobile is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="text"
                                    maxLength={10}
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                        {
                                            "is-invalid":
                                                errors?.phone?.message && errors?.phone?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors?.phone?.message && !errors?.phone?.message,
                                        }
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                        {errors?.phone?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors?.phone?.message}</span>
                            </div>
                        )}
                    </div>
                    {/*<div className="mx-1">*/}
                    {/*    <button type='button'*/}
                    {/*            className='btn btn-primary mx-1 mt-lg-14'*/}
                    {/*    >*/}
                    {/*        เพิ่มลูกค้าใหม่*/}
                    {/*    </button>*/}
                    {/*</div>*/}
                </div>
                <div className='d-flex flex-rows justify-content-start mt-3'>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className=''>ผู้ติดต่อประสานงาน</span>
                        </label>
                        <Controller
                            name="contactName"
                            control={control}
                            rules={{
                                required: "contactName is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="text"
                                    maxLength={30}
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                        {
                                            "is-invalid":
                                                errors?.contactName?.message && errors?.contactName?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors?.contactName?.message && !errors?.contactName?.message,
                                        }
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                        {errors?.contactName?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors?.contactName?.message}</span>
                            </div>
                        )}
                    </div>
                </div>
                <h4 className='modal-title mt-10'>Car List</h4>
                <div className='mt-2'>
                    <div className='d-flex flex-row justify-content-between text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0'>
                        <p className="min-w-25px">Products</p>
                        <p className="min-w-25px">Plate</p>
                        <p className="min-w-25px">Prices/Day</p>
                        <p className="min-w-25px">Select Drivers</p>
                    </div>
                    <div className='d-flex flex-row justify-content-between text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0'>
                        <div className='min-w-125px'>
                            {carName}
                        </div>
                        <div>
                            <Controller
                                name="carId"
                                control={control}
                                rules={{
                                    required: "car is require",
                                }}
                                render={({field: {value, onChange}}) => (
                                    <select onChange={(e) => {
                                        setCarName(e.target.value)
                                        onChange(e)
                                    }} value={value} className="form-select" aria-label="Select example">
                                        {
                                            carList && carList.map((data, index) => (
                                                <option key={`index_order_car_${index}`}
                                                        value={data?.id}>{data?.vinNo || 'Please select'} {data?.brand}</option>
                                            ))
                                        }
                                    </select>
                                )}
                            />
                        </div>
                        <div>
                            <Controller
                                name="pricePerDay"
                                control={control}
                                rules={{
                                    required: "field is require",
                                }}
                                render={({field: {value, onChange}}) => (
                                    <input
                                        type="number"
                                        min={0}
                                        value={value}
                                        onChange={onChange}
                                        className={clsx(
                                            "form-control form-control-lg form-control-solid w-150px",
                                            {
                                                "is-invalid":
                                                    errors?.pricePerDay?.message && errors?.pricePerDay?.message,
                                            },
                                            {
                                                "is-valid":
                                                    errors?.pricePerDay?.message && !errors?.pricePerDay?.message,
                                            }
                                        )}
                                        placeholder=""
                                    />
                                )}
                            />
                            {errors?.pricePerDay?.message && (
                                <div className="fv-plugins-message-container text-danger">
                                    <span role="alert">{errors?.pricePerDay?.message}</span>
                                </div>
                            )}
                        </div>
                        <div className='min-w-125px'>
                            <Controller
                                name="employeeId"
                                control={control}
                                rules={{
                                    required: "field is require",
                                }}
                                render={({field: {value, onChange}}) => (
                                    <select onChange={onChange} value={value} className="form-select">
                                        {
                                            empList && empList.map((data, index) => {
                                                return <option key={`index_order_emp_${index}`}
                                                               value={data?.id}>{data?.firstName || 'Please select'}</option>
                                            })
                                        }
                                    </select>
                                )}
                            />
                        </div>
                    </div>
                </div>
                <h4 className='modal-title mt-10'>รายละเอียดงาน</h4>
                <div className='d-flex flex-rows justify-content-start mt-3'>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className='required'>สถานที่ไปรับ</span>
                        </label>
                        <Controller
                            name="pickUpPlace"
                            control={control}
                            rules={{
                                required: "field is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="text"
                                    min={0}
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid w-275px",
                                        {
                                            "is-invalid":
                                                errors?.pickUpPlace?.message && errors?.pickUpPlace?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors?.pickUpPlace?.message && !errors?.pickUpPlace?.message,
                                        }
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                        {errors?.pickUpPlace?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors?.pricePerDay?.message}</span>
                            </div>
                        )}
                    </div>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className='required'>เวลานัดหมาย</span>
                        </label>
                        <Controller
                            name="pickUpTime"
                            control={control}
                            rules={{
                                required: "field is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="time"
                                    min={0}
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid w-275px",
                                        {
                                            "is-invalid":
                                                errors?.pickUpTime?.message && errors?.pickUpTime?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors?.pickUpTime?.message && !errors?.pickUpTime?.message,
                                        }
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                        {errors?.pickUpTime?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors?.pickUpTime?.message}</span>
                            </div>
                        )}
                    </div>
                </div>
                <div className='d-flex flex-rows justify-content-start mt-3'>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className='required'>สถานที่ปลายทาง</span>
                        </label>
                        <Controller
                            name="dropPlace"
                            control={control}
                            rules={{
                                required: "field is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="text"
                                    min={0}
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid w-275px",
                                        {
                                            "is-invalid":
                                                errors?.dropPlace?.message && errors?.dropPlace?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors?.dropPlace?.message && !errors?.dropPlace?.message,
                                        }
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                        {errors?.dropPlace?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors?.dropPlace?.message}</span>
                            </div>
                        )}
                    </div>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className='required'>คาดว่าจบงาน</span>
                        </label>
                        <Controller
                            name="dropTime"
                            control={control}
                            rules={{
                                required: "field is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="time"
                                    min={0}
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid w-275px",
                                        {
                                            "is-invalid":
                                                errors?.dropTime?.message && errors?.dropTime?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors?.dropTime?.message && !errors?.dropTime?.message,
                                        }
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                        {errors?.dropTime?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors?.dropTime?.message}</span>
                            </div>
                        )}
                    </div>
                </div>
                <div className='d-flex flex-rows justify-content-start mt-3 mb-10'>
                    <div className="mx-1">
                        <label className='col-form-label fw-bold fs-6'>
                            <span className=''>หมายเหตุ</span>
                        </label>
                        <Controller
                            name="description"
                            control={control}
                            rules={{
                                required: false,
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="text"
                                    min={0}
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid w-275px",
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                    </div>
                </div>
                <Accordion defaultActiveKey={['0']} alwaysOpen>
                    <Accordion.Item eventKey="0">
                        <Accordion.Header>
                            <div className="d-flex align-items-center">
                                <div>
                                    <span className="fw-bolder fs-2">Payments</span><br/>
                                    <span className="fw-bold fs-6 text-muted">กรอกรายละเอียด ข้อมูลในการชำระเงินของลูกค้า</span>
                                </div>
                            </div>
                        </Accordion.Header>
                        <Accordion.Body>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 row">
                                <label className='fw-bolder fs-6 mb-0 col-xl-12'>
                                    <span>Payment Option</span>
                                </label>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-3 row mb-3">
                                <div className="col-xl-4 me-5">
                                    <Controller
                                        name="paymentType"
                                        control={control}
                                        rules={{
                                            required: false,
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <select onChange={onChange} className="form-select"
                                                    aria-label="Select Payment method">
                                                {
                                                    typePayment && typePayment.map((value, index) => {
                                                        return (
                                                            <option key={`index_order_type_payment_${index}`}
                                                                    value={value.value}>{value.label}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                        )}
                                    />
                                </div>
                                <div className="form-check form-check-custom form-check-solid col-xl-4">
                                    <Controller
                                        name="isPayment"
                                        control={control}
                                        rules={{
                                            required: false,
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input className="form-check-input"
                                                   type="checkbox"
                                                   onChange={onChange}
                                                   checked={value === true}
                                                   id="flexCheckPrice"/>
                                        )}
                                    />
                                    <label className="form-check-label fw-bolder" htmlFor="flexCheckPrice">
                                        ตรวจสอบยอดเงินแล้ว
                                    </label>
                                </div>
                            </div>
                            <div className="d-flex flex-row jutify-content-start p-0 m-0 row">
                                <label className='fw-bolder fs-6 mb-0 col-xl-4'>
                                    <span>ยอดเงิน</span>
                                </label>
                                <label className='fw-bolder fs-6 mb-0 col-xl-4'>
                                    <span>วันที่ชำระ</span>
                                </label>
                                <label className='fw-bolder fs-6 mb-0 col-xl-4'>
                                    <span>Upload slip</span>
                                </label>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-3 row">
                                <div className="col-xl-4">
                                    <Controller
                                        name="paymentAmount"
                                        control={control}
                                        rules={{
                                            required: "field is require",
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="number"
                                                min={0}
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid",
                                                    {
                                                        "is-invalid":
                                                            errors?.paymentAmount?.message && errors?.paymentAmount?.message,
                                                    },
                                                    {
                                                        "is-valid":
                                                            errors?.paymentAmount?.message && !errors?.paymentAmount?.message,
                                                    }
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                    {errors?.paymentAmount?.message && (
                                        <div className="fv-plugins-message-container text-danger">
                                            <span role="alert">{errors?.paymentAmount?.message}</span>
                                        </div>
                                    )}
                                </div>
                                <div className="col-xl-4">
                                    <Controller
                                        name="paymentDate"
                                        control={control}
                                        rules={{
                                            required: "field is require",
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="date"
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid",
                                                    {
                                                        "is-invalid":
                                                            errors?.paymentDate?.message && errors?.paymentDate?.message,
                                                    },
                                                    {
                                                        "is-valid":
                                                            errors?.paymentDate?.message && !errors?.paymentDate?.message,
                                                    }
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                    {errors?.paymentDate?.message && (
                                        <div className="fv-plugins-message-container text-danger">
                                            <span role="alert">{errors?.paymentDate?.message}</span>
                                        </div>
                                    )}
                                </div>
                                <div className="col-xl-4">
                                    {/*<button*/}
                                    {/*    type="button"*/}
                                    {/*    className="btn btn-primary me-3"*/}
                                    {/*>*/}
                                    {/*    Upload Slip*/}
                                    {/*</button>*/}
                                    <Form.Control onChange={(e) => {
                                        // @ts-ignore
                                        console.log(e.target?.files[0])
                                        // @ts-ignore
                                        setSlip(e.target?.files[0])
                                    }} className="me-3" type="file" placeholder={'Upload slip'}/>

                                    {/*<button*/}
                                    {/*    type="button"*/}
                                    {/*    className="btn btn-primary"*/}
                                    {/*>*/}
                                    {/*    <i*/}
                                    {/*        className="bi bi-plus-lg fs-2"*/}
                                    {/*    />*/}
                                    {/*</button>*/}
                                </div>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 row">
                                <label className='fw-bolder fs-6 mb-0 col-xl-12'>
                                    <span>หมายเหตุ</span>
                                </label>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-3 row">
                                <div className="col-xl-8">
                                    <Controller
                                        name="paymentDescription"
                                        control={control}
                                        rules={{
                                            required: false,
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="text"
                                                maxLength={50}
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid",
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                </div>
                            </div>
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="1">
                        <Accordion.Header>
                            <div className="d-flex align-items-center">
                                <div>
                                    <span className="fw-bolder fs-2">Car Information</span><br/>
                                    <span className="fw-bold fs-6 text-muted">กรอกรายละเอียด ข้อมูลในการชำระเงินของลูกค้า</span>
                                </div>
                            </div>
                        </Accordion.Header>
                        <Accordion.Body>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 row mb-5">
                                <label className='fw-bolder fs-4 text-decoration-underline mb-0 col-xl-12'>
                                    <span>สรุปการใช้งานรถ</span>
                                </label>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <label className='fw-bolder fs-6 mb-0 col-xl-3'>
                                    <span>ระยะไมล์รถปัจจุบัน</span>
                                </label>
                                <label className='fw-bolder fs-6 mb-0 col-xl-3'>
                                    <span>ระยะทางที่วิ่งไปทั้งหมด</span>
                                </label>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <div className="col-xl-3">
                                    <Controller
                                        name="currentMile"
                                        control={control}
                                        rules={{
                                            required: "field is require",
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="text"
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid",
                                                    {
                                                        "is-invalid":
                                                            errors?.currentMile?.message && errors?.currentMile?.message,
                                                    },
                                                    {
                                                        "is-valid":
                                                            errors?.currentMile?.message && !errors?.currentMile?.message,
                                                    }
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                    {errors?.currentMile?.message && (
                                        <div className="fv-plugins-message-container text-danger">
                                            <span role="alert">{errors?.currentMile?.message}</span>
                                        </div>
                                    )}
                                </div>
                                <div className="col-xl-3">
                                    <Controller
                                        name="distantTotalMile"
                                        control={control}
                                        rules={{
                                            required: "field is require",
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="text"
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid",
                                                    {
                                                        "is-invalid":
                                                            errors?.distantTotalMile?.message && errors?.distantTotalMile?.message,
                                                    },
                                                    {
                                                        "is-valid":
                                                            errors?.distantTotalMile?.message && !errors?.distantTotalMile?.message,
                                                    }
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                    {errors?.distantTotalMile?.message && (
                                        <div className="fv-plugins-message-container text-danger">
                                            <span role="alert">{errors?.distantTotalMile?.message}</span>
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <label className='fw-bolder fs-6 mb-0 col-xl-3'>
                                    <span>เลขไมล์เมื่อออกรถ</span>
                                </label>
                                <label className='fw-bolder fs-6 mb-0 col-xl-3'>
                                    <span>เลขไมล์เมื่อคืนรถ</span>
                                </label>
                                <label className='fw-bolder fs-6 mb-0 col-xl-3'>
                                    <span>น้ำมันเหลือที่เปอร์เซนต์</span>
                                </label>
                                <label className='fw-bolder fs-6 mb-0 col-xl-3'>
                                    <span>เฉลี่ยน้ำมัน กิโล / บาท</span>
                                </label>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <div className="col-xl-3">
                                    <Controller
                                        name="startMile"
                                        control={control}
                                        rules={{
                                            required: "field is require",
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="number"
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid",
                                                    {
                                                        "is-invalid":
                                                            errors?.startMile?.message && errors?.startMile?.message,
                                                    },
                                                    {
                                                        "is-valid":
                                                            errors?.startMile?.message && !errors?.startMile?.message,
                                                    }
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                    {errors?.startMile?.message && (
                                        <div className="fv-plugins-message-container text-danger">
                                            <span role="alert">{errors?.startMile?.message}</span>
                                        </div>
                                    )}
                                </div>
                                <div className="col-xl-3">
                                    <Controller
                                        name="endMile"
                                        control={control}
                                        rules={{
                                            required: "field is require",
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="number"
                                                min={0}
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid",
                                                    {
                                                        "is-invalid":
                                                            errors?.endMile?.message && errors?.endMile?.message,
                                                    },
                                                    {
                                                        "is-valid":
                                                            errors?.endMile?.message && !errors?.endMile?.message,
                                                    }
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                    {errors?.endMile?.message && (
                                        <div className="fv-plugins-message-container text-danger">
                                            <span role="alert">{errors?.endMile?.message}</span>
                                        </div>
                                    )}
                                </div>
                                <div className="col-xl-3">
                                    <Controller
                                        name="oilPercent"
                                        control={control}
                                        rules={{
                                            required: "field is require",
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="number"
                                                min={0}
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid",
                                                    {
                                                        "is-invalid":
                                                            errors?.oilPercent?.message && errors?.oilPercent?.message,
                                                    },
                                                    {
                                                        "is-valid":
                                                            errors?.oilPercent?.message && !errors?.oilPercent?.message,
                                                    }
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                    {errors?.oilPercent?.message && (
                                        <div className="fv-plugins-message-container text-danger">
                                            <span role="alert">{errors?.oilPercent?.message}</span>
                                        </div>
                                    )}
                                </div>
                                <div className="col-xl-3">
                                    <Controller
                                        name="oilAVG"
                                        control={control}
                                        rules={{
                                            required: "field is require",
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="number"
                                                min={0}
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid",
                                                    {
                                                        "is-invalid":
                                                            errors?.oilAVG?.message && errors?.oilAVG?.message,
                                                    },
                                                    {
                                                        "is-valid":
                                                            errors?.oilAVG?.message && !errors?.oilAVG?.message,
                                                    }
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                    {errors?.oilAVG?.message && (
                                        <div className="fv-plugins-message-container text-danger">
                                            <span role="alert">{errors?.oilAVG?.message}</span>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="2">
                        <Accordion.Header>
                            <div className="d-flex align-items-center">
                                <div>
                                    <span className="fw-bolder fs-2">Billing Address</span><br/>
                                    <span
                                        className="fw-bold fs-6 text-muted">ที่อยู่สำหรับเรียกเก็บเงินคือที่อยู่ที่เชื่อมต่อกับวิธีการชำระเงินของลูกค้า</span>
                                </div>
                            </div>
                        </Accordion.Header>
                        <Accordion.Body>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-8 row">
                                {/*<div className="form-check form-check-custom form-check-solid col-xl-12">*/}
                                {/*    <input className="form-check-input me-2" type="checkbox" value=""*/}
                                {/*           id="flexCheckDefaultInfo"/>*/}
                                {/*    <label className="form-check-label fw-bolder" htmlFor="flexCheckDefaultInfo">*/}
                                {/*        ใช้ข้อมูลเดิมในระบบ*/}
                                {/*    </label>*/}
                                {/*</div>*/}
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <label className='fw-bolder fs-6 mb-0 ps-0 col-xl-12'>
                                    <span>Customer name</span>
                                </label>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <div className="col-xl-8 ps-0">
                                    <Controller
                                        name="customerName"
                                        control={control}
                                        rules={{
                                            required: "field is require",
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="text"
                                                maxLength={50}
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid",
                                                    {
                                                        "is-invalid":
                                                            errors?.customerName?.message && errors?.customerName?.message,
                                                    },
                                                    {
                                                        "is-valid":
                                                            errors?.customerName?.message && !errors?.customerName?.message,
                                                    }
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                    {errors?.customerName?.message && (
                                        <div className="fv-plugins-message-container text-danger">
                                            <span role="alert">{errors?.customerName?.message}</span>
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <label className='fw-bolder fs-6 mb-0 ps-0 col-xl-12'>
                                    <span className="required">Address Line 1</span>
                                </label>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <div className="col-xl-8 ps-0">
                                    <Controller
                                        name="address1"
                                        control={control}
                                        rules={{
                                            required: false,
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="text"
                                                maxLength={50}
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid"
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                </div>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <label className='fw-bolder fs-6 mb-0 ps-0 col-xl-12'>
                                    <span className="required">Mobile Phone</span>
                                </label>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <div className="col-xl-8 ps-0">
                                    <Controller
                                        name="contactPhone"
                                        control={control}
                                        rules={{
                                            required: false,
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="text"
                                                maxLength={10}
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid"
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                </div>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <label className='fw-bolder fs-6 mb-0 ps-0 col-xl-4'>
                                    <span>Province</span>
                                </label>
                                <label className='fw-bolder fs-6 mb-0 ps-0 col-xl-4'>
                                    <span>PostCode</span>
                                </label>
                            </div>
                            <div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">
                                <div className="col-xl-4 ps-0">
                                    <Controller
                                        name="province"
                                        control={control}
                                        rules={{
                                            required: false,
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <select onChange={onChange} className="form-select"
                                                    aria-label="Select City">
                                                {
                                                    PROVICE && PROVICE.map((value, index) => (
                                                        <option key={`index_order_province_${index}`}
                                                                value={value?.PROVINCE_NAME}>{value?.PROVINCE_NAME}</option>
                                                    ))
                                                }
                                            </select>
                                        )}
                                    />
                                </div>
                                <div className="col-xl-4 ps-0">
                                    <Controller
                                        name="postCode"
                                        control={control}
                                        rules={{
                                            required: false,
                                        }}
                                        render={({field: {value, onChange}}) => (
                                            <input
                                                type="text"
                                                maxLength={50}
                                                value={value}
                                                onChange={onChange}
                                                className={clsx(
                                                    "form-control form-control-lg form-control-solid"
                                                )}
                                                placeholder=""
                                            />
                                        )}
                                    />
                                </div>
                            </div>
                            {/*<div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">*/}
                            {/*    <label className='fw-bolder fs-6 mb-0 ps-0 col-xl-4'>*/}
                            {/*        <span>Country</span>*/}
                            {/*    </label>*/}
                            {/*    <label className='fw-bolder fs-6 mb-0 ps-0 col-xl-4'>*/}
                            {/*        <span>Post Code</span>*/}
                            {/*    </label>*/}
                            {/*</div>*/}
                            {/*<div className="d-flex flex-row justify-content-start p-0 m-0 mb-3 row">*/}
                            {/*    <div className="col-xl-4 ps-0">*/}
                            {/*        <select className="form-select" aria-label="Select City">*/}
                            {/*            <option>Select Option</option>*/}
                            {/*            <option value="1">One</option>*/}
                            {/*            <option value="2">Two</option>*/}
                            {/*            <option value="3">Three</option>*/}
                            {/*        </select>*/}
                            {/*    </div>*/}
                            {/*    <div className="col-xl-4 ps-0">*/}
                            {/*        <input*/}
                            {/*            type='text'*/}
                            {/*            className="form-control"*/}
                            {/*        />*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
                <div className='d-flex flex-row justify-content-end mt-2'>
                    <Button variant="secondary" type={'button'} onClick={handleClose}>
                        Discard
                    </Button>
                    <Button variant="primary" className='mx-2' type={'submit'}>
                        Add Order
                    </Button>
                </div>
            </form>
        )
    }
    return (
        <ModalApp
            show={show}
            handleOpen={handleOpen}
            handleClose={handleClose}
            type={'submit'}
            chil={<PaymentModal/>}
            title={'Edit Order'}/>
    )
}

export default EditOrder;
