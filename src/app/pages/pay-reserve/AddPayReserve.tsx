import clsx from "clsx";
import { remove } from "lodash";
import { FC, useState } from "react";
import { Controller, useFieldArray, useForm } from "react-hook-form";
import { createPayReserveAPI } from "../../../api/PayReserveApi";
import { PageTitle } from "../../../_metronic/layout/core";
type PayReserveItem = {
  amount: number;
  createdDate: string;
  id: number;
  isActive: boolean;
  title: string;
  totalAmount: number;
  totalItem: number;
  updatedDate: string;
};
const AddPayReserve: FC = () => {
  const invoiceId = `${new Date().getFullYear()}${new Date().getMonth()}${new Date().getDate()}${new Date().getHours()}${new Date().getMinutes()}`  
  const {
    handleSubmit,
    register,
    control,
    formState: { errors },
  } = useForm({
    defaultValues: {
      payReserve: {
        invoiceNo: `${new Date().getFullYear()}${new Date().getMonth()}${new Date().getDate()}${new Date().getHours()}${new Date().getMinutes()}`,
        createBy: "ADMIN",
        payDate: "",
        dueDate: "",
        billFrom: "",
        userFrom: "",
        emailFrom: "",
        noteFrom: "",
        billTo: "",
        userTo: "",
        emailTo: "",
        noteTo: "",
        amount: 0,
        discount: 0,
        taxAmount: 0,
        currency: "THB",
        isPayment: false,
        isLateFee: false,
        description: "",
        payReserveItem: [],
      },
      payReserveItems: [
        {
          amount: 0,
          title: "",
          totalAmount: 0,
          totalItem: 0,
        },
      ],
    },
  });

  const { fields, append, remove } = useFieldArray({
    control,
    name: "payReserveItems",
  });

  const onSubmit = async (data: any) => {
    // console.log("data", data)

    const body = {
        ...data?.payReserve
    }
    body.payReserveItem = data.payReserveItems;
    // console.log('body', body);
    const result = await createPayReserveAPI({ body: body});

    window.location.href = '/pay-reserve'
    

  };

  return (
    <>
      <PageTitle breadcrumbs={[]}>
        Home - Car Service - Add Pay Reserve Add
      </PageTitle>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="row g-5 g-xxl-8">
          <div className="col-xl-8">
            <div className="mb-5 mb-xxl-8 bg-white">
              {/* begin::Body */}
              <form onSubmit={undefined} noValidate className="form">
                <div className="card-body border-top p-9">
                  <div className="row mb-6">
                    <div className="row mb-6">
                      <div className="col-lg-12">
                        <div className="row">
                          <div className="col-lg-12 fv-row d-flex justify-content-between">
                            <label className="col-form-label fw-bold fs-6">
                              Date:
                            </label>
                            <Controller
                              name={`payReserve.payDate`}
                              control={control}
                              render={({ field: { onChange, value } }) => (
                                <input
                                  type="date"
                                  value={value}
                                  onChange={onChange}
                                  className="w-150px form-control form-control-lg form-control-solid mb-3 mb-lg-0"
                                  
                                  placeholder=""
                                  name={`payReserve.payDate`}
                                />
                              )}
                            />
                            <span className="text-gray-800  fs-2 fw-bolder me-1">
                              Invoice#{invoiceId}
                            </span>
                            <label className="col-form-label fw-bold fs-6">
                              Date:
                            </label>
                            <Controller
                              name={`payReserve.dueDate`}
                              control={control}
                              rules={{
                                required: true,
                              }}
                              render={({ field: { onChange, value } }) => (
                                <input
                                  type="date"
                                  value={value}
                                  onChange={onChange}
                                  className={clsx(
                                    "w-150px form-control form-control-lg form-control-solid mb-3 mb-lg-0",
                                    {
                                      'is-invalid': errors.payReserve?.payDate?.message && errors.payReserve?.payDate?.message,
                                    },
                                    {
                                      'is-valid': errors.payReserve?.payDate?.message && !errors.payReserve?.payDate?.message,
                                    }
                                  )}
                                  placeholder=""
                                  name={`payReserve.dueDate`}
                                />
                              )}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row mb-6 mt-6 border-bottom-dotted border-gray-300" />
                    <div className="row mb-6">
                      <div className="col-lg-12">
                        <div className="row">
                          <div className="col-lg-6 fv-row">
                            <label className="col-form-label fw-bold fs-6">
                              Bill from
                            </label>
                            <Controller
                              name={`payReserve.userFrom`}
                              control={control}
                              rules={{
                                required: 'กรุณาระบุข้อมูล',
                              }}
                              render={({ field: { onChange, value } }) => (
                                <input
                                  type="text"
                                  value={value}
                                  onChange={onChange}
                                  className={clsx(
                                    "form-control form-control-lg form-control-solid mb-3 mb-lg-0",
                                    {
                                      'is-invalid': errors.payReserve?.userFrom?.message && errors.payReserve?.userFrom?.message,
                                    },
                                    {
                                      'is-valid': errors.payReserve?.message && !errors.payReserve?.message,
                                    }
                                  )}
                                  placeholder=""
                                  name={`payReserve.userFrom`}
                                />
                              )}
                            />
                            <Controller
                              name={`payReserve.emailFrom`}
                              control={control}
                              render={({ field: { onChange, value } }) => (
                                <input
                                  type="text"
                                  value={value}
                                  onChange={onChange}
                                  className="form-control mt-2 form-control-lg form-control-solid mb-3 mb-lg-0"
                                  placeholder="Email"
                                  name={`payReserve.emailFrom`}
                                />
                              )}
                            />
                            <Controller
                              name={`payReserve.noteFrom`}
                              control={control}
                              render={({ field: { onChange, value } }) => (
                                <textarea
                                  rows={4}
                                  value={value}
                                  onChange={onChange}
                                  className="mt-3 form-control form-control-lg form-control-solid mb-3 mb-lg-0"
                                  placeholder=""
                                  name={`payReserve.noteFrom`}
                                />
                              )}
                            />
                          </div>
                          <div className="col-lg-6 fv-row">
                            <label className="col-form-label fw-bold fs-6">
                              Bill to
                            </label>
                            <Controller
                              name={`payReserve.billTo`}
                              control={control}
                              render={({ field: { onChange, value } }) => (
                                <input
                                  type="text"
                                  value={value}
                                  onChange={onChange}
                                  className={clsx(
                                    "form-control form-control-lg form-control-solid mb-3 mb-lg-0",
                                    {
                                      'is-invalid': errors.payReserve?.billTo?.message && errors.payReserve?.billTo?.message,
                                    },
                                    {
                                      'is-valid': errors.payReserve?.message && !errors.payReserve?.message,
                                    }
                                  )}
                                  placeholder=""
                                  name={`payReserve.billTo`}
                                />
                              )}
                            />
                            <Controller
                              name={`payReserve.emailTo`}
                              control={control}
                              render={({ field: { onChange, value } }) => (
                                <input
                                  type="text"
                                  value={value}
                                  onChange={onChange}
                                  className="form-control form-control-lg mt-2 form-control-solid mb-3 mb-lg-0"
                                  placeholder="Email"
                                  name={`payReserve.emailTo`}
                                />
                              )}
                            />
                            <Controller
                              name={`payReserve.noteTo`}
                              control={control}
                              render={({ field: { onChange, value } }) => (
                                <textarea
                                  rows={4}
                                  value={value}
                                  onChange={onChange}
                                  className="mt-3 form-control form-control-lg form-control-solid mb-3 mb-lg-0"
                                  placeholder=""
                                  name={`payReserve.noteTo`}
                                />
                              )}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row mb-6">
                      <div className="col-lg-12">
                        <div className="table-responsive">
                          {/* begin::Table */}
                          <table className="table table-row-gray-300 align-middle gs-0 gy-4">
                            {/* begin::Table head */}
                            <thead>
                              <tr className="fw-bolder text-gray-400">
                                <th className="min-w-150px">ITEM</th>
                                <th className="min-w-50px">QTY</th>
                                <th className="min-w-50px">PRICE</th>
                                <th className="min-w-50px">TOTAL</th>
                                <th className="min-w-50px">ACTION</th>
                              </tr>
                            </thead>
                            {/* end::Table head */}
                            {fields &&
                              fields.map((d, index) => {
                                return (
                                  <tr
                                    className="border-bottom border-gray-300"
                                    key={`index_of_payreserve_item_${index}`}
                                  >
                                    <td>
                                      <Controller
                                        name={`payReserveItems.${index}.title`}
                                        control={control}
                                        render={({
                                          field: { onChange, value },
                                        }) => (
                                          <input
                                            type="text"
                                            value={value}
                                            onChange={onChange}
                                            className="w-75 form-control form-control-lg form-control-solid mb-3 mb-lg-0 mx-1"
                                            placeholder=""
                                            name={`payReserveItems[${index}].title`}
                                          />
                                        )}
                                      />
                                    </td>
                                    <td>
                                      <Controller
                                        name={`payReserveItems.${index}.totalItem`}
                                        control={control}
                                        render={({
                                          field: { onChange, value },
                                        }) => (
                                          <input
                                            type="number"
                                            min={0}
                                            value={value}
                                            onChange={onChange}
                                            className="w-50px form-control form-control-lg form-control-solid mb-3 mb-lg-0"
                                            placeholder=""
                                            name={`payReserveItems[${index}].totalItem`}
                                          />
                                        )}
                                      />
                                    </td>
                                    <td>
                                      <Controller
                                        name={`payReserveItems.${index}.amount`}
                                        control={control}
                                        render={({
                                          field: { onChange, value },
                                        }) => (
                                          <input
                                            type="number"
                                            min={0}
                                            value={value}
                                            onChange={onChange}
                                            className="w-100px form-control form-control-lg form-control-solid mb-3 mb-lg-0"
                                            placeholder=""
                                            name={`payReserveItems[${index}].amount`}
                                          />
                                        )}
                                      />
                                    </td>
                                    <td>
                                      <Controller
                                        name={`payReserveItems.${index}.totalAmount`}
                                        control={control}
                                        render={({
                                          field: { onChange, value },
                                        }) => (
                                          <input
                                            type="number"
                                            min={0}
                                            value={value}
                                            onChange={onChange}
                                            className="w-100px form-control form-control-lg form-control-solid mb-3 mb-lg-0"
                                            placeholder=""
                                            name={`payReserveItems[${index}].totalAmount`}
                                          />
                                        )}
                                      />
                                    </td>
                                    <td>
                                      <div className="symbol symbol-45px me-5 text-end">
                                        {/* <span className="menu-icon">
                                      <i
                                        className={clsx(
                                          "bi fs-3",
                                          "/media/icons/duotune/general/gen027.svg"
                                        )}
                                      />
                                    </span> */}
                                        <button
                                          type="button"
                                          onClick={() => {
                                            remove(index);
                                          }}
                                          disabled={index === 0}
                                          className="btn btn-danger col-form-label text-white fw-bold fs-6"
                                        >
                                          Delete
                                        </button>
                                        {index === fields.length - 1 ? (
                                          <button
                                            type="button"
                                            onClick={() => {
                                              append({
                                                amount: 0,
                                                title: "",
                                                totalAmount: 0,
                                                totalItem: 0,
                                              });
                                            }}
                                            className="btn btn-primary mx-2 col-form-label text-white fw-bold fs-6"
                                          >
                                            Add
                                          </button>
                                        ) : null}
                                      </div>
                                    </td>
                                  </tr>
                                );
                              })}
                          </table>
                          {/* end::Table */}
                          {/* <tr className="border-bottom border-bottom-dotted border-gray-300">
                            <td>
                              <input
                                type="text"
                                className="w-75 form-control form-control-lg form-control-solid mb-3 mb-lg-0 mx-6"
                                placeholder="First name"
                              />
                            </td>
                          </tr> */}
                        </div>
                      </div>
                    </div>
                    <div className="row mb-6">
                      <div className="col-lg-6">
                        {/* <label className='col-form-label text-muted fw-bold fs-6'></label> */}
                      </div>
                      <div className="col-lg-6">
                        <div className="d-flex flex-row justify-content-between">
                          <label className="col-form-label text-dark fw-bold fs-6">
                            Sub total
                          </label>
                          <label className="col-form-label text-dark fw-bold fs-6"></label>
                        </div>
                        <div className="d-flex flex-row justify-content-between">
                          <label className="col-form-label text-muted fw-bold fs-6">
                            Add tax
                          </label>
                          <Controller
                            name={`payReserve.taxAmount`}
                            control={control}
                            render={({ field: { onChange, value } }) => (
                              <input
                                type="text"
                                value={value}
                                onChange={onChange}
                                className="form-control form-control-lg mt-2 form-control-solid text-dark fw-bold fs-6 mb-3 w-150px mb-lg-0"
                                placeholder="tax"
                                name={`payReserve.taxAmount`}
                              />
                            )}
                          />
                        </div>
                        <div className="d-flex flex-row justify-content-between">
                          <label className="col-form-label text-muted fw-bold fs-6">
                            Add discount
                          </label>
                          <Controller
                            name={`payReserve.discount`}
                            control={control}
                            render={({ field: { onChange, value } }) => (
                              <input
                                type="text"
                                value={value}
                                onChange={onChange}
                                className="form-control form-control-lg mt-2 form-control-solid text-dark fw-bold fs-6 mb-3 w-150px mb-lg-2"
                                placeholder="discount"
                                name={`payReserve.discount`}
                              />
                            )}
                          />
                        </div>
                        <div className="border-bottom-dotted border-gray-300" />
                        <div className="d-flex flex-row justify-content-between mt-6">
                          <label className="col-form-label text-dark fw-bold fs-6">
                            Total
                          </label>
                          <Controller
                            name={`payReserve.amount`}
                            control={control}
                            render={({ field: { onChange, value } }) => (
                              <input
                                type="text"
                                value={value}
                                onChange={onChange}
                                className="form-control form-control-lg mt-2 form-control-solid text-dark fw-bold fs-6 mb-3 w-150px mb-lg-0"
                                placeholder="total"
                                name={`payReserve.amount`}
                              />
                            )}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row mb-6">
                      <div className="col-lg-12">
                        <label className="col-form-label fw-bold fs-6">
                          Note
                        </label>
                        <div className="row">
                          <div className="col-lg-12 fv-row">
                            <Controller
                              name={`payReserve.description`}
                              control={control}
                              render={({ field: { onChange, value } }) => (
                                <textarea
                                  rows={4}
                                  value={value}
                                  onChange={onChange}
                                  className="form-control mt-2 form-control-lg form-control-solid mb-3 mb-lg-0"
                                  placeholder="description"
                                  name={`payReserve.description`}
                                />
                              )}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              {/* end::Body */}
            </div>
          </div>
          <div className="col-xl-4">
            <div className="mb-5 mb-xxl-8">
              {/* begin::Body */}
              <div className="flex-grow-1">
                <div className="d-flex justify-content-center align-items-center flex-wrap mb-2 card">
                  <div className="d-flex flex-column mb-5">
                    <div className="row">
                      <div className="col-lg-12 mx-5">
                        <label className="col-form-label text-dark fw-bold fs-6">
                          Currency
                        </label>
                        <div className="mt-6 w-75 d-flex flex-row justify-content-around">
                          <Controller
                            name={`payReserve.currency`}
                            control={control}
                            render={({ field: { onChange, value } }) => (
                              <select
                                onChange={onChange}
                                value={value}
                                className="form-select"
                                aria-label="Select example"
                              >
                                <option>Select Currency</option>
                                <option value="THB">THB</option>
                                <option value="JYP">JYP</option>
                                <option value="USD">USD</option>
                              </select>
                            )}
                          />
                        </div>
                      </div>
                      {/* <div className="col-lg-12 mx-5">
                        <div className="d-flex flex-row justify-content-between">
                          <label className="col-form-label text-dark fw-bold fs-6">
                            Payment method
                          </label>
                          <div className="col-form-label text-dark fw-bold fs-6 form-check form-switch form-check-custom form-check-solid me-10">
                            <input
                              className="form-check-input h-20px w-30px"
                              type="checkbox"
                              value=""
                              id="flexSwitch20x30"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-12 mx-5">
                        <div className="d-flex flex-row justify-content-between">
                          <label className="col-form-label text-dark fw-bold fs-6">
                            Late feeds
                          </label>
                          <div className="col-form-label text-dark fw-bold fs-6 form-check form-switch form-check-custom form-check-solid me-10">
                            <input
                              className="form-check-input h-20px w-30px"
                              type="checkbox"
                              value=""
                              id="flexSwitch20x30"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-12 mx-5">
                        <div className="d-flex flex-row justify-content-between">
                          <label className="col-form-label text-dark fw-bold fs-6">
                            Note
                          </label>
                          <div className="col-form-label text-dark fw-bold fs-6 form-check form-switch form-check-custom form-check-solid me-10">
                            <input
                              className="form-check-input h-20px w-30px"
                              type="checkbox"
                              value=""
                              id="flexSwitch20x30"
                            />
                          </div>
                        </div>
                      </div> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" className="btn btn-primary">
              Reserve Add
            </button>
          </div>
        </div>
      </form>
    </>
  );
};

export default AddPayReserve;
