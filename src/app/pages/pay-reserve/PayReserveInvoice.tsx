import { FC, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { getPayReserveIdAPI } from "../../../api/PayReserveApi";
import { KTSVG } from "../../../_metronic/helpers";
import { PageTitle } from "../../../_metronic/layout/core";

type PayReserveType = {
    amount: number;
    billFrom: string;
    billTo: string;
    createBy: string;
    createdDate: string;
    currency: string;
    description: string;
    discount: number;
    dueDate: string;
    emailFrom: string;
    emailTo: string;
    id: number;
    invoiceNo: string;
    isLateFee: boolean
    isNote: boolean
    isPayment: boolean
    noteFrom: string;
    noteTo: string;
    payDate: string;
    payReserveItem: PayReserveItem[]
    status: string;
    taxAmount: number;
    updatedDate: string;
    userFrom: string;
    userTo: string;
}
type PayReserveItem = {
    amount: number
    createdDate: string;
    id: number
    isActive: boolean
    title: string;
    totalAmount: number
    totalItem: number
    updatedDate:string;
}
const PayReserveInvoice: FC = () => {
    const valueList = [
        {
          desription: 'Car 1',
          hours: '8',
          rate: '400',
          amount: '3200'
        },
        {
          desription: 'Car 1',
          hours: '8',
          rate: '400',
          amount: '3200'
        },
        {
          desription: 'Car 1',
          hours: '8',
          rate: '400',
          amount: '3200'
        }
      ]

    const { id } = useParams();
    const [items, setItems] = useState<PayReserveType| null>(null);

    console.log('id:', id);
    
    const getDetail = async() => {
        try {
            const result = await getPayReserveIdAPI({id: id || ''});

            if(result?.data) {
                setItems(result?.data)
            }
            
        } catch (e:any) {
            console.log('error', e);
            
        }
    }

    useEffect(() => {
        getDetail()
    }, [])
    return (
        <>
            <PageTitle breadcrumbs={[]}>Pay reserve - Invoice Detail</PageTitle>
            <div className='card mb-5 mb-xl-10'>
                <div className='card mb-5 mb-xl-10' id='kt_profile_details_view'>
                    <div className='card-body p-9'>
                        <div className='row'>
                            <div className='col-lg-8'>
                                <div className='row mb-7'>
                                    <div className='col-lg-8'>
                                        <h2 className='fw-bold text-dark'>INVOICE MANAGEMENT</h2>
                                        <div
                                            className='d-none d-md-flex flex-row-fluid bgi-no-repeat bgi-position-y-center bgi-position-x-right bgi-size-cover'
                                            style={{
                                                transform: 'translateX(10%) rotate(-26deg)',
                                                backgroundImage: `url('/media/icons/duotune/finance/fin002.svg')`,
                                            }}
                                        ></div>
                                        <span className='fw-bolder fs-6 text-muted'>{items?.createBy}</span>
                                    </div>
                                    <div className='col-lg-4'>
                                        {/* <Link to='/customer/detail/invoice' className='btn btn-secondary align-self-center'>
                                            Pay Now
                                        </Link> */}
                                    </div>
                                </div>
                                <div className='row mb-7'>
                                    <div className='col-lg-4'>
                                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Issue Date:</div>
                                        <div className='fw-bold'>{items?.createdDate}</div>
                                    </div>
                                    <div className='col-lg-8 fv-row'>
                                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Due Date:</div>
                                        <span className='fw-bolder fs-6 me-2'>{items?.dueDate}</span>
                                        <span className='fw-bolder fs-6 me-2 text-gray-600'>Due in 7 days</span>
                                    </div>
                                </div>

                                <div className='row mb-7'>
                                    <div className='col-lg-4'>
                                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Issue for:</div>
                                        <div className='fw-bold'>{items?.billTo}</div>
                                    </div>
                                    <div className='col-lg-8 fv-row'>
                                        <div className='fs-6 fw-bolder text-gray-600 mb-1'>Issue By:</div>
                                        <span className='fw-bolder fs-6 me-2'>{items?.userFrom}</span>
                                    </div>
                                </div>
                                <div className='row mb-7'>
                                    <div className='col-lg-12'>
                                        <div className='table-responsive'>
                                            {/* begin::Table */}
                                            <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
                                                {/* begin::Table head */}
                                                <thead>
                                                    <tr className='fw-bolder text-muted'>
                                                        <th className='min-w-150px'>Description</th>
                                                        <th className='min-w-140px'>Total</th>
                                                        <th className='min-w-120px'>Rate</th>
                                                        <th className='min-w-120px'>Amount</th>
                                                    </tr>
                                                </thead>
                                                {/* end::Table head */}
                                                {/* begin::Table body */}
                                                <tbody>
                                                    {
                                                        items && items.payReserveItem.map((d, index) => (
                                                            <tr key={`invoice_${index}`}>
                                                                <td>
                                                                    <a href='#' className='text-dark fw-bolder text-hover-primary fs-6'>
                                                                        {d.title}
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <span className='text-dark fw-bolder text-hover-primary d-block mb-1 fs-6'>{d.totalItem}</span>
                                                                </td>
                                                                <td>
                                                                    <span className='text-dark fw-bolder text-hover-primary d-block mb-1 fs-6'>{new Intl.NumberFormat('en-EN').format(d.amount)|| 0}</span>
                                                                </td>
                                                                <td>
                                                                    <span className='text-dark fw-bolder text-hover-primary d-block mb-1 fs-6'>{new Intl.NumberFormat('en-EN').format(d.totalAmount )|| 0}</span>
                                                                </td>
                                                            </tr>
                                                        ))
                                                    }
                                                </tbody>
                                                {/* end::Table body */}
                                            </table>
                                            {/* end::Table */}
                                        </div>
                                        <div className='row mb-7 mt-4'>
                                            <div className='col-7'></div>
                                            <div className='col-5'>
                                                <div className='row mb-7'>
                                                    <label className='col-lg-8 fw-bold text-muted'>SubTotal</label>
                                                    <div className='col-lg-4'>
                                                        <span className='fw-bolder fs-6 text-dark'>{0}</span>
                                                    </div>
                                                </div>
                                                <div className='row mb-7'>
                                                    <label className='col-lg-8 fw-bold text-muted'>VAT 0%</label>

                                                    <div className='col-lg-4 fv-row'>
                                                        <span className='fw-bold fs-6'>{0}</span>
                                                    </div>
                                                </div>

                                                {/* <div className='row mb-7'>
                                                    <label className='col-lg-8 fw-bold text-muted'>SubTotal + VAT</label>
                                                    <div className='col-lg-4 d-flex align-items-center'>
                                                        <span className='fw-bolder fs-6 me-2'>{3200 * 4}</span>
                                                    </div>
                                                </div> */}

                                                <div className='row mb-7'>
                                                    <label className='col-lg-8 fw-bold text-muted'>Total</label>
                                                    <div className='col-lg-4'>
                                                        <span className='fw-bolder fs-6 me-2'>{items?.amount}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-4'>
                                <div className='notice d-flex bg-secondary rounded border-secondary border border-dashed p-6'>
                                    <KTSVG
                                        path='icons/duotune/general/gen044.svg'
                                        className='svg-icon-2tx svg-icon-warning me-4'
                                    />
                                    <div className='d-flex flex-stack flex-grow-1 h-100'>
                                        <div className='fw-bold'>
                                            <div className='card-body p-9'>
                                                <div className='fs-6 fw-bolder text-gray-600 mb-1'>Payment Detail:</div>
                                                <div className='d-flex flex-column flex-wrap align-items-start'>
                                                    <div className='mt-4' id='kt_signin_email'>
                                                        <div className='fs-6 fw-bolder mb-1'>Payment</div>
                                                        <div className='fw-bold text-gray-600'>{items?.id}</div>
                                                    </div>
                                                    <div className='mt-4' id='kt_signin_email'>
                                                        <div className='fs-6 fw-bolder mb-1'>Account</div>
                                                        <div className='fw-bold text-gray-600'>{items?.createBy}</div>
                                                    </div>
                                                    <div className='mt-4' id='kt_signin_email'>
                                                        <div className='fs-6 fw-bolder mb-1'>Payment term</div>
                                                        <div className='fw-bold text-gray-600'>14 Day Due in 7</div>
                                                    </div>
                                                </div>
                                                <div className='fs-6 fw-bolder text-gray-600 mb-1 mt-5'>Project Overview:</div>
                                                <div className='d-flex flex-column flex-wrap align-items-start'>
                                                    <div className='mt-4' id='kt_signin_email'>
                                                        <div className='fs-6 fw-bolder mb-1'>Project Name</div>
                                                        <div className='fw-bold text-gray-600'>{items?.billFrom}</div>
                                                    </div>
                                                    <div className='mt-4' id='kt_signin_email'>
                                                        <div className='fs-6 fw-bolder mb-1'>Completed by</div>
                                                        <div className='fw-bold text-gray-600'>{items?.billFrom}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default PayReserveInvoice;