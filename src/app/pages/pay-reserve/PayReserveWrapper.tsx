import { FC } from "react";
import { KTCard } from "../../../_metronic/helpers";
import { PageTitle } from "../../../_metronic/layout/core";
import PayReserveList from "./PayReserveList";

const PayReserveWrapper: FC = () => {
    return (
        <>
            <PageTitle breadcrumbs={[]}>Pay Reserve</PageTitle>
            <KTCard>
                <PayReserveList/>
            </KTCard>
        </>
    )
}

export default PayReserveWrapper;