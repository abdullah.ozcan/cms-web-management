import { FC, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { searchPayReserveAPI } from "../../../api/PayReserveApi";
import { KTSVG, toAbsoluteUrl } from "../../../_metronic/helpers";
type PayReserveType = {
    amount: number;
    billFrom: string;
    billTo: string;
    createBy: string;
    createdDate: string;
    currency: string;
    description: string;
    discount: number;
    dueDate: string;
    emailFrom: string;
    emailTo: string;
    id: number;
    invoiceNo: string;
    isLateFee: boolean
    isNote: boolean
    isPayment: boolean
    noteFrom: string;
    noteTo: string;
    payDate: string;
    payReserveItem: PayReserveItem[]
    status: string;
    taxAmount: number;
    updatedDate: string;
    userFrom: string;
    userTo: string;
}
type PayReserveItem = {
    amount: number
    createdDate: string;
    id: number
    isActive: boolean
    title: string;
    totalAmount: number
    totalItem: number
    updatedDate:string;
}
const PayReserveList: FC = () => {
    const [items, setItems] = useState<PayReserveType[]| null>([]);
    const [search, setSearch] = useState<string>('');
    const [page, setPage] = useState<number>(0);
    const [pageSize, setPageSize] = useState<number>(100);

    const onSearchPayReserve = async (page:number, pageSize:number, keyword:string) => {
        try {
            const result = await searchPayReserveAPI({
                body: {
                    page: page,
                    pageSize: pageSize,
                    keyword: keyword,
                }
            });

            if(result?.data) {
                const { data } = result?.data;
                setItems(data);
            }

        } catch (e: any) {
            console.log('error', e);

        }
    }

    const handleSearch = async(value:string) => {

        console.log(value);
        
        setSearch(value)
        await onSearchPayReserve(page, pageSize, value)
    }

    useEffect(() => {
        onSearchPayReserve(page, pageSize, search);
    }, [])
    return (
        <>
        <div className='card-header border-0 pt-6'>
            <div className='card-title'>
                {/* begin::Search */}
                <div className='d-flex align-items-center position-relative my-1'>
                    <KTSVG
                        path='/media/icons/duotune/general/gen021.svg'
                        className='svg-icon-1 position-absolute ms-6'
                    />
                    <input
                        type='text'
                        data-kt-user-table-filter='search'
                        className='form-control form-control-solid w-250px ps-14'
                        placeholder='Search'
                        value={search}
                        onChange={(e) => handleSearch(e.target.value)}
                    />
                </div>
                {/* end::Search */}
            </div>
            {/* begin::Card toolbar */}
            <div className='card-toolbar'>
                {/* begin::Group actions */}
                {/* {selected.length > 0 ? <UsersListGrouping /> : <UsersListToolbar />} */}
                <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
                    {/* begin::Add user */}
                    <Link to='/pay-reserve/add/' className='text-white btn btn-primary'>
                        <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
                        Add
                    </Link>
                    {/* end::Add user */}
                </div>
                {/* end::Group actions */}
            </div>
            {/* end::Card toolbar */}
        </div>
            <div className={`card mb-5 mb-xxl-8`}>
                {/* begin::Body */}
                <div className='card-body py-3'>
                    {/* begin::Table container */}
                    <div className='table-responsive'>
                        {/* begin::Table */}
                        <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
                            {/* begin::Table head */}
                            <thead>
                                <tr className='text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0'>
                                    <th className='min-w-30px'>Id</th>
                                    <th className='min-w-50px'>Pic</th>
                                    <th className='min-w-50px'>Create by</th>
                                    <th className="min-w-50px">ผู้รับเงิน</th>
                                    <th className='min-w-120px'>Note</th>
                                    <th className='min-w-50px'>Amount</th>
                                    <th className='min-w-50px'>Date</th>
                                    <th className='min-w-50px'>Actions</th>
                                </tr>
                            </thead>
                            {/* end::Table head */}
                            {/* begin::Table body */}
                            <tbody className="fw-bold text-gray-600">
                                {
                                    items && items.map((value, index) => (
                                        <tr key={`user_role_detail_list${index}`}>
                                            <td>
                                                <div className='d-flex align-items-center'>
                                                    <div className='d-flex justify-content-start flex-column'>
                                                        <label className='col-form-label fw-bold fs-6'>
                                                            <span className=''> {value.id}</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="d-flex align-items-center">
                                                    <div className='symbol symbol-45px me-5'>
                                                        <span className='symbol-label bg-light overflow-hidden'>
                                                            <img src={toAbsoluteUrl('/media/car/car-1.png')} 
                                                            className='h-100 w-100 align-self-end'
                                                            alt='' 
                                                            />
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <label className='col-form-label fw-bold fs-6'>
                                                    <span className=''>{value.createBy}</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label className="col-form-label fw-bold fs-6">
                                                    <span className="">{value.userFrom}</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label className='col-form-label fw-bold fs-6'>
                                                    <span className=''>{value?.description || '-'}</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label className='col-form-label fw-bold fs-6'>
                                                    <span className=''>{new Intl.NumberFormat('en-EN').format(value.amount)|| 0}</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label className='col-form-label fw-bold fs-6'>
                                                    <span className=''>{value.createdDate}</span>
                                                </label>
                                            </td>
                                            <td>
                                                <div className=''>
                                                    <Link to={`/pay-reserve/detail/${value?.id}`} className='btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2'>
                                                        VIEW
                                                    </Link>
                                                </div>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                            {/* end::Table body */}
                        </table>
                        {/* end::Table */}
                    </div>
                    {/* end::Table container */}
                </div>
                {/* begin::Body */}
            </div>
        </>
    )
}
export default PayReserveList;