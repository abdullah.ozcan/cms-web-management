import {FC, useEffect, useState} from "react";
import {Link} from "react-router-dom";
import {
    createCarRepairAPI, getListCarMaintenanceAPI,
    searchCarMaintenaceAnnounceAPI,
    searchCarMaintenaceAPI,
} from "../../../api/CarMaintenanceApi";
import {KTSVG, toAbsoluteUrl} from "../../../_metronic/helpers";
import ModalApp from "../../../_metronic/layout/components/ModalApp";
import {Controller, useForm} from "react-hook-form";
import {clsx} from "clsx";
import {getAllCarAPI, getCarByIdAPI} from "../../../api/CategoryApi";
import Pagination from "@vlsergey/react-bootstrap-pagination";

type Props = {
    className: string;
};
type CarType = {
    id: number,
    title: string;
    imgUrl: string;
    image: string;
    currentVin: string;
    beforeVin: string;
    year: string;
    regisDate: string;
    regisVinDate: string;
    age: number;
    categoryId: number;
    vinNo: string;
    mile: string;
    brand: string;
    fixTimes: string;
    model: string;
    description: string;
    status: string;
    isActive: boolean;
    updatedDate:string;
    createdDate: string;
}

type CarMaintenance = {
    age: null;
    beforeVin: string;
    brand: string;
    createdDate: string;
    currentVin: string;
    description: string;
    fixTimes: number;
    id: number;
    isActive: string;
    mile: string;
    model: string;
    regisDate: string;
    regisVinDate: string;
    repair: any[];
    status: string;
    title: string;
    updatedDate: string;
    vinNo: string;
    year: number;
};

type CarMaintenanceResponse = {
    currentMileCheck:number;
    nextMileCheck: number;
    checkDate: string;
    currentMile: number;
    description: string;
    isActive: boolean;
    status: string;
    id:number;
    imgUrl: string;
    createdDate: string;
    car: CarType
}
const CarMaintenanceList: FC<Props> = ({className}) => {

    const {
        handleSubmit,
        formState: {errors},
        control,
    } = useForm({
        defaultValues: {
            currentMileCheck: 0,
            nextMileCheck: 0,
            checkDate: '',
            currentMile: 0,
            description: '',
            car: '',
        },
    });

    const [carList, setCarList] = useState<CarMaintenance[]>([]);
    const [carAnnounceList, setCarAnnounceList] = useState<CarMaintenance[]>([]);
    const [totalCarAnn, setTotalCarAnn] = useState<number>(0);

    const [carListResponse, setCarListResponse] = useState<CarMaintenanceResponse[]>([]);
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(10);
    const [totalPage, setTotalPage] = useState<number>(1);
    const [search, setSearch] = useState<string>("");

    const [carSelectList, setCarSelectList] = useState<CarType[]>([]);

    const [showAdd, setShowAdd] = useState(false);
    const handleOpenAdd = () => {
        setShowAdd(true)
    }
    const handleCloseAdd = () => {
        setShowAdd(false)
    }

    const getCarMiantenance = async (
        page: number,
        pageSize: number,
        search: string
    ) => {
        try {
            const result = await searchCarMaintenaceAPI({
                body: {
                    page,
                    pageSize,
                    keyword: search,
                },
            });

            if (result?.data) {
                const {data} = result?.data;

                setCarList(data);
            }
        } catch (e: any) {
            console.log("e", e);
        }
    };

    const getCarMiantenanceAnnounce = async () => {
        try {
            const result = await searchCarMaintenaceAnnounceAPI({
                body: {
                    page: 0,
                    pageSize: 10,
                },
            });

            if (result?.data) {
                const {data, total} = result?.data;
                setTotalCarAnn(total);
                setCarAnnounceList(data);
            }
        } catch (e: any) {
            console.log("e", e);
        }
    };

    const getCarMaintenanceByType = async (page:number, pageSize:number, search:string) => {
        try {
            setSearch(search);
            const result = await getListCarMaintenanceAPI({body: {
                    "keyword":search,
                    "pageSize": pageSize,
                    "page": page
                }});
            if(result?.data) {
                const { data, totalPages } = result?.data;
                setCarListResponse(data)
                setTotalPage(totalPages)
            }
        } catch (e) {
            console.log('e', e)
        }
    }

    const handleSearch = (e: any) => {
        try {
            setSearch(e);
            if (e) {
                getCarMiantenance(page, pageSize, e);
            } else {
                getCarMiantenance(page, pageSize, "");
            }
        } catch (e: any) {
            console.log("e", e);
        }
    };

    const onCreate = async (value: any) => {
        try {
            const result = await getCarByIdAPI({ id: value?.car || "" });

            const body:any = {
                currentMileCheck: value?.currentMileCheck,
                nextMileCheck: value?.nextMileCheck,
                checkDate: value?.checkDate,
                currentMile: value?.currentMile,
                description: value?.description,
                car: result?.data,
            }
            console.log('body:', body)

            await createCarRepairAPI({body:body});

            window.location.href = '/car-maintenance'

        } catch (e: any) {
            console.log("error", e);
        }
    };

    const getAllCar = async () => {
        try {
            const result = await getAllCarAPI()
            if (result?.data) {
                let data: CarType[] = [
                    {
                        id: null,
                        vinNo: '',
                        brand: '',
                        model: ''
                    },
                    ...result?.data
                ]
                setCarSelectList(data)
            }
        } catch (e: any) {
            console.log('e', e)
        }
    }

    const handleChange = async (num:number) => {
        setPage(num);
        getCarMaintenanceByType(num, pageSize, search);
    }
    const ModalCarCheck = () => {
        return (
            <form className="" onSubmit={handleSubmit(onCreate)}>
                <div className="row mb-4">
                    <div className="col-lg-6 fv-row">
                        <label className="col-form-label fw-bold fs-6">
                            <span className="required">วันที่เช็ค</span>
                        </label>
                        <Controller
                            name="checkDate"
                            control={control}
                            rules={{
                                required: "checkDate is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="date"
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                        {
                                            "is-invalid":
                                                errors.checkDate?.message && errors.checkDate?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors.checkDate?.message && !errors.checkDate?.message,
                                        }
                                    )}
                                    placeholder="type"
                                />
                            )}
                        />
                        {errors.checkDate?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors.checkDate?.message}</span>
                            </div>
                        )}
                    </div>
                    <div className='col-lg-6 fv-row'>
                        <label className="col-form-label fw-bold fs-6">
                            <span className="required">เลือกรถ</span>
                        </label>
                        <Controller
                            name="car"
                            control={control}
                            rules={{
                                required: "car is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <select onChange={onChange} value={value} className="form-select"
                                        aria-label="Select example">
                                    {
                                        carSelectList && carSelectList.map((data, index) => (
                                            <option key={`index_order_car_${index}`}
                                                    value={data?.id}>{data?.vinNo || 'Please select'} {data?.brand}</option>
                                        ))
                                    }
                                </select>
                            )}
                        />
                    </div>
                </div>
                <div className="row mb-12">
                    <div className="col-lg-6 fv-row">
                        <label className="col-lg-4 col-form-label fw-bold fs-6">
                            <span className="required">ไมค์เช็คระยะล่าสุด</span>
                        </label>
                        <Controller
                            name="currentMileCheck"
                            control={control}
                            rules={{
                                required: "field is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="number"
                                    value={value}
                                    min={0}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                        {
                                            "is-invalid":
                                                errors.currentMileCheck?.message && errors.currentMileCheck?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors.currentMileCheck?.message && !errors.currentMileCheck?.message,
                                        }
                                    )}
                                    placeholder="type"
                                />
                            )}
                        />
                        {errors.currentMileCheck?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors.currentMileCheck?.message}</span>
                            </div>
                        )}
                    </div>
                    <div className="col-lg-6 fv-row">
                        <label className="col-lg-4 col-form-label fw-bold fs-6">
                            <span className="required">กำหนดครั้งต่อไป</span>
                        </label>
                        <Controller
                            name="nextMileCheck"
                            control={control}
                            rules={{
                                required: "field is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="text"
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                        {
                                            "is-invalid":
                                                errors.nextMileCheck?.message && errors.nextMileCheck?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors.nextMileCheck?.message && !errors.nextMileCheck?.message,
                                        }
                                    )}
                                    placeholder="type"
                                />
                            )}
                        />
                        {errors.nextMileCheck?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors.nextMileCheck?.message}</span>
                            </div>
                        )}
                    </div>
                </div>
                <div className="row mb-12">
                    <div className="col-lg-6 fv-row">
                        <label className="col-lg-4 col-form-label fw-bold fs-6">
                            <span className="required">เลขปัจจุบัน</span>
                        </label>
                        <Controller
                            name="currentMile"
                            control={control}
                            rules={{
                                required: "field is require",
                            }}
                            render={({field: {value, onChange}}) => (
                                <input
                                    type="number"
                                    value={value}
                                    min={0}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                        {
                                            "is-invalid":
                                                errors.currentMile?.message && errors.currentMile?.message,
                                        },
                                        {
                                            "is-valid":
                                                errors.currentMile?.message && !errors.currentMile?.message,
                                        }
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                        {errors.currentMile?.message && (
                            <div className="fv-plugins-message-container text-danger">
                                <span role="alert">{errors.currentMile?.message}</span>
                            </div>
                        )}
                    </div>
                    <div className="col-lg-6 fv-row">
                        <label className="col-lg-4 col-form-label fw-bold fs-6">
                            <span className="">คงเหลือ</span>
                        </label>
                        <input
                            type="text"
                            disabled
                            value={0}
                            //   value={watch('nextMileCheck') - watch('currentMile')}
                            className="form-control form-control-lg form-control-solid"
                            placeholder=""
                        />
                    </div>
                </div>
                <div className="row mb-12">
                    <div className="col-lg-12 fv-row">
                        <label className="col-lg-4 col-form-label fw-bold fs-6">
                            <span className="">หมายเหตุ</span>
                        </label>
                        <Controller
                            name="description"
                            control={control}
                            rules={{
                                required: false,
                            }}
                            render={({field: {value, onChange}}) => (
                                <textarea
                                    rows={4}
                                    value={value}
                                    onChange={onChange}
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                    )}
                                    placeholder=""
                                />
                            )}
                        />
                    </div>
                </div>
                <div className="d-flex flex-row justify-content-end">
                    <button className="btn btn-secondary " onClick={handleCloseAdd}>
                        Close
                    </button>
                    <button className="btn btn-primary mx-2" type='submit'>
                        Save Changes
                    </button>
                </div>
            </form>
        );
    };

    useEffect(() => {
        getCarMiantenanceAnnounce();
        getAllCar()
        getCarMaintenanceByType(page, pageSize, search);
    }, []);
    return (
        <div className={`card ${className}`}>
            <ModalApp
                show={showAdd}
                handleOpen={handleOpenAdd}
                handleClose={handleCloseAdd}
                type={'submit'}
                chil={<ModalCarCheck/>}
                title={'Add Car Maintenance'}/>
            {totalCarAnn && totalCarAnn > 0 ? (
                <div className="bg-light-danger d-flex align-items-center py-10 rounded mx-10 mt-10">
                    <i className="bi bi-exclamation-circle fs-5x me-4 px-3 ps-10 text-danger"></i>
                    <div className="d-flex justify-content-start flex-column">
            <span className="fw-bolder fs-2 mb-3">
              มีรถรอการส่งซ่อม {totalCarAnn} คัน (
              <Link to="#" className="px-1">
                คลิกดูรายละเอียด
              </Link>
              )
            </span>
                        {carAnnounceList &&
                            carAnnounceList.map((value, index) => (
                                <span className="fw-bold d-block fs-7 px-3" key={`index_of_car_ann_${index}`}>
                  1. {value.title} {value?.vinNo || '-'} / กำหนดซ่อมบำรุง
                  [{value?.repair[0]?.checkDate || '-'}]
                </span>
                            ))}
                    </div>
                </div>
            ) : null}
            <div className="card-header border-0 pt-6">
                <div className="card-title">
                    {/* begin::Search */}
                    <div className="d-flex align-items-center position-relative my-1">
                        <KTSVG
                            path="/media/icons/duotune/general/gen021.svg"
                            className="svg-icon-1 position-absolute ms-6"
                        />
                        <input
                            type="text"
                            data-kt-user-table-filter="search"
                            className="form-control form-control-solid w-250px ps-14"
                            placeholder="Search"
                            value={search}
                            onChange={(e) => getCarMaintenanceByType(page, pageSize, e.target.value)}
                        />
                        {/* begin::Filter Button */}
                        <button
                            disabled={false}
                            type="button"
                            className="btn btn-light-primary me-3 mx-3"
                            data-kt-menu-trigger="click"
                            data-kt-menu-placement="bottom-end"
                        >
                            <KTSVG
                                path="/media/icons/duotune/general/gen031.svg"
                                className="svg-icon-2"
                            />
                            Filter
                        </button>
                        {/* end::Filter Button */}
                        <div
                            className="menu menu-sub menu-sub-dropdown w-300px w-md-325px"
                            data-kt-menu="true"
                        >
                            {/* begin::Header */}
                            <div className="px-7 py-5">
                                <div className="fs-5 text-dark fw-bolder">Filter Options</div>
                            </div>
                            {/* end::Header */}

                            {/* begin::Separator */}
                            <div className="separator border-gray-200"></div>
                            {/* end::Separator */}

                            {/* begin::Content */}
                            {/* end::Content */}
                        </div>
                    </div>
                    {/* end::Search */}
                </div>
                {/* begin::Card toolbar */}
                <div className="card-toolbar">
                    {/* begin::Group actions */}
                    {/* {selected.length > 0 ? <UsersListGrouping /> : <UsersListToolbar />} */}
                    <div
                        className="d-flex justify-content-end"
                        data-kt-user-table-toolbar="base"
                    >
                        {/* begin::Add user */}
                        <button type="button" className="btn btn-primary" onClick={(e) => {
                            e.preventDefault()
                            handleOpenAdd()
                        }}>
                            <KTSVG
                                path="/media/icons/duotune/arrows/arr075.svg"
                                className="svg-icon-2"
                            />
                            Add
                        </button>
                        {/* <ModalMaintenance
                        show={show}
                        handleClose={handleClose}
                        title={'Maintenance'}
                        chil={<ModalMaintenanceLayout />}
                    /> */}
                        {/* end::Add user */}
                    </div>
                    {/* end::Group actions */}
                </div>
                {/* end::Card toolbar */}
            </div>
            {/* begin::Body */}
            <div className="card-body py-3">
                {/* begin::Table container */}
                <div className="table-responsive">
                    {/* begin::Table */}
                    <table className="table align-middle gs-0 gy-4">
                        {/* begin::Table head */}
                        <thead>
                        <tr className="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                            <th className="w-25px">
                                <div className="form-check form-check-sm form-check-custom form-check-solid">
                                    <input
                                        className="form-check-input"
                                        type="checkbox"
                                        value="1"
                                        data-kt-check="true"
                                        data-kt-check-target=".widget-13-check"
                                    />
                                </div>
                            </th>
                            <th className="min-w-120px">รูป</th>
                            <th className="min-w-125px">รถ</th>
                            <th className="min-w-150px">ทะเบียน</th>
                            <th className="min-w-120px">เลขไมล์</th>
                            <th className="min-w-120px">ไมค์ปัจจุบัน</th>
                            <th className="min-w-120px">วันที่เข้าซ่อม</th>
                            <th className="min-w-120px">วันที่ต้องชำระ</th>
                            <th className="min-w-120px rounded-end">Action</th>
                        </tr>
                        </thead>
                        {/* end::Table head */}
                        {/* begin::Table body */}
                        <tbody className="fw-bold text-gray-600">
                        {carListResponse &&
                            carListResponse.map((value, index) => (
                                <tr>
                                    <td>
                                        <div className="form-check form-check-sm form-check-custom form-check-solid">
                                            <input
                                                className="form-check-input widget-13-check"
                                                type="checkbox"
                                                value="1"
                                            />
                                        </div>
                                    </td>
                                    <td>
                                        <div className="d-flex align-items-center">
                                            <div className="symbol symbol-45px">
                          <span className="symbol-label bg-light">
                            <img
                                src={value?.imgUrl}
                                className="h-75 align-self-end"
                                alt=""
                            />
                          </span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                      <span className="text-dark fw-bold d-block fs-6 mt-1">
                        {value?.car?.title || "-"}
                      </span>
                                    </td>
                                    <td>
                      <span className="text-dark fw-bold d-block fs-6 mt-1">
                        {value?.car?.vinNo || "-"}
                      </span>
                                    </td>
                                    <td>
                      <span className="text-dark fw-bold d-block fs-6 mt-1">
                        {value?.car?.mile || "-"}
                      </span>
                                    </td>
                                    <td>
                      <span className="text-dark fw-bold d-block fs-6 mt-1">
                        {value?.currentMile || "-"}
                      </span>
                                    </td>
                                    <td>
                      <span className="text-dark fw-bold d-block fs-6 mt-1">
                        {value?.checkDate || "-"}
                      </span>
                                    </td>
                                    <td>
                      <span className="text-dark fw-bold d-block fs-6 mt-1">
                        {value?.createdDate || "-"}
                      </span>
                                    </td>
                                    <td className="">
                                        <Link
                                            to={`/category/car/view/${value?.car?.id}`}
                                            className="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2"
                                        >
                                            VIEW
                                        </Link>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                        {/* end::Table body */}
                    </table>
                    {/* end::Table */}
                    <div className='d-flex justify-content-end mt-2'>
                        <Pagination
                            value={page}
                            firstPageValue={1}
                            onChange={async (e)=> {await handleChange(Number(e.target?.value))}}
                            totalPages={totalPage}/>
                    </div>
                </div>
                {/* end::Table container */}
            </div>
            {/* begin::Body */}
        </div>
    );
};

export default CarMaintenanceList;
