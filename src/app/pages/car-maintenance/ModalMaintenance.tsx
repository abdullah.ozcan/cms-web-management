import { FC } from 'react'
import { Link } from 'react-router-dom';
import { KTSVG } from '../../../_metronic/helpers';

const ModalMaintenance: FC = () => {
    return (
        <div className='bg-light-danger d-flex align-items-center py-10 rounded'>
            <i className='bi bi-exclamation-circle fs-5x me-4 px-3 ps-10 text-danger'></i>
            <div className='d-flex justify-content-start flex-column'>
                <span className='fw-bolder fs-2 mb-3'>
                    มีรถรอการส่งซ่อม 3 คัน (
                    <Link to='#' className='px-1'>
                        คลิกดูรายละเอียด
                    </Link>
                    )
                </span>
                <span className='fw-bold d-block fs-7 px-3'>
                    1. Mercedes Benz Sprinter กศ-2566 / กำหนดซ่อมบำรุง [15/01/2564]
                </span>
                <span className='fw-bold d-block fs-7 px-3'>
                    2. Mercedes Benz Sprinter กศ-2566 / กำหนดซ่อมบำรุง [15/01/2564]
                </span>
                <span className='fw-bold d-block fs-7 px-3'>
                    3. Mercedes Benz Sprinter กศ-2566 / กำหนดซ่อมบำรุง [15/01/2564]
                </span>
            </div>
        </div>
    )
}

export default ModalMaintenance;