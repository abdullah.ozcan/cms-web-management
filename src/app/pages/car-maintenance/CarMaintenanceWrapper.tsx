import { FC } from "react";
import { KTCard } from "../../../_metronic/helpers";
import { PageTitle } from "../../../_metronic/layout/core";
import CarMaintenanceList from "./CarMaintenanceList";


const CarMaintenanceWrapper: FC = () => {
    return (
        <>
            <PageTitle breadcrumbs={[]}>Car Maintenance</PageTitle>
            <KTCard>
              {/* <Header/> */}
              <CarMaintenanceList className={""}/>
            </KTCard>
        </>
    )
}

export default CarMaintenanceWrapper;