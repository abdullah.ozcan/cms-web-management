import { FC } from 'react'
import { PageTitle } from '../../../_metronic/layout/core'
import RoleListCard from './RoleListCard'

const RoleListWrapper: FC = () => {
    return (
        <>
            <PageTitle breadcrumbs={[]}>Role List</PageTitle>
            <RoleListCard/>
        </>
    )
}

export default RoleListWrapper