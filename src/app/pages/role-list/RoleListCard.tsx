import { FC, useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import {
  createRoleApi,
  listMasterRoleApi, mapMenuRoleApi,
} from "../../../api/RoleApi";
import { useForm, Controller } from "react-hook-form";
import { clsx } from "clsx";

type UserRole = {
  id: number;
  name: string;
  isActive: number;
  createdDate: string;
  updatedDate: string;
  menuId: number;
};

type Menu = {
  id: number;
  title: string;
  isActive: boolean;
  createdDate: string;
  updatedDate: string;
  role: UserRole[];
};
type MapMenu = {
  title: string;
  menuList: string[];
}

const RoleListCard: FC = () => {
  const [roleList, setRoleList] = useState<any>(null);
  const [mapMenuList, setMapMenuList] = useState<MapMenu[]>([]);

  const getMenu = async () => {
    try {
      const result = await  mapMenuRoleApi();

      if(result?.data) {
        const { resultList } = result?.data;
        setMapMenuList(resultList)
      }

    } catch (e) {
      console.log('e', e)
    }
  }

  const handleGetRole = async () => {
    try {
      const result = await listMasterRoleApi();

      if (result?.data) {
        const { data } = result;
        console.log("data", data);
        setRoleList(data);
      }
    } catch (e: any) {
      console.log("error", e);
    }
  };

  useEffect(() => {
    handleGetRole();
    getMenu();
  }, []);


  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const {
    handleSubmit,
    formState: { errors },
    control,
  } = useForm({
    defaultValues: {
      title: "",
      menuId: null,
    },
  });
  const onCreate = async (value: any) => {
    try {

      await createRoleApi(value);


      window.location.href = "/rolesList";
    } catch (e: any) {
      console.log("error", e);
    }
  };

  const findItem = (key:string) => {
    return mapMenuList.filter((d) => d.title === key)

  }
  return (
    <div className="row g-5">
      <Modal show={show} onHide={handleClose} size={"lg"}>
        <Modal.Header closeButton>
          <Modal.Title>Create Role</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className="" onSubmit={handleSubmit(onCreate)}>
            <label className="col-form-label fw-bold fs-6">
              <span className="required">หัวข้อ</span>
            </label>
            <Controller
              name="title"
              control={control}
              rules={{
                required: "title is require",
              }}
              render={({ field: { value, onChange } }) => (
                <input
                  type="text"
                  maxLength={30}
                  value={value}
                  onChange={onChange}
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                    {
                      "is-invalid":
                        errors.title?.message && errors.title?.message,
                    },
                    {
                      "is-valid":
                        errors.title?.message && !errors.title?.message,
                    }
                  )}
                  placeholder=""
                />
              )}
            />
            {errors.title?.message && (
              <div className="fv-plugins-message-container text-danger">
                <span role="alert">{errors.title?.message}</span>
              </div>
            )}
            <div className="d-flex flex-row justify-content-end mt-5">
              <button className="btn btn-secondary" onClick={handleClose}>
                Close
              </button>
              <button className="btn btn-primary mx-2" type="submit">
                Save Changes
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      <div>
      </div>
      {roleList &&
        roleList.map((value: any, index: number) => (
          <div
            className="col-lg-3 card card-stretch-50 card-flush card-bordered mx-4 h-100"
            key={`index_of_card_role${index}`}
          >
            <div className="mb-5">
              <div className="card-body d-flex justify-content-start flex-column">
                <span className="fw-bold card-title fs-1 h-50px">
                  {value.name}
                </span>
                <div className="text-gray-700">
                  {
                      findItem(value?.name).map((d: any, index: number) => {
                        return d && d.menuList.map((val:string,index:number) =>
                            (
                                <span
                                    className="d-block text-muted h-30px"
                                    key={`index_of_detail_role_${index}`}
                                >
                              {     val || "-"}
                                </span>
                            ))
                      })}
                </div>
              </div>
            </div>
          </div>
        ))}
      <div className="col-lg-3 card card-stretch-50 card-flush card-bordered mx-4 h-150px">
        <div className="mb-5">
          <div className="card-body d-flex justify-content-start flex-column">
            <div className="text-gray-700 text-center">
              <button
                className="btn btn-secondary d-block text-white"
                onClick={() => {
                  handleShow();
                }}
              >
                Add New Role
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RoleListCard;
