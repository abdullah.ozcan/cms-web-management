import { Route, Routes, Outlet, Navigate } from 'react-router-dom'
import { PageLink, PageTitle } from '../../../_metronic/layout/core'
import { UsersListWrapper } from '../apps/user-management/users-list/UsersList';
import { UsersList } from '../profile/components/UsersList';

const usersBreadcrumbs: Array<PageLink> = [
  {
    title: 'User Management',
    path: '/apps/user-management/users',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const UsersPageComponent = () => {
  return (
    <>
      <PageTitle breadcrumbs={usersBreadcrumbs}>Users list</PageTitle>
      <UsersList />
    </>
  )
}

export default UsersPageComponent
