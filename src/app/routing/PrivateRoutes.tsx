import {lazy, FC, Suspense} from 'react'
import {Route, Routes, Navigate} from 'react-router-dom'
import {MasterLayout} from '../../_metronic/layout/MasterLayout'
import TopBarProgress from 'react-topbar-progress-indicator'
import DashboardPage from '../pages/dashboard/DashboardWrapper'
import {MenuTestPage} from '../pages/MenuTestPage'
import {getCSSVariableValue} from '../../_metronic/assets/ts/_utils'
import OrderWrapper from '../pages/order/OrderWrapper'
import CustomerWrapper from '../pages/customer/Customer'
import CustomerDetail from '../pages/customer/CustomerDetail'
import CustomerInVoice from '../pages/customer/view/CustomerInVoice'
import CategoryWrapper from '../pages/category/CategoryWrapper'
import AddCategory from '../pages/category/view/AddCategory'
import CategoryView from '../pages/category/view/CategoryView'
import CarList from '../pages/category/view/CarList'
import CarDetail from '../pages/category/view/CarDetail'
import CarView from '../pages/category/view/CarView'
import PayReserveWrapper from '../pages/pay-reserve/PayReserveWrapper'
import AddPayReserve from '../pages/pay-reserve/AddPayReserve'
import PayReserveInvoice from '../pages/pay-reserve/PayReserveInvoice'
import CarMaintenanceWrapper from '../pages/car-maintenance/CarMaintenanceWrapper'
import AgentWrapper from '../pages/agents/AgentWrapper'
import AgentDetail from '../pages/agents/AgentDetail'
import EmployeeWrapper from '../pages/employee/EmployeeWrapper'
import AddEmployee from '../pages/employee/AddEmployee'
import EmployeeDetail from '../pages/employee/EmployeeDetail'
import OrderDetail from '../pages/order/OrderDrtail'
import RoleListWrapper from '../pages/role-list/RoleListWrapper'
import UserManagementWrapper from '../pages/user-management/UserManagementWrapper'
import OrderInvoice from '../pages/order/OrderInvoice'
import CarUpdate from "../pages/category/view/CarUpdate";

const PrivateRoutes = () => {
  const BuilderPageWrapper = lazy(() => import('../pages/layout-builder/BuilderPageWrapper'))
  const UserRolePageWrapper = lazy(() => import('../pages/user-role/UserRoleWrapper'))
  const ProfilePage = lazy(() => import('../modules/profile/ProfilePage'))
  const WizardsPage = lazy(() => import('../modules/wizards/WizardsPage'))
  const AccountPage = lazy(() => import('../modules/accounts/AccountPage'))
  const WidgetsPage = lazy(() => import('../modules/widgets/WidgetsPage'))
  const ChatPage = lazy(() => import('../modules/apps/chat/ChatPage'))
  const UsersPage = lazy(() => import('../modules/apps/user-management/UsersPage'))

  return (
    <Routes>
      <Route element={<MasterLayout />}>
        {/* Redirect to Dashboard after success login/registartion */}
        <Route path='auth/*' element={<Navigate to='/dashboard' />} />
        {/* Pages */}
        <Route path='dashboard' element={<DashboardPage />} />
        <Route path='order' element={<OrderWrapper />} />
        <Route path='customer' element={<CustomerWrapper />} />
        <Route path='usersList' element={<UsersPage />} />
        <Route path='usersRole' element={<UserRolePageWrapper />} />
        <Route path='rolesList' element={<RoleListWrapper />} />
        <Route path='userManage' element={<UserManagementWrapper/>} />
        <Route path='builder' element={<BuilderPageWrapper />} />
        <Route path='menu-test' element={<MenuTestPage />} />
        <Route path='category' element={<CategoryWrapper />} />
        <Route path='pay-reserve' element={<PayReserveWrapper />} />
        <Route path='car-maintenance' element={<CarMaintenanceWrapper />} />
        <Route path='agents' element={<AgentWrapper />} />
        <Route path='employee' element={<EmployeeWrapper />} />
        {/* Lazy Modules */}

        <Route
          path='crafted/pages/profile/*'
          element={
            <SuspensedView>
              <ProfilePage />
            </SuspensedView>
          }
        />
         <Route
          path='customer/detail/:id'
          element={
            <SuspensedView>
              <CustomerDetail />
            </SuspensedView>
          }
        />
         <Route
          path='customer/detail/invoice/*'
          element={
            <SuspensedView>
              <CustomerInVoice />
            </SuspensedView>
          }
        />
         <Route
          path='category/add/*'
          element={
            <SuspensedView>
              <AddCategory />
            </SuspensedView>
          }
        />
        <Route
          path='category/detail/*'
          element={
            <SuspensedView>
              <CategoryView />
            </SuspensedView>
          }
        />
        <Route
          path='category/car/:id'
          element={
            <SuspensedView>
              <CarList />
            </SuspensedView>
          }
        />
         <Route
          path='category/car/add/*'
          element={
            <SuspensedView>
              <CarList />
            </SuspensedView>
          }
        />
         <Route
          path='category/car/detail/:id'
          element={
            <SuspensedView>
              <CarDetail />
            </SuspensedView>
          }
        />
          <Route
              path='category/car/update/:id'
              element={
                  <SuspensedView>
                      <CarUpdate />
                  </SuspensedView>
              }
          />
         <Route
          path='category/car/view/:id'
          element={
            <SuspensedView>
              <CarView />
            </SuspensedView>
          }
        />
        <Route
          path='pay-reserve/add/*'
          element={
            <SuspensedView>
              <AddPayReserve />
            </SuspensedView>
          }
        />
         <Route
          path='pay-reserve/detail/:id'
          element={
            <SuspensedView>
              <PayReserveInvoice />
            </SuspensedView>
          }
        />
         <Route
          path='agents/detail/:id'
          element={
            <SuspensedView>
              <AgentDetail />
            </SuspensedView>
          }
        />
         <Route
          path='employee/add/*'
          element={
            <SuspensedView>
              <AddEmployee />
            </SuspensedView>
          }
        />
         <Route
          path='employee/detail/:id'
          element={
            <SuspensedView>
              <EmployeeDetail />
            </SuspensedView>
          }
        />
         <Route
          path='order/detail/:id'
          element={
            <SuspensedView>
              <OrderDetail />
            </SuspensedView>
          }
        />
        <Route
          path='order/detail/invoice/:id'
          element={
            <SuspensedView>
              <OrderInvoice/>
            </SuspensedView>
          }
        />
        <Route
          path='crafted/pages/wizards/*'
          element={
            <SuspensedView>
              <WizardsPage />
            </SuspensedView>
          }
        />
        <Route
          path='crafted/widgets/*'
          element={
            <SuspensedView>
              <WidgetsPage />
            </SuspensedView>
          }
        />
        <Route
          path='crafted/account/*'
          element={
            <SuspensedView>
              <AccountPage />
            </SuspensedView>
          }
        />
        <Route
          path='apps/chat/*'
          element={
            <SuspensedView>
              <ChatPage />
            </SuspensedView>
          }
        />
        <Route
          path='apps/user-management2/*'
          element={
            <SuspensedView>
              <UsersPage />
            </SuspensedView>
          }
        />
        {/* Page Not Found */}
        <Route path='*' element={<Navigate to='/error/404' />} />
      </Route>
    </Routes>
  )
}

const SuspensedView: FC = ({children}) => {
  const baseColor = getCSSVariableValue('--bs-primary')
  TopBarProgress.config({
    barColors: {
      '0': baseColor,
    },
    barThickness: 1,
    shadowBlur: 5,
  })
  return <Suspense fallback={<TopBarProgress />}>{children}</Suspense>
}

export {PrivateRoutes}
