import { FC } from 'react'
import { Modal, Button } from 'react-bootstrap'

type Prop = {
    show: boolean
    handleClose: () => void
    handleOpen?: () => void
    title: string
    chil: any
}

const ModalMaintenance: FC<Prop> = ({ show, title, handleClose, handleOpen, chil }) => {
    return (
        <Modal show={show} onHide={handleClose} size={'lg'} centered={true}>
            <Modal.Body>{chil}</Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={handleClose}>
                    Submit
                </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default ModalMaintenance;