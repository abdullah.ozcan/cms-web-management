import React, {useRef, useEffect} from 'react'
import {useLocation} from 'react-router'
import clsx from 'clsx'
import {checkIsActive, KTSVG, toAbsoluteUrl} from '../../../helpers'
import {Link} from 'react-router-dom'

type Props = {
  width?: number
  subTitle?: string
  height?: number
  to: string
  title: string
  icon?: string
  fontIcon?: string
  menuTrigger?: 'click' | `{default:'click', lg: 'hover'}`
  menuPlacement?: 'right-start' | 'bottom-start' | 'left-start'
  hasArrow?: boolean
  hasBullet?: boolean
  isMega?: boolean
  url?: string
  menuType?: string
}

const MenuInnerWithSub: React.FC<Props> = ({
  children,
  to,
  title,
  icon,
  fontIcon,
  menuTrigger,
  menuPlacement,
  hasArrow = false,
  hasBullet = false,
  isMega = false,
  height,
  width,
  url,
  subTitle,
                                             menuType
}) => {
  const menuItemRef = useRef<HTMLDivElement>(null)
  const {pathname} = useLocation()

  useEffect(() => {
    if (menuItemRef.current && menuTrigger && menuPlacement) {
      menuItemRef.current.setAttribute('data-kt-menu-trigger', menuTrigger)
      menuItemRef.current.setAttribute('data-kt-menu-placement', menuPlacement)
    }
  }, [menuTrigger, menuPlacement])


  const renderMenu = () => {
    if(menuType === 'main') {
      return  <div ref={menuItemRef} className='menu-item me-lg-1'>
        { url && (
          <Link className={'d-flex flex-column justify-content-center align-items-center mx-auto'} to={to}>
            <img width={width} height={height} src={toAbsoluteUrl(url)} alt='' />
            <span className='menu-title text-white mt-2'>{title}</span>
            <span className='menu-title text-white mt-2'>{subTitle}</span>
          </Link>
        )}
      </div>

    } else {
      return <div ref={menuItemRef} className='menu-item menu-lg-down-accordion me-lg-1'>
      <span
        className={clsx('menu-link py-3', {
          active: checkIsActive(pathname, to),
        })}
      >
        {hasBullet && (
          <span className='menu-bullet'>
            <span className='bullet bullet-dot'></span>
          </span>
        )}

        {icon && (
          <span className='menu-icon'>
            <KTSVG path={icon} className='svg-icon-2' />
          </span>
        )}

        { url && (
          <div className='d-flex flex-column justify-content-center align-items-center mx-auto'>
            <img width={width} height={height} src={toAbsoluteUrl(url)} alt='' />
            <span className='menu-title text-white mt-2'>{title}</span>
            <span className='menu-title text-white mt-2'>{subTitle}</span>
          </div>

        )}

        {fontIcon && (
          <span className='menu-icon'>
            <i className={clsx('bi fs-3', fontIcon)}></i>
          </span>
        )}

        {
          !url && (
            <span className='menu-title'>{title}</span>
          )
        }
        {hasArrow && <span className='menu-arrow'></span>}
      </span>
        <div
          className={clsx(
            'menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown',
            isMega ? 'w-100 w-lg-600px p-5 p-lg-5' : 'menu-rounded-0 py-lg-4 w-lg-225px'
          )}
          data-kt-menu-dismiss='true'
        >
          {children}
        </div>
      </div>
    }
  }
  return (
    <>
      {
        renderMenu()
      }
    </>
  )
}

export {MenuInnerWithSub}
