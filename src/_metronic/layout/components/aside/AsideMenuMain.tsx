/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import {useIntl} from 'react-intl'
import {KTSVG} from '../../../helpers'
import {AsideMenuItemWithSub} from './AsideMenuItemWithSub'
import {AsideMenuItem} from './AsideMenuItem'
import { MenuInnerWithSub } from '../header/MenuInnerWithSub'
import { MenuItem } from '../header/MenuItem'

export function AsideMenuMain() {
  const intl = useIntl()
  // {intl.formatMessage({id: 'MENU.ORDER'})}
  return (
    <>
      {/* <AsideMenuItem
        to='/dashboard'
        icon='/media/icons/duotune/art/art002.svg'
        title={intl.formatMessage({id: 'MENU.DASHBOARD'})}
        fontIcon='bi-app-indicator'
      />
     <AsideMenuItem
        to='/category'
        icon='/media/icons/duotune/art/art002.svg'
        title='Category'
        fontIcon='bi-app-indicator'
      />
       <AsideMenuItem
        to='/order'
        icon='/media/icons/duotune/art/art002.svg'
        title='Order and Booking'
        fontIcon='bi-app-indicator'
      />
      <AsideMenuItem
        to='/customer'
        icon='/media/icons/duotune/art/art002.svg'
        title='Customer'
        fontIcon='bi-app-indicator'
      />
       <AsideMenuItem
        to='/car-maintenance'
        icon='/media/icons/duotune/art/art002.svg'
        title='Car Maintenance'
        fontIcon='bi-app-indicator'
      />
       <AsideMenuItem
        to='/pay-reserve'
        icon='/media/icons/duotune/art/art002.svg'
        title='Pay Reserve'
        fontIcon='bi-app-indicator'
      />
      <AsideMenuItem
        to='/usersRole'
        icon='/media/icons/duotune/art/art002.svg'
        title={intl.formatMessage({id: 'MENU.USER.ROLE'})}
        fontIcon='bi-app-indicator'
      />
       <AsideMenuItem
        to='/employee'
        icon='/media/icons/duotune/art/art002.svg'
        title='Employee'
        fontIcon='bi-app-indicator'
      /> */}
       <MenuInnerWithSub menuType={'main'} subTitle={'รายการทั้งหมด'} width={40} height={40} url='/media/car/icon-sum.png' title='Summary' to='/order' menuPlacement='right-start' menuTrigger='click'/>
      <MenuInnerWithSub
        subTitle={'จัดการเช่ารถ'}
        width={40} height={40} url='/media/car/icon-car.png' title='CAR RENT' to='/apps' menuPlacement='right-start' menuTrigger='click'>
        <MenuItem
          to='/dashboard'
          icon=''
          title={intl.formatMessage({ id: 'MENU.DASHBOARD' })}
        />
        <MenuItem
          to='/order'
          icon=''
          title={intl.formatMessage({ id: 'MENU.ORDER.BOOKING' })}
        />
         <MenuItem
          to='/customer'
          icon=''
          title={intl.formatMessage({ id: 'MENU.CUSTOMER' })}
        />
        <MenuItem
          to='/agents'
          icon=''
          title={intl.formatMessage({ id: 'MENU.AGENT' })}
        />
        {/*<MenuItem*/}
        {/*  to='/driver'*/}
        {/*  icon=''*/}
        {/*  title={intl.formatMessage({ id: 'MENU.DRIVER' })}*/}
        {/*/>*/}
        <MenuItem
          to='/category'
          icon=''
          title={intl.formatMessage({ id: 'MENU.CATEGORY' })}
        />
        <MenuItem
          to='/pay-reserve'
          icon=''
          title={intl.formatMessage({ id: 'MENU.PAY.RESERVE' })}
        />
        <MenuItem
          to='/car-maintenance'
          icon=''
          title={intl.formatMessage({ id: 'MENU.CAR.MAINTAIN' })}
        />
        {/*<MenuItem*/}
        {/*  to='userManage'*/}
        {/*  icon=''*/}
        {/*  title={intl.formatMessage({ id: 'MENU.USERMANAGE' })}*/}
        {/*/>*/}
      </MenuInnerWithSub>
      <MenuInnerWithSub subTitle={'จัดการโรงแรม'} width={40} height={40} url='/media/car/icon-hotel.png' title='HOTEL' to='/apps' menuPlacement='right-start' menuTrigger='click'/>
      <MenuInnerWithSub subTitle={'ตั๋วเครื่องบิน'} width={40} height={40} url='/media/car/icon-ticket.png' title='TICKET' to='/apps' menuPlacement='right-start' menuTrigger='click'/>
      <MenuInnerWithSub subTitle={'วีซ่า'} width={40} height={40} url='/media/car/icon-visa.png' title='VISA' to='/apps' menuPlacement='right-start' menuTrigger='click'/>
      <MenuInnerWithSub subTitle={'ตั๋วผ่านประตู'} width={40} height={40} url='/media/car/icon-gate.png' title='GATE' to='/apps' menuPlacement='right-start' menuTrigger='click'/>
      <MenuInnerWithSub subTitle={'โปรแกรมทัวร์'} width={40} height={40} url='/media/car/icon-tour.png' title='TOUR' to='/apps' menuPlacement='right-start' menuTrigger='click'/>
      <MenuInnerWithSub subTitle={'กลอฟ์'} width={40} height={40} url='/media/car/icon-golf.png' title='GOLF' to='/apps' menuPlacement='right-start' menuTrigger='click'/>
      <MenuInnerWithSub subTitle={'ไวน์'} width={40} height={40} url='/media/car/icon-wine.png' title='WINE' to='/apps' menuPlacement='right-start' menuTrigger='click'/>
      <MenuInnerWithSub subTitle={'ฟอนิเจอร์'} width={40} height={40} url='/media/car/icon-forniture.png' title='FORNITURE' to='/apps' menuPlacement='right-start' menuTrigger='click'/>
    </>
  )
}
