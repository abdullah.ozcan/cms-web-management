import {FC} from "react"
import {Modal, Button} from "react-bootstrap"

type Prop = {
    show?: boolean
    handleClose?: () => void
    handleOpen?: () => void
    type?: string
    title: string
    chil?: any
}
const ModalApp: FC<Prop> = ({show, title, handleClose, handleOpen, chil, type}) => {
    return (
        <Modal show={show} onHide={handleClose} size={'lg'}>
            <Modal.Header closeButton>
                <Modal.Title>{title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{chil}</Modal.Body>
            {
                type !== 'submit' ?
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={handleClose}>
                            Save Changes
                        </Button>
                    </Modal.Footer>
                    : null
            }

        </Modal>
    )
}

export default ModalApp;