import { FC } from 'react'
import { Modal, Button } from 'react-bootstrap'

type Prop = {
    show: boolean
    handleClose: () => void
    handleOpen?: () => void
    title: string
    chil: any
}

const ModalUpdateUserRole: FC<Prop> = ({ show, title, handleClose, handleOpen, chil }) => {
    return (
        <Modal show={show} onHide={handleClose} size={'lg'}>
            <Modal.Header closeButton>
                <Modal.Title>{title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{chil}</Modal.Body>
            {/* <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Discord
                </Button>
                <Button variant="primary" onClick={handleClose}>
                    Submit
                </Button>
            </Modal.Footer> */}
        </Modal>
    )
}

export default ModalUpdateUserRole;