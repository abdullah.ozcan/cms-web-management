import {FC, useEffect, useState} from "react";
import Pagination from 'react-bootstrap/Pagination';

type Props = {
    total?: number;
    totalPage: number;
    page?: number;
    onChangePage?: ()=>void;

}
const CustomPagination:FC<Props> = ({ total, totalPage, page=1, onChangePage}) => {

    const [divPage, setDivPage ] = useState<number>(3)
    const [temp, setTemp ] = useState<number[]>([])
    const [p, setP ] = useState<number>(page | 1)

    console.log('p', p)

    const initValue = () => {
        let start = 1;
        let list = [];

        const stop = totalPage/ divPage
        while (start < stop) {
            list.push(start)
            start = start+1
        }
        setTemp(list)
    }

    const onClick = (p:number) => {
        console.log('p', p, temp[temp.length-1])

        if(p === temp[temp.length-1]) {
            const stop = temp[temp.length-1] +5
            let newStart = temp[temp.length-1]
            let list = [];
            while (newStart < stop) {


                list.push(newStart)
                newStart = newStart+1
            }
            setTemp(list)
        }
    }

    const back = (b: number) => {

    }

    useEffect(() => {
        initValue();
    }, [])
    return (
        <div className='mt-2'>
            {/*<Pagination value={1} totalPages={3} disabled size="sm" />*/}
        </div>
    )
}

export default CustomPagination;
